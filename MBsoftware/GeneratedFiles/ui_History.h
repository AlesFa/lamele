/********************************************************************************
** Form generated from reading UI file 'History.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HISTORY_H
#define UI_HISTORY_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_History
{
public:
    QToolButton *buttonPlus;
    QToolButton *buttonMinus;
    QFrame *frameMeasurements;
    QLabel *label;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *layoutMeasurements;
    QLabel *labelCurrentType;
    QLabel *labelCounter;
    QGraphicsView *imageViewMainWindow0;
    QLabel *label_5;
    QListWidget *historyList;
    QToolButton *buttonUpload;
    QLabel *labelGood;

    void setupUi(QWidget *History)
    {
        if (History->objectName().isEmpty())
            History->setObjectName(QString::fromUtf8("History"));
        History->resize(988, 861);
        buttonPlus = new QToolButton(History);
        buttonPlus->setObjectName(QString::fromUtf8("buttonPlus"));
        buttonPlus->setGeometry(QRect(340, 20, 81, 71));
        QFont font;
        font.setFamily(QString::fromUtf8("Calibri"));
        font.setPointSize(24);
        font.setBold(true);
        font.setWeight(75);
        buttonPlus->setFont(font);
        buttonMinus = new QToolButton(History);
        buttonMinus->setObjectName(QString::fromUtf8("buttonMinus"));
        buttonMinus->setGeometry(QRect(430, 20, 81, 71));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Calibri"));
        font1.setPointSize(28);
        font1.setBold(true);
        font1.setWeight(75);
        buttonMinus->setFont(font1);
        frameMeasurements = new QFrame(History);
        frameMeasurements->setObjectName(QString::fromUtf8("frameMeasurements"));
        frameMeasurements->setGeometry(QRect(40, 110, 450, 371));
        frameMeasurements->setMinimumSize(QSize(450, 200));
        frameMeasurements->setMaximumSize(QSize(500, 16777215));
        frameMeasurements->setFrameShape(QFrame::Box);
        frameMeasurements->setFrameShadow(QFrame::Raised);
        label = new QLabel(frameMeasurements);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 10, 201, 41));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Calibri"));
        font2.setPointSize(16);
        font2.setBold(true);
        font2.setItalic(true);
        font2.setWeight(75);
        label->setFont(font2);
        label->setFrameShape(QFrame::NoFrame);
        label->setFrameShadow(QFrame::Raised);
        verticalLayoutWidget = new QWidget(frameMeasurements);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(9, 69, 431, 121));
        layoutMeasurements = new QVBoxLayout(verticalLayoutWidget);
        layoutMeasurements->setSpacing(6);
        layoutMeasurements->setContentsMargins(11, 11, 11, 11);
        layoutMeasurements->setObjectName(QString::fromUtf8("layoutMeasurements"));
        layoutMeasurements->setSizeConstraint(QLayout::SetMinimumSize);
        layoutMeasurements->setContentsMargins(0, 0, 0, 0);
        labelCurrentType = new QLabel(frameMeasurements);
        labelCurrentType->setObjectName(QString::fromUtf8("labelCurrentType"));
        labelCurrentType->setGeometry(QRect(220, 10, 221, 41));
        labelCurrentType->setFont(font2);
        labelCurrentType->setFrameShape(QFrame::Box);
        labelCurrentType->setFrameShadow(QFrame::Raised);
        labelCurrentType->setTextFormat(Qt::PlainText);
        labelCurrentType->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        labelCounter = new QLabel(History);
        labelCounter->setObjectName(QString::fromUtf8("labelCounter"));
        labelCounter->setGeometry(QRect(546, 42, 191, 31));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Calibri"));
        font3.setPointSize(14);
        font3.setBold(true);
        font3.setWeight(75);
        labelCounter->setFont(font3);
        imageViewMainWindow0 = new QGraphicsView(History);
        imageViewMainWindow0->setObjectName(QString::fromUtf8("imageViewMainWindow0"));
        imageViewMainWindow0->setGeometry(QRect(499, 140, 481, 491));
        imageViewMainWindow0->setMinimumSize(QSize(0, 0));
        imageViewMainWindow0->setFrameShape(QFrame::WinPanel);
        imageViewMainWindow0->setFrameShadow(QFrame::Sunken);
        label_5 = new QLabel(History);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(510, 110, 131, 31));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Calibri"));
        font4.setPointSize(16);
        font4.setBold(false);
        font4.setWeight(50);
        label_5->setFont(font4);
        label_5->setFrameShape(QFrame::Panel);
        historyList = new QListWidget(History);
        historyList->setObjectName(QString::fromUtf8("historyList"));
        historyList->setGeometry(QRect(40, 490, 451, 361));
        historyList->setMinimumSize(QSize(0, 300));
        buttonUpload = new QToolButton(History);
        buttonUpload->setObjectName(QString::fromUtf8("buttonUpload"));
        buttonUpload->setGeometry(QRect(730, 660, 251, 71));
        QFont font5;
        font5.setFamily(QString::fromUtf8("Calibri"));
        font5.setPointSize(12);
        font5.setBold(false);
        font5.setWeight(50);
        buttonUpload->setFont(font5);
        labelGood = new QLabel(History);
        labelGood->setObjectName(QString::fromUtf8("labelGood"));
        labelGood->setGeometry(QRect(740, 30, 231, 41));
        labelGood->setFrameShape(QFrame::Box);
        labelGood->setFrameShadow(QFrame::Sunken);

        retranslateUi(History);

        QMetaObject::connectSlotsByName(History);
    } // setupUi

    void retranslateUi(QWidget *History)
    {
        History->setWindowTitle(QCoreApplication::translate("History", "History", nullptr));
        buttonPlus->setText(QCoreApplication::translate("History", "+", nullptr));
        buttonMinus->setText(QCoreApplication::translate("History", "-", nullptr));
        label->setText(QCoreApplication::translate("History", "Measurements:", nullptr));
        labelCurrentType->setText(QCoreApplication::translate("History", "TextLabel", nullptr));
        labelCounter->setText(QCoreApplication::translate("History", "TextLabel", nullptr));
        label_5->setText(QCoreApplication::translate("History", "CAMERA 0:", nullptr));
        buttonUpload->setText(QCoreApplication::translate("History", "UPLOAD TO IMAGE PROCESS", nullptr));
        labelGood->setText(QCoreApplication::translate("History", "label", nullptr));
    } // retranslateUi

};

namespace Ui {
    class History: public Ui_History {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HISTORY_H
