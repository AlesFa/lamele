/****************************************************************************
** Meta object code from reading C++ file 'imageProcessing.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "stdafx.h"
#include "../../imageProcessing.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'imageProcessing.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_imageProcessing_t {
    QByteArrayData data[51];
    char stringdata0[725];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_imageProcessing_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_imageProcessing_t qt_meta_stringdata_imageProcessing = {
    {
QT_MOC_LITERAL(0, 0, 15), // "imageProcessing"
QT_MOC_LITERAL(1, 16, 11), // "imagesReady"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 16), // "OnPressedImageUp"
QT_MOC_LITERAL(4, 46, 18), // "OnPressedImageDown"
QT_MOC_LITERAL(5, 65, 14), // "OnPressedCamUp"
QT_MOC_LITERAL(6, 80, 16), // "OnPressedCamDown"
QT_MOC_LITERAL(7, 97, 22), // "ConvertImageForDisplay"
QT_MOC_LITERAL(8, 120, 11), // "imageNumber"
QT_MOC_LITERAL(9, 132, 10), // "ShowImages"
QT_MOC_LITERAL(10, 143, 17), // "ResizeDisplayRect"
QT_MOC_LITERAL(11, 161, 13), // "OnProcessCam0"
QT_MOC_LITERAL(12, 175, 13), // "OnProcessCam1"
QT_MOC_LITERAL(13, 189, 13), // "OnProcessCam2"
QT_MOC_LITERAL(14, 203, 13), // "OnProcessCam3"
QT_MOC_LITERAL(15, 217, 13), // "OnProcessCam4"
QT_MOC_LITERAL(16, 231, 33), // "OnDoneEditingLineInsertImageI..."
QT_MOC_LITERAL(17, 265, 21), // "onPressedAddParameter"
QT_MOC_LITERAL(18, 287, 25), // "onPressedRemoveParameters"
QT_MOC_LITERAL(19, 313, 25), // "onPressedUpdateParameters"
QT_MOC_LITERAL(20, 339, 26), // "onPressedSurfaceInspection"
QT_MOC_LITERAL(21, 366, 15), // "mousePressEvent"
QT_MOC_LITERAL(22, 382, 12), // "QMouseEvent*"
QT_MOC_LITERAL(23, 395, 5), // "event"
QT_MOC_LITERAL(24, 401, 17), // "mouseReleaseEvent"
QT_MOC_LITERAL(25, 419, 17), // "ProcessingCamera0"
QT_MOC_LITERAL(26, 437, 2), // "id"
QT_MOC_LITERAL(27, 440, 10), // "imageIndex"
QT_MOC_LITERAL(28, 451, 4), // "draw"
QT_MOC_LITERAL(29, 456, 17), // "ProcessingCamera1"
QT_MOC_LITERAL(30, 474, 17), // "ProcessingCamera2"
QT_MOC_LITERAL(31, 492, 17), // "ProcessingCamera3"
QT_MOC_LITERAL(32, 510, 17), // "ProcessingCamera4"
QT_MOC_LITERAL(33, 528, 12), // "CheckDefects"
QT_MOC_LITERAL(34, 541, 10), // "indexImage"
QT_MOC_LITERAL(35, 552, 7), // "nrPiece"
QT_MOC_LITERAL(36, 560, 14), // "InspectSurface"
QT_MOC_LITERAL(37, 575, 14), // "CMemoryBuffer*"
QT_MOC_LITERAL(38, 590, 5), // "image"
QT_MOC_LITERAL(39, 596, 4), // "Rect"
QT_MOC_LITERAL(40, 601, 6), // "region"
QT_MOC_LITERAL(41, 608, 9), // "ClearDraw"
QT_MOC_LITERAL(42, 618, 16), // "SetCurrentBuffer"
QT_MOC_LITERAL(43, 635, 10), // "dispWindow"
QT_MOC_LITERAL(44, 646, 9), // "dispImage"
QT_MOC_LITERAL(45, 656, 7), // "ZoomOut"
QT_MOC_LITERAL(46, 664, 6), // "ZoomIn"
QT_MOC_LITERAL(47, 671, 9), // "ZoomReset"
QT_MOC_LITERAL(48, 681, 11), // "OnLoadImage"
QT_MOC_LITERAL(49, 693, 19), // "OnLoadMultipleImage"
QT_MOC_LITERAL(50, 713, 11) // "OnSaveImage"

    },
    "imageProcessing\0imagesReady\0\0"
    "OnPressedImageUp\0OnPressedImageDown\0"
    "OnPressedCamUp\0OnPressedCamDown\0"
    "ConvertImageForDisplay\0imageNumber\0"
    "ShowImages\0ResizeDisplayRect\0OnProcessCam0\0"
    "OnProcessCam1\0OnProcessCam2\0OnProcessCam3\0"
    "OnProcessCam4\0OnDoneEditingLineInsertImageIndex\0"
    "onPressedAddParameter\0onPressedRemoveParameters\0"
    "onPressedUpdateParameters\0"
    "onPressedSurfaceInspection\0mousePressEvent\0"
    "QMouseEvent*\0event\0mouseReleaseEvent\0"
    "ProcessingCamera0\0id\0imageIndex\0draw\0"
    "ProcessingCamera1\0ProcessingCamera2\0"
    "ProcessingCamera3\0ProcessingCamera4\0"
    "CheckDefects\0indexImage\0nrPiece\0"
    "InspectSurface\0CMemoryBuffer*\0image\0"
    "Rect\0region\0ClearDraw\0SetCurrentBuffer\0"
    "dispWindow\0dispImage\0ZoomOut\0ZoomIn\0"
    "ZoomReset\0OnLoadImage\0OnLoadMultipleImage\0"
    "OnSaveImage"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_imageProcessing[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      35,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  189,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,  190,    2, 0x0a /* Public */,
       4,    0,  191,    2, 0x0a /* Public */,
       5,    0,  192,    2, 0x0a /* Public */,
       6,    0,  193,    2, 0x0a /* Public */,
       7,    1,  194,    2, 0x0a /* Public */,
       9,    0,  197,    2, 0x0a /* Public */,
      10,    0,  198,    2, 0x0a /* Public */,
      11,    0,  199,    2, 0x0a /* Public */,
      12,    0,  200,    2, 0x0a /* Public */,
      13,    0,  201,    2, 0x0a /* Public */,
      14,    0,  202,    2, 0x0a /* Public */,
      15,    0,  203,    2, 0x0a /* Public */,
      16,    0,  204,    2, 0x0a /* Public */,
      17,    0,  205,    2, 0x0a /* Public */,
      18,    0,  206,    2, 0x0a /* Public */,
      19,    0,  207,    2, 0x0a /* Public */,
      20,    0,  208,    2, 0x0a /* Public */,
      21,    1,  209,    2, 0x0a /* Public */,
      24,    1,  212,    2, 0x0a /* Public */,
      25,    3,  215,    2, 0x0a /* Public */,
      29,    3,  222,    2, 0x0a /* Public */,
      30,    3,  229,    2, 0x0a /* Public */,
      31,    3,  236,    2, 0x0a /* Public */,
      32,    3,  243,    2, 0x0a /* Public */,
      33,    3,  250,    2, 0x0a /* Public */,
      36,    5,  257,    2, 0x0a /* Public */,
      41,    0,  268,    2, 0x0a /* Public */,
      42,    2,  269,    2, 0x0a /* Public */,
      45,    0,  274,    2, 0x0a /* Public */,
      46,    0,  275,    2, 0x0a /* Public */,
      47,    0,  276,    2, 0x0a /* Public */,
      48,    0,  277,    2, 0x0a /* Public */,
      49,    0,  278,    2, 0x0a /* Public */,
      50,    0,  279,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Int, QMetaType::Int,    8,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 22,   23,
    QMetaType::Void, 0x80000000 | 22,   23,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   26,   27,   28,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   26,   27,   28,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   26,   27,   28,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   26,   27,   28,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   26,   27,   28,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   34,   35,   28,
    QMetaType::Int, 0x80000000 | 37, 0x80000000 | 39, QMetaType::Int, QMetaType::Int, QMetaType::Int,   38,   40,   27,   35,   28,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   43,   44,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void imageProcessing::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<imageProcessing *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->imagesReady(); break;
        case 1: _t->OnPressedImageUp(); break;
        case 2: _t->OnPressedImageDown(); break;
        case 3: _t->OnPressedCamUp(); break;
        case 4: _t->OnPressedCamDown(); break;
        case 5: { int _r = _t->ConvertImageForDisplay((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 6: _t->ShowImages(); break;
        case 7: _t->ResizeDisplayRect(); break;
        case 8: _t->OnProcessCam0(); break;
        case 9: _t->OnProcessCam1(); break;
        case 10: _t->OnProcessCam2(); break;
        case 11: _t->OnProcessCam3(); break;
        case 12: _t->OnProcessCam4(); break;
        case 13: _t->OnDoneEditingLineInsertImageIndex(); break;
        case 14: _t->onPressedAddParameter(); break;
        case 15: _t->onPressedRemoveParameters(); break;
        case 16: _t->onPressedUpdateParameters(); break;
        case 17: _t->onPressedSurfaceInspection(); break;
        case 18: _t->mousePressEvent((*reinterpret_cast< QMouseEvent*(*)>(_a[1]))); break;
        case 19: _t->mouseReleaseEvent((*reinterpret_cast< QMouseEvent*(*)>(_a[1]))); break;
        case 20: { int _r = _t->ProcessingCamera0((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 21: { int _r = _t->ProcessingCamera1((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 22: { int _r = _t->ProcessingCamera2((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 23: { int _r = _t->ProcessingCamera3((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 24: { int _r = _t->ProcessingCamera4((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 25: { int _r = _t->CheckDefects((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 26: { int _r = _t->InspectSurface((*reinterpret_cast< CMemoryBuffer*(*)>(_a[1])),(*reinterpret_cast< Rect(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 27: _t->ClearDraw(); break;
        case 28: _t->SetCurrentBuffer((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 29: _t->ZoomOut(); break;
        case 30: _t->ZoomIn(); break;
        case 31: _t->ZoomReset(); break;
        case 32: _t->OnLoadImage(); break;
        case 33: _t->OnLoadMultipleImage(); break;
        case 34: _t->OnSaveImage(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (imageProcessing::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&imageProcessing::imagesReady)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject imageProcessing::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_imageProcessing.data,
    qt_meta_data_imageProcessing,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *imageProcessing::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *imageProcessing::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_imageProcessing.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int imageProcessing::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 35)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 35;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 35)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 35;
    }
    return _id;
}

// SIGNAL 0
void imageProcessing::imagesReady()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
