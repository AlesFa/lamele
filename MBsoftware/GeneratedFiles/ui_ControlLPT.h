/********************************************************************************
** Form generated from reading UI file 'ControlLPT.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONTROLLPT_H
#define UI_CONTROLLPT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LPT
{
public:
    QHBoxLayout *horizontalLayout;
    QGroupBox *groupData;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *layoutData;
    QGroupBox *groupStatus;
    QVBoxLayout *verticalLayout_4;
    QVBoxLayout *layoutStatus;
    QGroupBox *groupControl;
    QVBoxLayout *verticalLayout_5;
    QVBoxLayout *layoutControl;

    void setupUi(QWidget *LPT)
    {
        if (LPT->objectName().isEmpty())
            LPT->setObjectName(QString::fromUtf8("LPT"));
        LPT->resize(593, 301);
        horizontalLayout = new QHBoxLayout(LPT);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        groupData = new QGroupBox(LPT);
        groupData->setObjectName(QString::fromUtf8("groupData"));
        verticalLayout_2 = new QVBoxLayout(groupData);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        layoutData = new QVBoxLayout();
        layoutData->setObjectName(QString::fromUtf8("layoutData"));

        verticalLayout_2->addLayout(layoutData);


        horizontalLayout->addWidget(groupData);

        groupStatus = new QGroupBox(LPT);
        groupStatus->setObjectName(QString::fromUtf8("groupStatus"));
        verticalLayout_4 = new QVBoxLayout(groupStatus);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        layoutStatus = new QVBoxLayout();
        layoutStatus->setObjectName(QString::fromUtf8("layoutStatus"));

        verticalLayout_4->addLayout(layoutStatus);


        horizontalLayout->addWidget(groupStatus);

        groupControl = new QGroupBox(LPT);
        groupControl->setObjectName(QString::fromUtf8("groupControl"));
        verticalLayout_5 = new QVBoxLayout(groupControl);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        layoutControl = new QVBoxLayout();
        layoutControl->setObjectName(QString::fromUtf8("layoutControl"));

        verticalLayout_5->addLayout(layoutControl);


        horizontalLayout->addWidget(groupControl);


        retranslateUi(LPT);

        QMetaObject::connectSlotsByName(LPT);
    } // setupUi

    void retranslateUi(QWidget *LPT)
    {
        LPT->setWindowTitle(QCoreApplication::translate("LPT", "LPT", nullptr));
        groupData->setTitle(QCoreApplication::translate("LPT", "GroupBox", nullptr));
        groupStatus->setTitle(QCoreApplication::translate("LPT", "GroupBox", nullptr));
        groupControl->setTitle(QCoreApplication::translate("LPT", "GroupBox", nullptr));
    } // retranslateUi

};

namespace Ui {
    class LPT: public Ui_LPT {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONTROLLPT_H
