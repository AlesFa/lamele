/********************************************************************************
** Form generated from reading UI file 'GeneralSettings.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GENERALSETTINGS_H
#define UI_GENERALSETTINGS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_GeneralSettings
{
public:
    QDialogButtonBox *buttonBox;
    QGroupBox *groupBox_3;
    QWidget *layoutWidget_4;
    QHBoxLayout *horizontalLayout_19;
    QLabel *label_19;
    QSpacerItem *horizontalSpacer_8;
    QCheckBox *AutoCleaningEnable;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label_20;
    QLineEdit *editCleanPeriod;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_22;
    QLineEdit *editCleanDurationBottom;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_29;
    QLineEdit *editCleanDurationTop;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_30;
    QLineEdit *editCleanDelayBottom;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_23;
    QLineEdit *editCleanDelayTop;
    QGroupBox *groupBox_4;
    QWidget *horizontalLayoutWidget_3;
    QHBoxLayout *horizontalLayout_3;
    QGroupBox *groupBox;
    QWidget *verticalLayoutWidget_3;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label_11;
    QLineEdit *distanceToValveS0;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_12;
    QLineEdit *valveDurS0;
    QHBoxLayout *horizontalLayout_13;
    QLabel *label_13;
    QLineEdit *valve2delayS0;
    QHBoxLayout *horizontalLayout_14;
    QLabel *label_14;
    QLineEdit *valve2DruS0;
    QGroupBox *groupBox_2;
    QWidget *verticalLayoutWidget_4;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_15;
    QLabel *label_15;
    QLineEdit *distanceToValveS1;
    QHBoxLayout *horizontalLayout_16;
    QLabel *label_16;
    QLineEdit *valveDurS1;
    QHBoxLayout *horizontalLayout_17;
    QLabel *label_17;
    QLineEdit *valve2delayS1;
    QHBoxLayout *horizontalLayout_18;
    QLabel *label_18;
    QLineEdit *valve2DruS1;
    QGroupBox *groupBox_5;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_10;
    QSpacerItem *horizontalSpacer_7;
    QCheckBox *testMode;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_20;
    QLabel *label_21;
    QSpacerItem *horizontalSpacer_9;
    QCheckBox *vibratorEnable;
    QHBoxLayout *horizontalLayout_22;
    QLabel *label_28;
    QSpacerItem *horizontalSpacer_11;
    QCheckBox *autoStartInputEnable;
    QGroupBox *groupBox_7;
    QGroupBox *groupBox_6;
    QWidget *verticalLayoutWidget_5;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_25;
    QLineEdit *editNrPieces;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_26;
    QLineEdit *stopSysemEdit;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_7;
    QRadioButton *radioButtonNonStopMode;
    QRadioButton *radioButtonAutoStopMode;
    QRadioButton *radioButtonDelayStopMode;
    QGroupBox *groupBox_8;
    QWidget *layoutWidget_5;
    QHBoxLayout *horizontalLayout_21;
    QLabel *label_24;
    QSpacerItem *horizontalSpacer_10;
    QCheckBox *outputEnable;
    QWidget *verticalLayoutWidget_6;
    QVBoxLayout *verticalLayout_8;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_27;
    QLineEdit *outputSignalDuration;

    void setupUi(QWidget *GeneralSettings)
    {
        if (GeneralSettings->objectName().isEmpty())
            GeneralSettings->setObjectName(QString::fromUtf8("GeneralSettings"));
        GeneralSettings->resize(579, 859);
        buttonBox = new QDialogButtonBox(GeneralSettings);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(190, 820, 381, 31));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(buttonBox->sizePolicy().hasHeightForWidth());
        buttonBox->setSizePolicy(sizePolicy);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        groupBox_3 = new QGroupBox(GeneralSettings);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setGeometry(QRect(310, 240, 261, 261));
        QFont font;
        font.setFamily(QString::fromUtf8("Calibri"));
        font.setPointSize(11);
        font.setBold(true);
        font.setWeight(75);
        groupBox_3->setFont(font);
        layoutWidget_4 = new QWidget(groupBox_3);
        layoutWidget_4->setObjectName(QString::fromUtf8("layoutWidget_4"));
        layoutWidget_4->setGeometry(QRect(10, 30, 233, 23));
        horizontalLayout_19 = new QHBoxLayout(layoutWidget_4);
        horizontalLayout_19->setSpacing(6);
        horizontalLayout_19->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_19->setObjectName(QString::fromUtf8("horizontalLayout_19"));
        horizontalLayout_19->setContentsMargins(0, 0, 0, 0);
        label_19 = new QLabel(layoutWidget_4);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        label_19->setEnabled(true);
        label_19->setMinimumSize(QSize(120, 0));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Calibri"));
        font1.setPointSize(10);
        font1.setBold(true);
        font1.setWeight(75);
        label_19->setFont(font1);

        horizontalLayout_19->addWidget(label_19);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_19->addItem(horizontalSpacer_8);

        AutoCleaningEnable = new QCheckBox(layoutWidget_4);
        AutoCleaningEnable->setObjectName(QString::fromUtf8("AutoCleaningEnable"));
        AutoCleaningEnable->setFont(font1);

        horizontalLayout_19->addWidget(AutoCleaningEnable);

        verticalLayoutWidget = new QWidget(groupBox_3);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(10, 60, 231, 181));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_20 = new QLabel(verticalLayoutWidget);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setMinimumSize(QSize(120, 0));
        label_20->setFont(font1);

        horizontalLayout->addWidget(label_20);

        editCleanPeriod = new QLineEdit(verticalLayoutWidget);
        editCleanPeriod->setObjectName(QString::fromUtf8("editCleanPeriod"));
        editCleanPeriod->setMinimumSize(QSize(60, 0));
        editCleanPeriod->setMaximumSize(QSize(60, 16777215));

        horizontalLayout->addWidget(editCleanPeriod);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_22 = new QLabel(verticalLayoutWidget);
        label_22->setObjectName(QString::fromUtf8("label_22"));
        label_22->setMinimumSize(QSize(120, 0));
        label_22->setFont(font1);

        horizontalLayout_2->addWidget(label_22);

        editCleanDurationBottom = new QLineEdit(verticalLayoutWidget);
        editCleanDurationBottom->setObjectName(QString::fromUtf8("editCleanDurationBottom"));
        editCleanDurationBottom->setEnabled(true);
        editCleanDurationBottom->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_2->addWidget(editCleanDurationBottom);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_29 = new QLabel(verticalLayoutWidget);
        label_29->setObjectName(QString::fromUtf8("label_29"));
        label_29->setMinimumSize(QSize(120, 0));
        label_29->setFont(font1);

        horizontalLayout_8->addWidget(label_29);

        editCleanDurationTop = new QLineEdit(verticalLayoutWidget);
        editCleanDurationTop->setObjectName(QString::fromUtf8("editCleanDurationTop"));
        editCleanDurationTop->setEnabled(true);
        editCleanDurationTop->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_8->addWidget(editCleanDurationTop);


        verticalLayout->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_30 = new QLabel(verticalLayoutWidget);
        label_30->setObjectName(QString::fromUtf8("label_30"));
        label_30->setMinimumSize(QSize(120, 0));
        label_30->setFont(font1);

        horizontalLayout_9->addWidget(label_30);

        editCleanDelayBottom = new QLineEdit(verticalLayoutWidget);
        editCleanDelayBottom->setObjectName(QString::fromUtf8("editCleanDelayBottom"));
        editCleanDelayBottom->setEnabled(true);
        editCleanDelayBottom->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_9->addWidget(editCleanDelayBottom);


        verticalLayout->addLayout(horizontalLayout_9);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_23 = new QLabel(verticalLayoutWidget);
        label_23->setObjectName(QString::fromUtf8("label_23"));
        label_23->setMinimumSize(QSize(120, 0));
        label_23->setFont(font1);

        horizontalLayout_4->addWidget(label_23);

        editCleanDelayTop = new QLineEdit(verticalLayoutWidget);
        editCleanDelayTop->setObjectName(QString::fromUtf8("editCleanDelayTop"));
        editCleanDelayTop->setEnabled(true);
        editCleanDelayTop->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_4->addWidget(editCleanDelayTop);


        verticalLayout->addLayout(horizontalLayout_4);

        groupBox_4 = new QGroupBox(GeneralSettings);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setGeometry(QRect(20, 10, 551, 221));
        groupBox_4->setFont(font);
        horizontalLayoutWidget_3 = new QWidget(groupBox_4);
        horizontalLayoutWidget_3->setObjectName(QString::fromUtf8("horizontalLayoutWidget_3"));
        horizontalLayoutWidget_3->setGeometry(QRect(0, 30, 541, 181));
        horizontalLayout_3 = new QHBoxLayout(horizontalLayoutWidget_3);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        groupBox = new QGroupBox(horizontalLayoutWidget_3);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        QFont font2;
        font2.setBold(false);
        font2.setWeight(50);
        groupBox->setFont(font2);
        verticalLayoutWidget_3 = new QWidget(groupBox);
        verticalLayoutWidget_3->setObjectName(QString::fromUtf8("verticalLayoutWidget_3"));
        verticalLayoutWidget_3->setGeometry(QRect(10, 20, 221, 151));
        verticalLayout_3 = new QVBoxLayout(verticalLayoutWidget_3);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        label_11 = new QLabel(verticalLayoutWidget_3);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setMinimumSize(QSize(120, 0));
        label_11->setFont(font1);

        horizontalLayout_11->addWidget(label_11);

        distanceToValveS0 = new QLineEdit(verticalLayoutWidget_3);
        distanceToValveS0->setObjectName(QString::fromUtf8("distanceToValveS0"));
        distanceToValveS0->setMaximumSize(QSize(60, 16777215));
        distanceToValveS0->setFont(font1);

        horizontalLayout_11->addWidget(distanceToValveS0);


        verticalLayout_3->addLayout(horizontalLayout_11);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setSpacing(6);
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        label_12 = new QLabel(verticalLayoutWidget_3);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setMinimumSize(QSize(120, 0));
        label_12->setFont(font1);

        horizontalLayout_12->addWidget(label_12);

        valveDurS0 = new QLineEdit(verticalLayoutWidget_3);
        valveDurS0->setObjectName(QString::fromUtf8("valveDurS0"));
        valveDurS0->setMaximumSize(QSize(60, 16777215));
        valveDurS0->setFont(font1);

        horizontalLayout_12->addWidget(valveDurS0);


        verticalLayout_3->addLayout(horizontalLayout_12);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setSpacing(6);
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        label_13 = new QLabel(verticalLayoutWidget_3);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setMinimumSize(QSize(120, 0));
        label_13->setFont(font1);

        horizontalLayout_13->addWidget(label_13);

        valve2delayS0 = new QLineEdit(verticalLayoutWidget_3);
        valve2delayS0->setObjectName(QString::fromUtf8("valve2delayS0"));
        valve2delayS0->setMaximumSize(QSize(60, 16777215));
        valve2delayS0->setFont(font1);

        horizontalLayout_13->addWidget(valve2delayS0);


        verticalLayout_3->addLayout(horizontalLayout_13);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setSpacing(6);
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        label_14 = new QLabel(verticalLayoutWidget_3);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setMinimumSize(QSize(120, 0));
        label_14->setFont(font1);

        horizontalLayout_14->addWidget(label_14);

        valve2DruS0 = new QLineEdit(verticalLayoutWidget_3);
        valve2DruS0->setObjectName(QString::fromUtf8("valve2DruS0"));
        valve2DruS0->setMaximumSize(QSize(60, 16777215));
        valve2DruS0->setFont(font1);

        horizontalLayout_14->addWidget(valve2DruS0);


        verticalLayout_3->addLayout(horizontalLayout_14);


        horizontalLayout_3->addWidget(groupBox);

        groupBox_2 = new QGroupBox(horizontalLayoutWidget_3);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Calibri"));
        font3.setPointSize(11);
        font3.setBold(false);
        font3.setWeight(50);
        groupBox_2->setFont(font3);
        verticalLayoutWidget_4 = new QWidget(groupBox_2);
        verticalLayoutWidget_4->setObjectName(QString::fromUtf8("verticalLayoutWidget_4"));
        verticalLayoutWidget_4->setGeometry(QRect(10, 20, 221, 151));
        verticalLayout_4 = new QVBoxLayout(verticalLayoutWidget_4);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setSpacing(6);
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        label_15 = new QLabel(verticalLayoutWidget_4);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setMinimumSize(QSize(120, 0));
        label_15->setFont(font1);

        horizontalLayout_15->addWidget(label_15);

        distanceToValveS1 = new QLineEdit(verticalLayoutWidget_4);
        distanceToValveS1->setObjectName(QString::fromUtf8("distanceToValveS1"));
        distanceToValveS1->setMaximumSize(QSize(60, 16777215));
        distanceToValveS1->setFont(font1);

        horizontalLayout_15->addWidget(distanceToValveS1);


        verticalLayout_4->addLayout(horizontalLayout_15);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setSpacing(6);
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        label_16 = new QLabel(verticalLayoutWidget_4);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setMinimumSize(QSize(120, 0));
        label_16->setFont(font1);

        horizontalLayout_16->addWidget(label_16);

        valveDurS1 = new QLineEdit(verticalLayoutWidget_4);
        valveDurS1->setObjectName(QString::fromUtf8("valveDurS1"));
        valveDurS1->setMaximumSize(QSize(60, 16777215));
        valveDurS1->setFont(font1);

        horizontalLayout_16->addWidget(valveDurS1);


        verticalLayout_4->addLayout(horizontalLayout_16);

        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setSpacing(6);
        horizontalLayout_17->setObjectName(QString::fromUtf8("horizontalLayout_17"));
        label_17 = new QLabel(verticalLayoutWidget_4);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setMinimumSize(QSize(120, 0));
        label_17->setFont(font1);

        horizontalLayout_17->addWidget(label_17);

        valve2delayS1 = new QLineEdit(verticalLayoutWidget_4);
        valve2delayS1->setObjectName(QString::fromUtf8("valve2delayS1"));
        valve2delayS1->setMaximumSize(QSize(60, 16777215));
        valve2delayS1->setFont(font1);

        horizontalLayout_17->addWidget(valve2delayS1);


        verticalLayout_4->addLayout(horizontalLayout_17);

        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setSpacing(6);
        horizontalLayout_18->setObjectName(QString::fromUtf8("horizontalLayout_18"));
        label_18 = new QLabel(verticalLayoutWidget_4);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setMinimumSize(QSize(120, 0));
        label_18->setFont(font1);

        horizontalLayout_18->addWidget(label_18);

        valve2DruS1 = new QLineEdit(verticalLayoutWidget_4);
        valve2DruS1->setObjectName(QString::fromUtf8("valve2DruS1"));
        valve2DruS1->setMaximumSize(QSize(60, 16777215));
        valve2DruS1->setFont(font1);

        horizontalLayout_18->addWidget(valve2DruS1);


        verticalLayout_4->addLayout(horizontalLayout_18);


        horizontalLayout_3->addWidget(groupBox_2);

        groupBox_5 = new QGroupBox(GeneralSettings);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        groupBox_5->setGeometry(QRect(10, 650, 511, 161));
        groupBox_5->setFont(font);
        verticalLayoutWidget_2 = new QWidget(groupBox_5);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(10, 30, 491, 111));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        label_10 = new QLabel(verticalLayoutWidget_2);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setEnabled(true);
        label_10->setMinimumSize(QSize(120, 0));
        label_10->setFont(font1);

        horizontalLayout_10->addWidget(label_10);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_7);

        testMode = new QCheckBox(verticalLayoutWidget_2);
        testMode->setObjectName(QString::fromUtf8("testMode"));
        testMode->setFont(font1);

        horizontalLayout_10->addWidget(testMode);


        verticalLayout_2->addLayout(horizontalLayout_10);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        horizontalLayout_20 = new QHBoxLayout();
        horizontalLayout_20->setSpacing(6);
        horizontalLayout_20->setObjectName(QString::fromUtf8("horizontalLayout_20"));
        label_21 = new QLabel(verticalLayoutWidget_2);
        label_21->setObjectName(QString::fromUtf8("label_21"));
        label_21->setEnabled(true);
        label_21->setMinimumSize(QSize(120, 0));
        label_21->setFont(font1);

        horizontalLayout_20->addWidget(label_21);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_20->addItem(horizontalSpacer_9);

        vibratorEnable = new QCheckBox(verticalLayoutWidget_2);
        vibratorEnable->setObjectName(QString::fromUtf8("vibratorEnable"));
        vibratorEnable->setFont(font1);

        horizontalLayout_20->addWidget(vibratorEnable);


        verticalLayout_5->addLayout(horizontalLayout_20);


        verticalLayout_2->addLayout(verticalLayout_5);

        horizontalLayout_22 = new QHBoxLayout();
        horizontalLayout_22->setSpacing(6);
        horizontalLayout_22->setObjectName(QString::fromUtf8("horizontalLayout_22"));
        label_28 = new QLabel(verticalLayoutWidget_2);
        label_28->setObjectName(QString::fromUtf8("label_28"));
        label_28->setEnabled(true);
        label_28->setMinimumSize(QSize(120, 0));
        label_28->setFont(font1);

        horizontalLayout_22->addWidget(label_28);

        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_22->addItem(horizontalSpacer_11);

        autoStartInputEnable = new QCheckBox(verticalLayoutWidget_2);
        autoStartInputEnable->setObjectName(QString::fromUtf8("autoStartInputEnable"));
        autoStartInputEnable->setFont(font1);

        horizontalLayout_22->addWidget(autoStartInputEnable);


        verticalLayout_2->addLayout(horizontalLayout_22);

        groupBox_7 = new QGroupBox(GeneralSettings);
        groupBox_7->setObjectName(QString::fromUtf8("groupBox_7"));
        groupBox_7->setGeometry(QRect(40, 240, 261, 391));
        groupBox_7->setFont(font);
        groupBox_6 = new QGroupBox(groupBox_7);
        groupBox_6->setObjectName(QString::fromUtf8("groupBox_6"));
        groupBox_6->setGeometry(QRect(0, 170, 251, 101));
        groupBox_6->setFont(font);
        verticalLayoutWidget_5 = new QWidget(groupBox_6);
        verticalLayoutWidget_5->setObjectName(QString::fromUtf8("verticalLayoutWidget_5"));
        verticalLayoutWidget_5->setGeometry(QRect(10, 20, 235, 71));
        verticalLayout_6 = new QVBoxLayout(verticalLayoutWidget_5);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_25 = new QLabel(verticalLayoutWidget_5);
        label_25->setObjectName(QString::fromUtf8("label_25"));
        label_25->setMinimumSize(QSize(120, 0));
        label_25->setFont(font1);

        horizontalLayout_5->addWidget(label_25);

        editNrPieces = new QLineEdit(verticalLayoutWidget_5);
        editNrPieces->setObjectName(QString::fromUtf8("editNrPieces"));
        editNrPieces->setMinimumSize(QSize(60, 0));
        editNrPieces->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_5->addWidget(editNrPieces);


        verticalLayout_6->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_26 = new QLabel(verticalLayoutWidget_5);
        label_26->setObjectName(QString::fromUtf8("label_26"));
        label_26->setMinimumSize(QSize(120, 0));
        label_26->setFont(font1);

        horizontalLayout_6->addWidget(label_26);

        stopSysemEdit = new QLineEdit(verticalLayoutWidget_5);
        stopSysemEdit->setObjectName(QString::fromUtf8("stopSysemEdit"));
        stopSysemEdit->setMinimumSize(QSize(60, 0));
        stopSysemEdit->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_6->addWidget(stopSysemEdit);


        verticalLayout_6->addLayout(horizontalLayout_6);

        layoutWidget = new QWidget(groupBox_7);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(30, 40, 161, 101));
        verticalLayout_7 = new QVBoxLayout(layoutWidget);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        verticalLayout_7->setContentsMargins(0, 0, 0, 0);
        radioButtonNonStopMode = new QRadioButton(layoutWidget);
        radioButtonNonStopMode->setObjectName(QString::fromUtf8("radioButtonNonStopMode"));
        radioButtonNonStopMode->setChecked(true);

        verticalLayout_7->addWidget(radioButtonNonStopMode);

        radioButtonAutoStopMode = new QRadioButton(layoutWidget);
        radioButtonAutoStopMode->setObjectName(QString::fromUtf8("radioButtonAutoStopMode"));

        verticalLayout_7->addWidget(radioButtonAutoStopMode);

        radioButtonDelayStopMode = new QRadioButton(layoutWidget);
        radioButtonDelayStopMode->setObjectName(QString::fromUtf8("radioButtonDelayStopMode"));

        verticalLayout_7->addWidget(radioButtonDelayStopMode);

        groupBox_8 = new QGroupBox(groupBox_7);
        groupBox_8->setObjectName(QString::fromUtf8("groupBox_8"));
        groupBox_8->setGeometry(QRect(10, 280, 241, 101));
        layoutWidget_5 = new QWidget(groupBox_8);
        layoutWidget_5->setObjectName(QString::fromUtf8("layoutWidget_5"));
        layoutWidget_5->setGeometry(QRect(0, 20, 233, 23));
        horizontalLayout_21 = new QHBoxLayout(layoutWidget_5);
        horizontalLayout_21->setSpacing(6);
        horizontalLayout_21->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_21->setObjectName(QString::fromUtf8("horizontalLayout_21"));
        horizontalLayout_21->setContentsMargins(0, 0, 0, 0);
        label_24 = new QLabel(layoutWidget_5);
        label_24->setObjectName(QString::fromUtf8("label_24"));
        label_24->setEnabled(true);
        label_24->setMinimumSize(QSize(120, 0));
        label_24->setFont(font1);

        horizontalLayout_21->addWidget(label_24);

        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_21->addItem(horizontalSpacer_10);

        outputEnable = new QCheckBox(layoutWidget_5);
        outputEnable->setObjectName(QString::fromUtf8("outputEnable"));
        outputEnable->setFont(font1);

        horizontalLayout_21->addWidget(outputEnable);

        verticalLayoutWidget_6 = new QWidget(groupBox_8);
        verticalLayoutWidget_6->setObjectName(QString::fromUtf8("verticalLayoutWidget_6"));
        verticalLayoutWidget_6->setGeometry(QRect(0, 50, 231, 41));
        verticalLayout_8 = new QVBoxLayout(verticalLayoutWidget_6);
        verticalLayout_8->setSpacing(6);
        verticalLayout_8->setContentsMargins(11, 11, 11, 11);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        verticalLayout_8->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_27 = new QLabel(verticalLayoutWidget_6);
        label_27->setObjectName(QString::fromUtf8("label_27"));
        label_27->setMinimumSize(QSize(120, 0));
        label_27->setFont(font1);

        horizontalLayout_7->addWidget(label_27);

        outputSignalDuration = new QLineEdit(verticalLayoutWidget_6);
        outputSignalDuration->setObjectName(QString::fromUtf8("outputSignalDuration"));
        outputSignalDuration->setMinimumSize(QSize(60, 0));
        outputSignalDuration->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_7->addWidget(outputSignalDuration);


        verticalLayout_8->addLayout(horizontalLayout_7);


        retranslateUi(GeneralSettings);

        QMetaObject::connectSlotsByName(GeneralSettings);
    } // setupUi

    void retranslateUi(QWidget *GeneralSettings)
    {
        GeneralSettings->setWindowTitle(QCoreApplication::translate("GeneralSettings", "GeneralSettings", nullptr));
        groupBox_3->setTitle(QCoreApplication::translate("GeneralSettings", "Cleaning  Sequence", nullptr));
        label_19->setText(QCoreApplication::translate("GeneralSettings", "Auto cleaning mode ", nullptr));
        AutoCleaningEnable->setText(QCoreApplication::translate("GeneralSettings", "Enable", nullptr));
        label_20->setText(QCoreApplication::translate("GeneralSettings", "Cleaning Period [min]", nullptr));
        label_22->setText(QCoreApplication::translate("GeneralSettings", "Clean time bottom glass[ms]", nullptr));
        label_29->setText(QCoreApplication::translate("GeneralSettings", "Clean time top glass [ms]", nullptr));
        label_30->setText(QCoreApplication::translate("GeneralSettings", "Delay clean bottom  start [s]", nullptr));
        label_23->setText(QCoreApplication::translate("GeneralSettings", "Dealy clean top glass start [s]", nullptr));
        groupBox_4->setTitle(QCoreApplication::translate("GeneralSettings", "Sorting Valve settings", nullptr));
        groupBox->setTitle(QCoreApplication::translate("GeneralSettings", "Station 0", nullptr));
        label_11->setText(QCoreApplication::translate("GeneralSettings", "Offest to valve", nullptr));
        label_12->setText(QCoreApplication::translate("GeneralSettings", "Valve open Duration[ms]", nullptr));
        label_13->setText(QCoreApplication::translate("GeneralSettings", "Valve 2 delay[ms]", nullptr));
        label_14->setText(QCoreApplication::translate("GeneralSettings", "Valve 2 open Duration[ms]", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("GeneralSettings", "Station 1", nullptr));
        label_15->setText(QCoreApplication::translate("GeneralSettings", "Offset to valve", nullptr));
        label_16->setText(QCoreApplication::translate("GeneralSettings", "Valve open Duration[ms]", nullptr));
        label_17->setText(QCoreApplication::translate("GeneralSettings", "Valve 2 delay[ms]", nullptr));
        label_18->setText(QCoreApplication::translate("GeneralSettings", "Valve 2 open Duration[ms]", nullptr));
        groupBox_5->setTitle(QCoreApplication::translate("GeneralSettings", "General settings", nullptr));
        label_10->setText(QCoreApplication::translate("GeneralSettings", "I/O test mode", nullptr));
        testMode->setText(QCoreApplication::translate("GeneralSettings", "Enable", nullptr));
        label_21->setText(QCoreApplication::translate("GeneralSettings", "Elevator vibrator", nullptr));
        vibratorEnable->setText(QCoreApplication::translate("GeneralSettings", "Enable", nullptr));
        label_28->setText(QCoreApplication::translate("GeneralSettings", "Auto start input signal", nullptr));
        autoStartInputEnable->setText(QCoreApplication::translate("GeneralSettings", "Enable", nullptr));
        groupBox_7->setTitle(QCoreApplication::translate("GeneralSettings", "Working mode", nullptr));
        groupBox_6->setTitle(QCoreApplication::translate("GeneralSettings", "Auto stop settings", nullptr));
        label_25->setText(QCoreApplication::translate("GeneralSettings", "Nr. of pieces in box", nullptr));
        label_26->setText(QCoreApplication::translate("GeneralSettings", "Stop system [s]", nullptr));
        radioButtonNonStopMode->setText(QCoreApplication::translate("GeneralSettings", "NON - STOP MODE", nullptr));
        radioButtonAutoStopMode->setText(QCoreApplication::translate("GeneralSettings", "AUTO STOP MODE", nullptr));
        radioButtonDelayStopMode->setText(QCoreApplication::translate("GeneralSettings", "AUTO DELAY MODE", nullptr));
        groupBox_8->setTitle(QCoreApplication::translate("GeneralSettings", "output signal", nullptr));
        label_24->setText(QCoreApplication::translate("GeneralSettings", "Output signal", nullptr));
        outputEnable->setText(QCoreApplication::translate("GeneralSettings", "Enable", nullptr));
        label_27->setText(QCoreApplication::translate("GeneralSettings", "Output signal duration [ms]", nullptr));
    } // retranslateUi

};

namespace Ui {
    class GeneralSettings: public Ui_GeneralSettings {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GENERALSETTINGS_H
