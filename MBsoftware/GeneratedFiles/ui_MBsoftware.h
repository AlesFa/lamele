/********************************************************************************
** Form generated from reading UI file 'MBsoftware.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MBSOFTWARE_H
#define UI_MBSOFTWARE_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MBsoftwareClass
{
public:
    QWidget *centralWidget;
    QFrame *frameStatus;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout;
    QFrame *frameDeviceStatus;
    QLabel *labelDeviceStatus;
    QListWidget *listWidgetStatus;
    QLabel *labelLogin;
    QLabel *labelMode;
    QLabel *labelVibrator;
    QLabel *labelElevator;
    QLabel *labelTakt;
    QLabel *labelTaktGood;
    QLabel *labelWorkingMode;
    QSpacerItem *verticalSpacer;
    QTabWidget *MainMenuBar;
    QWidget *tabLogin;
    QHBoxLayout *horizontalLayout_4;
    QToolButton *buttonLogin;
    QSpacerItem *horizontalSpacer_2;
    QWidget *tabSettings;
    QHBoxLayout *horizontalLayout_6;
    QToolButton *buttonGeneralSettings;
    QToolButton *buttonResetGoodBox;
    QSpacerItem *horizontalSpacer_5;
    QWidget *tabTypes;
    QHBoxLayout *horizontalLayout_3;
    QToolButton *buttonSetTolerance;
    QToolButton *buttonTypeSelect;
    QToolButton *buttonAddType;
    QToolButton *buttonRemoveType;
    QToolButton *buttonResetCounters;
    QSpacerItem *horizontalSpacer_3;
    QWidget *tabCameras;
    QWidget *tabSignals;
    QWidget *tabImageProcessing;
    QHBoxLayout *horizontalLayout;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout_2;
    QToolButton *buttonImageProcessing;
    QSpacerItem *horizontalSpacer;
    QWidget *tabStatistcs;
    QHBoxLayout *horizontalLayout_5;
    QToolButton *buttonHistory;
    QToolButton *buttonStatusBox;
    QSpacerItem *horizontalSpacer_4;
    QToolButton *buttonAboutUS;
    QLabel *labelLogo;
    QFrame *statisticsFrame;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QLabel *labelGood;
    QLabel *labelTotal;
    QLabel *labelBAD;
    QLabel *labelBadProcent;
    QLabel *label_10;
    QLabel *label_9;
    QLabel *labelGoodProcent;
    QLabel *label_3;
    QLabel *label_2;
    QGraphicsView *imageViewMainWindow_0;
    QFrame *frameMeasurements;
    QLabel *label;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *layoutMeasurements;
    QLabel *labelCurrentType;
    QFrame *statisticsFrame_2;
    QWidget *gridLayoutWidget_2;
    QGridLayout *gridLayout_2;
    QLabel *labelGoodProcent_2;
    QLabel *label_13;
    QLabel *label_8;
    QLabel *label_12;
    QLabel *labelGood_2;
    QLabel *labelTotal_2;
    QLabel *labelBAD_2;
    QLabel *labelBadProcent_2;
    QLabel *label_14;
    QLabel *labelSpeed0;
    QGraphicsView *imageViewMainWindow_1;
    QFrame *statisticsFrame_3;
    QWidget *gridLayoutWidget_3;
    QGridLayout *gridLayout_3;
    QLabel *labelGoodProcent_3;
    QLabel *label_15;
    QLabel *label_11;
    QLabel *label_16;
    QLabel *labelGood_3;
    QLabel *labelTotal_3;
    QLabel *labelBAD_3;
    QLabel *labelBadProcent_3;
    QLabel *label_17;
    QLabel *labelSpeed1;
    QFrame *frameMeasurements_2;
    QLabel *label_5;
    QWidget *verticalLayoutWidget_3;
    QVBoxLayout *layoutMeasurements_2;
    QLabel *labelCurrentType_2;
    QFrame *frameGOODBOX;
    QLabel *label_4;
    QWidget *layoutWidget;
    QGridLayout *gridLayout_4;
    QLabel *labelPieceInBoxGood;
    QLabel *labelPieceInBox;
    QLabel *labelPieceInBox2;
    QLabel *labelPieceInBoxAll;
    QFrame *frameChart_4;
    QWidget *verticalLayoutWidget_11;
    QVBoxLayout *chartLayout_4;
    QFrame *frameChart;
    QWidget *verticalLayoutWidget_4;
    QVBoxLayout *chartLayout;
    QFrame *frameChart_3;
    QWidget *verticalLayoutWidget_8;
    QVBoxLayout *chartLayout_3;
    QFrame *frameChart_2;
    QWidget *verticalLayoutWidget_5;
    QVBoxLayout *chartLayout_2;

    void setupUi(QMainWindow *MBsoftwareClass)
    {
        if (MBsoftwareClass->objectName().isEmpty())
            MBsoftwareClass->setObjectName(QString::fromUtf8("MBsoftwareClass"));
        MBsoftwareClass->setWindowModality(Qt::NonModal);
        MBsoftwareClass->resize(1920, 1087);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MBsoftwareClass->sizePolicy().hasHeightForWidth());
        MBsoftwareClass->setSizePolicy(sizePolicy);
        MBsoftwareClass->setMinimumSize(QSize(1920, 1080));
        MBsoftwareClass->setMaximumSize(QSize(16777215, 16777215));
        MBsoftwareClass->setCursor(QCursor(Qt::ArrowCursor));
        MBsoftwareClass->setAutoFillBackground(false);
        MBsoftwareClass->setToolButtonStyle(Qt::ToolButtonIconOnly);
        MBsoftwareClass->setTabShape(QTabWidget::Triangular);
        MBsoftwareClass->setDockNestingEnabled(false);
        centralWidget = new QWidget(MBsoftwareClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(centralWidget->sizePolicy().hasHeightForWidth());
        centralWidget->setSizePolicy(sizePolicy1);
        centralWidget->setMinimumSize(QSize(1280, 860));
        centralWidget->setMaximumSize(QSize(1920, 1300));
        frameStatus = new QFrame(centralWidget);
        frameStatus->setObjectName(QString::fromUtf8("frameStatus"));
        frameStatus->setGeometry(QRect(1611, 120, 300, 621));
        frameStatus->setMinimumSize(QSize(300, 0));
        frameStatus->setMaximumSize(QSize(300, 16777215));
        frameStatus->setFrameShape(QFrame::Box);
        frameStatus->setFrameShadow(QFrame::Raised);
        frameStatus->setLineWidth(4);
        verticalLayoutWidget_2 = new QWidget(frameStatus);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(9, 14, 281, 601));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        frameDeviceStatus = new QFrame(verticalLayoutWidget_2);
        frameDeviceStatus->setObjectName(QString::fromUtf8("frameDeviceStatus"));
        frameDeviceStatus->setMinimumSize(QSize(0, 250));
        frameDeviceStatus->setFrameShape(QFrame::StyledPanel);
        frameDeviceStatus->setFrameShadow(QFrame::Plain);
        frameDeviceStatus->setLineWidth(1);
        labelDeviceStatus = new QLabel(frameDeviceStatus);
        labelDeviceStatus->setObjectName(QString::fromUtf8("labelDeviceStatus"));
        labelDeviceStatus->setGeometry(QRect(10, 10, 141, 20));
        QFont font;
        font.setFamily(QString::fromUtf8("Calibri"));
        font.setPointSize(16);
        font.setBold(true);
        font.setWeight(75);
        labelDeviceStatus->setFont(font);
        listWidgetStatus = new QListWidget(frameDeviceStatus);
        listWidgetStatus->setObjectName(QString::fromUtf8("listWidgetStatus"));
        listWidgetStatus->setGeometry(QRect(10, 30, 261, 211));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Calibri"));
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setWeight(75);
        listWidgetStatus->setFont(font1);

        verticalLayout->addWidget(frameDeviceStatus);

        labelLogin = new QLabel(verticalLayoutWidget_2);
        labelLogin->setObjectName(QString::fromUtf8("labelLogin"));
        labelLogin->setMinimumSize(QSize(200, 43));
        labelLogin->setMaximumSize(QSize(16777215, 43));
        labelLogin->setFont(font);
        labelLogin->setStyleSheet(QString::fromUtf8(""));
        labelLogin->setFrameShape(QFrame::Panel);
        labelLogin->setFrameShadow(QFrame::Sunken);
        labelLogin->setLineWidth(3);

        verticalLayout->addWidget(labelLogin);

        labelMode = new QLabel(verticalLayoutWidget_2);
        labelMode->setObjectName(QString::fromUtf8("labelMode"));
        labelMode->setMinimumSize(QSize(200, 43));
        labelMode->setMaximumSize(QSize(16777215, 43));
        labelMode->setFont(font);
        labelMode->setStyleSheet(QString::fromUtf8(""));
        labelMode->setFrameShape(QFrame::Panel);
        labelMode->setFrameShadow(QFrame::Sunken);
        labelMode->setLineWidth(3);

        verticalLayout->addWidget(labelMode);

        labelVibrator = new QLabel(verticalLayoutWidget_2);
        labelVibrator->setObjectName(QString::fromUtf8("labelVibrator"));
        labelVibrator->setMinimumSize(QSize(200, 43));
        labelVibrator->setMaximumSize(QSize(16777215, 43));
        labelVibrator->setFont(font);
        labelVibrator->setStyleSheet(QString::fromUtf8(""));
        labelVibrator->setFrameShape(QFrame::Panel);
        labelVibrator->setFrameShadow(QFrame::Sunken);
        labelVibrator->setLineWidth(3);

        verticalLayout->addWidget(labelVibrator);

        labelElevator = new QLabel(verticalLayoutWidget_2);
        labelElevator->setObjectName(QString::fromUtf8("labelElevator"));
        labelElevator->setMinimumSize(QSize(200, 43));
        labelElevator->setMaximumSize(QSize(16777215, 43));
        labelElevator->setFont(font);
        labelElevator->setStyleSheet(QString::fromUtf8(""));
        labelElevator->setFrameShape(QFrame::Panel);
        labelElevator->setFrameShadow(QFrame::Sunken);
        labelElevator->setLineWidth(3);

        verticalLayout->addWidget(labelElevator);

        labelTakt = new QLabel(verticalLayoutWidget_2);
        labelTakt->setObjectName(QString::fromUtf8("labelTakt"));
        labelTakt->setMinimumSize(QSize(200, 43));
        labelTakt->setMaximumSize(QSize(16777215, 43));
        labelTakt->setFont(font);
        labelTakt->setStyleSheet(QString::fromUtf8(""));
        labelTakt->setFrameShape(QFrame::Panel);
        labelTakt->setFrameShadow(QFrame::Sunken);
        labelTakt->setLineWidth(3);

        verticalLayout->addWidget(labelTakt);

        labelTaktGood = new QLabel(verticalLayoutWidget_2);
        labelTaktGood->setObjectName(QString::fromUtf8("labelTaktGood"));
        labelTaktGood->setMinimumSize(QSize(200, 43));
        labelTaktGood->setMaximumSize(QSize(16777215, 43));
        labelTaktGood->setFont(font);
        labelTaktGood->setStyleSheet(QString::fromUtf8(""));
        labelTaktGood->setFrameShape(QFrame::Panel);
        labelTaktGood->setFrameShadow(QFrame::Sunken);
        labelTaktGood->setLineWidth(3);

        verticalLayout->addWidget(labelTaktGood);

        labelWorkingMode = new QLabel(verticalLayoutWidget_2);
        labelWorkingMode->setObjectName(QString::fromUtf8("labelWorkingMode"));
        labelWorkingMode->setMinimumSize(QSize(200, 43));
        labelWorkingMode->setMaximumSize(QSize(16777215, 43));
        labelWorkingMode->setFont(font);
        labelWorkingMode->setStyleSheet(QString::fromUtf8(""));
        labelWorkingMode->setFrameShape(QFrame::Panel);
        labelWorkingMode->setFrameShadow(QFrame::Sunken);
        labelWorkingMode->setLineWidth(3);

        verticalLayout->addWidget(labelWorkingMode);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        MainMenuBar = new QTabWidget(centralWidget);
        MainMenuBar->setObjectName(QString::fromUtf8("MainMenuBar"));
        MainMenuBar->setGeometry(QRect(9, 9, 640, 150));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(MainMenuBar->sizePolicy().hasHeightForWidth());
        MainMenuBar->setSizePolicy(sizePolicy2);
        MainMenuBar->setMinimumSize(QSize(640, 120));
        MainMenuBar->setMaximumSize(QSize(16777215, 150));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Comic Sans MS"));
        font2.setPointSize(10);
        MainMenuBar->setFont(font2);
        MainMenuBar->setTabPosition(QTabWidget::North);
        MainMenuBar->setTabShape(QTabWidget::Triangular);
        MainMenuBar->setMovable(true);
        MainMenuBar->setTabBarAutoHide(true);
        tabLogin = new QWidget();
        tabLogin->setObjectName(QString::fromUtf8("tabLogin"));
        horizontalLayout_4 = new QHBoxLayout(tabLogin);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        buttonLogin = new QToolButton(tabLogin);
        buttonLogin->setObjectName(QString::fromUtf8("buttonLogin"));
        buttonLogin->setMinimumSize(QSize(100, 100));
        buttonLogin->setFocusPolicy(Qt::ClickFocus);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/MBsoftware32Bit/res/LoginBig.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonLogin->setIcon(icon);
        buttonLogin->setIconSize(QSize(80, 80));
        buttonLogin->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonLogin->setAutoRaise(true);

        horizontalLayout_4->addWidget(buttonLogin);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_2);

        MainMenuBar->addTab(tabLogin, QString());
        tabSettings = new QWidget();
        tabSettings->setObjectName(QString::fromUtf8("tabSettings"));
        horizontalLayout_6 = new QHBoxLayout(tabSettings);
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        buttonGeneralSettings = new QToolButton(tabSettings);
        buttonGeneralSettings->setObjectName(QString::fromUtf8("buttonGeneralSettings"));
        buttonGeneralSettings->setMinimumSize(QSize(100, 100));
        buttonGeneralSettings->setFont(font2);
        buttonGeneralSettings->setFocusPolicy(Qt::ClickFocus);
        QIcon icon1;
        icon1.addFile(QString::fromUtf8("res/settings.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonGeneralSettings->setIcon(icon1);
        buttonGeneralSettings->setIconSize(QSize(70, 90));
        buttonGeneralSettings->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonGeneralSettings->setAutoRaise(true);

        horizontalLayout_6->addWidget(buttonGeneralSettings);

        buttonResetGoodBox = new QToolButton(tabSettings);
        buttonResetGoodBox->setObjectName(QString::fromUtf8("buttonResetGoodBox"));
        buttonResetGoodBox->setMinimumSize(QSize(100, 100));
        buttonResetGoodBox->setFont(font2);
        buttonResetGoodBox->setFocusPolicy(Qt::ClickFocus);
        buttonResetGoodBox->setAcceptDrops(false);
        buttonResetGoodBox->setAutoFillBackground(false);
        QIcon icon2;
        icon2.addFile(QString::fromUtf8("res/resetButton.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonResetGoodBox->setIcon(icon2);
        buttonResetGoodBox->setIconSize(QSize(60, 60));
        buttonResetGoodBox->setCheckable(false);
        buttonResetGoodBox->setPopupMode(QToolButton::DelayedPopup);
        buttonResetGoodBox->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonResetGoodBox->setAutoRaise(true);

        horizontalLayout_6->addWidget(buttonResetGoodBox);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_5);

        MainMenuBar->addTab(tabSettings, QString());
        tabTypes = new QWidget();
        tabTypes->setObjectName(QString::fromUtf8("tabTypes"));
        horizontalLayout_3 = new QHBoxLayout(tabTypes);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        buttonSetTolerance = new QToolButton(tabTypes);
        buttonSetTolerance->setObjectName(QString::fromUtf8("buttonSetTolerance"));
        buttonSetTolerance->setMinimumSize(QSize(100, 100));
        buttonSetTolerance->setFont(font2);
        buttonSetTolerance->setFocusPolicy(Qt::ClickFocus);
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/MBsoftware32Bit/res/ParametersEditBig.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonSetTolerance->setIcon(icon3);
        buttonSetTolerance->setIconSize(QSize(60, 60));
        buttonSetTolerance->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonSetTolerance->setAutoRaise(true);

        horizontalLayout_3->addWidget(buttonSetTolerance);

        buttonTypeSelect = new QToolButton(tabTypes);
        buttonTypeSelect->setObjectName(QString::fromUtf8("buttonTypeSelect"));
        buttonTypeSelect->setMinimumSize(QSize(100, 100));
        buttonTypeSelect->setFont(font2);
        buttonTypeSelect->setFocusPolicy(Qt::ClickFocus);
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/MBsoftware32Bit/res/TypesLarge.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonTypeSelect->setIcon(icon4);
        buttonTypeSelect->setIconSize(QSize(70, 70));
        buttonTypeSelect->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonTypeSelect->setAutoRaise(true);

        horizontalLayout_3->addWidget(buttonTypeSelect);

        buttonAddType = new QToolButton(tabTypes);
        buttonAddType->setObjectName(QString::fromUtf8("buttonAddType"));
        buttonAddType->setMinimumSize(QSize(100, 100));
        buttonAddType->setFont(font2);
        buttonAddType->setFocusPolicy(Qt::ClickFocus);
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/MBsoftware32Bit/res/TypeNewIco.ico"), QSize(), QIcon::Normal, QIcon::Off);
        buttonAddType->setIcon(icon5);
        buttonAddType->setIconSize(QSize(60, 60));
        buttonAddType->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonAddType->setAutoRaise(true);

        horizontalLayout_3->addWidget(buttonAddType);

        buttonRemoveType = new QToolButton(tabTypes);
        buttonRemoveType->setObjectName(QString::fromUtf8("buttonRemoveType"));
        buttonRemoveType->setMinimumSize(QSize(100, 100));
        buttonRemoveType->setFont(font2);
        buttonRemoveType->setFocusPolicy(Qt::ClickFocus);
        buttonRemoveType->setAcceptDrops(false);
        buttonRemoveType->setAutoFillBackground(false);
        QIcon icon6;
        icon6.addFile(QString::fromUtf8("res/X.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonRemoveType->setIcon(icon6);
        buttonRemoveType->setIconSize(QSize(60, 60));
        buttonRemoveType->setCheckable(false);
        buttonRemoveType->setPopupMode(QToolButton::DelayedPopup);
        buttonRemoveType->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonRemoveType->setAutoRaise(true);

        horizontalLayout_3->addWidget(buttonRemoveType);

        buttonResetCounters = new QToolButton(tabTypes);
        buttonResetCounters->setObjectName(QString::fromUtf8("buttonResetCounters"));
        buttonResetCounters->setMinimumSize(QSize(100, 100));
        buttonResetCounters->setFont(font2);
        buttonResetCounters->setFocusPolicy(Qt::ClickFocus);
        buttonResetCounters->setAcceptDrops(false);
        buttonResetCounters->setAutoFillBackground(false);
        buttonResetCounters->setIcon(icon2);
        buttonResetCounters->setIconSize(QSize(60, 60));
        buttonResetCounters->setCheckable(false);
        buttonResetCounters->setPopupMode(QToolButton::DelayedPopup);
        buttonResetCounters->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonResetCounters->setAutoRaise(true);

        horizontalLayout_3->addWidget(buttonResetCounters);

        horizontalSpacer_3 = new QSpacerItem(830, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        MainMenuBar->addTab(tabTypes, QString());
        tabCameras = new QWidget();
        tabCameras->setObjectName(QString::fromUtf8("tabCameras"));
        MainMenuBar->addTab(tabCameras, QString());
        tabSignals = new QWidget();
        tabSignals->setObjectName(QString::fromUtf8("tabSignals"));
        MainMenuBar->addTab(tabSignals, QString());
        tabImageProcessing = new QWidget();
        tabImageProcessing->setObjectName(QString::fromUtf8("tabImageProcessing"));
        tabImageProcessing->setAcceptDrops(false);
        horizontalLayout = new QHBoxLayout(tabImageProcessing);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        groupBox = new QGroupBox(tabImageProcessing);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setFont(font2);
        groupBox->setAutoFillBackground(false);
        groupBox->setTitle(QString::fromUtf8(""));
        horizontalLayout_2 = new QHBoxLayout(groupBox);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        buttonImageProcessing = new QToolButton(groupBox);
        buttonImageProcessing->setObjectName(QString::fromUtf8("buttonImageProcessing"));
        buttonImageProcessing->setMinimumSize(QSize(100, 100));
        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/MBsoftware32Bit/res/imageProcessing.png"), QSize(), QIcon::Normal, QIcon::Off);
        buttonImageProcessing->setIcon(icon7);
        buttonImageProcessing->setIconSize(QSize(60, 45));
        buttonImageProcessing->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonImageProcessing->setAutoRaise(true);

        horizontalLayout_2->addWidget(buttonImageProcessing);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);


        horizontalLayout->addWidget(groupBox);

        MainMenuBar->addTab(tabImageProcessing, QString());
        tabStatistcs = new QWidget();
        tabStatistcs->setObjectName(QString::fromUtf8("tabStatistcs"));
        horizontalLayout_5 = new QHBoxLayout(tabStatistcs);
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        buttonHistory = new QToolButton(tabStatistcs);
        buttonHistory->setObjectName(QString::fromUtf8("buttonHistory"));
        buttonHistory->setMinimumSize(QSize(100, 100));
        QIcon icon8;
        icon8.addFile(QString::fromUtf8("res/history.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonHistory->setIcon(icon8);
        buttonHistory->setIconSize(QSize(70, 70));
        buttonHistory->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonHistory->setAutoRaise(true);

        horizontalLayout_5->addWidget(buttonHistory);

        buttonStatusBox = new QToolButton(tabStatistcs);
        buttonStatusBox->setObjectName(QString::fromUtf8("buttonStatusBox"));
        buttonStatusBox->setMinimumSize(QSize(100, 100));
        QIcon icon9;
        icon9.addFile(QString::fromUtf8("res/status.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonStatusBox->setIcon(icon9);
        buttonStatusBox->setIconSize(QSize(70, 80));
        buttonStatusBox->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonStatusBox->setAutoRaise(true);

        horizontalLayout_5->addWidget(buttonStatusBox);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_4);

        buttonAboutUS = new QToolButton(tabStatistcs);
        buttonAboutUS->setObjectName(QString::fromUtf8("buttonAboutUS"));
        buttonAboutUS->setMinimumSize(QSize(100, 100));
        QIcon icon10;
        icon10.addFile(QString::fromUtf8("res/about.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonAboutUS->setIcon(icon10);
        buttonAboutUS->setIconSize(QSize(70, 80));
        buttonAboutUS->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonAboutUS->setAutoRaise(true);

        horizontalLayout_5->addWidget(buttonAboutUS);

        MainMenuBar->addTab(tabStatistcs, QString());
        labelLogo = new QLabel(centralWidget);
        labelLogo->setObjectName(QString::fromUtf8("labelLogo"));
        labelLogo->setGeometry(QRect(1610, 10, 301, 101));
        labelLogo->setPixmap(QPixmap(QString::fromUtf8("res/MB.bmp")));
        labelLogo->setScaledContents(true);
        statisticsFrame = new QFrame(centralWidget);
        statisticsFrame->setObjectName(QString::fromUtf8("statisticsFrame"));
        statisticsFrame->setGeometry(QRect(660, 20, 531, 171));
        statisticsFrame->setFrameShape(QFrame::Box);
        statisticsFrame->setFrameShadow(QFrame::Sunken);
        gridLayoutWidget = new QWidget(statisticsFrame);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 40, 501, 121));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        labelGood = new QLabel(gridLayoutWidget);
        labelGood->setObjectName(QString::fromUtf8("labelGood"));
        labelGood->setFont(font);
        labelGood->setStyleSheet(QString::fromUtf8(""));
        labelGood->setFrameShape(QFrame::Panel);
        labelGood->setFrameShadow(QFrame::Sunken);
        labelGood->setLineWidth(3);
        labelGood->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelGood, 0, 1, 1, 1);

        labelTotal = new QLabel(gridLayoutWidget);
        labelTotal->setObjectName(QString::fromUtf8("labelTotal"));
        labelTotal->setFont(font);
        labelTotal->setFrameShape(QFrame::Panel);
        labelTotal->setFrameShadow(QFrame::Sunken);
        labelTotal->setLineWidth(3);
        labelTotal->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelTotal, 2, 1, 1, 2);

        labelBAD = new QLabel(gridLayoutWidget);
        labelBAD->setObjectName(QString::fromUtf8("labelBAD"));
        labelBAD->setFont(font);
        labelBAD->setFrameShape(QFrame::Panel);
        labelBAD->setFrameShadow(QFrame::Sunken);
        labelBAD->setLineWidth(3);
        labelBAD->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelBAD, 1, 1, 1, 1);

        labelBadProcent = new QLabel(gridLayoutWidget);
        labelBadProcent->setObjectName(QString::fromUtf8("labelBadProcent"));
        labelBadProcent->setFont(font);
        labelBadProcent->setFrameShape(QFrame::Panel);
        labelBadProcent->setFrameShadow(QFrame::Sunken);
        labelBadProcent->setLineWidth(3);
        labelBadProcent->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelBadProcent, 1, 2, 1, 1);

        label_10 = new QLabel(gridLayoutWidget);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setMinimumSize(QSize(180, 0));
        label_10->setFont(font);
        label_10->setFrameShape(QFrame::Panel);
        label_10->setFrameShadow(QFrame::Sunken);
        label_10->setLineWidth(3);

        gridLayout->addWidget(label_10, 2, 0, 1, 1);

        label_9 = new QLabel(gridLayoutWidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setMinimumSize(QSize(180, 0));
        label_9->setFont(font);
        label_9->setStyleSheet(QString::fromUtf8("background-color: rgb(150, 255, 150);\n"
""));
        label_9->setFrameShape(QFrame::Panel);
        label_9->setFrameShadow(QFrame::Sunken);
        label_9->setLineWidth(3);

        gridLayout->addWidget(label_9, 0, 0, 1, 1);

        labelGoodProcent = new QLabel(gridLayoutWidget);
        labelGoodProcent->setObjectName(QString::fromUtf8("labelGoodProcent"));
        labelGoodProcent->setFont(font);
        labelGoodProcent->setStyleSheet(QString::fromUtf8(""));
        labelGoodProcent->setFrameShape(QFrame::Panel);
        labelGoodProcent->setFrameShadow(QFrame::Sunken);
        labelGoodProcent->setLineWidth(3);
        labelGoodProcent->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelGoodProcent, 0, 2, 1, 1);

        label_3 = new QLabel(gridLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMinimumSize(QSize(180, 0));
        label_3->setFont(font);
        label_3->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 150, 150);\n"
""));
        label_3->setFrameShape(QFrame::Panel);
        label_3->setFrameShadow(QFrame::Sunken);
        label_3->setLineWidth(3);

        gridLayout->addWidget(label_3, 1, 0, 1, 1);

        label_2 = new QLabel(statisticsFrame);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(10, 0, 351, 41));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Calibri"));
        font3.setPointSize(16);
        font3.setBold(true);
        font3.setItalic(true);
        font3.setWeight(75);
        label_2->setFont(font3);
        label_2->setFrameShape(QFrame::NoFrame);
        label_2->setFrameShadow(QFrame::Sunken);
        imageViewMainWindow_0 = new QGraphicsView(centralWidget);
        imageViewMainWindow_0->setObjectName(QString::fromUtf8("imageViewMainWindow_0"));
        imageViewMainWindow_0->setGeometry(QRect(460, 220, 350, 521));
        imageViewMainWindow_0->setMinimumSize(QSize(0, 0));
        imageViewMainWindow_0->setFrameShape(QFrame::WinPanel);
        imageViewMainWindow_0->setFrameShadow(QFrame::Sunken);
        frameMeasurements = new QFrame(centralWidget);
        frameMeasurements->setObjectName(QString::fromUtf8("frameMeasurements"));
        frameMeasurements->setGeometry(QRect(20, 220, 431, 311));
        frameMeasurements->setMinimumSize(QSize(400, 200));
        frameMeasurements->setMaximumSize(QSize(500, 16777215));
        frameMeasurements->setFrameShape(QFrame::Box);
        frameMeasurements->setFrameShadow(QFrame::Raised);
        label = new QLabel(frameMeasurements);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 10, 201, 21));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Calibri"));
        font4.setPointSize(14);
        font4.setBold(true);
        font4.setItalic(false);
        font4.setWeight(75);
        label->setFont(font4);
        label->setFrameShape(QFrame::NoFrame);
        label->setFrameShadow(QFrame::Raised);
        verticalLayoutWidget = new QWidget(frameMeasurements);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(10, 40, 391, 121));
        layoutMeasurements = new QVBoxLayout(verticalLayoutWidget);
        layoutMeasurements->setSpacing(6);
        layoutMeasurements->setContentsMargins(11, 11, 11, 11);
        layoutMeasurements->setObjectName(QString::fromUtf8("layoutMeasurements"));
        layoutMeasurements->setSizeConstraint(QLayout::SetMinimumSize);
        layoutMeasurements->setContentsMargins(0, 0, 0, 0);
        labelCurrentType = new QLabel(frameMeasurements);
        labelCurrentType->setObjectName(QString::fromUtf8("labelCurrentType"));
        labelCurrentType->setGeometry(QRect(180, 10, 221, 21));
        labelCurrentType->setFont(font4);
        labelCurrentType->setFrameShape(QFrame::Box);
        labelCurrentType->setFrameShadow(QFrame::Raised);
        labelCurrentType->setTextFormat(Qt::PlainText);
        labelCurrentType->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        statisticsFrame_2 = new QFrame(centralWidget);
        statisticsFrame_2->setObjectName(QString::fromUtf8("statisticsFrame_2"));
        statisticsFrame_2->setGeometry(QRect(20, 580, 431, 161));
        statisticsFrame_2->setFrameShape(QFrame::Box);
        statisticsFrame_2->setFrameShadow(QFrame::Sunken);
        gridLayoutWidget_2 = new QWidget(statisticsFrame_2);
        gridLayoutWidget_2->setObjectName(QString::fromUtf8("gridLayoutWidget_2"));
        gridLayoutWidget_2->setGeometry(QRect(10, 30, 391, 122));
        gridLayout_2 = new QGridLayout(gridLayoutWidget_2);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        labelGoodProcent_2 = new QLabel(gridLayoutWidget_2);
        labelGoodProcent_2->setObjectName(QString::fromUtf8("labelGoodProcent_2"));
        labelGoodProcent_2->setMinimumSize(QSize(0, 36));
        QFont font5;
        font5.setFamily(QString::fromUtf8("Calibri"));
        font5.setPointSize(12);
        font5.setBold(true);
        font5.setItalic(false);
        font5.setWeight(75);
        labelGoodProcent_2->setFont(font5);
        labelGoodProcent_2->setStyleSheet(QString::fromUtf8(""));
        labelGoodProcent_2->setFrameShape(QFrame::Panel);
        labelGoodProcent_2->setFrameShadow(QFrame::Sunken);
        labelGoodProcent_2->setLineWidth(3);
        labelGoodProcent_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(labelGoodProcent_2, 0, 2, 1, 1);

        label_13 = new QLabel(gridLayoutWidget_2);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setMinimumSize(QSize(150, 36));
        label_13->setFont(font5);
        label_13->setFrameShape(QFrame::Panel);
        label_13->setFrameShadow(QFrame::Sunken);
        label_13->setLineWidth(3);

        gridLayout_2->addWidget(label_13, 2, 0, 1, 1);

        label_8 = new QLabel(gridLayoutWidget_2);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setMinimumSize(QSize(150, 36));
        label_8->setFont(font5);
        label_8->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 150, 150);\n"
""));
        label_8->setFrameShape(QFrame::Panel);
        label_8->setFrameShadow(QFrame::Sunken);
        label_8->setLineWidth(3);

        gridLayout_2->addWidget(label_8, 1, 0, 1, 1);

        label_12 = new QLabel(gridLayoutWidget_2);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setMinimumSize(QSize(150, 36));
        label_12->setFont(font5);
        label_12->setStyleSheet(QString::fromUtf8("background-color: rgb(150, 255, 150);\n"
""));
        label_12->setFrameShape(QFrame::Panel);
        label_12->setFrameShadow(QFrame::Sunken);
        label_12->setLineWidth(3);

        gridLayout_2->addWidget(label_12, 0, 0, 1, 1);

        labelGood_2 = new QLabel(gridLayoutWidget_2);
        labelGood_2->setObjectName(QString::fromUtf8("labelGood_2"));
        labelGood_2->setFont(font5);
        labelGood_2->setStyleSheet(QString::fromUtf8(""));
        labelGood_2->setFrameShape(QFrame::Panel);
        labelGood_2->setFrameShadow(QFrame::Sunken);
        labelGood_2->setLineWidth(3);
        labelGood_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(labelGood_2, 0, 1, 1, 1);

        labelTotal_2 = new QLabel(gridLayoutWidget_2);
        labelTotal_2->setObjectName(QString::fromUtf8("labelTotal_2"));
        labelTotal_2->setMinimumSize(QSize(0, 36));
        labelTotal_2->setFont(font5);
        labelTotal_2->setFrameShape(QFrame::Panel);
        labelTotal_2->setFrameShadow(QFrame::Sunken);
        labelTotal_2->setLineWidth(3);
        labelTotal_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(labelTotal_2, 2, 1, 1, 2);

        labelBAD_2 = new QLabel(gridLayoutWidget_2);
        labelBAD_2->setObjectName(QString::fromUtf8("labelBAD_2"));
        labelBAD_2->setMinimumSize(QSize(0, 36));
        labelBAD_2->setFont(font5);
        labelBAD_2->setFrameShape(QFrame::Panel);
        labelBAD_2->setFrameShadow(QFrame::Sunken);
        labelBAD_2->setLineWidth(3);
        labelBAD_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(labelBAD_2, 1, 1, 1, 1);

        labelBadProcent_2 = new QLabel(gridLayoutWidget_2);
        labelBadProcent_2->setObjectName(QString::fromUtf8("labelBadProcent_2"));
        labelBadProcent_2->setMinimumSize(QSize(0, 36));
        labelBadProcent_2->setFont(font5);
        labelBadProcent_2->setFrameShape(QFrame::Panel);
        labelBadProcent_2->setFrameShadow(QFrame::Sunken);
        labelBadProcent_2->setLineWidth(3);
        labelBadProcent_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(labelBadProcent_2, 1, 2, 1, 1);

        label_14 = new QLabel(statisticsFrame_2);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setGeometry(QRect(10, 0, 221, 31));
        label_14->setFont(font5);
        label_14->setFrameShape(QFrame::NoFrame);
        label_14->setFrameShadow(QFrame::Sunken);
        labelSpeed0 = new QLabel(statisticsFrame_2);
        labelSpeed0->setObjectName(QString::fromUtf8("labelSpeed0"));
        labelSpeed0->setGeometry(QRect(230, 0, 201, 29));
        labelSpeed0->setFont(font5);
        labelSpeed0->setFrameShape(QFrame::Box);
        labelSpeed0->setFrameShadow(QFrame::Raised);
        labelSpeed0->setTextFormat(Qt::PlainText);
        labelSpeed0->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        imageViewMainWindow_1 = new QGraphicsView(centralWidget);
        imageViewMainWindow_1->setObjectName(QString::fromUtf8("imageViewMainWindow_1"));
        imageViewMainWindow_1->setGeometry(QRect(830, 220, 350, 521));
        imageViewMainWindow_1->setMinimumSize(QSize(350, 0));
        imageViewMainWindow_1->setMaximumSize(QSize(350, 540));
        imageViewMainWindow_1->setFrameShape(QFrame::WinPanel);
        imageViewMainWindow_1->setFrameShadow(QFrame::Sunken);
        statisticsFrame_3 = new QFrame(centralWidget);
        statisticsFrame_3->setObjectName(QString::fromUtf8("statisticsFrame_3"));
        statisticsFrame_3->setGeometry(QRect(1190, 580, 421, 161));
        QFont font6;
        font6.setBold(false);
        font6.setWeight(50);
        statisticsFrame_3->setFont(font6);
        statisticsFrame_3->setFrameShape(QFrame::Box);
        statisticsFrame_3->setFrameShadow(QFrame::Sunken);
        gridLayoutWidget_3 = new QWidget(statisticsFrame_3);
        gridLayoutWidget_3->setObjectName(QString::fromUtf8("gridLayoutWidget_3"));
        gridLayoutWidget_3->setGeometry(QRect(10, 30, 391, 121));
        gridLayoutWidget_3->setFont(font6);
        gridLayout_3 = new QGridLayout(gridLayoutWidget_3);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        labelGoodProcent_3 = new QLabel(gridLayoutWidget_3);
        labelGoodProcent_3->setObjectName(QString::fromUtf8("labelGoodProcent_3"));
        labelGoodProcent_3->setFont(font5);
        labelGoodProcent_3->setStyleSheet(QString::fromUtf8(""));
        labelGoodProcent_3->setFrameShape(QFrame::Panel);
        labelGoodProcent_3->setFrameShadow(QFrame::Sunken);
        labelGoodProcent_3->setLineWidth(3);
        labelGoodProcent_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_3->addWidget(labelGoodProcent_3, 0, 2, 1, 1);

        label_15 = new QLabel(gridLayoutWidget_3);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setMinimumSize(QSize(150, 36));
        label_15->setFont(font5);
        label_15->setFrameShape(QFrame::Panel);
        label_15->setFrameShadow(QFrame::Sunken);
        label_15->setLineWidth(3);

        gridLayout_3->addWidget(label_15, 2, 0, 1, 1);

        label_11 = new QLabel(gridLayoutWidget_3);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setMinimumSize(QSize(150, 0));
        label_11->setFont(font5);
        label_11->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 150, 150);\n"
""));
        label_11->setFrameShape(QFrame::Panel);
        label_11->setFrameShadow(QFrame::Sunken);
        label_11->setLineWidth(3);

        gridLayout_3->addWidget(label_11, 1, 0, 1, 1);

        label_16 = new QLabel(gridLayoutWidget_3);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setMinimumSize(QSize(150, 0));
        label_16->setFont(font5);
        label_16->setStyleSheet(QString::fromUtf8("background-color: rgb(150, 255, 150);\n"
""));
        label_16->setFrameShape(QFrame::Panel);
        label_16->setFrameShadow(QFrame::Sunken);
        label_16->setLineWidth(3);

        gridLayout_3->addWidget(label_16, 0, 0, 1, 1);

        labelGood_3 = new QLabel(gridLayoutWidget_3);
        labelGood_3->setObjectName(QString::fromUtf8("labelGood_3"));
        labelGood_3->setFont(font5);
        labelGood_3->setStyleSheet(QString::fromUtf8(""));
        labelGood_3->setFrameShape(QFrame::Panel);
        labelGood_3->setFrameShadow(QFrame::Sunken);
        labelGood_3->setLineWidth(3);
        labelGood_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_3->addWidget(labelGood_3, 0, 1, 1, 1);

        labelTotal_3 = new QLabel(gridLayoutWidget_3);
        labelTotal_3->setObjectName(QString::fromUtf8("labelTotal_3"));
        labelTotal_3->setFont(font5);
        labelTotal_3->setFrameShape(QFrame::Panel);
        labelTotal_3->setFrameShadow(QFrame::Sunken);
        labelTotal_3->setLineWidth(3);
        labelTotal_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_3->addWidget(labelTotal_3, 2, 1, 1, 2);

        labelBAD_3 = new QLabel(gridLayoutWidget_3);
        labelBAD_3->setObjectName(QString::fromUtf8("labelBAD_3"));
        labelBAD_3->setFont(font5);
        labelBAD_3->setFrameShape(QFrame::Panel);
        labelBAD_3->setFrameShadow(QFrame::Sunken);
        labelBAD_3->setLineWidth(3);
        labelBAD_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_3->addWidget(labelBAD_3, 1, 1, 1, 1);

        labelBadProcent_3 = new QLabel(gridLayoutWidget_3);
        labelBadProcent_3->setObjectName(QString::fromUtf8("labelBadProcent_3"));
        labelBadProcent_3->setFont(font5);
        labelBadProcent_3->setFrameShape(QFrame::Panel);
        labelBadProcent_3->setFrameShadow(QFrame::Sunken);
        labelBadProcent_3->setLineWidth(3);
        labelBadProcent_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_3->addWidget(labelBadProcent_3, 1, 2, 1, 1);

        label_17 = new QLabel(statisticsFrame_3);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setGeometry(QRect(10, 0, 221, 31));
        label_17->setFont(font5);
        label_17->setFrameShape(QFrame::NoFrame);
        label_17->setFrameShadow(QFrame::Sunken);
        labelSpeed1 = new QLabel(statisticsFrame_3);
        labelSpeed1->setObjectName(QString::fromUtf8("labelSpeed1"));
        labelSpeed1->setGeometry(QRect(210, 0, 211, 29));
        labelSpeed1->setFont(font5);
        labelSpeed1->setFrameShape(QFrame::Box);
        labelSpeed1->setFrameShadow(QFrame::Raised);
        labelSpeed1->setTextFormat(Qt::PlainText);
        labelSpeed1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        frameMeasurements_2 = new QFrame(centralWidget);
        frameMeasurements_2->setObjectName(QString::fromUtf8("frameMeasurements_2"));
        frameMeasurements_2->setGeometry(QRect(1190, 220, 421, 311));
        frameMeasurements_2->setMinimumSize(QSize(400, 200));
        frameMeasurements_2->setMaximumSize(QSize(500, 16777215));
        frameMeasurements_2->setFrameShape(QFrame::Box);
        frameMeasurements_2->setFrameShadow(QFrame::Raised);
        label_5 = new QLabel(frameMeasurements_2);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(10, 10, 201, 21));
        label_5->setFont(font3);
        label_5->setFrameShape(QFrame::NoFrame);
        label_5->setFrameShadow(QFrame::Raised);
        verticalLayoutWidget_3 = new QWidget(frameMeasurements_2);
        verticalLayoutWidget_3->setObjectName(QString::fromUtf8("verticalLayoutWidget_3"));
        verticalLayoutWidget_3->setGeometry(QRect(10, 40, 391, 121));
        layoutMeasurements_2 = new QVBoxLayout(verticalLayoutWidget_3);
        layoutMeasurements_2->setSpacing(6);
        layoutMeasurements_2->setContentsMargins(11, 11, 11, 11);
        layoutMeasurements_2->setObjectName(QString::fromUtf8("layoutMeasurements_2"));
        layoutMeasurements_2->setSizeConstraint(QLayout::SetMinimumSize);
        layoutMeasurements_2->setContentsMargins(0, 0, 0, 0);
        labelCurrentType_2 = new QLabel(frameMeasurements_2);
        labelCurrentType_2->setObjectName(QString::fromUtf8("labelCurrentType_2"));
        labelCurrentType_2->setGeometry(QRect(170, 10, 221, 21));
        labelCurrentType_2->setFont(font3);
        labelCurrentType_2->setFrameShape(QFrame::Box);
        labelCurrentType_2->setFrameShadow(QFrame::Raised);
        labelCurrentType_2->setTextFormat(Qt::PlainText);
        labelCurrentType_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        frameGOODBOX = new QFrame(centralWidget);
        frameGOODBOX->setObjectName(QString::fromUtf8("frameGOODBOX"));
        frameGOODBOX->setGeometry(QRect(1210, 20, 331, 171));
        frameGOODBOX->setFrameShape(QFrame::Box);
        frameGOODBOX->setFrameShadow(QFrame::Sunken);
        label_4 = new QLabel(frameGOODBOX);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(10, 0, 331, 41));
        label_4->setFont(font3);
        label_4->setFrameShape(QFrame::NoFrame);
        label_4->setFrameShadow(QFrame::Sunken);
        layoutWidget = new QWidget(frameGOODBOX);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 40, 311, 111));
        gridLayout_4 = new QGridLayout(layoutWidget);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        labelPieceInBoxGood = new QLabel(layoutWidget);
        labelPieceInBoxGood->setObjectName(QString::fromUtf8("labelPieceInBoxGood"));
        labelPieceInBoxGood->setMinimumSize(QSize(50, 36));
        labelPieceInBoxGood->setMaximumSize(QSize(150, 36));
        labelPieceInBoxGood->setFont(font);
        labelPieceInBoxGood->setStyleSheet(QString::fromUtf8(""));
        labelPieceInBoxGood->setFrameShape(QFrame::Panel);
        labelPieceInBoxGood->setFrameShadow(QFrame::Sunken);
        labelPieceInBoxGood->setLineWidth(3);

        gridLayout_4->addWidget(labelPieceInBoxGood, 0, 0, 1, 1);

        labelPieceInBox = new QLabel(layoutWidget);
        labelPieceInBox->setObjectName(QString::fromUtf8("labelPieceInBox"));
        labelPieceInBox->setMinimumSize(QSize(50, 36));
        labelPieceInBox->setMaximumSize(QSize(150, 36));
        labelPieceInBox->setFont(font);
        labelPieceInBox->setStyleSheet(QString::fromUtf8(""));
        labelPieceInBox->setFrameShape(QFrame::Panel);
        labelPieceInBox->setFrameShadow(QFrame::Sunken);
        labelPieceInBox->setLineWidth(3);
        labelPieceInBox->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_4->addWidget(labelPieceInBox, 0, 1, 1, 1);

        labelPieceInBox2 = new QLabel(layoutWidget);
        labelPieceInBox2->setObjectName(QString::fromUtf8("labelPieceInBox2"));
        labelPieceInBox2->setMinimumSize(QSize(50, 36));
        labelPieceInBox2->setMaximumSize(QSize(150, 36));
        labelPieceInBox2->setFont(font);
        labelPieceInBox2->setStyleSheet(QString::fromUtf8(""));
        labelPieceInBox2->setFrameShape(QFrame::Panel);
        labelPieceInBox2->setFrameShadow(QFrame::Sunken);
        labelPieceInBox2->setLineWidth(3);

        gridLayout_4->addWidget(labelPieceInBox2, 1, 0, 1, 1);

        labelPieceInBoxAll = new QLabel(layoutWidget);
        labelPieceInBoxAll->setObjectName(QString::fromUtf8("labelPieceInBoxAll"));
        labelPieceInBoxAll->setMinimumSize(QSize(50, 36));
        labelPieceInBoxAll->setMaximumSize(QSize(150, 36));
        labelPieceInBoxAll->setFont(font);
        labelPieceInBoxAll->setStyleSheet(QString::fromUtf8(""));
        labelPieceInBoxAll->setFrameShape(QFrame::Panel);
        labelPieceInBoxAll->setFrameShadow(QFrame::Sunken);
        labelPieceInBoxAll->setLineWidth(3);
        labelPieceInBoxAll->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_4->addWidget(labelPieceInBoxAll, 1, 1, 1, 1);

        frameChart_4 = new QFrame(centralWidget);
        frameChart_4->setObjectName(QString::fromUtf8("frameChart_4"));
        frameChart_4->setGeometry(QRect(1453, 770, 468, 319));
        frameChart_4->setFrameShape(QFrame::StyledPanel);
        frameChart_4->setFrameShadow(QFrame::Raised);
        verticalLayoutWidget_11 = new QWidget(frameChart_4);
        verticalLayoutWidget_11->setObjectName(QString::fromUtf8("verticalLayoutWidget_11"));
        verticalLayoutWidget_11->setGeometry(QRect(10, 10, 411, 281));
        chartLayout_4 = new QVBoxLayout(verticalLayoutWidget_11);
        chartLayout_4->setSpacing(6);
        chartLayout_4->setContentsMargins(11, 11, 11, 11);
        chartLayout_4->setObjectName(QString::fromUtf8("chartLayout_4"));
        chartLayout_4->setContentsMargins(0, 0, 0, 0);
        frameChart = new QFrame(centralWidget);
        frameChart->setObjectName(QString::fromUtf8("frameChart"));
        frameChart->setGeometry(QRect(32, 770, 468, 319));
        frameChart->setFrameShape(QFrame::StyledPanel);
        frameChart->setFrameShadow(QFrame::Raised);
        verticalLayoutWidget_4 = new QWidget(frameChart);
        verticalLayoutWidget_4->setObjectName(QString::fromUtf8("verticalLayoutWidget_4"));
        verticalLayoutWidget_4->setGeometry(QRect(10, 10, 411, 281));
        chartLayout = new QVBoxLayout(verticalLayoutWidget_4);
        chartLayout->setSpacing(6);
        chartLayout->setContentsMargins(11, 11, 11, 11);
        chartLayout->setObjectName(QString::fromUtf8("chartLayout"));
        chartLayout->setContentsMargins(0, 0, 0, 0);
        frameChart_3 = new QFrame(centralWidget);
        frameChart_3->setObjectName(QString::fromUtf8("frameChart_3"));
        frameChart_3->setGeometry(QRect(980, 770, 467, 319));
        frameChart_3->setFrameShape(QFrame::StyledPanel);
        frameChart_3->setFrameShadow(QFrame::Raised);
        verticalLayoutWidget_8 = new QWidget(frameChart_3);
        verticalLayoutWidget_8->setObjectName(QString::fromUtf8("verticalLayoutWidget_8"));
        verticalLayoutWidget_8->setGeometry(QRect(10, 10, 411, 281));
        chartLayout_3 = new QVBoxLayout(verticalLayoutWidget_8);
        chartLayout_3->setSpacing(6);
        chartLayout_3->setContentsMargins(11, 11, 11, 11);
        chartLayout_3->setObjectName(QString::fromUtf8("chartLayout_3"));
        chartLayout_3->setContentsMargins(0, 0, 0, 0);
        frameChart_2 = new QFrame(centralWidget);
        frameChart_2->setObjectName(QString::fromUtf8("frameChart_2"));
        frameChart_2->setGeometry(QRect(506, 770, 468, 319));
        frameChart_2->setFrameShape(QFrame::StyledPanel);
        frameChart_2->setFrameShadow(QFrame::Raised);
        verticalLayoutWidget_5 = new QWidget(frameChart_2);
        verticalLayoutWidget_5->setObjectName(QString::fromUtf8("verticalLayoutWidget_5"));
        verticalLayoutWidget_5->setGeometry(QRect(10, 10, 411, 281));
        chartLayout_2 = new QVBoxLayout(verticalLayoutWidget_5);
        chartLayout_2->setSpacing(6);
        chartLayout_2->setContentsMargins(11, 11, 11, 11);
        chartLayout_2->setObjectName(QString::fromUtf8("chartLayout_2"));
        chartLayout_2->setContentsMargins(0, 0, 0, 0);
        MBsoftwareClass->setCentralWidget(centralWidget);

        retranslateUi(MBsoftwareClass);

        MainMenuBar->setCurrentIndex(6);


        QMetaObject::connectSlotsByName(MBsoftwareClass);
    } // setupUi

    void retranslateUi(QMainWindow *MBsoftwareClass)
    {
        MBsoftwareClass->setWindowTitle(QCoreApplication::translate("MBsoftwareClass", "MBsoftware", nullptr));
        labelDeviceStatus->setText(QCoreApplication::translate("MBsoftwareClass", "DEVICE STATUS:", nullptr));
        labelLogin->setText(QString());
        labelMode->setText(QString());
        labelVibrator->setText(QString());
        labelElevator->setText(QString());
        labelTakt->setText(QString());
        labelTaktGood->setText(QString());
        labelWorkingMode->setText(QString());
        buttonLogin->setText(QCoreApplication::translate("MBsoftwareClass", "Login", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabLogin), QCoreApplication::translate("MBsoftwareClass", "Login", nullptr));
        buttonGeneralSettings->setText(QCoreApplication::translate("MBsoftwareClass", "System settings", nullptr));
        buttonResetGoodBox->setText(QCoreApplication::translate("MBsoftwareClass", "RESET GOOD BOX", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabSettings), QCoreApplication::translate("MBsoftwareClass", "Settings", nullptr));
        buttonSetTolerance->setText(QCoreApplication::translate("MBsoftwareClass", "Edit tolerance", nullptr));
        buttonTypeSelect->setText(QCoreApplication::translate("MBsoftwareClass", "Type select", nullptr));
        buttonAddType->setText(QCoreApplication::translate("MBsoftwareClass", "Add type", nullptr));
        buttonRemoveType->setText(QCoreApplication::translate("MBsoftwareClass", "Remove type", nullptr));
        buttonResetCounters->setText(QCoreApplication::translate("MBsoftwareClass", "Reset Counters", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabTypes), QCoreApplication::translate("MBsoftwareClass", "Types", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabCameras), QCoreApplication::translate("MBsoftwareClass", "Cameras", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabSignals), QCoreApplication::translate("MBsoftwareClass", "I/O signals", nullptr));
        buttonImageProcessing->setText(QCoreApplication::translate("MBsoftwareClass", "ImageProcessing Dialog", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabImageProcessing), QCoreApplication::translate("MBsoftwareClass", "ImageProcessing", nullptr));
        buttonHistory->setText(QCoreApplication::translate("MBsoftwareClass", "History", nullptr));
        buttonStatusBox->setText(QCoreApplication::translate("MBsoftwareClass", "Status window\n"
"", nullptr));
        buttonAboutUS->setText(QCoreApplication::translate("MBsoftwareClass", "About Us", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabStatistcs), QCoreApplication::translate("MBsoftwareClass", "Statistics && Status", nullptr));
        labelLogo->setText(QString());
        labelGood->setText(QString());
        labelTotal->setText(QString());
        labelBAD->setText(QString());
        labelBadProcent->setText(QString());
        label_10->setText(QCoreApplication::translate("MBsoftwareClass", "TOTAL:", nullptr));
        label_9->setText(QCoreApplication::translate("MBsoftwareClass", "GOOD:", nullptr));
        labelGoodProcent->setText(QString());
        label_3->setText(QCoreApplication::translate("MBsoftwareClass", "BAD:", nullptr));
        label_2->setText(QCoreApplication::translate("MBsoftwareClass", "ALL STATION TOTAL  COUNTERS", nullptr));
        label->setText(QCoreApplication::translate("MBsoftwareClass", "Station 0:", nullptr));
        labelCurrentType->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelGoodProcent_2->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        label_13->setText(QCoreApplication::translate("MBsoftwareClass", "TOTAL:", nullptr));
        label_8->setText(QCoreApplication::translate("MBsoftwareClass", "BAD:", nullptr));
        label_12->setText(QCoreApplication::translate("MBsoftwareClass", "GOOD:", nullptr));
        labelGood_2->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelTotal_2->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelBAD_2->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelBadProcent_2->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        label_14->setText(QCoreApplication::translate("MBsoftwareClass", "STATION 0 COUNTERS:", nullptr));
        labelSpeed0->setText(QString());
        labelGoodProcent_3->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        label_15->setText(QCoreApplication::translate("MBsoftwareClass", "TOTAL:", nullptr));
        label_11->setText(QCoreApplication::translate("MBsoftwareClass", "BAD:", nullptr));
        label_16->setText(QCoreApplication::translate("MBsoftwareClass", "GOOD:", nullptr));
        labelGood_3->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelTotal_3->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelBAD_3->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelBadProcent_3->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        label_17->setText(QCoreApplication::translate("MBsoftwareClass", "STATION 1 COUNTERS:", nullptr));
        labelSpeed1->setText(QString());
        label_5->setText(QCoreApplication::translate("MBsoftwareClass", "Station 1:", nullptr));
        labelCurrentType_2->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        label_4->setText(QCoreApplication::translate("MBsoftwareClass", "GOOD BOX COUNTERS", nullptr));
        labelPieceInBoxGood->setText(QCoreApplication::translate("MBsoftwareClass", "CURRENT:", nullptr));
        labelPieceInBox->setText(QString());
        labelPieceInBox2->setText(QCoreApplication::translate("MBsoftwareClass", "MAX PIECES:", nullptr));
        labelPieceInBoxAll->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MBsoftwareClass: public Ui_MBsoftwareClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MBSOFTWARE_H
