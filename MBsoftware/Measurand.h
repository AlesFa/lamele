﻿#pragma once

#include "stdafx.h"
#include <QObject>

#define POMNILNIK_LAMEL 8
#define NR_CENTER  15
#define TMP_POMNILNIK 10




class Measurand : public QObject  //razred ki ga uporabljam za prenos meritev med razredi imageProcessing in MBSoftware32bit 
{
	Q_OBJECT

public:
	Measurand(QObject *parent);
	Measurand();
	~Measurand();


	int currentType;
	int prevType;
//za lamele

	int tmpNrLamel;
	int tmpIzmerjena[TMP_POMNILNIK];
	//CPointFloat tmpCenter[TMP_POMNILNIK];
	float tmpPloscina[TMP_POMNILNIK];
	CPointFloat tmpTockeLamel[TMP_POMNILNIK][360];
	float tmpdistanceFromCenter[TMP_POMNILNIK][360];
	float tmpAngleLamel[TMP_POMNILNIK];
	float tmpDolzinaLamel[TMP_POMNILNIK];
	float tmpSirinaLamel[TMP_POMNILNIK];
	float tmpVisinaLamel[TMP_POMNILNIK];

	QRect displayRect[NR_STATIONS][POMNILNIK_LAMEL];
	float angle[NR_STATIONS][POMNILNIK_LAMEL];
	int isActive[NR_STATIONS][POMNILNIK_LAMEL];
	int isMeasured[NR_STATIONS][POMNILNIK_LAMEL];
	int isProcessed[NR_STATIONS][POMNILNIK_LAMEL];
	int area[NR_STATIONS][POMNILNIK_LAMEL];
	float sirina[NR_STATIONS][POMNILNIK_LAMEL];
	float visina[NR_STATIONS][POMNILNIK_LAMEL];
	float visinaOdstopanja[NR_STATIONS][POMNILNIK_LAMEL];
	float dolzina[NR_STATIONS][POMNILNIK_LAMEL];
	float odstopanja[NR_STATIONS][POMNILNIK_LAMEL];
	float speed[NR_STATIONS][POMNILNIK_LAMEL];
	int isMeasuredSpeed[NR_STATIONS][POMNILNIK_LAMEL];
	CPointFloat center[NR_STATIONS][POMNILNIK_LAMEL][NR_CENTER];
	int imageIndexObdelava[NR_STATIONS][POMNILNIK_LAMEL][NR_CENTER];
	
	int measureSpeedCounter[NR_STATIONS][POMNILNIK_LAMEL];
	float distanceFromCenter[NR_STATIONS][POMNILNIK_LAMEL][360];
	CPointFloat tockeLamele[NR_STATIONS][POMNILNIK_LAMEL][360];
	vector <CPointFloat> tockeKrogaZgoraj[NR_STATIONS][POMNILNIK_LAMEL];
	vector <CPointFloat> tockeKrogaSpodaj[NR_STATIONS][POMNILNIK_LAMEL];
	vector <float > distanceToCircleZgoraj[NR_STATIONS][POMNILNIK_LAMEL];
	vector <float > distanceToCircleSpodaj[NR_STATIONS][POMNILNIK_LAMEL];
	CCircle krogZgoraj[NR_STATIONS][POMNILNIK_LAMEL];
	CCircle krogSpodaj[NR_STATIONS][POMNILNIK_LAMEL];
	int xOffsetDraw[NR_STATIONS][POMNILNIK_LAMEL];
	int yOffsetDraw[NR_STATIONS][POMNILNIK_LAMEL];
	int imageIndex[NR_STATIONS][POMNILNIK_LAMEL];
	float processTime[NR_STATIONS][POMNILNIK_LAMEL];
	int isGood[NR_STATIONS][POMNILNIK_LAMEL];

	int currLamela[NR_STATIONS];

	float refDistanceFromCenter[NR_STATIONS][360];
	void ResetMeasurand(int tstation,int index);
	//za lamele 

};
