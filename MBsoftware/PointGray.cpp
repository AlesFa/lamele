#include "stdafx.h"
#include "PointGray.h"


#include "FlyCapture2Platform.h"
#include "FlyCapture2Defs.h"

#include "FlyCapture2.h"
#include "Camera.h"
#include "CameraBase.h"
#include <iostream>
#include <sstream>
#include "PointGray.h"
#include <QtConcurrent>

using namespace FlyCapture2;
using namespace std;



bool grabLoop = true;



CPointGray::CPointGray()
{
	cam = NULL;



	cam = new Camera;

}

CPointGray::~CPointGray()
{
	FlyCapture2::Error error;

	TriggerMode triggerMode;
	grabLoop = false;
	//TerminateThread(ThreadGrabImage, 0);

	if (cam != NULL)
	{
		// Turn trigger mode off.
		triggerMode.onOff = false;
		error = cam->SetTriggerMode(&triggerMode);
		if (error != PGRERROR_OK)
		{
			//PrintError(error);
			return;
		}
		Sleep(100);

		// Stop capturing images
		error = cam->StopCapture();
		if (error != PGRERROR_OK)
		{
			//PrintError(error);
			return;
		}
		Sleep(100);

		error = cam->Disconnect();
		if (error != PGRERROR_OK)
		{
			//PrintError(error);
			return;
		}
	}
	//delete cam;

}







int CPointGray::Init(int selectGrabber, QString cameraName, int width, int height, int depth, int offsetX, int offsetY)
{
	return 0;
}
int CPointGray::Init(QString serialNumber, QString VideoFormatS)
{
	Init();
	return 1;
}
int CPointGray::Init()
{



	bufferSize = this->depth * this->width * this->height;

	InterfaceType ifType;
	unsigned int serialNumberInt = 0;
	//serialNumberInt = atoi((CStringA)serialNumber);

	serialNumberInt = (unsigned int)serialNumber.toInt();
	error = busMgr.GetCameraFromSerialNumber(serialNumberInt, &guid);
	if (error != PGRERROR_OK)
	{
		return 0;

	}



	error = busMgr.GetInterfaceTypeFromGuid(&guid, &ifType);
	if (error != PGRERROR_OK)
	{
		return 0;
	}
	if (cam != NULL)
	{
		delete cam;
		cam = NULL;
	}

	if (ifType == INTERFACE_GIGE)
	{
		//cam = new GigECamera;
	}
	else
	{
		cam = new Camera;
	}


	//Declare a Property struct. 





	error = cam->Connect(&guid);
	if (error != PGRERROR_OK)
	{

		return -1;
	}


	//Nastavitev exposure in gain mora biti za cam->Connect, sicer nima u�inka
	//SetExposureAbsolute(defaultExposure);
	//SetGainAbsolute(defaultGain);


	//set image RESOLUTION

	bool supported;
	const Mode k_fmt7Mode = MODE_0;
	const PixelFormat	k_fmt7PixFmt = PIXEL_FORMAT_MONO8;
	fmt7Info.mode = k_fmt7Mode;
	error = cam->GetFormat7Info(&fmt7Info, &supported);
	if (error != PGRERROR_OK)
	{
		//PrintError(error);
		return -1;
	}
	//preverim width and height ter nastavimo format
	if (width > fmt7Info.maxWidth || height > fmt7Info.maxHeight)
		return -1;
	if (depth == 1)
	{
		const PixelFormat	k_fmt7PixFmt = PIXEL_FORMAT_MONO8;
		conversionCode = COLOR_GRAY2RGB;
	}
	if (depth == 3)
	{
		const PixelFormat	k_fmt7PixFmt = PIXEL_FORMAT_BGR;
		conversionCode = COLOR_BGR2RGB;
	}
	//preverimo format slike
	if ((k_fmt7PixFmt & fmt7Info.pixelFormatBitField) == 0)
	{
		// Pixel format not supported!
		cout << "Pixel format is not supported" << endl;
		return -1;
	}


	fmt7ImageSettings.mode = k_fmt7Mode;
	fmt7ImageSettings.offsetX = offsetX;
	fmt7ImageSettings.offsetY = offsetY;
	fmt7ImageSettings.width = width;
	fmt7ImageSettings.height = height;
	fmt7ImageSettings.pixelFormat = k_fmt7PixFmt;


	Property frmRate;
	frmRate.type = FRAME_RATE;
	frmRate.absValue = FPS;
	error = cam->SetProperty(&frmRate);
	if (error != PGRERROR_OK)
	{
		//PrintError(error);
		return -1;
	}




	bool valid;


	// Validate the settings to make sure that they are valid
	error = cam->ValidateFormat7Settings(
		&fmt7ImageSettings,
		&valid,
		&fmt7PacketInfo);
	if (error != PGRERROR_OK)
	{
		//	PrintError(error);
		return -1;
	}
	error = cam->SetFormat7Configuration(
		&fmt7ImageSettings,
		fmt7PacketInfo.recommendedBytesPerPacket);
	if (error != PGRERROR_OK)
	{
		//PrintError(error);
		return -1;
	}


	if (trigger)
	{
		grabLoop = true;
		triggerMode.mode = 0;
		triggerMode.source = 0;
		triggerMode.parameter = 0;
		triggerMode.onOff = true;
		triggerMode.polarity = 0;
		//cam->SetTriggerMode(&triggerMode);

	}
	else
	{
		// Set camera to trigger mode 0
		triggerMode.onOff = true;
		triggerMode.mode = 0;
		triggerMode.parameter = 0;


		// A source of 7 means software trigger
		triggerMode.source = 7;

	}
	error = cam->SetTriggerMode(&triggerMode);
	if (error != PGRERROR_OK)
	{

		return -1;
	}
	const unsigned int k_softwareTrigger = 0x62C;

	unsigned int regVal = 0;



	// Get the camera configuration
	FC2Config config;

	// Set the grab timeout to 5 seconds
	config.grabTimeout = 1.0/FPS *1000;

	// Set the camera configuration
	error = cam->SetConfiguration(&config);
	if (error != PGRERROR_OK)
	{

		return -1;
	}

	GetGainAbsolute();
	GetExposureAbsolute();

	return 1;

}

int CPointGray::Start(double FPS, int trigger)
{
	return Start();
}

int CPointGray::Start()
{
	// Camera is ready, start capturing images
	error = cam->StartCapture();
	if (error != PGRERROR_OK)
	{

		return -1;
	}
	isLive = 1;
	if (trigger == false)
		GrabImage();

	//frameReadyFreq.SetStart();

	cameraOpened++;
	int ImageSize;
	InitImages(width, height, depth);
	ImageSize = image.size();
	if (trigger)
		 QtConcurrent::run(this, &CPointGray::DoGrabLoop);
	//QFuture<void> future = QtConcurrent::run(this, &CPointGray::DoGrabLoop);
	return 1;
}

bool CPointGray::SetGainAbsolute(double value)
{


	prop.type = GAIN;
	//Ensure the property is on. 
	prop.onOff = true;
	//Ensure auto-adjust mode is off. 
	prop.autoManualMode = false;
	//Ensure the property is set up to use absolute value control. 
	prop.absControl = false;
	//Set the absolute value of shutter to 20 ms. 
	//prop.absValue = value;
	prop.valueA = value;
	//Set the property.
	cam->SetProperty(&prop);
	currentGain = value;


	return true;
}


double CPointGray::GetExposureAbsolute()
{
	PropertyInfo propInfo;
	propInfo.type = SHUTTER;
	cam->GetPropertyInfo(&propInfo);
	maxExpo = propInfo.absMax;
	minExpo = propInfo.absMin;

	return 0;
}


int CPointGray::GetGainAbsolute()
{

	PropertyInfo propInfo;
	propInfo.type = GAIN;
	cam->GetPropertyInfo(&propInfo);
	maxGain = propInfo.max;
	minGain = propInfo.min;

	/*Property gain;
	gain.type = GAIN;
	error = cam->GetProperty(&gain);
	maxGain = gain.valueA;
	minGain = gain.valueB;*/
	return 0;
}



bool CPointGray::SaveReferenceSettings()
{
	QString filePath;
	QStringList values;
	QVariant var;

	filePath = QDir::currentPath() + QString("/%1/nastavitveKamer/PointGray_%2.ini").arg(REF_FOLDER_NAME).arg(serialNumber);
	QSettings settings(filePath, QSettings::IniFormat);


	//inputs
	for (int i = 0; i < num_images; i++)
	{
		values = settings.value(QString("EXPO%1").arg(i)).toStringList();

		if (values.size() > 0)
		{
			settings.setValue(QString("EXPO%1").arg(i), QString("%1").arg(currentExposure));
			settings.sync();
			values.clear();
		}

	}
	for (int i = 0; i < num_images; i++)
	{
		values = settings.value(QString("GAIN%1").arg(i)).toStringList();

		if (values.size() > 0)
		{
			settings.setValue(QString("GAIN%1").arg(i), QString("%1").arg(currentGain));

			settings.sync();
		}

	}

	return 0;

}

bool CPointGray::LoadReferenceSettings()
{
	QString filePath;
	QStringList values;
	QVariant var;

	filePath = QDir::currentPath() + QString("/%1/nastavitveKamer/PointGray_%2.ini").arg(REF_FOLDER_NAME).arg(serialNumber);
	QSettings settings(filePath, QSettings::IniFormat);


	//inputs
	for (int i = 0; i < num_images; i++)
	{
		values = settings.value(QString("EXPO%1").arg(i)).toStringList();

		if (values.size() > 0)
		{
			exposure.push_back(values[0].toFloat());
			//input[i].SetSignal(values[0], adresses[i + 3], values[1].toInt());
			values.clear();
		}
		else
		{
			exposure.push_back(0);
			settings.setValue(QString("EXPO%1").arg(i), QString("0"));
			settings.sync();
		}


	}
	for (int i = 0; i < num_images; i++)
	{
		values = settings.value(QString("GAIN%1").arg(i)).toStringList();

		if (values.size() > 0)
		{
			gain.push_back(values[0].toInt());
			values.clear();
		}
		else
		{
			gain.push_back(0);
			settings.setValue(QString("GAIN%1").arg(i), QString("0"));
			settings.sync();
		}


	}


	return 0;
}




int CPointGray::DoGrabLoop()
{
	const unsigned int k_softwareTrigger = 0x62C;
	const unsigned int k_fireVal = 0x80000000;
	// FlyCapture2::Error error;
	// unsigned char* buff;
	Image image3;
	int ImageSize;


	while (grabLoop)
	{
		if (liveImage)
		{
			error = cam->WriteRegister(k_softwareTrigger, k_fireVal);
			if (error != PGRERROR_OK)
			{

				return false;
			}
		}
		error = cam->RetrieveBuffer(&image3);
		if (error == PGRERROR_OK)
		{
			if (liveImage == 1)
			{
				imageIndex = 0;
			}
			else
			{
				if (imageIndex > (image.size() - 1))
					imageIndex = 0;
			}
			const uint8_t *pImageBuffer = (uint8_t *)image3.GetData();


			//Za�etek Martinove kode za zrcaljenje: dobro dela v GrabImage, v DoGrabLoop pa se ob�asno prikazujejo napake

			int vrstica;
			int bytesPerLine = (this->width)*(this->depth);
			bool zrcaliOkoliVodoravne = this->rotadedHorizontal;	//nevtraliziram ne�eleno zrcaljenje okoli vodoravne osi, ki se zgodi neznano kje v programu
			bool zrcaliOkoliNavpicne = this->rotatedVertical;


			if (!zrcaliOkoliVodoravne && !zrcaliOkoliNavpicne)	//brez zrcaljenja
			{
				std::copy(&pImageBuffer[0], &pImageBuffer[0] + bufferSize, &image[imageIndex].buffer->data[0]);
			}
			else
			{
				for (vrstica = 0; vrstica < this->height; vrstica++)
				{
					if (!zrcaliOkoliVodoravne && zrcaliOkoliNavpicne)	//z zrcaljenjem okoli navpi�ne osi
						std::reverse_copy(&pImageBuffer[0] + vrstica * bytesPerLine, &pImageBuffer[0] + (vrstica + 1)*bytesPerLine - 1, &image[imageIndex].buffer->data[0] + vrstica * bytesPerLine);

					if (zrcaliOkoliVodoravne && !zrcaliOkoliNavpicne)  //z zrcaljenjem okoli vodoravne osi
						std::copy(&pImageBuffer[0] + vrstica * bytesPerLine, &pImageBuffer[0] + (vrstica + 1)*bytesPerLine - 1, &image[imageIndex].buffer->data[0] + (this->height - (vrstica + 1))*bytesPerLine);

					if (zrcaliOkoliVodoravne && zrcaliOkoliNavpicne) //z zrcaljenjem okoli vodoravne in navpi�ne osi																																
						std::reverse_copy(&pImageBuffer[0] + vrstica * bytesPerLine, &pImageBuffer[0] + +(vrstica + 1)*bytesPerLine - 1, &image[imageIndex].buffer->data[0] + (this->height - (vrstica + 1))*bytesPerLine);
				}
			}

			//Konec Martinove kode za zrcaljenje
			OnFrameReady();
			frameCounter++;
			imageReady[imageIndex] = 1;
			imageIndex++;
			
			if (imageIndex >= num_images)
				imageIndex = 0;
		
		}
	}
	return 1;
}

bool CPointGray::SetExposureAbsolute(double value)
{
	//Declare a Property struct. 
	//Define the property to adjust. 


	PropertyInfo propInfo;
	propInfo.type = SHUTTER;
	cam->GetPropertyInfo(&propInfo);
	
	prop.type = SHUTTER;
	
	//Ensure the property is on. 
	prop.onOff = true;
	//Ensure auto-adjust mode is off. 
	prop.autoManualMode = false;
	//Ensure the property is set up to use absolute value control. 
	prop.absControl = true;
	//Set the absolute value of shutter to 20 ms. 

	prop.absValue = (value);

	//Set the property.
	cam->SetProperty(&prop);
	currentExposure = (value);
	return true;
}
void CPointGray::AutoGainOnce()
{

}


void CPointGray::AutoGainContinuous()
{

}


void CPointGray::AutoExposureOnce()
{

}


void CPointGray::AutoExposureContinuous()
{

}


void CPointGray::AutoWhiteBalance()
{

}


bool CPointGray::IsColorCamera()
{
	return 0;
}
int CPointGray::GrabImage()
{
	const unsigned int k_softwareTrigger = 0x62C;
	const unsigned int k_fireVal = 0x80000000;
	FlyCapture2::Error error;
	unsigned char* buff;

	error = cam->WriteRegister(k_softwareTrigger, k_fireVal);
	if (error != PGRERROR_OK)
	{

		return false;
	}


	//return true;
	error = cam->RetrieveBuffer(&image2);
	if (error != PGRERROR_OK)
	{
		return false;
	}



	//popravek za ve� slik na isti kameri
	imageIndex++;
	if (imageIndex > num_images - 1)
		imageIndex = 0;
	//


	/*
	frameCounter++;
	imageReady[imageIndex] = 1;
	if (realTimeProcessing)
		theApp.OnFrameReady(id);
		*/


	return 0;
}

int CPointGray::GrabImage(int imageNr)
{



	const unsigned int k_softwareTrigger = 0x62C;
	const unsigned int k_fireVal = 0x80000000;
	FlyCapture2::Error error;
	unsigned char* buff;


	if (isLive)
	{
		//if (liveImage)


		error = cam->WriteRegister(k_softwareTrigger, k_fireVal);
		if (error != PGRERROR_OK)
		{

			return false;
		}


		//return true;
		error = cam->RetrieveBuffer(&image2);
		if (error != PGRERROR_OK)
		{
			return false;
		}


		const uint8_t *pImageBuffer = (uint8_t *)image2.GetData();


		//Za�etek Martinove kode za zrcaljenje
		int vrstica;
		int bytesPerLine = (this->width)*(this->depth);
		bool zrcaliOkoliVodoravne = !this->rotadedHorizontal;	//nevtraliziram ne�eleno zrcaljenje okoli vodoravne osi, ki se zgodi neznano kje v programu
		bool zrcaliOkoliNavpicne = this->rotatedVertical;


		if (!zrcaliOkoliVodoravne && !zrcaliOkoliNavpicne)	//brez zrcaljenja
		{
			std::copy(&pImageBuffer[0], &pImageBuffer[0] + bufferSize, &image[0].buffer->data[0]);
		

		}
		else
		{
			for (vrstica = 0; vrstica < this->height; vrstica++)
			{
				if (!zrcaliOkoliVodoravne && zrcaliOkoliNavpicne)	//z zrcaljenjem okoli navpi�ne osi
					std::reverse_copy(&pImageBuffer[0] + vrstica * bytesPerLine, &pImageBuffer[0] + (vrstica + 1)*bytesPerLine - 1, &image[imageNr].buffer[0] + vrstica * bytesPerLine);

				if (zrcaliOkoliVodoravne && !zrcaliOkoliNavpicne)  //z zrcaljenjem okoli vodoravne osi
					std::copy(&pImageBuffer[0] + vrstica * bytesPerLine, &pImageBuffer[0] + (vrstica + 1)*bytesPerLine - 1, &image[imageNr].buffer[0] + (this->height - (vrstica + 1))*bytesPerLine);

				if (zrcaliOkoliVodoravne && zrcaliOkoliNavpicne) //z zrcaljenjem okoli vodoravne in navpi�ne osi																																
					std::reverse_copy(&pImageBuffer[0] + vrstica * bytesPerLine, &pImageBuffer[0] + +(vrstica + 1)*bytesPerLine - 1, &image[imageNr].buffer[0] + (this->height - (vrstica + 1))*bytesPerLine);
			}
		}


		//Konec Martinove kode za zrcaljenje

		//Dodal Martin po zgledu CBasler::GrabImage

		frameCounter++;
		imageReady[imageNr] = 1;
		emit frameReadySignal(id, imageIndex);
	//	if (realTimeProcessing)
		//	theApp.OnFrameReady(id);
	}

	return 0;
}


void CPointGray::CloseGrabber()
{

}

bool CPointGray::OnSettingsImage()
{




	return 0;
}


bool CPointGray::IsDeviceValid()
{

	return 0;
}




bool CPointGray::EnableLive()
{

	
	// Set camera to trigger mode 0
	triggerMode.onOff = true;
	triggerMode.mode = 0;
	triggerMode.parameter = 0;


	// A source of 7 means software trigger
	triggerMode.source = 7;


	error = cam->SetTriggerMode(&triggerMode);
	if (error != PGRERROR_OK)
	{

		return false;
	}
	const unsigned int k_softwareTrigger = 0x62C;

	unsigned int regVal = 0;

	do
	{
		error = cam->ReadRegister(k_softwareTrigger, &regVal);
		if (error != PGRERROR_OK)
		{

			return false;
		}

	} while ((regVal >> 31) != 0);

	if (enableLive == 0)
	{
		grabLoop = true;
		QtConcurrent::run(this, &CPointGray::DoGrabLoop);
		liveImage = 1;

		enableLive = 1;
	}
	else
	{
		grabLoop = false;
		enableLive = false;
		liveImage = 0;
	}
	return false;
}

bool CPointGray::DisableLive()
{
	if (trigger)
	{
		grabLoop = true;
		liveImage = false;
	}
	triggerMode.onOff = true;
	triggerMode.mode = 0;
	triggerMode.parameter = 0;


	// A source of 7 means software trigger
	triggerMode.source = 0;


	error = cam->SetTriggerMode(&triggerMode);
	if (error != PGRERROR_OK)
	{

		return false;
	}
	const unsigned int k_softwareTrigger = 0x62C;

	unsigned int regVal = 0;

	do
	{
		error = cam->ReadRegister(k_softwareTrigger, &regVal);
		if (error != PGRERROR_OK)
		{

			return false;
		}

	} while ((regVal >> 31) != 0);


	//AfxBeginThread(ThreadGrabImage, this);
	//emit grabLoopSignal();
	enableLive = false;
	return false;
}

bool CPointGray::SetPartialOffsetX(int xOff)
{
	return false;
}
int	CPointGray::GetPartialOffsetX()
{
	return 0;
}
bool CPointGray::SetPartialOffsetY(int yOff)
{
	return false;
}
int	CPointGray::GetPartialOffsetY()
{
	return 0;
}

void CPointGray::OnFrameReady()
{
	emit showImageSignal(0);
	emit frameReadySignal(id, imageIndex);
}
