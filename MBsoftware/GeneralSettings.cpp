#include "stdafx.h"
#include "GeneralSettings.h"

/*GeneralSettings::GeneralSettings(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
}*/

GeneralSettings::GeneralSettings()
{
	ui.setupUi(this);


	connect(ui.buttonBox, SIGNAL(accepted()), this, SLOT(OnConfirm()));
	connect(ui.buttonBox, SIGNAL(rejected()), this, SLOT(OnRejected()));
	connect(ui.radioButtonNonStopMode, SIGNAL(clicked()), this, SLOT(OnClickedNonStop()));
	connect(ui.radioButtonAutoStopMode, SIGNAL(clicked()), this, SLOT(OnClickedStopMode()));
	connect(ui.radioButtonDelayStopMode, SIGNAL(clicked()), this, SLOT(OnClickedStopDelayMode()));

	LoadSettings();
	SetState();




}

GeneralSettings::~GeneralSettings()
{

}

void GeneralSettings::closeEvent(QCloseEvent * event)
{
	close();
}

void GeneralSettings::OnShowDialog()
{
	setWindowModality(Qt::ApplicationModal);
	SetState();
	show();
}

void GeneralSettings::SaveSettings()
{

		QStringList list;
		//Pot do konfugiracijskih datotek

		QString dirPath = QDir::currentPath() + QString("/%1/DynamicVariables/").arg(REF_FOLDER_NAME);
		QDir dir;

		dir.setPath(dirPath);
		if (!dir.exists())
			dir.mkdir(dirPath);

		int index = 0;
		QString filePath = dirPath + QString("generalSettings.ini");

		QSettings settings(filePath, QSettings::IniFormat);

	

		list.append(QString("%1").arg(testIOMode));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();

		list.append(QString("%1").arg(disToValveS0));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();

		list.append(QString("%1").arg(disToValveS1));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();

		list.append(QString("%1").arg(valveDurationS0));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();

		list.append(QString("%1").arg(valveDurationS1));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();

		list.append(QString("%1").arg(valve2DelayS0));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();

		list.append(QString("%1").arg(valve2DelayS1));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();

		list.append(QString("%1").arg(valve2DurationS0));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();

		list.append(QString("%1").arg(valve2DurationS1));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();

		//cleaning
		list.append(QString("%1").arg(cleanEnable));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();


		list.append(QString("%1").arg(cleanPeriod));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();


		list.append(QString("%1").arg(cleanDurationBottom));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();

		list.append(QString("%1").arg(cleanDurationTop));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();


		list.append(QString("%1").arg(cleanDelayBottom));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();

		list.append(QString("%1").arg(cleanDelayTop));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();

		list.append(QString("%1").arg(elevatorVibratorEnable));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();

		list.append(QString("%1").arg(pieceInBox));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();

	

		list.append(QString("%1").arg(autostopTime));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();

		list.append(QString("%1").arg(workingMode));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();

		list.append(QString("%1").arg(outSignalEnable));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();

		list.append(QString("%1").arg(outSignalDuration));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();

		list.append(QString("%1").arg(inputSignalEnable));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();



}

int GeneralSettings::LoadSettings()
{
	QStringList values;

	QString dirPath = QDir::currentPath() + QString("/%1/DynamicVariables/").arg(REF_FOLDER_NAME);
	QDir dir;

	dir.setPath(dirPath);
	if (!dir.exists())
	{
		dir.mkdir(dirPath);
		return 0;
	}

	int index = 0;
	QString filePath = dirPath + QString("generalSettings.ini");

	QSettings settings(filePath, QSettings::IniFormat);
	int n = 0;

	


		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		testIOMode = values[0].toInt();
		n++;

		//za lamele postaja 0 in 1 

		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		disToValveS0 = values[0].toFloat();
		n++;


		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		disToValveS1 = values[0].toFloat();
		n++;

		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		valveDurationS0 = values[0].toInt();
		n++;

		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		valveDurationS1 = values[0].toInt();
		n++;

		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		valve2DelayS0 = values[0].toInt();
		n++;

		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		valve2DelayS1 = values[0].toInt();
		n++;

		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		valve2DurationS0 = values[0].toInt();
		n++;

		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		valve2DurationS1 = values[0].toInt();
		n++;


		//lamele ciscenje
		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		cleanEnable = values[0].toInt();
		n++;

		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		cleanPeriod = values[0].toInt();
		n++;

		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		cleanDurationBottom = values[0].toInt();
		n++;

		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		cleanDurationTop = values[0].toInt();
		n++;

		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		cleanDelayBottom = values[0].toInt();
		n++;

		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		cleanDelayTop = values[0].toInt();
		n++;


		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		elevatorVibratorEnable = values[0].toInt();
		n++;

		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		pieceInBox = values[0].toInt();
		n++;

	

		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		autostopTime = values[0].toInt();
		n++;

		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		workingMode = values[0].toInt();
		n++;

		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		outSignalEnable = values[0].toInt();
		n++;

		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		outSignalDuration = values[0].toInt();
		n++;

		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		inputSignalEnable = values[0].toInt();
		n++;
	

		return 1;
}

void GeneralSettings::SetState()
{
	ui.testMode->setChecked(testIOMode);

	//lamele
	ui.distanceToValveS0->setText(QString("%1").arg(disToValveS0));
	ui.distanceToValveS1->setText(QString("%1").arg(disToValveS1));
	ui.valveDurS0->setText(QString("%1").arg(valveDurationS0));
	ui.valveDurS1->setText(QString("%1").arg(valveDurationS1));
	ui.valve2delayS0->setText(QString("%1").arg(valve2DelayS0));
	ui.valve2delayS1->setText(QString("%1").arg(valve2DelayS1));
	ui.valve2DruS0->setText(QString("%1").arg(valve2DurationS0));
	ui.valve2DruS1->setText(QString("%1").arg(valve2DurationS1));

	ui.AutoCleaningEnable->setChecked(cleanEnable);
	ui.editCleanPeriod->setText(QString("%1").arg(cleanPeriod));
	ui.editCleanDelayBottom->setText(QString("%1").arg(cleanDelayBottom));
	ui.editCleanDelayTop->setText(QString("%1").arg(cleanDelayTop));
	ui.editCleanDurationBottom->setText(QString("%1").arg(cleanDurationBottom));
	ui.editCleanDurationTop->setText(QString("%1").arg(cleanDurationTop));

	ui.vibratorEnable->setChecked(elevatorVibratorEnable);

	ui.editNrPieces->setText(QString("%1").arg(pieceInBox));
	ui.stopSysemEdit->setText(QString("%1").arg(autostopTime));

	if(workingMode == 0)
	{
		ui.editNrPieces->setEnabled(false);
		ui.stopSysemEdit->setEnabled(false);
		ui.radioButtonNonStopMode->setChecked(true);
	}
	else if (workingMode == 1)
	{
		ui.editNrPieces->setEnabled(true);
		ui.stopSysemEdit->setEnabled(false);
		ui.radioButtonAutoStopMode->setChecked(true);
	}
	else
	{
		ui.editNrPieces->setEnabled(true);
		ui.stopSysemEdit->setEnabled(true);
		ui.radioButtonDelayStopMode->setChecked(true);
	}

	ui.outputEnable->setChecked(outSignalEnable);
	ui.outputSignalDuration->setText(QString("%1").arg(outSignalDuration));
	ui.autoStartInputEnable->setChecked(inputSignalEnable);

}

void GeneralSettings::OnConfirm()
{

	//lamele
	disToValveS0 = ui.distanceToValveS0->displayText().toFloat();
	disToValveS1 = ui.distanceToValveS1->displayText().toFloat();
	valveDurationS0 = ui.valveDurS0->displayText().toInt();
	valveDurationS1 = ui.valveDurS1->displayText().toInt();
	valve2DelayS0 = ui.valve2delayS0->displayText().toInt();
	valve2DelayS1 = ui.valve2delayS1->displayText().toInt();
	valve2DurationS0 = ui.valve2DruS0->displayText().toInt();
	valve2DurationS1 = ui.valve2DruS1->displayText().toInt();

	cleanEnable = ui.AutoCleaningEnable->isChecked();
	cleanPeriod = ui.editCleanPeriod->displayText().toInt();
	cleanDurationBottom = ui.editCleanDurationBottom->displayText().toInt();
	cleanDelayBottom = ui.editCleanDelayBottom->displayText().toInt();
	cleanDurationTop = ui.editCleanDurationTop->displayText().toInt();
	cleanDelayTop = ui.editCleanDelayTop->displayText().toInt();

	testIOMode = ui.testMode->isChecked();
	elevatorVibratorEnable = ui.vibratorEnable->isChecked();

	
	pieceInBox = ui.editNrPieces->displayText().toInt();
	autostopTime = ui.stopSysemEdit->displayText().toInt();

	if (ui.radioButtonNonStopMode->isChecked() == true)
	{
		workingMode = 0;
	}
	else if (ui.radioButtonAutoStopMode->isChecked() == true)
	{
		workingMode = 1;
	}else
		workingMode = 2;



	outSignalEnable = ui.outputEnable->isChecked();

	outSignalDuration = ui.outputSignalDuration->displayText().toInt();

	inputSignalEnable = ui.autoStartInputEnable->isChecked();



	SaveSettings();
	closeEvent(NULL);
}

void GeneralSettings::OnRejected()
{
	closeEvent(NULL);
}

void GeneralSettings::OnClickedNonStop()
{

	ui.editNrPieces->setEnabled(false);
	ui.stopSysemEdit->setEnabled(false);
	
}

void GeneralSettings::OnClickedStopMode()
{

	ui.editNrPieces->setEnabled(true);
	ui.stopSysemEdit->setEnabled(false);
}

void GeneralSettings::OnClickedStopDelayMode()
{
	ui.editNrPieces->setEnabled(true);
	ui.stopSysemEdit->setEnabled(true);
}
