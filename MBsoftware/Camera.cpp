#include "stdafx.h"
#include "Camera.h"

int CCamera::objectCounter = 0;
int CCamera::cameraOpened = 0;
bool CCamera::libraryInitialized = false;

CCamera::CCamera(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	
	frameCounter = 0;
	prevFrameCounter = 0;
	isLive = 0;
	isLiveOld = 0;
	cameraName = "";
	videoFormat = "";
	vendorName = "";
	FPS = 0;
	rotatedVertical = true;
	rotadedHorizontal = false;
	height = 0;
	width = 0;
	depth = 1;
//	formatType = CV_8UC1;
	widthDisp = 0;
	heightDisp = 0;
	xOffsetDisp = 0;
	yOffsetDisp = 0;
	activeImage = 0;
	isFrameReady = false;
	listener = false;
	trigger = false;
	lockImage = false;
	noFrameCounter = 0;
	grabImage = false;
	imageIndex = 0;
	maxGrabbedImages = 0;
	realTimeProcessing = false;
	grabberInitialized = false;
	triggerTest = false;
	grabSucceeded = true;
	triggerCounter = 0;
	defaultExposure = 0.0001;
	defaultGain = 50;
	id = objectCounter;
	triggerOutput = 0;
	triggIndex = 0;
	objectCounter++;
	currentExposure = 0;
	currentGain = 0;
	minGain = 0;
	maxGain = 0;
	minExpo = 0;
	maxExpo = 0;
	enableLive = false;
	isSceneAdded = false;
	zoomFactor = 1;
	frameReadyCounter = 0;
	testCounter = 0;
//	conversionCode = CV_GRAY2RGB;
	num_images = 1; //po defaultu rezervira vsaj eno sliko

	for (int i = 0; i < num_buff; i++)
	{
		//triggerTimer[i].elapsed = -1; //na zacetku so vsi negativni, da se pri triganju loci ze prispele
		encoderValues[i] = 0;
	}

	ClearLockingTable();

	setWindowTitle( QString("camera:%1").arg(id));
	ui.imageView->setCursor(Qt::CrossCursor);

	connect(ui.buttonLive, SIGNAL(clicked()), this, SLOT(ShowLive()));
	connect(this, SIGNAL(showImageSignal(int)), this,SLOT(Live(int)));

	connect(ui.buttonZoomIn, SIGNAL(clicked()), this, SLOT(ZoomIn()));
	connect(ui.buttonZoomOut, SIGNAL(clicked()), this, SLOT(ZoomOut()));
	connect(ui.buttonReset, SIGNAL(clicked()), this, SLOT(ResetZoom()));
	connect(ui.buttonSave, SIGNAL(clicked()), this, SLOT(SaveSettings()));
	connect(ui.buttonSaveImage, SIGNAL(clicked()), this, SLOT(OnSaveImage()));

	ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor * 100));

}

CCamera::CCamera(const CCamera& camera)// Copy constructor
{
	frameCounter = camera.frameCounter;
	prevFrameCounter = camera.prevFrameCounter;
	isLive = camera.isLive;
	isLiveOld = camera.isLiveOld;
	cameraName = camera.cameraName;
	videoFormat = camera.videoFormat;
	FPS = camera.FPS;
	rotatedVertical = camera.rotatedVertical;
	rotadedHorizontal = camera.rotadedHorizontal;
	height = camera.height;
	width = camera.width;
	depth = camera.depth;
	activeImage = camera.activeImage;
	vendorName = camera.vendorName;
	isFrameReady = camera.isFrameReady;
	listener = camera.listener;
	trigger = camera.trigger;
	lockImage = camera.lockImage;
	noFrameCounter = camera.noFrameCounter;
	grabImage = camera.grabImage;
	imageIndex = camera.imageIndex;
	maxGrabbedImages = camera.maxGrabbedImages;
	id = objectCounter;
	triggerCounter = camera.triggerCounter;
	realTimeProcessing = camera.realTimeProcessing;
	triggerOutput = camera.triggerOutput;
	widthDisp = camera.widthDisp;
	heightDisp = camera.heightDisp;
	enableLive = camera.enableLive;
	isSceneAdded = camera.isSceneAdded;
	conversionCode = camera.conversionCode;
	zoomFactor = camera.zoomFactor;
	objectCounter++;
}


CCamera::~CCamera()
{

	image.erase(image.begin(), image.end());
	imageReady.erase(imageReady.begin(), imageReady.end());
	disconnect(this, 0, 0, 0);
	close();
}

void CCamera::closeEvent(QCloseEvent *event)
{
	if (enableLive == true)
	{
		ui.buttonLive->setText("Enable Live");
		ui.buttonLive->setStyleSheet("background: rgb(76, 255, 11);");
		DisableLive();
	}

}




void CCamera::ShowLive()
{

	if (enableLive == true)
	{
		ui.buttonLive->setText("Enable Live");
		ui.buttonLive->setStyleSheet("background: rgb(76, 255, 11);");
		DisableLive();
	}
	else
	{
		ui.buttonLive->setText("Disable Live");
		ui.buttonLive->setStyleSheet("background: rgb(255, 59, 33)");
		EnableLive();
	
	}

}

void CCamera::Live()
{
	if (this->isVisible())
		ShowImages();
}

void CCamera::Live(int imageIndex)
{
	if (this->isVisible())
		ShowImages(imageIndex);
}

void CCamera::ExposureChanged(double value)
{
	
	if (currentExposure != value)
	{
		SetExposureAbsolute(value);
	}

}

void CCamera::GainChanged(int value)
{
	int value1 = value;
	if (currentGain != value)
	{
		SetGainAbsolute(value);
	}
}

void CCamera::XoffsetChanged(int value)
{
	
	int partValue = value / 4;
	int cur2Value = partValue * 4;
	if (offsetX != value)
	{
		//if (currValue % 4 == 0)
		//ui.SpinOffsetX->setValue(cur2Value);
		//ui.sliderOffsetX->setValue(cur2Value);
		SetPartialOffsetX(value);
	}

}

void CCamera::YoffsetChanged(int value)
{
	int partValue = value / 4;
	int cur2Value = partValue * 4;
	//if (currValue % 4 == 0)
	if (offsetY != value)
	{
		//ui.spinOffsetY->setValue(cur2Value);
		//ui.sliderOffsetY->setValue(cur2Value);
		SetPartialOffsetY(value);
	}

}

void CCamera::SaveSettings()
{

	SaveReferenceSettings();
}

void CCamera::OnSaveImage()
{
//image.fill(Qt::red); // A red rectangle.
QString fileName = QFileDialog::getSaveFileName(this, tr("Save Image "),
	QString(),
	tr("Images (*.bmp)"));
if (!fileName.isEmpty())
{
	String name;

	name = fileName.toStdString();
	imwrite(name,*image[0].buffer);
}



}

void CCamera::SliderExpoChanged(int value)
{
	float newValue;

	newValue = minExpo * exp(value*0.01);


	float bla = 0.04;
	ui.spinExpo->setValue(newValue);
}

void CCamera::SpinExpoChanged(double value)
{
	

	float k = (log(value / minExpo));

	ui.sliderExpo->setValue(ceil(k*100));
}

int CCamera::InitImages(int width, int height, int depth)
{


	for (int i = 0; i < num_images; i++)
	{
		if (image.size() != num_images)
		{
			image.push_back(CMemoryBuffer(width, height, depth));
			
			imageReady.push_back(0);
		}
		else
			break;
	}

	return 0;
}
int CCamera::InitImages()
{
	return InitImages(width, height, depth);
}


void CCamera::CopyImage(int sourceIndex, int destIndex)
{

}

void CCamera::SetDisplay()
{
	ui.sliderExpo->setRange(0, (int)ceil(log(maxExpo/minExpo)*100));
	ui.sliderExpo->setSingleStep(1);
	ui.sliderExpo->setMaximum((int)ceil(log(maxExpo / minExpo) * 100));
	ui.sliderExpo->setInvertedAppearance(false);
	SpinExpoChanged(currentExposure);
	ui.spinExpo->setMaximum(400000000);
	ui.spinExpo->setMinimum(0);
	ui.spinExpo->setSingleStep(1);
	//ui.spinExpo->setPrefix("1/");
	ui.spinExpo->setSuffix("  ms");

	//ui.spinExpo->setSingleStep(100);
	//ui.spinExpo->setMaximum((int)(1.0/minExpo));
	//ui.spinExpo->setMinimum(int(1.0/ maxExpo));
	ui.spinExpo->setValue(currentExposure);





	ui.sliderGain->setMinimum( minGain);
	ui.sliderGain->setMaximum(maxGain);
	ui.sliderGain->setValue(currentGain);
	ui.sliderGain->setSingleStep(1);
	ui.spinGain->setMaximum(maxGain);
	ui.spinGain->setMinimum(minGain);
	ui.spinGain->setSingleStep(1);
	ui.spinGain->setValue((int)currentGain);


	ui.sliderOffsetX->setMinimum(minOffsetX);
	ui.sliderOffsetX->setMaximum(maxOffsetX);
	ui.sliderOffsetX->setTickInterval(4);
	//ui.sliderOffsetX->ste
	ui.sliderOffsetX->setValue(offsetX);
	ui.SpinOffsetX->setMaximum(maxOffsetX);
	ui.SpinOffsetX->setMinimum(minOffsetX);
	ui.SpinOffsetX->setSingleStep(4);
	ui.SpinOffsetX->setValue(offsetX);

	ui.sliderOffsetY->setMinimum(minOffsetY);
	ui.sliderOffsetY->setMaximum(maxOffsetY);
	ui.sliderOffsetY->setValue(offsetY);
	//ui.sliderOffsetY->setSingleStep(4);
	//ui.sliderOffsetY->setTickInterval(4);
	ui.spinOffsetY->setMaximum(maxOffsetY);
	ui.spinOffsetY->setMinimum(minOffsetY);
	ui.spinOffsetY->setSingleStep(4);
	ui.spinOffsetY->setValue(offsetY);


	ui.labelManufacturer->setText(QString("%1 - %2 : %3").arg(vendorName).arg(cameraName).arg(videoFormat));


	connect(ui.spinExpo, SIGNAL(valueChanged(double)), this, SLOT(SpinExpoChanged(double)));
	connect(ui.sliderExpo, SIGNAL(valueChanged(int)), this, SLOT(SliderExpoChanged(int)));
	connect(ui.spinGain, SIGNAL(valueChanged(int)), ui.sliderGain, SLOT(setValue(int)));
	connect(ui.sliderGain, SIGNAL(valueChanged(int)), ui.spinGain, SLOT(setValue(int)));
	connect(ui.SpinOffsetX, SIGNAL(valueChanged(int)), ui.sliderOffsetX, SLOT(setValue(int)));
	connect(ui.sliderOffsetX, SIGNAL(valueChanged(int)), ui.SpinOffsetX, SLOT(setValue(int)));
	connect(ui.spinOffsetY, SIGNAL(valueChanged(int)), ui.sliderOffsetY, SLOT(setValue(int)));
	connect(ui.sliderOffsetY, SIGNAL(valueChanged(int)), ui.spinOffsetY, SLOT(setValue(int)));
	connect(ui.sliderOffsetX, SIGNAL(valueChanged(int)),this, SLOT(XoffsetChanged(int)));
	connect(ui.sliderOffsetY, SIGNAL(valueChanged(int)),this, SLOT(YoffsetChanged(int)));
	connect(ui.spinGain, SIGNAL(valueChanged(int)), this, SLOT(GainChanged(int)));
	connect(ui.spinExpo, SIGNAL(valueChanged(double)), this, SLOT(ExposureChanged(double)));
}


void CCamera::ClearLockingTable()
{
	int i;
	lockCounter = 0;
	for (i = 0; i < num_buff; i++)
		lockedImagesTable[i] = 0;
}

int CCamera::EnableStrobe(int polarity)
{
	return 0;
}

int CCamera::DisableStrobe()
{
	return 0;
}


int CCamera::Init(int selectGrabber, QString cameraName, int VideoFormat, QString VideoFormatS)
{
	return 0;
}

int CCamera::Init(QString serialNumber, QString VideoFormatS)
{
	return 0;
}
int CCamera::Init()
{
	return 0;
}



int CCamera::Start()
{
	return 0;
}
int CCamera::Start(double FPS, bool trigger, bool listener)
{
	return 0;
}
int CCamera::Start(double FPS, bool trigger, bool listener, bool flipV, bool flipH)//imagingSource
{
	return 0;
}

int CCamera::MemoryUnlock(int imageNr)
{
	return 0;
}
void CCamera::MemoryUnlockAll()
{}
int CCamera::MemoryLock()
{
	return 0;
}
bool CCamera::SetWhiteBalanceAbsolute(long red, long green, long blue)
{
	return true;
}

bool CCamera::SetExposureAbsolute(double dExposure)
{
	return false;
}
double CCamera::GetExposureAbsolute()
{
	return 0;
}
bool CCamera::SetGainAbsolute(double dGain)
{
	return false;
}

int CCamera::GetGainAbsolute()
{
	return 0;
}

int CCamera::Init(int selectGrabber, QString cameraName, int width, int height, int depth, int offsetX, int offsetY)
{
	return 0;
}
int CCamera::Init(QString cameraName, int width, int height, int depth, int offsetX, int offsetY)
{
	return 0;
}
int CCamera::Start(double FPS, int trigger)
{
	return 0;
}
int CCamera::Start(double FPS, int trigger, bool flipV, bool flipH)
{
	return 0;
}

void CCamera::AutoGainOnce()
{}
void CCamera::AutoGainContinuous()
{}
void CCamera::AutoExposureOnce()
{}
void CCamera::AutoExposureContinuous()
{}
void CCamera::AutoWhiteBalance()
{}
bool CCamera::IsColorCamera()
{
	return false;
}
int CCamera::GrabImage(int imageNr)
{
	return 0;
}
void CCamera::CloseGrabber(void)
{
}
bool CCamera::OnSettingsImage()
{
	return false;
}
bool CCamera::SaveReferenceSettings()
{
	return false;
}
bool CCamera::LoadReferenceSettings()
{
	return false;
}
bool CCamera::IsDeviceValid()
{
	return false;
}


bool CCamera::EnableLive()
{
	return false;
}

bool CCamera::DisableLive()
{
	return false;
}

void CCamera::ShowDialog(int rights)
{
	if ((rights == 1)||(rights == 2))
	{
		ui.buttonSave->setEnabled(true);
		ui.sliderExpo->setEnabled(true);
		ui.sliderGain->setEnabled(true);
		ui.sliderOffsetX->setEnabled(true);
		ui.sliderOffsetY->setEnabled(true);
		ui.spinExpo->setEnabled(true);
		ui.spinGain->setEnabled(true);
		ui.spinOffsetY->setEnabled(true);
		ui.SpinOffsetX->setEnabled(true);
	}
	else
	{
		ui.buttonSave->setEnabled(false);
		ui.sliderExpo->setEnabled(false);
		ui.sliderGain->setEnabled(false);
		ui.sliderOffsetX->setEnabled(false);
		ui.sliderOffsetY->setEnabled(false);
		ui.spinExpo->setEnabled(false);
		ui.spinGain->setEnabled(false);
		ui.spinOffsetY->setEnabled(false);
		ui.SpinOffsetX->setEnabled(false);
	}
	SetDisplay();
	ResizeDisplayRect();

	ShowImages();
	setWindowState(Qt::WindowActive);
	activateWindow();

	show();

}

void CCamera::ResizeDisplayRect()
{
//	if ((ui.imageView->width() != width) || (ui.imageView->height() != height))
	{

		if ((width != 0) && (height != 0))
		{
			//resize(width + 10, height + 90);

			//ui.imageView->resize(width + 10, height + 10);
			ui.imageView->setGeometry(QRect(0, 0, width, height));
			ui.imageView->setAlignment(Qt::AlignTop | Qt::AlignLeft);
			ui.imageView->setSceneRect(QRectF(0, 0, width, height));


			//za zoom: ui.imageView->fitInView(QRectF(0, 0, image[activeImage].cols+ 10, image[activeImage].rows + 10), Qt::IgnoreAspectRatio);
		}
	}
}

int CCamera::ConvertImageForDisplay(int imageNumber)
{
	
	Mat test;
	if ((image.size() > 0) && (width != 0) && (height != 0))
	{
		if(isLive)
			{
		cv::cvtColor(image[imageNumber].buffer[0], test, conversionCode);
		drawPicture = test(Rect(0, 0,width,height));

		return 1;
			}
	}

	return 0;
}

void CCamera::ShowImages()
{
	if (ConvertImageForDisplay(0))
	{
	QImage qimgOriginal((uchar*)drawPicture.data, drawPicture.cols, drawPicture.rows, drawPicture.step, QImage::Format_RGB888);

		if (!isSceneAdded)
		{
			//DISPLAY
		
			sceneVideo = new QGraphicsScene(this);
			ui.imageView->setScene(sceneVideo);
			
			pixmapVideo = new QGraphicsPixmapItem(QPixmap::fromImage(qimgOriginal));
			//QGraphicsPixmapItem item(QPixmap::fromImage(qimgOriginal));
		//	pixmapVideo = sceneVideo->addPixmap(QPixmap::fromImage(qimgOriginal));
			sceneVideo->addItem(pixmapVideo);
			
			
			sceneVideo->installEventFilter(this);
				
			
			isSceneAdded = true;
		}

		pixmapVideo->setPixmap(QPixmap::fromImage(qimgOriginal));

	}
}

void CCamera::ShowImages(int imageIndex)
{
	if (ConvertImageForDisplay(imageIndex))
	{
		QImage qimgOriginal((uchar*)drawPicture.data, drawPicture.cols, drawPicture.rows, drawPicture.step, QImage::Format_RGB888);

		if (!isSceneAdded)
		{
			//DISPLAY

			sceneVideo = new QGraphicsScene(this);
			ui.imageView->setScene(sceneVideo);

			pixmapVideo = new QGraphicsPixmapItem(QPixmap::fromImage(qimgOriginal));
			//QGraphicsPixmapItem item(QPixmap::fromImage(qimgOriginal));
		//	pixmapVideo = sceneVideo->addPixmap(QPixmap::fromImage(qimgOriginal));
			sceneVideo->addItem(pixmapVideo);


			sceneVideo->installEventFilter(this);


			isSceneAdded = true;
		}

		pixmapVideo->setPixmap(QPixmap::fromImage(qimgOriginal));

	}
}

void CCamera::SaveImage(int imageindex, int id)
{
	/*if (image.size() > imageindex)
	{
		string file = format("../RefImages/%d.bmp", id);
		imwrite(file, image[imageindex]);
	}*/
}


void CCamera::SetPacketSize(int size)
{
}

bool CCamera::SetPartialOffsetX(int xOff)
{
	return false;
}

int CCamera::GetPartialOffsetX()
{
	return 0;
}

bool CCamera::SetPartialOffsetY(int yOff)
{
	return false;
}

int CCamera::GetPartialOffsetY()
{
	return 0;
}

int CCamera::ReadInput()
{
	return 0;
}


void CCamera::Next()
{
	activeImage++;
	
	ShowImages();
}
void CCamera::Previous()
{
	activeImage--;
	if (activeImage < 0)

	ShowImages();

}
void CCamera::ZoomIn()
{
	zoomFactor = zoomFactor + 0.1;

	ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor * 100));
	ui.imageView->scale(1.1, 1.1);
}
void CCamera::ZoomOut()
{
	zoomFactor = zoomFactor - 0.1;
	if (zoomFactor > 0.09)
	{
		ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor * 100));
		//ui.imageView->fitInView(QRectF(0, 0, currImage->buffer[0].cols *zoomFactor, currImage->buffer[0].rows *zoomFactor), Qt::KeepAspectRatioByExpanding);
		ui.imageView->scale(0.9, 0.9);
	}
	else
	{
		zoomFactor = 0.1;
		ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor * 100));
	}

}

void CCamera::ResetZoom()
{
	zoomFactor = 1;
	ui.imageView->resetTransform();
	ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor * 100));

}



bool CCamera::eventFilter(QObject *obj, QEvent *event)
{
	uchar b = 0, g = 0, r = 0, in = 0;
	QPoint point_mouse;
	QGraphicsLineItem * item;
	QPointF position;
	int x, y;

	if (event->type() == QEvent::GraphicsSceneMousePress)
	{
		const QGraphicsSceneMouseEvent* const me = static_cast<const QGraphicsSceneMouseEvent*>(event);
		const QPointF position = me->scenePos();
		int bla = 100;

		bla = 100;
		return true;
	}

	else if (event->type() == QEvent::GraphicsSceneMouseMove)
	{
		const QGraphicsSceneMouseEvent* const me = static_cast<const QGraphicsSceneMouseEvent*>(event);
		position = me->scenePos();

		x = (int)position.x();
		y = (int)position.y();

		if ((x < width) && (y < height) && (x > -1) && (y > -1))
		{
			if (depth == 1)
			{
				in = image[0].buffer[0].data[image[0].buffer[0].channels()*(image[0].buffer[0].cols*y + x)];
				ui.labelin->setText(QString("Int = %1 ").arg(in));
			}
			else
			{
				b = image[0].buffer[0].data[image[0].buffer[0].channels()*(image[0].buffer[0].cols*y + x) + 0];
				g = image[0].buffer[0].data[image[0].buffer[0].channels()*(image[0].buffer[0].cols*y + x) + 1];
				r = image[0].buffer[0].data[image[0].buffer[0].channels()*(image[0].buffer[0].cols*y + x) + 2];
				ui.labelR->setText(QString("R = %1 ").arg(r));
				ui.labelG->setText(QString("G = %1 ").arg(g));
				ui.labelB->setText(QString("B = %1 ").arg(b));
				ui.labelin->setText(QString("Int = %1 ").arg(in));
			}


			ui.labelX->setText(QString("X = %1").arg(x));
			ui.labelY->setText(QString("Y = %1").arg(y));




		}
		return true;

	}
	else
		return false;


	
	return true;
}
void CCamera::keyPressEvent(QKeyEvent * event)
{
	switch (event->key())
	{
	case  Qt::Key_Shift:
		ui.imageView->setDragMode(QGraphicsView::DragMode::ScrollHandDrag);
		break;
	}
}
void CCamera::keyReleaseEvent(QKeyEvent * event)
{
	switch (event->key())
	{
	case  Qt::Key_Shift:
		ui.imageView->setDragMode(QGraphicsView::DragMode::NoDrag);
		ui.imageView->setCursor(Qt::CrossCursor);
		break;
	}
}

