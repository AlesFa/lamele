#include "stdafx.h"
#include "TCP.h"


	std::vector<QTcpSocket*>					TCP::tcpConnections;
	QTimer*										TCP::connectionTimer;

TCP::~TCP()
{
	ui.TextEdit->clear();
}
TCP::TCP(QString ipAddress, int port, bool isServer)
{
	ui.setupUi(this);
	ui.TextEdit->setReadOnly(true);
	ui.TextEdit->setMaximumBlockCount(500);
	clientConnected = false;
	address = ipAddress;
	dataBufCounter = 0;
	this->port = port;
	numOfConnections = 0; 
	waitTime = 1000; // po pribli�no 6ih sekundah  client neha po�iljati pro�njo za povezavo 
	connectionTimer = new QTimer();
	connectionTimer->setSingleShot(true);


	if (isServer)
	{
		this->isServer = true;
		server = new QTcpServer();
		StartServer();
	}
	else
	{
		this->isServer = false;
		client = new QTcpSocket();
		connect(client, SIGNAL(readyRead()), this, SLOT(OnClientReadyRead()));
		connect(client, SIGNAL(connected()), this, SLOT(OnClientConnected()));
		connect(client, SIGNAL(disconnected()), this, SLOT(OnClientDisconnected()));
		connect(ui.buttonSend, SIGNAL(pressed()), this, SLOT(OnClickedSend()));
		connect(connectionTimer, SIGNAL(timeout()), this, SLOT(OnConnectionTimeout()));
		connect(this, SIGNAL(appendText(QString)), this, SLOT(OnAppendText(QString)));
	
		StartClient();
	}

}

void TCP::OnShowDialog()
{
	setWindowState(Qt::WindowActive);
	activateWindow();

		show();
}

void TCP::StartServer()
{

	connect(server, SIGNAL(newConnection()), this, SLOT(onNewConnection()));
	connect(server, SIGNAL(readyRead()), this, SLOT(OnServerReadyRead()));
	//Pretvorimo naslov v ustrezen format in �akamo na povezavo
	addr.setAddress(address);
	server->listen(addr, port);
	//Za�nemo od�tevati �as do povezave
	//connectionTimer->start(waitTime);
}

void TCP::StartClient()
{

	QString text(QString("CONNECTING TO SERVER: IP: %1 , Port: %2").arg(address).arg(port));
	//ui.TextEdit->appendPlainText(text);
	emit appendText(text);
	client->connectToHost(address, port);
	connectionTimer->start(waitTime);
}

void TCP::WriteData(QByteArray text)
{
	QByteArray sendData = text;
	QByteArray newText = text;
	newText.prepend("Sent:");
	QByteArrayMatcher carrageReturn;
	carrageReturn.setPattern("\r\n");
	

	if (carrageReturn.indexIn(sendData, 0) == -1)
	{
		sendData.append("\r\n");

	}
	else
	{
	newText.chop(2);
	}
	emit appendText(newText);
	//ui.TextEdit->appendPlainText(newText); //samo za prikaz v konzolo

	char param[100];

	if (isServer)
	{

	}
	else
	{
	if	(clientConnected == true)
		{
		client->write(sendData);
		client->flush();
	

		}
	}
}

void TCP::Disconnect()
{
	if (isServer)
	{

	}
	else
	{
		client->disconnectFromHost();
		client->close();
	}
}

void TCP::OnNewConnection()
{
	//Shranimo novo povezavo
	QTcpSocket *socket = server->nextPendingConnection();
	tcpConnections.push_back(socket);
	numOfConnections++;
	connect(tcpConnections.back(), SIGNAL(readyRead()), this, SLOT(OnServerReadyRead()));

	QHostAddress IP;
	QString ipString;
	IP = socket->peerAddress();
	ipString = IP.toString();

	//Nehamo poslu�ati in ustavimo �tevec
	server->close();
	connectionTimer->stop();

	//Potrdimo povezavo odjemalcu
	tcpConnections.back()->write("Connection established");
	tcpConnections.back()->flush();
}

void TCP::OnConnectionTimeout()
{
	//Ob izteku �asa za povezavo nehamo poslu�ati s stre�nikom in po�ljemo primeren signal
	//server->close();
	//emit(connectionTimeout());
	int state = -1;
	if (isServer)
	{

	}
	else
	{
		state = client->state();
		if (state == 2)//se se povezuje ponovno preverimo cez en timer
			connectionTimer->start();
		else if (state == 0)
		{
			client->disconnectFromHost();
			StartClient();
			connectionTimer->start();
		}
		else if (state == 3)//connected to host
		{

		}
			
	}
}

void TCP::OnServerReadyRead()
{
	//Preberemo podatke od odjemalca
	recievedData = tcpConnections[0]->readAll();
	//ui.TextEdit->appendPlainText(recievedData);
	emit appendText(recievedData);
	//emit(dataReady());
}

void TCP::OnClientReadyRead()
{
	//QString msg = client->readAll();
	char message[REQUEST_SIZE];
	int statusCode = 0;
	bool split = false;
	QByteArray tmpArray;
	QByteArray tmpArray2;
	QByteArray tmpArray3;
	int parseCounter = 0;
	QByteArray msg;
	

	if (clientConnected == true) 		// still connected ?
	{
		statusCode = client->read(message, REQUEST_SIZE);
		msg = QByteArray(message, statusCode);
		msg.chop(2);
		msg.prepend("Receive:");
		//ui.TextEdit->appendPlainText(msg);
		emit appendText(msg);
		if (statusCode < 0)
		{
			int bla = 100;
		}
		else
		{
			for (int i = 0; i < statusCode; i++)
			{
				dataBuf[i] = message[i];

			}
			for (int i = 0; i < statusCode; i++)
			{
				dataBuf[dataBufCounter] = message[i];

				if ((dataBuf[dataBufCounter - 1] == 13) && (dataBuf[dataBufCounter] == 10))//koda je napisana za projekt palcke LITVA
				{
					tmpArray.clear();
					tmpArray2.clear();
					tmpArray3.clear();
					split = false;
					for (int j = 0; j < dataBufCounter; j++)
					{
						tmpArray.append(dataBuf[j]);
						if (j > 0)
						{
							if (split == true)
							{
								tmpArray3.append(tmpArray[j]);
							}
							else
							{
								tmpArray2.append(tmpArray[j]);
							}
							if (tmpArray[j] == '-')
								split = true;
						}
					}

					dataBufCounter = 0;
					split = false;
					if (parseCounter > REQUEST_SIZE)
						parseCounter = 0;
					//intBarvaArray[parseCounter] = integer;
					returnCharArray[parseCounter] = tmpArray[0];
					returnIntArray[parseCounter] = tmpArray2.toInt();
					//intBarvaSecundArray[parseCounter] = atoi(secInt);
					parseCounter++;
				}
				else
				{
					dataBufCounter++;
				}
			}
		}

		emit dataReady(parseCounter);
	}


}

void TCP::OnClientConnected()
{
	clientConnected = true;
	//ui.TextEdit->appendPlainText("------------------------------CONNECTED TO SERVER------------------------------");
	//ui.TextEdit->appendPlainText("------------------------------CONNECTED TO SERVER------------------------------");
	emit appendText("------------------------------CONNECTED TO SERVER------------------------------");
}

void TCP::OnClientDisconnected()
{
	clientConnected = false;
	//ui.TextEdit->appendPlainText("------------------------------CONNECTION  LOST------------------------------");
	emit appendText("------------------------------CONNECTION  LOST------------------------------");
	connectionTimer->start();//poskus ponovne povezave 
}

void TCP::OnClickedSend()
{
	QString sendText;
	QByteArray sendArray;
	
	 sendText = ui.lineSend->text();

	 sendArray = sendText.toLocal8Bit();

	 ui.lineSend->clear();
	 if (sendArray.size() > 0)
		 WriteData(sendArray);
}

void TCP::OnAppendText(QString text)
{
	ui.TextEdit->appendPlainText(text);
}







