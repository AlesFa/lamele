#include "stdafx.h"
#include "CProperty.h"


CProperty::CProperty()
	
{
	propType = -1;
	propName = "";
	boolValue = false;
	intValue = 0;
	intMaxValue = 32767;
	intMinValue = -32767;
	doubleMaxValue = HUGE_VAL;
	doubleValue = 0;
	doubleMinValue = -HUGE_VAL;
	rectValue = QRect(0, 0, 0, 0);
	

}

CProperty::~CProperty()
{
}

