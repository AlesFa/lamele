﻿#include "stdafx.h"
#include "MBsoftware.h"




//include za media timer ( multimedia timer )
#include<mmsystem.h>
#pragma comment(lib, "winmm.lib")

MBsoftware*									MBsoftware::glob;
std::vector<CCamera*>						MBsoftware::cam;
std::vector<CMemoryBuffer*>					MBsoftware::images;
std::vector<Serial*>						MBsoftware::serial;
std::vector<SMCdrive*>						MBsoftware::smc;
std::vector<ControlCardMB*>					MBsoftware::controlCardMB;
std::vector<Pcie*>							MBsoftware::pcieDio;
std::vector<Types*>							MBsoftware::types[NR_STATIONS];
std::vector<ControlLPT*>					MBsoftware::lpt;
std::vector<TCP*>							MBsoftware::tcp;
History*									MBsoftware::history;
std::vector<StatusBox*>						MBsoftware::statusBox;
std::vector<LogWindow*>						MBsoftware::log;
Timer										MBsoftware::MMTimer;
Timer										MBsoftware::viewTimerTimer;
Timer										MBsoftware::viewTimerTimer2;
std::vector<Timer*>							MBsoftware::TestFun;
int											MBsoftware::mmCounter;
int											MBsoftware::viewCounter;
imageProcessing*							MBsoftware::imageProcess;
LoginWindow*								MBsoftware::login;
AboutUS*										MBsoftware::about;
int counter = 0;
int											MBsoftware::currentType = 0;
int											MBsoftware::prevType = 0;
int											MBsoftware::obdelanaStevec = 0;

Measurand									MBsoftware::measureObject; //object with mesurand parameters
GeneralSettings*							MBsoftware::settings; //generalSettings


QTcpSocket*									MBsoftware::client;
QFuture<int>								MBsoftware::future[4];

vector<int>						MBsoftware::clearLogIndex;
vector<int>						MBsoftware::logIndex;
vector<QString>						MBsoftware::logText;
QReadWriteLock					MBsoftware::lockReadWrite;
//QSerialPort*					serial;
MMRESULT				FTimerID;
int								MBsoftware::teachMode[NR_STATIONS];
// spremenljivke za trenutni projekt
int sprejetaCounter[2] = { 0,0 };
int obdelanaCounter[2] = { 0,0 };
//cakalnaVrsta
int cakalnaVrstaDolzina[2] = { -1,-1 };
int cakalnaVrstaMaxDolzina[2] = { 20,20 };
int cakalnaVrstaLastIn[2] = { -1,-1 };
int cakalnaVrstaFirstOut[2] = { -1,-1 };
int cakalnaVrstaNrImage[2][20];
int cakalnaVrstaNrCam[2][20];


int writeCountersTimer = 0;

int updateImage[NR_STATIONS] = { 0,0 };
int lamelaIndex[NR_STATIONS] = { -1,-1 };

int watchDog = 0;
int timeStemp[NR_STATIONS][0xfff];
int timeStampIndex = 0;


int refPieceMeasured =0;
int refPieceStation;
bool refDataOk = false;
int processStatus = 0;
int savedProcessStatus = 0; //external signal pause
int startKey = 0;
int triggingcamera = 0;
int timeToCleanCounter = 0;

int delayCleanConterBottom = 0;
int delayCleanConterTop = 0;
int cleanCounterTop = 0;
int cleanCounterbottom = 0;
bool cleanTopFinished = false;
bool cleanBottomFinished = false;
int elevatorOnCounter = 0;

int AutoStopCounter = 0;
int cameraErrorIsSet[2] = { 0,0 };
int prevTestMode = 0;
int vibratorStatus = 0;
int elevatorStatus = 0;
int emgMode = 0;
CPointFloat	obrobaLamele[NR_STATIONS][POMNILNIK_LAMEL][360];
int	obrobaOK[NR_STATIONS][POMNILNIK_LAMEL][360];
int	obrobaZgorajOK[NR_STATIONS][POMNILNIK_LAMEL][360];
int	obrobaSpodajOK[NR_STATIONS][POMNILNIK_LAMEL][360];
CPointFloat tockeZgorajDraw[NR_STATIONS][POMNILNIK_LAMEL][360];
CPointFloat tockeSpodajDraw[NR_STATIONS][POMNILNIK_LAMEL][360];
int pieceCounter = 0;
int prevPieceCounter = 0;
int taktCounter = 0;
int taktGoodCounter = 0;

//graf QBARCHART
#define CHART_DATA_SIZE 100
#define GAUSS_SIZE 20
float dataArray[NR_STATIONS][11][CHART_DATA_SIZE];
int charDataCounter[NR_STATIONS] = { 0,0 };
float minValueChart[NR_STATIONS];
float maxValueChart[NR_STATIONS];
int chartParam[2];
int chartParamPrev[2];

int countTest = 0;
float value = 50;

int noDowelsCounter = 0;


int valveOpenS0 = 0;
int valveDurationCounterS0 = 0;
int valveOpenS1 = 0;
int valveDurationCounterS1 = 0;

int goodValveCounter = 0;

int redLight = 0;
int greenLight = 0;
int blinking = 0;

bool resetBoxCounter = false;

int pieceInBox = 0;//koliko kosov je v skatli
int pauseCounter =  0; 
bool typeChanged = 0;

bool outputSig = false;
int outputSigCounter = 0;

int testValveDuration = 0;
int testValveTop;
int testValveBottom;
QString logPath;
QFile *m_logFile;


int  oneSideStuck = 0;
int st0PieceOn = 0;
int st1PieceOn = 0;
int station0StuckCounter = 0;
int station1StuckCounter = 0;

//0 ->OFF
//1->ON
//2->BLINKING

void MBsoftware::closeEvent(QCloseEvent *event)
{

	disconnect(this, 0, 0, 0);

	//first you have to close all timers
	timeKillEvent(FTimerID);
	timeEndPeriod(1);
	OnTimer->stop();
	OnTimer2->stop();

	
	for (int i = 0; i < cam.size(); i++)
	{
		delete (cam[i]);
	}
	
	for (int i = 0; i < images.size(); i++)
	{
		delete (images[i]);
	}
	for (int i = 0; i < TestFun.size(); i++)
	{
		delete (TestFun[i]);
	}
	//lpt.clear();
		for (int i = 0; i < lpt.size(); i++)
		{
			lpt[i]->SetControlByteAllOFF();
			lpt[i]->SetDataByte(0x00);
			lpt[i]->close();

			//delete (lpt[i]);
		}
		lpt.clear();

		for (int i = 0; i < controlCardMB.size(); i++)
		{
			controlCardMB[i]->StopTimer(0);
			controlCardMB[i]->StopTimer(2);
			controlCardMB[i]->StopTimer(3);
			controlCardMB[i]->StopTimer(1);
			controlCardMB[i]->SetOutputByte(0, 0x00);
			controlCardMB[i]->SetOutputByte(1, 0x00);

			delete (controlCardMB[i]);
		}
		controlCardMB.clear();

		for (int i = 0; i < serial.size(); i++)
		{
			delete (serial[i]);
		}
		serial.clear();

		for (int i = 0; i < statusBox.size(); i++)
		{
			delete (statusBox[i]);
		}
		for (int i = 0; i < smc.size(); i++)
		{
			delete (smc[i]);
		}
		for (int i = 0; i < pcieDio.size(); i++)
		{
			delete (pcieDio[i]);
		}
		for (int i = 0; i < tcp.size(); i++)
		{
			delete (tcp[i]);
		}

		for (int i = 0; i < 2; i++) //izbrisemo kar je bilo prikazano na zaslonu 
		{
			qDeleteAll(showScene[i]->items());
			delete showScene[i];
		}
		//showScene.clear();
		for (int i = 0; i < NR_STATIONS; i++)
		{
			delete parametersTable[i];
			for (int j = 0; j < types[i].size(); j++)
			{
				delete types[i][j];
				
			}
		}
		//log[0]->AppendMessage("Application STOP.");

		for (int i = 0; i < log.size(); i++)
		{
			delete log[i];
		}
		log.clear();

		AddLog("Application stoped", 3);
		logText.clear();
		logIndex.clear();
		delete history;
		delete imageProcess;
		delete login;
		delete settings;
		delete about;
		
		

}

MBsoftware::MBsoftware(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	glob = this;

	
	teachMode[0] = 0;
	teachMode[1] = 0;
	QThread::currentThread()->setPriority(QThread::HighPriority);
	SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);

	//tcp.push_back(new TCP("172.16.10.2", 2000, false));
	//connect(tcp[0], SIGNAL(dataReady(int)), this, SLOT(OnTcpDataReady(int)));
	
	lpt.push_back(new ControlLPT(0x378));
	lpt[0]->SetControlByteAllOFF();

	lpt.push_back(new ControlLPT(0x4FF8));
	lpt[1]->SetControlByteAllOFF();


	ReadTypes();
	LoadVariables();
	
	about = new AboutUS();
	login = new LoginWindow();
	settings = new GeneralSettings();
	statusBox.push_back(new StatusBox);
	TestFun.push_back(new Timer());
	TestFun.push_back(new Timer());
	TestFun.push_back(new Timer());

#if defined _DEBUG
	login->currentRights = 1;
	login->currentUser = "Administrator";
#else
	login->currentRights = 0;
	login->currentUser = "";
#endif

	//log.push_back(new LogWindow());
	if (!QDir("C:/log/").exists())
		QDir().mkdir("C:/log");
	logPath = "C:/log/workingLog_" + QDateTime::currentDateTime().toString("dd_MM_yyyy") + ".txt";
	m_logFile = new QFile();
	m_logFile->setFileName(logPath);
	m_logFile->open(QIODevice::Append | QIODevice::Text);

	AddLog("Application STARTED!", 3);
	CreateChart();
	isSceneAdded = false;


	int visina = 540;
	int sirina = 720;
	int numImages = 20;
	cam.push_back(new ImagingSource());
	cam.back()->serialNumber = "44910028";
	cam.back()->width = sirina;
	cam.back()->height = visina;

	cam.back()->depth = 1;
	//cam.back()->offsetX = 32;
	//cam.back()->offsetY = 340;
	cam.back()->FPS = 200;
	cam.back()->trigger = true;
	cam.back()->realTimeProcessing = true; //true -> kliče  v MBCameraView
	cam.back()->triggerOutput = 1;
	cam.back()->rotadedHorizontal = true;
	cam.back()->rotatedVertical = false;
	cam.back()->num_images = numImages;
	cam.back()->LoadReferenceSettings();
	QString videoFormat = QString("Y800 (%1x%2)").arg(cam.back()->width).arg(cam.back()->height);

	if (cam.back()->Init(cam.back()->serialNumber, videoFormat) == 1)
	{
		if (cam.back()->Start())
		{
			cam.back()->SetExposureAbsolute(cam.back()->exposure[0]);
			cam.back()->SetGainAbsolute(cam.back()->gain[0]);
			cam.back()->SetPartialOffsetX(cam.back()->savedOffsetX[0]);
			cam.back()->SetPartialOffsetY(cam.back()->savedOffsetY[0]);
			cam.back()->EnableStrobe(0);
		}
	}
	if (!cam.back()->isLive) //ce smo v testnem nacinu in kamera nif odprta, inicializiramo vse slike
		cam.back()->InitImages(cam.back()->width, cam.back()->height, cam.back()->depth);

	//kamera 1
	cam.push_back(new ImagingSource());
	cam.back()->serialNumber = "20910612";
	cam.back()->width = sirina;
	cam.back()->height = visina;

	cam.back()->depth = 1;
	//cam.back()->offsetX = 32;
	//cam.back()->offsetY = 340;
	cam.back()->FPS = 200;
	cam.back()->trigger = true;
	cam.back()->realTimeProcessing = true; //true -> kliče  v MBCameraView
	cam.back()->triggerOutput = 1;
	cam.back()->rotadedHorizontal = true;
	cam.back()->rotatedVertical = true;
	cam.back()->num_images = numImages;
	cam.back()->LoadReferenceSettings();
	videoFormat = QString("Y800 (%1x%2)").arg(cam.back()->width).arg(cam.back()->height);

	if (cam.back()->Init(cam.back()->serialNumber, videoFormat) == 1)
	{
		if (cam.back()->Start())
		{
			cam.back()->SetExposureAbsolute(cam.back()->exposure[0]);
			cam.back()->SetGainAbsolute(cam.back()->gain[0]);
			cam.back()->SetPartialOffsetX(cam.back()->savedOffsetX[0]);
			cam.back()->SetPartialOffsetY(cam.back()->savedOffsetY[0]);
			cam.back()->EnableStrobe(0);
		}
	}
	if (!cam.back()->isLive) //ce smo v testnem nacinu in kamera ni odprta, inicializiramo vse slike
		cam.back()->InitImages(cam.back()->width, cam.back()->height, cam.back()->depth);
	QString tmp;
	for (int i = 0; i < 18; i++)
	{
		tmp = QString("C:\\images\\%1.bmp").arg(i);
		cam[1]->image[i].LoadBuffer(tmp);
		cam[0]->image[i].LoadBuffer(tmp);
		//cam[0]->imageReady[i] = 1;
	}
	

	
	for (int i = 0; i < 1000; i++)
	{
		//tmp = QString("C:\\images\\eti\\%1.bmp").arg(i);
		images.push_back(new CMemoryBuffer(sirina, visina, 1));
		//images[i]->LoadBuffer(tmp);
	}

	


	//images[0]->LoadBuffer(tmp);

	imageProcess = new imageProcessing();
	imageProcess->ConnectImages(cam, images);
	imageProcess->ConnectMeasurand(&measureObject);


	for (int i = 0; i < NR_STATIONS; i++)
	{
		imageProcess->ConnectTypes(i,types[i]);
	}
	imageProcess->LoadTypesProperty();//nalozimo nastavitve funkcij za vse tipe



	//timer za viewDisplay
	OnTimer = new QTimer(this);
	OnTimer2 = new QTimer(this);


	//dinamično dodajanje ikon v tabih
	CreateCameraTab();
	CreateSignalsTab();

	CreateMeasurementsTable();


	//Povezave SIGNAL-SLOT
	connect(OnTimer, SIGNAL(timeout()), this, SLOT(ViewTimer()));
	connect(OnTimer2, SIGNAL(timeout()), this, SLOT(ViewTimer2()));
	OnTimer->start(100);
	OnTimer2->start(100);

	for (int i = 0; i < cam.size(); i++)
	{
		connect(cam[i], SIGNAL(frameReadySignal(int, int)), this, SLOT(OnFrameReady(int, int)));
	}

	connect(imageProcess, SIGNAL(imagesReady()), this, SLOT(ProcessImages()));
	connect(ui.buttonImageProcessing, SIGNAL(pressed()), this, SLOT(OnClickedShowImageProcessing()));
	connect(ui.buttonTypeSelect, SIGNAL(pressed()), this, SLOT(OnClickedSelectType()));
	connect(ui.buttonSetTolerance, SIGNAL(pressed()), this, SLOT(OnClickedEditTypes()));
	connect(ui.buttonAddType, SIGNAL(pressed()), this, SLOT(OnClickedAddTypes()));
	//connect(ui.buttonTeachType, SIGNAL(pressed()), this, SLOT(OnClickedTeachType()));
	connect(ui.buttonRemoveType, SIGNAL(pressed()), this, SLOT(OnClickedRemoveTypes()));
	connect(ui.buttonResetCounters, SIGNAL(pressed()), this, SLOT(OnClickedResetCounters()));
	connect(ui.buttonLogin, SIGNAL(pressed()), this, SLOT(OnClickedLogin()));
	connect(ui.buttonStatusBox, SIGNAL(pressed()), this, SLOT(OnClickedStatusBox()));
	connect(ui.buttonHistory, SIGNAL(pressed()), this, SLOT(OnClickedShowHistory()));
	connect(ui.buttonGeneralSettings, SIGNAL(pressed()), this, SLOT(OnClickedGeneralSettings()));
	connect(ui.buttonResetGoodBox, SIGNAL(pressed()), this, SLOT(OnClickedResetGoodBox()));
	connect(ui.buttonAboutUS, SIGNAL(pressed()), this, SLOT(OnClickedAboutUs()));
	
	
	
	connect(this, SIGNAL(saveRef(int, int)), this, SLOT(RefPieceMeasured(int,int )));


	//connect(ui.buttonSMCdialog, SIGNAL(pressed()), this, SLOT(OnClickedSmcButton()));
	////		MULTIMEDIA	TIMER		////
	UINT uDelay = 1;				// 1 = 1000Hz, 10 = 100Hz
	UINT uResolution = 1;
	DWORD dwUser = NULL;
	UINT fuEvent = TIME_PERIODIC;

	// timeBeginPeriod(1);
	FTimerID = timeSetEvent(uDelay, uResolution, OnMultimediaTimer, dwUser, fuEvent);


	DrawImageOnScreen();


	history = new History();
	SetHistory();
	connect(history, SIGNAL(callFromHistory(int , int)), this, SLOT(HistoryCall(int ,int)));

	for (int i = 0; i < 12; i++)
	{
		ui.listWidgetStatus->addItem("");
	}
	for (int j = 0; j < NR_STATIONS; j++)
	{
		for (int i = 0; i < 0xfff; i++)
		{
			timeStemp[j][i] = -1;
		}
	}
	timeStampIndex = 0;


	
	//UpdateChart(0,chartParam[0], 0);
	//UpdateChart(1,chartParam[1], 0);
	//ui.frameDeviceStatus->setFocus(Qt::NoFocus);
	//ui.frameDeviceStatus->setFocusPolicy(Qt::NoFocus);
	//setAttribute(Qt::WA_ShowWithoutActivating);
	grabKeyboard();
	//activateWindow();
	//SetFocus(FocusPolicyStrongFocus);

}

void MBsoftware::OnFrameReady(int camera, int imageIndex)
{
	//if(!cam[camera]->enableLive)
//QtConcurrent::run(imageProcess, &imageProcessing::ProcessingCamera1, camera, imageIndex, 0);

}

void MBsoftware::OnTcpDataReady(int parse)
{

}

void MBsoftware::HistoryCall(int station, int currentPiece)
{

	if(currentPiece >= 0 )
	*cam[station]->image[0].buffer = history->cam0Image[station][currentPiece].clone();

	//cam[NrCam]->image[imageIndex].buffer[0].clone();
}






void MBsoftware::MeasurePiece(int index)
{


	//measureObject.isProcessed[index] = 1;


	


	
}

void MBsoftware::MeasurePiece(int station, int index)
{
	int curStation = station;
	float speed = 0;
	int xSum = 0;
	int xSumCounter = 0;
	int delta = 0;
	float korFaktor = types[station][currentType]->correctFactor[0];
	float timeToValve = 0;
	CPointFloat lastMeasuredCenter;
	int currTime;
	int angle;
	int newIndex;
	QMessageBox::StandardButton reply;
	vector <float>	odstopanjaOdKroga;
	float distanceFromCenter[360];
	float odstopanja[360];
	int isGood = 0;
	int valveDelay = 5;
	float distanceToValve[2];
	float avrValue = 0;
	float avrCounter = 0;
	distanceToValve[0] = settings->disToValveS0;
	distanceToValve[1] = settings->disToValveS1;
	float valveDuration[2];
	valveDuration[0] = settings->valveDurationS0;
	valveDuration[1] = settings->valveDurationS1;
	float valve2Duration[2];
	valve2Duration[0] = settings->valve2DurationS0;
	valve2Duration[1] = settings->valve2DurationS1;
	float valve2Delay[2];
	valve2Delay[0] = settings->valve2DelayS0;
	valve2Delay[1] = settings->valve2DelayS1;
	lamelaIndex[station] = index;
	int badOffset = 150;
	int startTest;
	int stopTest;


	types[station][currentType]->SetMeasuredValue(measureObject.dolzina[station][index], 0);
	types[station][currentType]->SetMeasuredValue(measureObject.sirina[station][index], 1);
	types[station][currentType]->SetMeasuredValue(measureObject.visina[station][index], 2);
	types[station][currentType]->SetMeasuredValue(measureObject.visinaOdstopanja[station][index], 3);
	types[station][currentType]->SetMeasuredValue(measureObject.area[station][index], 4);
	types[station][currentType]->SetMeasuredValue(measureObject.krogZgoraj[station][index].radius, 5);
	types[station][currentType]->SetMeasuredValue(measureObject.krogSpodaj[station][index].radius, 6);

	float maxOdstopanje = (types[station][currentType]->nominal[7] + types[station][currentType]->toleranceHigh[7])/ types[station][currentType]->correctFactor[7];
	float minOdstopanja = (types[station][currentType]->nominal[7] + types[station][currentType]->toleranceLow[7])/ types[station][currentType]->correctFactor[7];



	float odstopanje = 0;

	int stZaporednihOdstopanjZg = 0;
	int maxZaporednihOdstopanjZg = 0;

	int stZaporednihOdstopanjSp = 0;
	int maxZaporednihOdstopanjSp = 0;

	int nrOdstopanj = 0;
	float avrOdstopanja = 0;
	float endOdstopanjaZg = 0;
	float endOdstopanjaSp = 0;

	//reset toc za risanje 
	for (int i = 0; i < 360; i++)
	{
		obrobaLamele[station][index][i]  = CPointFloat(0,0);
		obrobaOK[station][index][i] = 0;
		obrobaZgorajOK[station][index][i] = 0;
		obrobaSpodajOK[station][index][i] = 0;
		 tockeZgorajDraw[station][index][i] = CPointFloat(0, 0);
		 tockeSpodajDraw[station][index][i] = CPointFloat(0, 0);
	}
	for (int i = 0; i < measureObject.tockeKrogaZgoraj[station][index].size(); i++)
	{
		if (i < 360)
		{
			odstopanje = measureObject.distanceToCircleZgoraj[station][index][i] - measureObject.krogZgoraj[station][index].radius;
			odstopanjaOdKroga.push_back(odstopanje);
			avrValue += odstopanje;
			avrCounter++;

			if (odstopanje < minOdstopanja)
			{
				obrobaZgorajOK[station][index][i] = 2;
				avrOdstopanja += odstopanje;
				stZaporednihOdstopanjZg++;
			}
			else if (odstopanje > maxOdstopanje)
			{
				obrobaZgorajOK[station][index][i] = 3;
				avrOdstopanja += odstopanje;
				stZaporednihOdstopanjZg++;
			}
			else
			{
				obrobaZgorajOK[station][index][i] = 1;
				stZaporednihOdstopanjZg = 0;
				avrOdstopanja = 0;
			}
			tockeZgorajDraw[station][index][i].SetPointFloat(measureObject.tockeKrogaZgoraj[station][index][i].x + measureObject.xOffsetDraw[station][index], measureObject.tockeKrogaZgoraj[station][index][i].y + measureObject.yOffsetDraw[station][index]);

			if (stZaporednihOdstopanjZg > maxZaporednihOdstopanjZg)
			{
				maxZaporednihOdstopanjZg = stZaporednihOdstopanjZg;
				endOdstopanjaZg = avrOdstopanja / stZaporednihOdstopanjZg;
			}
		}
	}

	avrOdstopanja = 0;
	for (int i = 0; i < measureObject.tockeKrogaSpodaj[station][index].size(); i++)
	{
		if (i < 360)
		{
			odstopanje = measureObject.distanceToCircleSpodaj[station][index][i] - measureObject.krogSpodaj[station][index].radius;
			odstopanjaOdKroga.push_back(odstopanje);

			avrValue += odstopanje;
			avrCounter++;

			if (odstopanje < minOdstopanja)
			{
				obrobaSpodajOK[station][index][i] = 2;
				avrOdstopanja += odstopanje;
				stZaporednihOdstopanjSp++;
			}
			else if (odstopanje > maxOdstopanje)
			{
				obrobaSpodajOK[station][index][i] = 3;
				avrOdstopanja += odstopanje;
				stZaporednihOdstopanjSp++;
			}
			else
			{
				obrobaSpodajOK[station][index][i] = 1;
				stZaporednihOdstopanjSp = 0;
				avrOdstopanja = 0;
			}
			tockeSpodajDraw[station][index][i].SetPointFloat(measureObject.tockeKrogaSpodaj[station][index][i].x + measureObject.xOffsetDraw[station][index], measureObject.tockeKrogaSpodaj[station][index][i].y + measureObject.yOffsetDraw[station][index]);

			if (stZaporednihOdstopanjSp > maxZaporednihOdstopanjSp)
			{
				maxZaporednihOdstopanjSp = stZaporednihOdstopanjSp;
				endOdstopanjaSp = avrOdstopanja / stZaporednihOdstopanjSp;
			}
		}
	}

	if (maxZaporednihOdstopanjSp > maxZaporednihOdstopanjZg)
	{
		types[station][currentType]->SetMeasuredValue(maxZaporednihOdstopanjSp, 8);
		if (types[station][currentType]->IsGood(8) == 0)
		{
			types[station][currentType]->SetMeasuredValue(endOdstopanjaSp, 7);
		}
		else
			types[station][currentType]->SetMeasuredValue(0, 7);

	}
	else
	{
		types[station][currentType]->SetMeasuredValue(maxZaporednihOdstopanjZg, 8);
		if (types[station][currentType]->IsGood(8) == 0)
		{
			types[station][currentType]->SetMeasuredValue(endOdstopanjaZg, 7);
		}
		else
			types[station][currentType]->SetMeasuredValue(0, 7);

	}

	avrValue /= avrCounter;
	float stDev = 0;
	for (int i = 0; i < odstopanjaOdKroga.size(); i++)
	{
		stDev += pow(odstopanjaOdKroga[i] - avrValue,2);
	}
	stDev = sqrt(stDev);
	types[station][currentType]->SetMeasuredValue(stDev, 9);

	if (types[station][currentType]->AllGood() == 1)
	{
		types[station][currentType]->goodMeasurementsCounter++;
		isGood = 1;
	}
	else
	{
		types[station][currentType]->badMeasurementsCounter++;
		isGood = 0;
	}
	measureObject.isGood[station][index] = isGood;


	//izracuna hitrost kosa
	for (int i = 0; i < NR_CENTER; i++)
	{
		if (measureObject.center[station][index][i].x > 0)
		{
			if (i > 0)
			{
				xSum += measureObject.center[station][index][i].x - measureObject.center[station][index][i-1].x;
				lastMeasuredCenter = measureObject.center[station][index][i];
				xSumCounter++;
			}
		}
	}
	if (xSumCounter > 0)
		xSum /= xSumCounter;
 	speed = xSum* korFaktor / 6.024;//to je FPS[ms]

	measureObject.speed[station][index] = speed;

	float calculatedDistanceToValve;
	int openValveTime;
	int openValveDuration;

	calculatedDistanceToValve = 720 / 2 - lastMeasuredCenter.x;//pridem na sredino slike in iz sredine slike pristejem razdaljo
	//calculatedDistanceToValve = -100;
	calculatedDistanceToValve *= korFaktor;

	calculatedDistanceToValve   = calculatedDistanceToValve + distanceToValve[station];
	float distanceInM = calculatedDistanceToValve * 0.001;

	//speed = 3;

 	float speedFactor = 1;
	timeToValve = (calculatedDistanceToValve / speed * speedFactor) - measureObject.processTime[station][index];

	currTime = timeStampIndex;
	int valveDur;
	int valve2Dur;
	if (isGood == 0)  //v priemeru slabega kos podaljsam cas zaprtega ventila zaradi anomalije
	{
		timeToValve = round(timeToValve - badOffset);
		valveDur = valveDuration[station] +badOffset;
	}
	else
	{
		timeToValve = round(timeToValve);
		valveDur = valveDuration[station];
	}
	//ventil 0
	openValveTime = (currTime +int(timeToValve)) & 0xfff;
	int counter = openValveTime;
	if (counter < currTime)
		counter = (currTime) & 0xfff;
	for (int i = 0; i < valveDur; i++)
	{
		if (timeStemp[station][counter] != 0)//je ze vpisano v primeru dobrega in slabega kosa skupaj. Da ne pihnem slabega kosa
		{
			if (isGood == 1)
				timeStemp[station][counter] = 1;
			else
				timeStemp[station][counter] = 0;
		}
		counter++;
		counter = counter & 0xfff;
	}


//ventil 2
	counter = 0;
	openValveTime = ((currTime + (int)timeToValve + int(valve2Delay[station])) & 0xfff);
	 counter = openValveTime;
	 if (counter < currTime)
		 counter = (currTime) & 0xfff;

	for (int i = 0; i < valve2Duration[station]; i++)
	{
		if (timeStemp[station][counter] != 0)//je ze vpisano v primeru dobrega in slabega kosa skupaj. Da ne pihnem slabega kosa
		{
			//if (isGood == 1)
				timeStemp[station][counter] = 2;

		}
		counter++;
		counter = counter & 0xfff;
	}
	

	counter = openValveTime;
	if (counter < currTime)
		counter = (currTime) & 0xfff;




	//measureObject.isActive[station][index] = 0;

	UpdateHistory(station, index);
	pieceCounter++;


	updateImage[station] = 1;
}

void MBsoftware::UpdateHistory(int station, int index)
{
	
		for (int j = 0; j < types[station][currentType]->parameterCounter; j++)
		{
			history->measuredValue[station][history->lastIn[station]][j] = types[station][currentType]->measuredValue[j];
			history->nominal[station][history->lastIn[station]][j] = types[station][currentType]->nominal[j];
			history->toleranceHigh[station][history->lastIn[station]][j] = types[station][currentType]->toleranceHigh[j];
			history->toleranceLow[station][history->lastIn[station]][j] = types[station][currentType]->toleranceLow[j];
			history->isConditional[station][history->lastIn[station]][j] = types[station][currentType]->isConditional[j];
			history->isActive[station][history->lastIn[station]][j] = types[station][currentType]->isActive[j];
			history->name[station][history->lastIn[station]][j] = types[station][currentType]->name[j];
		}
		history->cam0Image[station][history->lastIn[station]] = cam[station]->image[measureObject.imageIndex[station][index]].buffer->clone();

		history->pieceNum[station][history->lastIn[station]] = index;
		history->speed[station][history->lastIn[station]] = measureObject.speed[station][index];
		history->angle[station][history->lastIn[station]] = measureObject.angle[station][index];
		history->isGood[station][history->lastIn[station]] = measureObject.isGood[station][index];

	history->currentPiece[history->lastIn[station]] = index;
	history->UpdateCounter(station);

	//graf
	for (int i = 0; i < types[station][currentType]->parameterCounter; i++)
	{
		dataArray[station][i][charDataCounter[station]] = types[station][currentType]->measuredValue[i];
	}
	charDataCounter[station]++;
	if (charDataCounter[station] >= CHART_DATA_SIZE)
		charDataCounter[station] = 0;
}

void MBsoftware::SetHistory()
{
	//najprej pobrisemo stare vektorje
	for (int i = 0; i < NR_STATIONS; i++)
	{
		for (int j = 0; j < HISTORY_SIZE; j++)
		{

			history->measuredValue[i][j].clear();
			history->nominal[i][j].clear();
			history->toleranceHigh[i][j].clear();
			history->toleranceLow[i][j].clear();
			history->toleranceLowCond[i][j].clear();
			history->toleranceHighCond[i][j].clear();
			history->name[i][j].clear();
			history->isActive[i][j].clear();
			history->isConditional[i][j].clear();
			history->cam0Image[i].clear();
		}
		history->parametersCounter[i].clear();
	}


	for (int i = 0; i < NR_STATIONS; i++)
	{
		for (int j = 0; j < HISTORY_SIZE; j++)
		{
			for (int k = 0; k < types[i][currentType]->parameterCounter; k++)
			{
				history->measuredValue[i][j].push_back(types[i][currentType]->measuredValue[k]);
				
				history->nominal[i][j].push_back(types[i][currentType]->nominal[k]);
				history->toleranceHigh[i][j].push_back(types[i][currentType]->toleranceHigh[k]);
				history->toleranceLow[i][j].push_back(types[i][currentType]->toleranceLow[k]);
				history->toleranceLowCond[i][j].push_back(types[i][currentType]->toleranceLowCond[k]);
				history->toleranceHighCond[i][j].push_back(types[i][currentType]->toleranceHighCond[k]);
				history->isActive[i][j].push_back(types[i][currentType]->isActive[k]);
				history->isConditional[i][j].push_back(types[i][currentType]->isConditional[k]);
				history->name[i][j].push_back(types[i][currentType]->name[k]);
				

			}
			history->cam0Image[i].push_back(Mat(images[0]->buffer->rows, images[0]->buffer->cols, CV_8UC1));
			//history->cam1Image[i].push_back(Mat(images[1]->buffer->rows, images[0]->buffer->cols, CV_8UC1));
		//	history->cam2Image[i].push_back(Mat(images[2]->buffer->rows, images[0]->buffer->cols, CV_8UC1));
		//	history->cam3Image[i].push_back(Mat(images[3]->buffer->rows, images[0]->buffer->cols, CV_8UC1));
			history->parametersCounter[i].push_back(types[i][currentType]->parameterCounter);
		}
	}
	history->CreateMeasurementsTable();


}

void MBsoftware::ReadRefPieceData()//pri spremembi tipa naloži podatke o lameli 
{
	QString dirPath;
	QString fileName;
	QString line;

	for (int j = 0; j < NR_STATIONS; j++)
	{
		dirPath = QDir::currentPath() + QString("/%1/types/RefDataStation%2").arg(REF_FOLDER_NAME).arg(j);

		if (!QDir(dirPath).exists())
			QDir().mkdir(dirPath);

		fileName = dirPath + QString("/refData_%1.txt").arg(types[j][currentType]->typeName);

		QFile file(fileName);
		if (file.open(QIODevice::ReadOnly))
		{
			QTextStream stream(&file);

			for (int i = 0; i < 360; i++)
			{
				line = stream.readLine();
				measureObject.refDistanceFromCenter[j][i] = line.toFloat();
			}
			refDataOk = true;
			file.close();
		}
		else
			refDataOk = false;
	}
}

void MBsoftware::SaveRefPieceData(int station, int index)
{
	QString fileName;
	QString line;
	QString dirPath;
	QDir dir;


		dirPath = QDir::currentPath() + QString("/%1/types/RefDataStation%2/").arg(REF_FOLDER_NAME).arg(station);

		if (!QDir(dirPath).exists())
			QDir().mkdir(dirPath);

		fileName = dirPath + QString("/refData_%1.txt").arg(types[station][currentType]->typeName);


		QFile file(fileName);
		if (file.open(QIODevice::WriteOnly | QIODevice::Text))
		{
			QTextStream stream(&file);
			for (int i = 0; i < 360; i++)
			{
				stream << measureObject.refDistanceFromCenter[station][i];
				stream << endl;

			}
			file.close();
		}

}

void MBsoftware::RefPieceMeasured(int station, int index)
{
	int rep = 0;
	int angle = 0;
	QMessageBox::StandardButton reply;
	float distance[180];
	int newIndex = 0;


	reply = QMessageBox::question(this, tr("QMessageBox::question()"),
		"Set new reference for current type?",
		QMessageBox::Yes | QMessageBox::No);

	if (reply == QMessageBox::Yes)
	{
		angle = lround((measureObject.angle[station][index]));///ker imamo samo 180 tock zato kot delimo za dve stopinji



		if (angle < 0)
			newIndex = (360+ angle);
		else 
			newIndex = angle;

		for (int i = 0; i < 360; i++)
		{

				measureObject.refDistanceFromCenter[station][i] = measureObject.distanceFromCenter[station][index][newIndex];


			newIndex++;
			if (newIndex > 359)
				newIndex = 0;
		
		}



		SaveRefPieceData( station, index);
		teachMode[station] = 0;
	}


}

void MBsoftware::OnMultimediaTimer(UINT uTimerID, UINT, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2)
{
	imageProcess->currentType = currentType; //samo zacasno za dodajanje propBrowserja
	imageProcess->testMode = settings->testIOMode;
	lpt[0]->GetStatusValue();
	lpt[1]->GetStatusValue();

	if (MMTimer.timeCounter >= 500)
	{
		MMTimer.SetStop();
		MMTimer.CalcFrequency(MMTimer.timeCounter);
		MMTimer.timeCounter = 0;
		MMTimer.SetStart();
	}

	MMTimer.timeCounter++;
	mmCounter++;


	//takt counter 
	if (mmCounter % 15000 == 0)
	{
		taktCounter = pieceCounter;
		pieceCounter = 0;
		taktGoodCounter = goodValveCounter;
		goodValveCounter = 0;
	}
	if (mmCounter % 300 == 0)
	{
		if (blinking == 0)
			blinking = 1;
		else
			blinking = 0;
		if (redLight == 1)
		{
			lpt[0]->SetOutputValue(6, 1);
		}
		else if (redLight == 0)
		{
			lpt[0]->SetOutputValue(6, 0);
		}
		else //blinking
		{
			lpt[0]->SetOutputValue(6, blinking);
		}
		if (greenLight == 1)
		{
			lpt[0]->SetOutputValue(7, 1);
		}
		else if (greenLight == 0)
		{
			lpt[0]->SetOutputValue(7, 0);
		}
		else //blinking
		{
			lpt[0]->SetOutputValue(7, blinking);
		}
	}

	//output signal
	if (settings->testIOMode == 0)
	{
		if (outputSig == true)
		{
			lpt[1]->SetOutputValue(7, 1);//output signal
			if (outputSigCounter > settings->outSignalDuration)
			{
				outputSig = false;
				lpt[1]->SetOutputValue(7, 0);
			}
			outputSigCounter++;
		}
		else
		{
			outputSigCounter = 0;
			lpt[1]->SetOutputValue(7, 0);
		}

		if (settings->inputSignalEnable == 1)//omogocen samo v primeru mirota
		{
			/*if ((lpt[1]->status[0].value == 1) && (lpt[1]->status[0].prevValue == 0))//external pause signal
			{
				savedProcessStatus = processStatus;
			}*/

			if ((lpt[1]->status[0].value == 0))//external pause signal
			{
				if (processStatus != 5)
				{
					savedProcessStatus = processStatus;
					processStatus = 5;
				}
				
				//gre na pauzo
			}


		}
	}

	if (processStatus != 0)
	{
		if ((lpt[1]->status[3].prevValue == 1) && (lpt[1]->status[3].value == 0) || (startKey == 1)) //stop tipka za stop delovanja
		{
			processStatus = 0;
			startKey = 0;
			glob->AddLog("DEVICE STOP", 3);
		}
	}



	if (mmCounter % 6 == 0)// WD in kamera trigger
	{
		lpt[1]->SetOutputValue(0, watchDog);

		if (settings->testIOMode == 0)
		{
			triggingcamera = 1;
		}
		else
			triggingcamera = 0;
		if (watchDog == 0)
		{
			watchDog = 1;
		}
		else
		{
			watchDog = 0;			
		}

		//triggingcamera = 0;
		if (triggingcamera == 1)
		{
			lpt[0]->SetControlByte(1, 1);
			lpt[0]->SetControlByte(2, 1);
			lpt[0]->SetControlByte(1, 0);
			lpt[0]->SetControlByte(2, 0);
		}
	}

	//status vibratorja in elevatorja
	if ((lpt[1]->data[2].value == 1) && (lpt[0]->status[3].value == 0)) //ročno stikalo je vklopljeno in vibrator dela
	{
		vibratorStatus = 1;
	}
	else if (lpt[0]->status[3].value == 1) // stikalo za vibraotr je izklopljeno
		vibratorStatus = 2;
	else
		vibratorStatus = 0; //vibrator je izklopljen

	if ((lpt[1]->data[4].value == 1) && (lpt[0]->status[4].value == 0)) //ročno stikalo je vklopljeno in vibrator dela
	{
		elevatorStatus = 1;
	}
	else if (lpt[0]->status[4].value == 1) // stikalo za vibraotr je izklopljeno
		elevatorStatus = 2;
	else
		elevatorStatus = 0; //vibrator je izklopljen






	//lpt[0]->status[0].value = 0;//to je samo zacasno da ne upostevam EMG signala
	if (lpt[0]->status[0].value == 1)// emegrancy stop
		processStatus = -2;
	else if ((settings->testIOMode == 1) && (prevTestMode == 0))
	{
		processStatus = 10;
		glob->AddLog("Test MODE Enabled!", 2);
	}
	else if ((settings->testIOMode == 0) && (prevTestMode == 1))
	{
		processStatus = 0;
		glob->AddLog("NORMAL MODE!", 0);
	}
	prevTestMode = settings->testIOMode;


	switch (processStatus)
	{
	case(-2)://emergancy stop
		{
			for (int i = 0; i < lpt.size(); i++)
			{
				lpt[i]->SetDataByte(0x00);
				lpt[i]->SetControlByteAllOFF();
			}
			triggingcamera = 0;
			emgMode = 1;
			if (lpt[0]->status[0].value == 0)//sproscen EMG
				processStatus = -1;
				

			redLight = 1;
			greenLight = 0; 
		break;
		}

	case(-1)://emergancy stop
	{
		for (int i = 0; i < lpt.size(); i++)
		{
			lpt[i]->SetDataByte(0x00);
			lpt[i]->SetControlByteAllOFF();
		}
		triggingcamera = 0;

		if (emgMode == 0)
		{
			processStatus = 0;
		}
		redLight = 1;
		greenLight = 0;
		break;
	}

	case(0): //zacetno stanje cakanje na tipko start
		{

		if ((lpt[1]->status[4].prevValue == 1) && (lpt[1]->status[4].value == 0) || ( startKey == 1)) //tipka start za zacetek delovanja
		{
			startKey = 0;
			//processStatus = 1;
			glob->AddLog("DEVICE START!", 0);

				processStatus = 3;

				if (resetBoxCounter == true)
				{
					resetBoxCounter = false;
					pieceInBox = 0;
				}
		}
		lpt[1]->SetOutputValue(1, 1);//power rele
		lpt[1]->SetOutputValue(2, 0);//vibrator
		lpt[1]->SetOutputValue(3, 0);//vibrator elevator
		lpt[1]->SetOutputValue(4, 0);//elevator
		station0StuckCounter = 0;
		station1StuckCounter = 0;

		if (testValveBottom == 1)
		{
			testValveDuration++;
			lpt[1]->SetOutputValue(5, 1);//clean 1
			if (testValveDuration > 2000)
			{
				testValveDuration = 0;
				testValveBottom = 0;
			}

		}
		else
		lpt[1]->SetOutputValue(5, 0);//clean 1

		if (testValveTop == 1)
		{
			testValveDuration++;
			lpt[1]->SetOutputValue(6, 1);//clean 1
			if (testValveDuration > 2000)
			{
				testValveDuration = 0;
				testValveTop = 0;
			}

		}
		else
		lpt[1]->SetOutputValue(6, 0);// clean 2

		elevatorOnCounter = 0;
		AutoStopCounter = 0;
		cleanCounterbottom = 0;
		cleanCounterTop = 0;
		delayCleanConterBottom = 0;
		cleanCounterbottom = 0;
		timeToCleanCounter = 0;
		redLight = 1;
		greenLight = 0;
		break;
		}
	case(1)://naprava deluje
		{
		lpt[1]->SetOutputValue(2, 1);//vibrator
		triggingcamera = 1; //trigganje kamer 

		if ((lpt[1]->status[3].prevValue == 1) && (lpt[1]->status[3].value == 0) || (startKey == 1)) //stop tipka za stop delovanja
		{
			processStatus = 0;
			startKey = 0;
			glob->AddLog("DEVICE STOP", 3);
		}

		if (lpt[0]->status[3].value == 0)//stikalo za vibrator --ce vibrator ne dela se tudi elevator ne sme zalufati
		{
			//preverjam senor za visino, ce je vec kot 2 sekundi brez kosov zazene elevator in
			if (lpt[1]->status[2].value == 0)
			{
				elevatorOnCounter++;
				if (elevatorOnCounter > 5000)
				{
					lpt[1]->SetOutputValue(4, 1);// elevator
					if(settings->elevatorVibratorEnable == 1)
						lpt[1]->SetOutputValue(3, 1);//vibrator elevator
					elevatorOnCounter = 0;
				}
			}
			else
			{
				elevatorOnCounter++;
				if (elevatorOnCounter > 5000)
				{
					lpt[1]->SetOutputValue(3, 0);//vibrator elevator
					lpt[1]->SetOutputValue(4, 0);//vibrator
					elevatorOnCounter = 0;
				}
			}
		}
		else
		{
			lpt[1]->SetOutputValue(3, 0);//vibrator elevator
			lpt[1]->SetOutputValue(4, 0);//elevator
		}


		if (pieceCounter != prevPieceCounter)
		{
			AutoStopCounter = 0;
		}
		else
			AutoStopCounter++;
		prevPieceCounter = pieceCounter;
		if (AutoStopCounter > 60000)
		{
			glob->AddLog("DEVICE EMPTY", 1);
			processStatus = 0;
		}

		//pregled ce je zataknjena ena linija

		if (st0PieceOn == 1)
		{
			station0StuckCounter = 0;
			st0PieceOn = 0;
		}
		else
			station0StuckCounter++;

		if (st1PieceOn == 1)
		{
			station1StuckCounter = 0;
			st1PieceOn = 0;
		}
		else
			station1StuckCounter++;

		if (station1StuckCounter == 20000)
		{
			glob->AddLog("Station 1 STUCKED", 2);

		}
		if (station0StuckCounter == 20000)
		{
			glob->AddLog("Station 0 STUCKED", 2);
		}

		if (station1StuckCounter == 60000)
		{
			glob->AddLog("Station 1 JAMMED", 1);
			processStatus = 0;
		}
		if (station0StuckCounter == 60000)
		{
			glob->AddLog("Station 0 JAMMED", 1);
			processStatus = 0;
		}
		//cleaning
		timeToCleanCounter++;
		if (settings->cleanEnable == true)//omogoceno auto ciscenje stekel
		{
			if (timeToCleanCounter > settings->cleanPeriod * 60 * 1000)
			{
				processStatus = 2;//gre v ciscenje
				cleanCounterbottom = 0;
				cleanCounterTop = 0;
				delayCleanConterBottom = 0;
				cleanCounterbottom = 0;
				timeToCleanCounter = 0;


			}
		}
		if (settings->workingMode == 0)//non stop brez ustavljanja 
		{


		}

		else if (settings->workingMode == 1) //auto stop device 
		{
			if (pieceInBox >= settings->pieceInBox)
			{
				glob->AddLog("DEVICE STOP! BOX FULL", 1);
				processStatus = 0;
				resetBoxCounter = true;
				if (settings->outSignalEnable)
					outputSig = true;
			}
		}
		else
		{
			if (pieceInBox >= settings->pieceInBox)
			{
				glob->AddLog("DEVICE PAUSE! BOX FULL", 1);
				processStatus = 4;
				resetBoxCounter = true;
				cleanCounterbottom = 0;
				cleanCounterTop = 0;
				pauseCounter = 0;
				if (settings->outSignalEnable)
					outputSig = true;
			}
		}
		if ((station0StuckCounter > 20000) || (station1StuckCounter > 20000))
		{
			redLight = 0;
			greenLight = 2;
		}
		else
		{
			redLight = 0;
			greenLight = 1;
		}
		break;

		}
	case(2)://cleaning mode 
	{
		lpt[1]->SetOutputValue(2, 0);//vibrator
		lpt[1]->SetOutputValue(3, 0);//vibrator eleveator
		lpt[1]->SetOutputValue(4, 0);// elevevator
		delayCleanConterBottom++;
		delayCleanConterTop++;


		if (cleanBottomFinished == false)
		{

			if (delayCleanConterBottom > settings->cleanDelayBottom * 1000)
			{
				cleanCounterbottom++;
				lpt[1]->SetOutputValue(5, 1);// clean 
				if (cleanCounterbottom > settings->cleanDurationBottom)
				{
					lpt[1]->SetOutputValue(5, 0);// clean 2
					cleanBottomFinished = true;

				}
	
			}
		}

		if (cleanTopFinished == false)
		{
			if (delayCleanConterTop > settings->cleanDelayTop * 1000)
			{
				cleanCounterTop++;
	
				lpt[1]->SetOutputValue(6, 1);// clean bottom
				if (cleanCounterTop > settings->cleanDurationTop)
				{

					lpt[1]->SetOutputValue(6, 0);// clean 2
					cleanTopFinished = true;
				}
			}
		}
	

		if ((cleanTopFinished == true) && (cleanBottomFinished == true))
		{
			cleanTopFinished = false;
			cleanBottomFinished = false;
			cleanCounterTop = 0;
			cleanCounterbottom = 0;
			
			delayCleanConterBottom = 0;
			delayCleanConterTop = 0;
			lpt[1]->SetOutputValue(6, 0);// clean 2
			lpt[1]->SetOutputValue(5, 0);// clean 2
			processStatus = 1;
		}

		redLight = 0;
		greenLight = 2;
		break;
	}
	case(3)://start cleaning
	{
		lpt[1]->SetOutputValue(2, 0);//vibrator
		lpt[1]->SetOutputValue(3, 0);//vibrator eleveator
		lpt[1]->SetOutputValue(4, 0);// elevevator
		delayCleanConterBottom++;
		delayCleanConterTop++;


		if (cleanBottomFinished == false)
		{

			lpt[1]->SetOutputValue(5, 1);// clean 
				cleanCounterbottom++;

				if (cleanCounterbottom > 1000)
				{
					lpt[1]->SetOutputValue(5, 0);// clean 2
					cleanBottomFinished = true;
					cleanCounterbottom = 0;

				}

		}

		if (cleanTopFinished == false)
		{
			if (delayCleanConterTop > 2000)
			{
				cleanCounterTop++;

				lpt[1]->SetOutputValue(6, 1);// clean bottom
				if (cleanCounterTop > 1000)
				{
					lpt[1]->SetOutputValue(6, 0);// clean 2
					cleanTopFinished = true;
				}
			}
		}


		if ((cleanTopFinished == true) && (cleanBottomFinished == true))
		{
			cleanTopFinished = false;
			cleanBottomFinished = false;
			cleanCounterTop = 0;
			cleanCounterbottom = 0;
			delayCleanConterBottom = 0;
			delayCleanConterTop = 0;
			lpt[1]->SetOutputValue(6, 0);// clean 2
			lpt[1]->SetOutputValue(5, 0);// clean 2
			processStatus = 1;
		}

		redLight = 0;
		greenLight = 2;
		break;
	}
	case (4)://ful box delay mode
	{
		lpt[1]->SetOutputValue(2, 0);//vibrator
		lpt[1]->SetOutputValue(3, 0);//vibrator eleveator
		lpt[1]->SetOutputValue(4, 0);// elevevator

		if (pauseCounter > settings->autostopTime * 1000)
		{
			processStatus =3;
			cleanCounterbottom = 0;
			cleanCounterTop = 0;
			delayCleanConterBottom = 0;
			cleanCounterbottom = 0;
			if (resetBoxCounter == true)
			{
				resetBoxCounter = false;
				pieceInBox = 0;
			}
		}
		pauseCounter++;
		redLight = 0;
		greenLight = 2;

		break;
	}
	case (5)://ful box delay mode
	{
		lpt[1]->SetOutputValue(1, 1);//power rele
		lpt[1]->SetOutputValue(2, 0);//vibrator
		lpt[1]->SetOutputValue(3, 0);//vibrator elevator
		lpt[1]->SetOutputValue(4, 0);//elevator
		lpt[1]->SetOutputValue(5, 0);//clean 1
		lpt[1]->SetOutputValue(6, 0);// clean 2

		elevatorOnCounter = 0;
		AutoStopCounter = 0;
		redLight = 2;
		greenLight = 2;

		if ((lpt[1]->status[0].value == 1))//external pause signal
		{
			processStatus = savedProcessStatus;//gre na pauzo
		}
	}


	case(10)://testni nacin za 
	{
		triggingcamera = 0;
		lpt[1]->SetOutputValue(1, 1);

		redLight = 1;
		greenLight = 1;

		break;
	}
	case(20)://umeritveni nacin
	{
		triggingcamera = 1;
		lpt[1]->SetOutputValue(1, 1);



		break;
	}
	}
	//BUFFER SLIK 
	for (int i = 0; i < NR_STATIONS; i++)
	{
		if (cam[i]->enableLive == false)
		{
			if (cam[i]->imageReady[sprejetaCounter[i]] == 1)
			{
				cam[i]->imageReady[sprejetaCounter[i]] = 2;

				if (cakalnaVrstaDolzina[i] < cakalnaVrstaMaxDolzina[i])
				{
					if (cakalnaVrstaDolzina[i] >= 0)
					{
						cakalnaVrstaDolzina[i]++;

						cakalnaVrstaLastIn[i]++;
						if (cakalnaVrstaLastIn[i] >= cakalnaVrstaMaxDolzina[i]) cakalnaVrstaLastIn[i] = 0;

					}
					else //primer, ko v čakalni vrsti ni bilo še nobenega elementa
					{
						cakalnaVrstaDolzina[i] = 1;
						cakalnaVrstaFirstOut[i] = 0;
						cakalnaVrstaLastIn[i] = 0;
					}
					cakalnaVrstaNrCam[i][cakalnaVrstaLastIn[i]] = i;
					cakalnaVrstaNrImage[i][cakalnaVrstaLastIn[i]] = sprejetaCounter[i];


				}
				else
				{
					//obdelavaDovoljHitra = false;   //PREPOČASNA OBDELAVA: ker je čakalna vrsta polna, moramo izpustiti obdelavo slike!
				}
				sprejetaCounter[i]++;
				if (sprejetaCounter[i] >= cam[i]->num_images)
					sprejetaCounter[i] = 0;
			}
		}
	}
	const int maxThreads = 1;  //POZOR: ne nastaviti višjega števila threadov od velikosti measureObject.threadIsFree[]

	int nrImage;
	int nrCam;
	for (int i = 0; i < NR_STATIONS; i++)
	{
		for (int nit = 0; nit < maxThreads && cakalnaVrstaDolzina[i] > 0; nit++)
		{
			if (imageProcess->threadsRunning[nit] == false)
			{
				nrImage = cakalnaVrstaNrImage[i][cakalnaVrstaFirstOut[i]];
				nrCam = cakalnaVrstaNrCam[i][cakalnaVrstaFirstOut[i]];

				imageProcess->threadsRunning[nit] = true;
				cam[nrCam]->image[nrImage].stThread = nit;

				//if (umeritevMode == false)
				//QtConcurrent::run(imageProcess, &imageProcessing::ProcessingCamera1, nrCam, nrImage, 0);//zacasno shranjujem slike zaradi moreditih slabih obdelav
				QtConcurrent::run(imageProcess, &imageProcessing::ProcessingCamera2, nrCam, nrImage, 0);
				


				//POZOR: na koncu funkcije, ki ko kličem paralelno, moram nastaviti  measureObject->threadIsFree[pSlika->stevilkaNiti] = true;

				cakalnaVrstaDolzina[i]--;
				cakalnaVrstaFirstOut[i]++;

				if (cakalnaVrstaFirstOut[i] >= cakalnaVrstaMaxDolzina[i]) cakalnaVrstaFirstOut[i] = 0;
			}

		}
	}

	//izmeri kos

		for (int i = 0; i < POMNILNIK_LAMEL; i++)
		{
			if ((measureObject.isMeasuredSpeed[0][i] == 1) && (measureObject.isMeasured[0][i] == 1) && measureObject.isProcessed[0][i] == 0)
			{
				measureObject.isProcessed[0][i] = 1;
				//QtConcurrent::run(glob, &MBsoftware::MeasurePiece, 0, i);
				glob->MeasurePiece(0,i);

			}
		}
	
	for (int i = 0; i < POMNILNIK_LAMEL; i++)
	{
		if ((measureObject.isMeasuredSpeed[1][i] == 1) && (measureObject.isMeasured[1][i] == 1) && measureObject.isProcessed[1][i] == 0)
		{
			measureObject.isProcessed[1][i] = 1;
			//QtConcurrent::run(glob, &MBsoftware::MeasurePiece, 1, i);
			glob->MeasurePiece(1, i);

		}
	}




	//odpiram ventil
	if (settings->testIOMode == 0)
	{
		for (int i = 0; i < NR_STATIONS; i++)
		{
			if (i == 0)
			{
				if (timeStemp[i][timeStampIndex] > 0)//odpre ventil za dolocen cas pri postaji 0
				{
					valveOpenS0 = 1;

					if (timeStemp[i][timeStampIndex] == 1)
					{
						lpt[0]->SetOutputValue(0, 1);
						
					}
					else
					{
						
						lpt[0]->SetOutputValue(0, 1);
						lpt[0]->SetOutputValue(1, 1);
					}
						
				}
				else //zapre ventil;
				{
					lpt[0]->SetOutputValue(0, 0);
					lpt[0]->SetOutputValue(1, 0);
					valveOpenS0 = 0;
				}
			}
			else
			{
				if (timeStemp[i][timeStampIndex] > 0)//odpre ventil za dolocen cas pri postaji 1
				{
					valveOpenS1 = 1;
					if (timeStemp[i][timeStampIndex] == 1)
					{
						lpt[0]->SetOutputValue(2, 1);
						
					}
					else
					{
						
						lpt[0]->SetOutputValue(2, 1);
						lpt[0]->SetOutputValue(3, 1);
					}
				}
				else //zapre ventil;
				{
					lpt[0]->SetOutputValue(2, 0);
					lpt[0]->SetOutputValue(3, 0);
					valveOpenS1 = 0;
				}
			}

		}
	}


	if (valveOpenS0 == 1)
	{
		valveDurationCounterS0++;
		if (valveDurationCounterS0 == settings->valveDurationS0-5)
		{
			goodValveCounter++;
			pieceInBox++;
			st0PieceOn = 1;
		}
	}
	else
		valveDurationCounterS0 = 0;



	if (valveOpenS1 == 1)
	{
		valveDurationCounterS1++;
		if (valveDurationCounterS1 == settings->valveDurationS1-5)
		{
			goodValveCounter++;
			pieceInBox++;
			st1PieceOn = 1;
		}
	}
	else
		valveDurationCounterS1 = 0;




	timeStemp[0][timeStampIndex] = -1;//po koncu ponastavimo stanje 
	timeStemp[1][timeStampIndex] = -1;//po koncu ponastavimo stanje 
	timeStampIndex++;
	timeStampIndex = timeStampIndex & 0xfff;

}



void MBsoftware::CreateCameraTab()
{

//QWidget *cameraWidget;
	QGroupBox *camGroup = new QGroupBox(ui.tabCameras);;
	QHBoxLayout *camLayout = new QHBoxLayout;
	QToolButton *camButton;
	QIcon camIcon;
	QSpacerItem *spacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Expanding);
	QSignalMapper* signalMapper = new QSignalMapper(this);
	QHBoxLayout *tabLayout = new QHBoxLayout;
	int k = 0;

	//QString fileP = QDir::currentPath() + QString("\\res\\cam%1.bmp").arg(k);
	QImage image;
	/*if (!image.isNull())
		camIcon.addPixmap(QPixmap::fromImage(image));*/

	for (int i = 0; i < cam.size(); i++)
	{
		image.load(QString(QDir::currentPath() + QString("\\res\\cam%1.bmp").arg(i)));
		if (!image.isNull())
			camIcon.addPixmap(QPixmap::fromImage(image));

		camButton = new QToolButton();
		camButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
		
		camButton->setText(QString("CAM%1").arg(i));
		camButton->setFixedSize(QSize(TAB_BUTTTON_SIZE, TAB_BUTTTON_SIZE));
		camButton->setIcon(camIcon);
		camButton->setIconSize(QSize(TAB_ICON_SIZE, TAB_ICON_SIZE));
		camButton->setAutoRaise(true);
		camLayout->addWidget(camButton);
		connect(camButton, SIGNAL(clicked()), signalMapper, SLOT(map()));
		signalMapper->setMapping(camButton, i);
	}


	
	tabLayout->addWidget(camGroup);
	tabLayout->addSpacerItem(spacer);
	ui.tabCameras->setLayout(tabLayout);
	camGroup->setLayout(camLayout);
	connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(OnClickedShowCamButton(int)));

}

void MBsoftware::CreateSignalsTab()
{


	QHBoxLayout *lptLayout = new QHBoxLayout;
	QToolButton *lptButton;
	QHBoxLayout *smartCardLayout = new QHBoxLayout;
	QToolButton *smartCardButton;

	QHBoxLayout *tcpLayout = new QHBoxLayout;
	QToolButton *tcpButton;

	QIcon camIcon;
	QIcon smartIcon;
	QSpacerItem *spacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Expanding);
	QSignalMapper* signalMapper = new QSignalMapper(this);
	QSignalMapper* signalSmartCardMapper = new QSignalMapper(this);
	QHBoxLayout *tabLayout = new QHBoxLayout;
	
	int k = 0;

	
	QImage image;

	if (lpt.size() > 0)
	{
		QGroupBox *lptGroup = new QGroupBox(ui.tabSignals);
		lptGroup->setTitle("LPT");

		image.load(QString(QDir::currentPath() + QString("\\res\\lptIcon.bmp")));

		if (!image.isNull())
			camIcon.addPixmap(QPixmap::fromImage(image));

		for (int i = 0; i < lpt.size(); i++)
		{

			lptButton = new QToolButton();
			lptButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
			lptButton->setText(QString("LPT%1").arg(i));
			lptButton->setFixedSize(QSize(TAB_BUTTTON_SIZE, TAB_BUTTTON_SIZE));
			lptButton->setIcon(camIcon);
			lptButton->setAutoRaise(true);
			lptButton->setIconSize(QSize(TAB_ICON_SIZE, TAB_ICON_SIZE));
			lptLayout->addWidget(lptButton);
			connect(lptButton, SIGNAL(clicked()), signalMapper, SLOT(map()));
			signalMapper->setMapping(lptButton, i);
		}
		tabLayout->addWidget(lptGroup);

		ui.tabSignals->setLayout(tabLayout);
		lptLayout->setMargin(0);
		lptGroup->setLayout(lptLayout);
		connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(OnClickedShowLptButton(int)));
	}
	
	if (controlCardMB.size() > 0)
	{
		QGroupBox *SmartCardGroup = new QGroupBox(ui.tabSignals);
		SmartCardGroup->setTitle("SmartCard");

		image.load(QString(QDir::currentPath() + QString("\\res\\serialPort.bmp")));

		if (!image.isNull())
			smartIcon.addPixmap(QPixmap::fromImage(image));

		for (int i = 0; i < controlCardMB.size(); i++)
		{
			smartCardButton = new QToolButton();
			smartCardButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
			smartCardButton->setText(QString("SmartC%1").arg(i));
			smartCardButton->setFixedSize(QSize(TAB_BUTTTON_SIZE, TAB_BUTTTON_SIZE));
			smartCardButton->setIcon(smartIcon);
			smartCardButton->setIconSize(QSize(TAB_ICON_SIZE, TAB_ICON_SIZE));
			smartCardButton->setAutoRaise(true);
			smartCardLayout->addWidget(smartCardButton);
			connect(smartCardButton, SIGNAL(clicked()), signalSmartCardMapper, SLOT(map()));
			signalSmartCardMapper->setMapping(smartCardButton, i);
		}

		tabLayout->addWidget(SmartCardGroup);
		smartCardLayout->setMargin(0);
		SmartCardGroup->setLayout(smartCardLayout);
		connect(signalSmartCardMapper, SIGNAL(mapped(int)), this, SLOT(OnClickedShowSmartCardButton(int)));

	}
	if (tcp.size() > 0)
	{
		QGroupBox *tcpGroup = new QGroupBox(ui.tabSignals);
		tcpGroup->setTitle("TCP");
		image.load(QString(QDir::currentPath() + QString("\\res\\localNetwork.bmp")));

		if (!image.isNull())
			smartIcon.addPixmap(QPixmap::fromImage(image));

		for (int i = 0; i < tcp.size(); i++)
		{
			tcpButton = new QToolButton();
			tcpButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
			tcpButton->setText(QString("TCP%1").arg(i));
			tcpButton->setFixedSize(QSize(TAB_BUTTTON_SIZE, TAB_BUTTTON_SIZE));
			tcpButton->setIcon(smartIcon);
			tcpButton->setIconSize(QSize(TAB_ICON_SIZE, TAB_ICON_SIZE));
			tcpButton->setAutoRaise(true);
			tcpLayout->addWidget(tcpButton);

			connect(tcpButton, &QPushButton::clicked,
				[=](int index) { OnClickedTCPconnction(i); });

		}
		tabLayout->addWidget(tcpGroup);
		tcpLayout->setMargin(0);
		tcpGroup->setLayout(tcpLayout);
		//connect(signalSmartCardMapper, SIGNAL(mapped(int)), this, SLOT(OnClickedShowSmartCardButton(int)));
	}


	tabLayout->addSpacerItem(spacer);



}

void MBsoftware::CreateMeasurementsTable()
{
	int height = 0;
	for (int i = 0; i < NR_STATIONS; i++)
	{
		parametersTable[i] = new QTableWidget();



	//measurementsBox

	//parametersTable[0] = new QTableWidget();
	parametersTable[i]->setStyleSheet("QHeaderView::section { background-color:rgb(236, 236, 236); }");
	parametersTable[i]->setMinimumWidth(420);


	height = (types[i][0]->parameterCounter + 1) * 25;
	parametersTable[i]->setMinimumHeight(height + 25);
	parametersTable[i]->setMinimumWidth(420);

	parametersTable[i]->setEditTriggers(QAbstractItemView::NoEditTriggers); //diable edit
	parametersTable[i]->setSelectionMode(QAbstractItemView::NoSelection); //diable edit
	QStringList celice;
	parametersTable[i]->setColumnCount(4);
	celice << "Parameter" << "Measured" << "Min" << "Max";
	parametersTable[i]->setHorizontalHeaderLabels(celice);
	parametersTable[i]->setColumnWidth(0, 160);
	parametersTable[i]->setColumnWidth(1, 78);
	parametersTable[i]->setColumnWidth(2, 78);
	parametersTable[i]->setColumnWidth(3, 78);
	
	parametersTable[i]->setRowCount(types[i][0]->parameterCounter);
	parametersTable[i]->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	if (i == 0)
	{
		ui.frameMeasurements->setMinimumHeight(height + 60); //25 za naslov nad meritvami
		ui.layoutMeasurements->addWidget(parametersTable[0], Qt::AlignTop);
	}
	else if (i == 1)
	{
		ui.frameMeasurements_2->setMinimumHeight(height + 60); //25 za naslov nad meritvami
		ui.layoutMeasurements_2->addWidget(parametersTable[1], Qt::AlignTop);
	}
	}

}

void MBsoftware::CreateChart()
{
	for (int i = 0; i < 2; i++)//2 postaji
	{
		set0[i] = new QBarSet("");
		setBad[i] = new QBarSet("");
		series0[i] = new QStackedBarSeries();
		series0[i]->append(set0[i]);
		series0[i]->append(setBad[i]);

		chart[i] = new QChart();
		chart[i]->addSeries(series0[i]);
		chart[i]->setTitle(QString("STATION %1 : LAST %2 measurements : parameter: %3").arg(i).arg(CHART_DATA_SIZE).arg(types[i][currentType]->name[chartParam[i]]));
		chart[i]->setAnimationOptions(QChart::NoAnimation);

		axisX[i] = new QValueAxis();
		axisX[i]->setRange(0, CHART_DATA_SIZE);
		chart[i]->addAxis(axisX[i], Qt::AlignBottom);
		series0[i]->attachAxis(axisX[i]);

		axisY[i] = new QValueAxis();
		chart[i]->addAxis(axisY[i], Qt::AlignLeft);
		series0[i]->attachAxis(axisY[i]);

		chartView[i] = new QChartView();
		chartView[i]->setChart(chart[i]);
		chartView[i]->setRenderHint(QPainter::Antialiasing);

		if(i == 0)
		ui.chartLayout->addWidget(chartView[i]);
		else
			ui.chartLayout_3->addWidget(chartView[i]);
		chart[i]->legend()->setVisible(false);

	}

	for (int i = 0; i < 2; i++)
	{
		seriesG[i] = new QLineSeries();
		//seriesG->append(setG);

		chartG[i] = new QChart();
		chartG[i]->addSeries(seriesG[i]);
		chartG[i]->setAnimationOptions(QChart::NoAnimation);


		axisXG[i] = new QValueAxis();
		axisXG[i]->setRange(0, GAUSS_SIZE);
		chartG[i]->addAxis(axisXG[i], Qt::AlignBottom);
		seriesG[i]->attachAxis(axisXG[i]);

		axisYG[i] = new QValueAxis();
		chartG[i]->addAxis(axisYG[i], Qt::AlignLeft);
		seriesG[i]->attachAxis(axisYG[i]);

		chartViewG[i] = new QChartView();
		chartViewG[i]->setChart(chartG[i]);
		chartViewG[i]->setRenderHint(QPainter::Antialiasing);

		if(i == 0)
		ui.chartLayout_2->addWidget(chartViewG[i]);
		else
			ui.chartLayout_4->addWidget(chartViewG[i]);
		chartG[i]->legend()->setVisible(false);
	}

}







void MBsoftware::AddLog(QString text, int type)
{
	logText.push_back(QString("%1:%2:%3 - %4").arg(QTime::currentTime().hour()).arg(QTime::currentTime().minute()).arg(QTime::currentTime().second()).arg(text));
	logIndex.push_back(type);
	WriteLogToFile(logPath, text, logIndex.back());
	EraseLastLOG();
}

void MBsoftware::WriteLogToFile(QString path, QString text, int type)
{
//lockWrite.lockForWrite();

		QString fileNameTmp = "C:/log/workingLog_" + QDateTime::currentDateTime().toString("dd_MM_yyyy") + ".txt";
		QString value;// + "";

		value = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss ") + text + type +  "\n";
		//ui.plainTextEdit->appendPlainText(value); // Adds the message to the widget
		//ui.plainTextEdit->verticalScrollBar()->setValue(ui.plainTextEdit->verticalScrollBar()->maximum()); // Scrolls to the bottom

		if (fileNameTmp.compare(path) != 0)
		{
			//ko se spremeni datum odpre nov fajl
			m_logFile->close();
			m_logFile->setFileName(fileNameTmp);
			//logPath = fileNameTmp;
			m_logFile->open(QIODevice::Append | QIODevice::Text);
		}
		if (m_logFile != 0)
		{
			QTextStream out(m_logFile);
			out.setCodec("UTF-8");
			out << value;
		} // Logs to file

}

void MBsoftware::WriteGlobCounters()
{
	QFile *file;
	QString filePath;
	if (!QDir("C:/data/").exists())
		QDir().mkdir("C:/data");
	filePath = "C:/data/CountersState.txt";
		file = new QFile();
		file->setFileName(filePath);

		QString value;// + "";

		
		//ui.plainTextEdit->appendPlainText(value); // Adds the message to the widget
		//ui.plainTextEdit->verticalScrollBar()->setValue(ui.plainTextEdit->verticalScrollBar()->maximum()); // Scrolls to the bottom

		if (file->open(QIODevice::Append | QIODevice::Text))
		{
			QTextStream out(file);
			out.setCodec("UTF-8");
			value = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss ") +QString( "STATION O GOOD: %1").arg(types[0][currentType]->goodMeasurementsCounter)+"  " + QString("STATION O BAD: %1").arg(types[0][currentType]->badMeasurementsCounter) + "\n";
			out << value;
			value = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss ") + QString("STATION 1 GOOD: %1").arg(types[1][currentType]->goodMeasurementsCounter) + "  " + QString("STATION 1 BAD: %1").arg(types[1][currentType]->badMeasurementsCounter) + "\n";
			out << value;
			value = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss ") + QString("PIECE IN GOOD BOX: %1").arg(pieceInBox)+  "\n";
			out << value;
		}

}

void MBsoftware::EraseLastLOG()
{
	if (logIndex.size() > 10)
	{
		logIndex.erase(logIndex.begin());
		logText.erase(logText.begin());
	}
}


void MBsoftware::ReadTypes()
{
	QString filePath;
	QStringList values;
	int count = 0;
	QString typePath;
	QString typeName;
	QDir dir;
	filePath = QDir::currentPath() + QString("/%1/types").arg(REF_FOLDER_NAME);

	if (!QDir(filePath).exists())
		QDir().mkdir(filePath);

	for (int i = 0; i < NR_STATIONS; i++)
	{

		//filePath = "D:\\Git_MBvision\\MBsoftware32bit\\MBsoftware32Bit\\Win32\\Reference\\signali\\ControlCardMB0.ini";
		filePath = QDir::currentPath() + QString("/%1/types/types_Station%2").arg(REF_FOLDER_NAME).arg(i);
		QSettings settings(filePath, QSettings::IniFormat);

		if (!QDir(filePath).exists())
			QDir().mkdir(filePath);




		dir.setPath(filePath);
		dir.setNameFilters(QStringList() << "*.ini");
		count = dir.count();
		QStringList list = dir.entryList();
		//inputs
		for (int j = 0; j < count; j++)
		{
			typePath = filePath;
			typeName = list.at(j);
			typeName.chop(4);
			types[i].push_back(new Types());

			//	cam.push_back(new ImagingSource());
			types[i].back()->stationNumber = i;
			types[i].back()->Init(typeName, typePath);

		}
	}


}

void MBsoftware::ShowErrorMessage()
{
	for (int i = 0; i < logIndex.size(); i++)
	{
		if (logIndex[i] == 0) //status ok - zelena
		{
			ui.listWidgetStatus->item(i)->setText(QString("%1").arg(logText[i]));
			ui.listWidgetStatus->item(i)->setBackgroundColor(QColor(150, 255, 150));
		}
		else if (logIndex[i] == 1) //napaka na napravi - rdeca
		{
			ui.listWidgetStatus->item(i)->setText(QString("%1").arg(logText[i]));
			ui.listWidgetStatus->item(i)->setBackgroundColor(QColor(255, 150, 150));
		}
		else if (logIndex[i] == 2) //warning
		{
			ui.listWidgetStatus->item(i)->setText(QString("%1").arg(logText[i]));
			ui.listWidgetStatus->item(i)->setBackgroundColor(QColor(Qt::yellow));
		}
		else //info - modra
		{
			ui.listWidgetStatus->item(i)->setText(QString("%1").arg(logText[i]));
			ui.listWidgetStatus->item(i)->setBackgroundColor(QColor(100, 150, 255));
		}
	}
	if (logIndex.size() > 5)
	{
		ui.listWidgetStatus->scrollToBottom();
	}
}

void MBsoftware::ClearErrorMessage(int index)
{
	ui.listWidgetStatus->item(index)->setBackgroundColor(QColor(236, 236, 236));
	ui.listWidgetStatus->item(index)->setText("");
	//log[0]->ClearWindowMessage(index);
}

void MBsoftware::AppendClearErrorMessageIndex(int index)
{
	int indexExists = 0;
	for (int i = 0; i < clearLogIndex.size(); i++)
	{
		if (clearLogIndex[i] == index)
		{
			indexExists = 1;
			break;
		}
	}
	if (indexExists == 0)
		clearLogIndex.push_back(index);
}


void MBsoftware::DrawMeasurements()
{


		/*if (currentType != prevType)
		{
			prevType = currentType;*/
		/*	for (int i = 0; i < NR_STATIONS; i++)
			{
				if (parametersTable[i]->rowCount() != types[i][currentType]->parameterCounter)
				{
					parametersTable[i]->setRowCount(types[i][currentType]->parameterCounter);
				}
			}
	//	}*/


	

	 QColor bcolor;

	 //v prihodnosti dodaj se za vec postaj
	 ui.labelCurrentType->setText(QString("TYPE: %1").arg(types[0][currentType]->typeName));
	 ui.labelCurrentType_2->setText(QString("TYPE: %1").arg(types[1][currentType]->typeName));

		 for (int i = 0; i < NR_STATIONS; i++)
		 {

			 
			 parametersTable[i]->setRowCount(types[i][currentType]->parameterCounter);
			 for (int row = 0; row < parametersTable[i]->rowCount(); row++)
			 {
				 //ce se je dodal nov parameter, dodamo novo vrstico

					 for (int column = 0; column < parametersTable[i]->columnCount(); column++)
					 {
						 QTableWidgetItem *newItem;
						 switch (column)
						 {
						 case 0:
							 newItem = new QTableWidgetItem(tr("%1").arg(types[i][currentType]->name[row]));
							 break;
						 case 1:
							 newItem = new QTableWidgetItem(tr("%1").arg(types[i][currentType]->measuredValue[row]));
							 break;
						 case 2:
							 newItem = new QTableWidgetItem(tr("%1").arg(types[i][currentType]->toleranceLow[row]));
							 break;

						 case 3:
							 newItem = new QTableWidgetItem(tr("%1").arg(types[i][currentType]->toleranceHigh[row]));
							 break;
						 default:
							 break;
						 }
						 parametersTable[i]->setItem(row, column, newItem);
					 }

			 }

			 //obarva vrstice dobre slabe
			 for (int row = 0; row < parametersTable[i]->rowCount(); row++)
			 {
				 types[i][currentType]->IsGood(row);
				 if (types[i][currentType]->isActive[row])
				 {
					 //measurements[i][vars->selectedType]->IsGood(row);

					 if (types[i][currentType]->isGood[row] == 1)
					 {
						 bcolor = QColor(150, 255, 150);
					 }
					 else if (types[i][currentType]->isGood[row] == 0)
						 bcolor = QColor(255, 150, 150);
					 else
						 bcolor = QColor(0, 170, 255);
				 }
				 else
				 {
					 bcolor = QColor(236, 236, 236);
				 }

				 parametersTable[i]->item(row, 0)->setBackground(bcolor);
				 parametersTable[i]->item(row, 1)->setBackground(bcolor);
				 parametersTable[i]->item(row, 2)->setBackground(bcolor);
				 parametersTable[i]->item(row, 3)->setBackground(bcolor);
				 //measurementsResoults->item(row, 1)->setText(QString("%1").arg(types[0][vars->selectedType]->measuredValue[row]));
			 }
		 }

}




void MBsoftware::PopulateStatusBox()
{
	int index = 0;

	index = 0;
	statusBox[0]->AddItem(index, QString("MM timer:  %1 Hz").arg(MMTimer.frequency));
	index++;
	statusBox[0]->AddItem(index, QString("View timer:  %1 Hz").arg(viewTimerTimer.frequency));
	index++;
	statusBox[0]->AddItem(index, QString("View timer2:  %1 Hz").arg(viewTimerTimer2.frequency));
	index++;
	for (int i = 0; i < cam.size(); i++)
	{
		statusBox[0]->AddItem(index, QString("cam %1 FPS:  %2").arg(i).arg(cam[i]->frameReadyFreq.frequency));
		index++;
	}
	for (int i = 0; i < imageProcess->processingTimer.size(); i++)
	{
		statusBox[0]->AddItem(index, QString("Obdelave%1 : %2 [ms]").arg(i).arg(imageProcess->processingTimer[i]->elapsed));
		index++;
	}
	for (int i = 0; i < TestFun.size(); i++)
	{
		statusBox[0]->AddItem(index, QString("GlavniStevec%1 : %2 [ms]").arg(i).arg(TestFun[i]->elapsed));
		index++;
	}

	for (int i = 0; i < cam.size(); i++)
	{
	
		statusBox[0]->AddItem(index, QString("frameCounter[%1]: %2").arg(i).arg(cam[i]->testCounter));
		index++;
		statusBox[0]->AddItem(index, QString("sprejetaCouner[%1]: %2").arg(i).arg(sprejetaCounter[i]));
		index++;
		statusBox[0]->AddItem(index, QString("obdelanaCounter[%1]: %2").arg(i).arg(imageProcess->obdelanaCounter[i]));
		index++;
	}

	statusBox[0]->AddItem(index, QString("processStatus: %1").arg(processStatus));
	index++;

	for (int i = 0; i < NR_STATIONS; i++)
	{
		statusBox[0]->AddItem(index, QString("cakalna vrsta Station %1: %2").arg(i).arg(cakalnaVrstaDolzina[i]));
		index++;
	}

	statusBox[0]->AddItem(index, QString("Station 0 CurrPiece: %1").arg(lamelaIndex[0]));
	index++;

	statusBox[0]->AddItem(index, QString("Station 1 CurrPiece: %1").arg(lamelaIndex[1]));
	index++;

	statusBox[0]->AddItem(index, QString("Tmp piece Station 0: %1").arg(imageProcess->tmpPieceCounter[0]));
	index++;

	statusBox[0]->AddItem(index, QString("Tmp piece Station 1: %1").arg(imageProcess->tmpPieceCounter[1]));
	index++;

	for (int i = 0; i < 2; i++)
	{
		statusBox[0]->AddItem(index, QString("TEACHMODE Station%1: %2").arg(i).arg(teachMode[i]));
		index++;

	}




}



void MBsoftware::myFunction(int cam, int image, int draw)
{
	int bla = 100;
	//imageProcess->ProcessingCamera0(cam, image, draw);
	
}



void MBsoftware::DrawImageOnScreen()
{
	if (!isSceneAdded)
	{
		for (int i = 0; i < 2; i++)
		{
			MBsoftware::showScene[i] = new QGraphicsScene(this);
		}
		ui.imageViewMainWindow_0->setScene(MBsoftware::showScene[0]);
		ui.imageViewMainWindow_1->setScene(MBsoftware::showScene[1]);

		isSceneAdded = true;
	}
	for (int i = 0; i < 2; i++)
	{
		qDeleteAll(showScene[i]->items());
	}

	ui.imageViewMainWindow_0->setAlignment(Qt::AlignTop | Qt::AlignLeft);
	ui.imageViewMainWindow_1->setAlignment(Qt::AlignTop | Qt::AlignLeft);


	//ui.imageViewMainWindow_0->scale(0.5, 0.5);
	//ui.imageViewMainWindow_1->scale(0.5, 0.5);


	
}

void MBsoftware::UpdateImageView(int station)
{

		qDeleteAll(showScene[station]->items());
		int index = lamelaIndex[station];


		if (lamelaIndex[station] > -1)
		{
			QImage qimgOriginal((uchar*)imageProcess->displayLamela[station][index].data, imageProcess->displayLamela[station][index].cols, imageProcess->displayLamela[station][index].rows, imageProcess->displayLamela[station][index].step, QImage::Format_Grayscale8);

			showScene[station]->addPixmap(QPixmap::fromImage(qimgOriginal));


			//tukaj risemo podatke o lameli
			for (int k = 0; k < 360; k++)
			{

				if (obrobaZgorajOK[station][index][k] == 1)
				{
					showScene[station]->addItem(tockeZgorajDraw[station][index][k].DrawDot(QPen(Qt::green), QBrush(Qt::green), 3));
				}

				else if (obrobaZgorajOK[station][index][k] == 2)
				{
					showScene[station]->addItem(tockeZgorajDraw[station][index][k].DrawDot(QPen(Qt::red), QBrush(Qt::red), 3));
				}
				else if (obrobaZgorajOK[station][index][k] == 3)
				{
					showScene[station]->addItem(tockeZgorajDraw[station][index][k].DrawDot(QPen(Qt::yellow), QBrush(Qt::yellow), 3));
				}

				if (obrobaSpodajOK[station][index][k] == 1)
				{
					showScene[station]->addItem(tockeSpodajDraw[station][index][k].DrawDot(QPen(Qt::green), QBrush(Qt::green), 3));
				}

				else if (obrobaSpodajOK[station][index][k] == 2)
				{
					showScene[station]->addItem(tockeSpodajDraw[station][index][k].DrawDot(QPen(Qt::red), QBrush(Qt::red), 3));
				}
				else if (obrobaSpodajOK[station][index][k] == 3)
				{
					showScene[station]->addItem(tockeSpodajDraw[station][index][k].DrawDot(QPen(Qt::yellow), QBrush(Qt::yellow), 3));
				}
			}




		}
}

void MBsoftware::UpdateChart(int station,int param, int counter)
{


	chart[station]->setTitle(QString("Measurement STATION %1: %2").arg(station).arg(types[station][currentType]->name[param]));

	float minValue = (types[station][currentType]->nominal[param] - types[station][currentType]->toleranceLow[param]) * 0.05;
	 minValueChart[station] = minValue - (types[station][currentType]->nominal[param] - types[station][currentType]->toleranceLow[param]);
	float maxValue = (types[station][currentType]->nominal[param] + types[station][currentType]->toleranceHigh[param]) * 0.05;
	 maxValueChart[station] = maxValue + (types[station][currentType]->nominal[param] + types[station][currentType]->toleranceHigh[param]);


	axisY[station]->setRange(minValueChart[station], maxValueChart[station]);
	//chart->addAxis(axisY, Qt::AlignLeft);
	//series0[station]->attachAxis(axisY[station]);
	series0[station]->setBarWidth(1);

	QColor color("green");
	set0[station]->setColor(color);
	QColor color1("red");
	setBad[station]->setColor(color1);
	
	set0[station]->remove(0, set0[station]->count());
	
	setBad[station]->remove(0, setBad[station]->count());
	
	int stop = counter - 1;
	if (stop < 0)
		stop = CHART_DATA_SIZE - 1;
	int count = 0;
	count = counter;
	for (int i = 0; i < CHART_DATA_SIZE; i++)
	{
		if (dataArray[station][param][count] > 0)
		{
			if ((dataArray[station][param][count] < types[station][currentType]->toleranceHigh[param] + types[station][currentType]->nominal[param]) && (dataArray[station][param][count] > types[station][currentType]->toleranceLow[param] + types[station][currentType]->nominal[param]))
			{
				set0[station]->append(dataArray[station][param][count]);
				setBad[station]->append(0);
			}
			else
			{
				setBad[station]->append(dataArray[station][param][count]);
				set0[station]->append(0);
			}
		}
		count++;
		if (count >= CHART_DATA_SIZE)
			count = 0;
	}


	chart[station]->axisX()->setRange(0, CHART_DATA_SIZE);
	chart[station]->axisY()->setRange(minValueChart[station], maxValueChart[station]);

	int testCount = 0;
	/*for (int i = 0; i < CHART_DATA_SIZE; i++)
	{
	
	}
	dataArray[param][0] = 30;
	dataArray[param][1] = 30;
	dataArray[param][2] = 30;
	dataArray[param][3] = 15;
	dataArray[param][4] = 15;
	dataArray[param][5] = 50;
	dataArray[param][6] = 50;
	dataArray[param][7] = 50;
	dataArray[param][8] = 30;
	dataArray[param][9] = 30;*/
	//chartGauss
	seriesG[station]->clear();
	chartG[station]->setTitle(QString("GAUSS STATION %1: %2").arg(station).arg(types[station][currentType]->name[param]));
	float maxMeasurement = 0;
	float  razpon =( maxValueChart[station] - minValueChart[station]) /GAUSS_SIZE;
	float minMeja = minValueChart[station];
	float maxMeja = minValueChart[station] + razpon;
	float range[GAUSS_SIZE];
	int nrOfMeasurements[GAUSS_SIZE];
	for (int i = 0; i < GAUSS_SIZE; i++)
	{
		nrOfMeasurements[i] = 0;
		for (int j = 0; j < CHART_DATA_SIZE; j++)
		{
			if ((dataArray[station][param][j] < maxMeja) && (dataArray[station][param][j] > minMeja))
				nrOfMeasurements[i]++;
		}
		minMeja = minMeja + razpon;
		maxMeja = maxMeja + razpon;
		range[i] = minMeja;
	}

	for (int i = 0; i < GAUSS_SIZE; i++)
	{
		if (nrOfMeasurements[i] > maxMeasurement)
			maxMeasurement = nrOfMeasurements[i];

		seriesG[station]->append(range[i], nrOfMeasurements[i]);
		//setG->append(nrOfMeasurements[i]);
	}
	seriesG[station]->setPointsVisible(true);
	chartG[station]->axisY()->setRange(0, maxMeasurement + 1);
	chartG[station]->axisX()->setRange(minValueChart[station], maxValueChart[station]);

}

void MBsoftware::UpdateChartData(int station, int param, int currCounter)
{
	int stop  = currCounter - 1;
	if (stop == -1)
		stop = CHART_DATA_SIZE - 1;
	


	if (set0[station]->count() > CHART_DATA_SIZE)//shifta podatke 
	{
		set0[station]->remove(0, 1);
		setBad[station]->remove(0, 1);
		//for (int i = 0; i < CHART_DATA_SIZE; i++)
		//{
			if ((dataArray[station][param][stop] < types[station][currentType]->toleranceHigh[param] + types[station][currentType]->nominal[param]) && (dataArray[station][param][stop] > types[station][currentType]->toleranceLow[param] + types[station][currentType]->nominal[param]))
			{
				set0[station]->append(dataArray[station][param][stop]);
					setBad[station]->append(0);
			}
			else
			{
				setBad[station]->append(dataArray[station][param][stop]);
				set0[station]->append(0);
			}
		//}
	}
	else
	{
		if ((dataArray[station][param][stop]< types[station][currentType]->toleranceHigh[param] + types[station][currentType]->nominal[param]) && (dataArray[station][param][stop] > types[station][currentType]->toleranceLow[param] + types[station][currentType]->nominal[param]))
		{
			set0[station]->append(dataArray[station][param][stop]);
			setBad[station]->append(0);
		}
		else
		{
			setBad[station]->append(dataArray[station][param][stop]);
			set0[station]->append(0);
		}
	}




	chart[station]->axisX()->setRange(0, CHART_DATA_SIZE);
	chart[station]->axisY()->setRange(minValueChart[station], maxValueChart[station]);


	


	if (currCounter == 0)
	{
		float maxMeasurement = 0;
		float  razpon = (maxValueChart[station] - minValueChart[station]) / GAUSS_SIZE;
		float minMeja = minValueChart[station];
		float maxMeja = minValueChart[station] + razpon;
		int nrOfMeasurements[GAUSS_SIZE];
		float range[GAUSS_SIZE];
		for (int i = 0; i < GAUSS_SIZE; i++)
		{
			nrOfMeasurements[i] = 0;
			for (int j = 0; j < CHART_DATA_SIZE; j++)
			{
				if ((dataArray[station][param][j] < maxMeja) && (dataArray[station][param][j] > minMeja))
					nrOfMeasurements[i]++;
			}
			minMeja = minMeja + razpon;
			maxMeja = maxMeja + razpon;
			range[i] = minMeja;
		}

		seriesG[station]->clear();
		//seriesG->remove(0, seriesG->count());
		for (int i = 0; i < GAUSS_SIZE; i++)
		{
			if (nrOfMeasurements[i] > maxMeasurement)
				maxMeasurement = nrOfMeasurements[i];
			seriesG[station]->append(range[i], nrOfMeasurements[i]);
			//setG->append(nrOfMeasurements[i]);
		}


	
		chartG[station]->axisX()->setRange(minValueChart[station], maxValueChart[station]);
		chartG[station]->axisY()->setRange(0, maxMeasurement + 1);
	}

	//setG->append()
	this->update();
	
}

void MBsoftware::ResetChartData()
{
	for (int k = 0; k < NR_STATIONS; k++)
	{
		for (int i = 0; i < 11; i++)
		{
			for (int j = 0; j < CHART_DATA_SIZE; j++)
			{
				dataArray[k][i][j] = 0;
			}
		}
		UpdateChart(k, chartParam[k], charDataCounter[k]);
	}
}






void MBsoftware::ViewTimer()
{
	int bla = 10;

	QFont font;
	if (viewTimerTimer.timeCounter >= 10)
	{
		viewTimerTimer.SetStop();
		viewTimerTimer.CalcFrequency(viewTimerTimer.timeCounter);
		viewTimerTimer.timeCounter = 0;
		viewTimerTimer.SetStart();
	}

	viewTimerTimer.timeCounter++;

	if (isActiveWindow())
	{
		grabKeyboard();
	}
	else
		releaseKeyboard();




	if (login->currentRights > 0) //uporabnik prijavljen
	{
		ui.labelLogin->setText(QString("%1 Logged in!").arg(login->currentUser));
		ui.labelLogin->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
	}
	else
	{
		ui.labelLogin->setText(QString("User logged out!"));
		ui.labelLogin->setStyleSheet(QStringLiteral("background-color: rgb(236, 236, 236);"));
	}

	if (processStatus == -2) //process status 
	{
		ui.labelMode->setText(QString("STATUS: EMG STOP!!"));
		ui.labelMode->setStyleSheet(QStringLiteral("background-color: rgb(255, 0, 0);"));
	}
	else if ((processStatus == -1)) //PRESS START 
	{
		ui.labelMode->setText(QString("STATUS: Press 'E' RESET EMG"));
		ui.labelMode->setStyleSheet(QStringLiteral("background-color: rgb(128, 0, 0);"));
	}
	else if ((processStatus == 0)) //PRESS START 
	{
		ui.labelMode->setText(QString("STATUS: PRESS START!!"));
		ui.labelMode->setStyleSheet(QStringLiteral("background-color: rgb(236, 236, 236);"));
	}
	else if ((processStatus == 1)) //SCANNER RUNNING
	{
		ui.labelMode->setText(QString("STATUS: SCANNER RUNNING!"));
		ui.labelMode->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
	}
	else if ((processStatus == 2)) //CLEANING
	{
		ui.labelMode->setText(QString("STATUS: CLEANING!!"));
		ui.labelMode->setStyleSheet(QStringLiteral("background-color: rgb(255,255,0);"));
	}
	else if ((processStatus == 3)) //START CLEANING
	{
		ui.labelMode->setText(QString("STATUS: START CLEANING!!"));
		ui.labelMode->setStyleSheet(QStringLiteral("background-color: rgb(255,255,0);"));
	}
	else if ((processStatus == 4)) //AUTO  STOP 
	{
		ui.labelMode->setText(QString("STATUS: AUTO STOP: %1[s]").arg(settings->autostopTime - pauseCounter /1000));
		ui.labelMode->setStyleSheet(QStringLiteral("background-color: rgb(255,255,0);"));
	}
	else if ((processStatus == 5)) //AUTO  STOP 
	{
		ui.labelMode->setText(QString("STATUS:EXTERNAL PAUSE "));
		ui.labelMode->setStyleSheet(QStringLiteral("background-color: rgb(255,255,0);"));
	}
	else if ((processStatus == 10)) //test mode
	{
		ui.labelMode->setText(QString("STATUS: TEST MODE"));
		ui.labelMode->setStyleSheet(QStringLiteral("background-color:  rgb(255,255,0);"));
	}

	int total;
	float goodProc, badProc, darkProc;
	for (int i = 0; i < NR_STATIONS; i++)
	{
		 total = types[i][currentType]->goodMeasurementsCounter + types[i][currentType]->badMeasurementsCounter;
		 goodProc = (float)types[i][currentType]->goodMeasurementsCounter / (float)total * 100;
		 badProc = (float)types[i][currentType]->badMeasurementsCounter / (float)total * 100;
		 darkProc = (float)types[i][currentType]->conditionalMeasurementsCounter / (float)total * 100;

		QString goodString(QString("%1").number(goodProc, 'f', 2));
		goodString.append(" %");
		QString badString(QString("%1").number(badProc, 'f', 2));
		badString.append(" %");
		QString darkString(QString("%1").number(darkProc, 'f', 2));
		darkString.append(" %");
		if (i == 0)
		{
			ui.labelGood_2->setText(QString("%1").arg(types[i][currentType]->goodMeasurementsCounter));
			ui.labelBAD_2->setText(QString("%1").arg(types[i][currentType]->badMeasurementsCounter));
			ui.labelGoodProcent_2->setText(goodString);
			ui.labelBadProcent_2->setText(badString);
			ui.labelTotal_2->setText(QString("%1").arg(total));
		}
		else
		{
			ui.labelGood_3->setText(QString("%1").arg(types[i][currentType]->goodMeasurementsCounter));
			ui.labelBAD_3->setText(QString("%1").arg(types[i][currentType]->badMeasurementsCounter));
			ui.labelGoodProcent_3->setText(goodString);
			ui.labelBadProcent_3->setText(badString);
			ui.labelTotal_3->setText(QString("%1").arg(total));
		}


	}
	int allGood = types[0][currentType]->goodMeasurementsCounter + types[1][currentType]->goodMeasurementsCounter;
	int allBad = types[0][currentType]->badMeasurementsCounter + types[1][currentType]->badMeasurementsCounter;
	int allTotal = allGood + allBad;;


	 //total = types[0][currentType]->goodMeasurementsCounter + types[0][currentType]->conditionalMeasurementsCounter + types[0][currentType]->badMeasurementsCounter;
	 goodProc = (float)allGood / (float)allTotal * 100;
	 badProc = allBad / (float)allTotal* 100;
	

	QString goodString(QString("%1").number(goodProc, 'f', 2));
	goodString.append(" %");
	QString badString(QString("%1").number(badProc, 'f', 2));
	badString.append(" %");
	ui.labelGood->setText(QString("%1").arg(allGood));

	ui.labelBAD->setText(QString("%1").arg(allBad));
	ui.labelGoodProcent->setText(goodString);

	ui.labelBadProcent->setText(badString);
	ui.labelTotal->setText(QString("%1").arg(allTotal));
	

	ui.labelTakt->setText(QString("All: %1 PCS/min").arg(taktCounter*4));
	ui.labelTaktGood->setText(QString("Good: %1 PCS/min").arg(taktGoodCounter*4));


	QString vib;
	QString ele;

	//vibrator in elevator status
	if (vibratorStatus == 1)
	{
		ui.labelVibrator->setText(QString("VIBRATOR BOWL: ON"));
		ui.labelVibrator->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
	}
	else if (vibratorStatus == 2)
	{
		ui.labelVibrator->setText(QString("VIBRATOR BOWL: SWITCH OFF"));
		ui.labelVibrator->setStyleSheet(QStringLiteral("background-color: rgb(250, 170, 0);"));
	}
	else if (vibratorStatus == 0)
	{
		ui.labelVibrator->setText(QString("VIBRATOR BOWL: OFF"));
		ui.labelVibrator->setStyleSheet(QStringLiteral("background-color: rgb(85, 170, 255);"));
	}

	if (elevatorStatus == 1)
	{
		ui.labelElevator->setText(QString("ELEVATOR: ON"));
		ui.labelElevator->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
	}
	else if (elevatorStatus == 2)
	{
		ui.labelElevator->setText(QString("ELEVATOR: SWITCH OFF"));
		ui.labelElevator->setStyleSheet(QStringLiteral("background-color: rgb(250, 170, 0);"));
	}
	else if (elevatorStatus == 0)
	{
		ui.labelElevator->setText(QString("ELEVATOR: OFF"));
		ui.labelElevator->setStyleSheet(QStringLiteral("background-color: rgb(85, 170, 255);"));
	}

	if (settings->workingMode == 0)
	{
		ui.labelWorkingMode->setText(QString("MODE: NON-STOP"));
		ui.labelPieceInBoxAll->setText(QString("inf."));
		//ui.labelElevator->setStyleSheet(QStringLiteral("background-color: rgb(85, 170, 255);"));
	}
	else if (settings->workingMode == 1)
	{
		ui.labelWorkingMode->setText(QString("MODE: FULL BOX STOP"));
		ui.labelPieceInBoxAll->setText(QString("%1").arg(settings->pieceInBox));
		//ui.labelElevator->setStyleSheet(QStringLiteral("background-color: rgb(85, 170, 255);"));
	}
	else
	{
		ui.labelWorkingMode->setText(QString("MODE: FULL BOX DELAY"));
		ui.labelPieceInBoxAll->setText(QString("%1").arg(settings->pieceInBox));
		//ui.labelElevator->setStyleSheet(QStringLiteral("background-color: rgb(85, 170, 255);"));
	}

	ui.labelPieceInBox->setText(QString(" %1 ").arg(pieceInBox));


	QString text;
	for (int i = 0; i < cam.size(); i++)
	{
		if ((cam[i]->isLive == 1) && (cam[i]->isLiveOld == false))
		{
			text = QString("Camera %1 ONLINE").arg(i);
			AddLog(text, 0);
			cameraErrorIsSet[i] = 0;
		}
		if ((cam[i]->isLive == 0) && (cameraErrorIsSet[i] == 0))
		{
			text = QString("Camera %1 OFFLINE").arg(i);
			AddLog(text, 1);
			cameraErrorIsSet[i] = 1;
		}
		cam[i]->isLiveOld = cam[i]->isLive;
	}

	/*noDowelsPrevLog = noDowelsLog;
	if ((machineStartLog == 1) && (machineStartPrevLog == 0))
	{
		text = QString("MACHINE START");
		AddLog(text, 0);
	}
	machineStartPrevLog = machineStartLog;

	if ((machineStopLog == 1) && (machineStopPrevLog == 0))
	{
		text = QString("MACHINE START");
		AddLog(text, 3);
	}
	machineStartPrevLog = machineStopLog;*/

	PopulateStatusBox();
	DrawMeasurements();
	ShowErrorMessage();

	//ui.labelLogin->setText(QString("%1 Logged in!").arg(login->currentUser));
	for (int i = 0; i < NR_STATIONS; i++)
	{
		if (i == 0)
		{
			if (lamelaIndex[i] > -1)
			{
				ui.labelSpeed0->setText(QString("Speed: %1 [m/s]").arg(measureObject.speed[i][lamelaIndex[i]]));
			}
		}
		else
			if (lamelaIndex[i] > -1)
			{
				ui.labelSpeed1->setText(QString("Speed: %1 [m/s]").arg(measureObject.speed[i][lamelaIndex[i]]));
			}

	}



	for (int i = 0; i < NR_STATIONS; i++)
	{
		if (updateImage[i] > 0)
		{
			UpdateImageView(i);//popravim sliko na samo eni postaji
			UpdateChartData(i,chartParam[i], charDataCounter[i]);//in dopolnim graf na eni pistaji
			updateImage[i] = 0;
		}
	}

	//this->update();
}

void MBsoftware::ViewTimer2()
{
	if (viewTimerTimer2.timeCounter >= 10)
	{
		viewTimerTimer2.SetStop();
		viewTimerTimer2.CalcFrequency(viewTimerTimer2.timeCounter);
		viewTimerTimer2.timeCounter = 0;
		viewTimerTimer2.SetStart();
	}

	viewTimerTimer2.timeCounter++;

	writeCountersTimer++;

	if (writeCountersTimer % 5 == 0)
	{
		types[0][currentType]->WriteCounters();
		types[1][currentType]->WriteCounters();
		SaveVariables();
	}
	for (int k = 0; k < NR_STATIONS; k++)//tukaj tudi porravljam tolerance za obe postaji naenkrat.
	{
		if ((chartParam[k] != chartParamPrev[k]) || (types[k][currentType]->parametersChanged == true))
		{

			UpdateChart(k, chartParam[k], charDataCounter[k]);

			if (types[k][currentType]->parametersChanged == true)
			{
				types[k][currentType]->parametersChanged = false;
				if (k == 0)
				{
					for (int i = 0; i < types[k][currentType]->parameterCounter; i++)
	//				for (int i = 0; i < 9; i++)
					{
						types[1][currentType]->toleranceHigh[i] = types[0][currentType]->toleranceHigh[i];
						types[1][currentType]->toleranceLow[i] = types[0][currentType]->toleranceLow[i];
						types[1][currentType]->nominal[i] = types[0][currentType]->nominal[i];
						types[1][currentType]->offset[i] = types[0][currentType]->offset[i];
						types[1][currentType]->name[i] = types[0][currentType]->name[i];
						types[1][currentType]->isActive[i] = types[0][currentType]->isActive[i];
						types[1][currentType]->isConditional[i] = types[0][currentType]->isConditional[i];

						types[1][currentType]->WriteParameters();
						types[1][currentType]->OnUpdate();

					}
				}
				else
				{
					for (int i = 0; i < types[k][currentType]->parameterCounter; i++)
					{
						types[0][currentType]->toleranceHigh[i] = types[1][currentType]->toleranceHigh[i];
						types[0][currentType]->toleranceLow[i] = types[1][currentType]->toleranceLow[i];
						types[0][currentType]->nominal[i] = types[1][currentType]->nominal[i];
						types[0][currentType]->offset[i] = types[1][currentType]->offset[i];
						types[0][currentType]->name[i] = types[1][currentType]->name[i];
						types[0][currentType]->isActive[i] = types[1][currentType]->isActive[i];
						types[0][currentType]->isConditional[i] = types[1][currentType]->isConditional[i];
						types[0][currentType]->WriteParameters();
						types[0][currentType]->OnUpdate();
					}
				}

			}


		}
		chartParamPrev[k] = chartParam[k];
	}

	if (typeChanged == true)
	{
		typeChanged = 0;
		ResetChartData();

	}
}

void MBsoftware::OnClickedShowLptButton(int lptNum)
{
	lpt[lptNum]->OnShowDialog();
}

void MBsoftware::OnClickedShowCamButton(int camNum)
{
	cam[camNum]->ShowDialog(login->currentRights);
}

void MBsoftware::OnClickedShowSmartCardButton(int cardNum)
{
	controlCardMB[cardNum]->ShowDialog();
}

void MBsoftware::OnClickedShowImageProcessing()
{
	QStringList list;
	QMessageBox::StandardButton reply;
	int stNum = 0;
	if (login->currentRights > 0)
	{
		imageProcess->ShowDialog(login->currentRights);
	}

	else
	{

		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			"You need to be logged in! Do you want to login?",
			QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
	imageProcess->ShowDialog(login->currentRights);
}

void MBsoftware::OnClickedSelectType(void)
{
	QStringList list;
	int stNum;
	int result;
	QMessageBox::StandardButton reply;

	if (login->currentRights > 0)
	{
		/*if (NR_STATIONS > 1)
		{
			for (int i = 0; i < NR_STATIONS; i++)
			{
				list.push_back(QString("%1").arg(i));
			}
			bool ok;
			QString text = QInputDialog::getItem(this, tr("select station"), tr("label"), list, 0, false, &ok);

			stNum = text.toInt();
		}
		else*/
			stNum = 0;

		DlgTypes dlg(this, types[stNum].size());

		for (int i = 0; i < types[stNum].size(); i++)
		{
			dlg.types[i] = types[stNum][i]->typeName;


		}
		result = dlg.ShowDialogSelectType(currentType);
		if (result >= 0)
		{
			prevType = currentType;
			currentType = result;
			SaveVariables();
			typeChanged = true;
			AddLog("Type Changed!", 3);
		}
	}
	else
	{
		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			"You need to be logged in! Do you want to login?",
			QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();


	int bla = 100;

}  
void MBsoftware::OnClickedEditTypes(void)
{
	QMessageBox::StandardButton reply;
	QStringList list;
	int stNum = 0;
	if (login->currentRights > 0)
	{
		if (NR_STATIONS > 1)
		{
			for (int i = 0; i < NR_STATIONS; i++)
			{
				list.push_back(QString("%1").arg(i));
			}
			bool ok;
			QString text = QInputDialog::getItem(this, tr("select station"), tr("label"), list, 0, false, &ok);

			stNum = text.toInt();
		}
		else
			stNum = 0;
		types[stNum][currentType]->OnShowDialog(login->currentRights);
	}

	else
	{
		
		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			"You need to be logged in! Do you want to login?",
			QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();

	

}
void MBsoftware::OnClickedRemoveTypes(void)
{
	bool ok;
	QMessageBox box;
	QInputDialog dialog;
	QStringList list;
	QString tip;
	QString destPath;
	QString sourcePath;
	QString sourcePropertyPath;
	QString sourcePath2;
	QString destPath2;
	QMessageBox::StandardButton reply;
	

	if (login->currentRights > 0)
	{

			if (types[0].size() > 1)
			{
				for (int i = 0; i < types[0].size(); i++)
					list.append(types[0][i]->typeName);

				dialog.setComboBoxItems(list);
				dialog.setWindowTitle("delete type");
				dialog.setLabelText("Select type to remove");
				if (dialog.exec() == IDOK)
				{
					for (int j = 0; j < NR_STATIONS; j++)
					{
						tip = dialog.textValue();
						destPath = QDir::currentPath() + QString("/%1/types/types_Station%2/DeletedTypes").arg(REF_FOLDER_NAME).arg(j);
						destPath2 = destPath;
						if (!QDir(destPath).exists())
							QDir().mkdir(destPath);

						destPath += QString("/%1_deleted.ini").arg(tip);
						destPath2 += QString("/counters_%1_deleted.txt").arg(tip);

						sourcePath = QDir::currentPath() + QString("/%1/types/types_Station%2").arg(REF_FOLDER_NAME).arg(j);
						sourcePath2 = sourcePath + QString("/counters_%1.txt").arg(tip);
						sourcePath += QString("/%1.ini").arg(tip);



						//QFile::rename(sourcePath, destPath);
						//QFile::rename(sourcePath2, destPath2);

						QFile::copy(sourcePath, destPath);
						QFile::copy(sourcePath2, destPath2);

						QFile::remove(sourcePath);
						QFile::remove(sourcePath2);

						int cur = 0;
						for (int i = 0; i < types[j].size(); i++)
						{
							if (tip == types[j][i]->typeName)
							{
								types[j].erase(types[j].begin() + i);
							}
						}
						//izbrisemo tudi dynamicVariables za property tipov 
						sourcePropertyPath = QDir::currentPath() + QString("/%1/dynamicVariables/types_Station%2/parameters_%3").arg(REF_FOLDER_NAME).arg(j).arg(tip);

						QDir propDir(sourcePropertyPath);
						propDir.removeRecursively();


						prevType = currentType;
						currentType = 0;
						types[j][currentType]->parametersChanged = true;
					}
				}

			}
			else
			{
				box.setStandardButtons(QMessageBox::Ok);
				box.setIcon(QMessageBox::Warning);
				box.setText("unable to delete all types!");
				box.exec();
			}
		

	}
	else
	{
	reply = QMessageBox::question(this, tr("QMessageBox::question()"),
		"You need to be logged in! Do you want to login?",
		QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();

}
void MBsoftware::OnClickedAddTypes(void)//zaenkrat kopira celoten TIP 
{
	bool ok;
	QInputDialog dialog;
	QStringList list;
	QString tip;
	QString filePath;
	QMessageBox::StandardButton reply;

	if (login->currentRights > 0)
	{

		dialog.setWindowTitle("Add new type");
		dialog.setLabelText("Insert type name:");

		if (dialog.exec() == IDOK)
		{
			tip = dialog.textValue();

			for (int j = 0; j < NR_STATIONS; j++)
			{
				filePath = QDir::currentPath() + QString("//%1/types/types_Station%2").arg(REF_FOLDER_NAME).arg(j);
				types[j].push_back(new Types);

				types[j].back()->stationNumber = 0;
				types[j].back()->Init(tip, filePath);

				for (int i = 0; i < types[j][currentType]->parameterCounter; i++)
				{
					types[j].back()->toleranceHigh.push_back(types[j][currentType]->toleranceHigh[i]);
					types[j].back()->toleranceLow.push_back(types[j][currentType]->toleranceLow[i]);
					types[j].back()->toleranceHighCond.push_back(types[j][currentType]->toleranceHighCond[i]);
					types[j].back()->toleranceLowCond.push_back(types[j][currentType]->toleranceLowCond[i]);
					types[j].back()->correctFactor.push_back(types[j][currentType]->correctFactor[i]);
					types[j].back()->offset.push_back(types[j][currentType]->offset[i]);
					types[j].back()->nominal.push_back(types[j][currentType]->nominal[i]);
					types[j].back()->isActive.push_back(types[j][currentType]->isActive[i]);
					types[j].back()->isConditional.push_back(types[j][currentType]->isConditional[i]);
					types[j].back()->name.push_back(types[j][currentType]->name[i]);

					types[j].back()->AddParameter(i);
					types[j].back()->measuredValue.push_back(0);
					types[j].back()->isGood.push_back(0);
					types[j].back()->conditional.push_back(0);
				}
				types[j].back()->parameterCounter = types[j][currentType]->parameterCounter;
				types[j].back()->WriteParameters();
				types[j].back()->WriteCounters();


				for (int i = 0; i < types[j][currentType]->prop.size(); i++)
				{
					types[j].back()->prop[i] = types[j][currentType]->prop[i];

				}
				imageProcess->ConnectTypes(j,types[j]);

				for (int i = 0; i < types[j][currentType]->prop.size(); i++)
				{
					imageProcess->SaveFunctionParametersOnNewType(j,types[j].size() - 1, i);
				}
			}

		}
	}
	else
	{
		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			"You need to be logged in! Do you want to login?",
			QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();
}
void MBsoftware::OnClickedTeachType(void)
{
	QMessageBox::StandardButton reply;


		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			"Set new reference for current type?",
			QMessageBox::Yes | QMessageBox::No);

		if (reply == QMessageBox::Yes)
		{
			reply = QMessageBox::information(this, tr("QMessageBox::information()"),
				"Make sure glass is clean and manual send piece thru scanner",
				QMessageBox::Yes);
		}


			teachMode[0] = 1;
			teachMode[1] = 1;


}
void MBsoftware::OnClickedResetCounters(void)
{
	QMessageBox::StandardButton reply;
	reply = QMessageBox::question(this, tr("QMessageBox::question()"),
		"Reset piece Counter?",
		QMessageBox::Yes | QMessageBox::No);

	if (reply == QMessageBox::Yes)
	{
		for (int i = 0; i < NR_STATIONS; i++)
		{
			types[i][currentType]->ResetCounters();
			types[i][currentType]->WriteCounters();
		}
		pieceInBox = 0;
		ResetChartData();
	}

}
void MBsoftware::OnClickedLogin(void)
{
	
	login->ShowDialog();
}
void MBsoftware::OnClickedStatusBox(void)
{
	statusBox[0]->ShowDialog();

}
void MBsoftware::OnClickedSmcButton(void)
{
	if(smc.size() > 0)
	smc[0]->ShowDialog();
}
void MBsoftware::OnClickedShowHistory(void)
{
	QStringList list;
	int stNum;
	if (NR_STATIONS > 1)
	{
		for (int i = 0; i < NR_STATIONS; i++)
		{
			list.push_back(QString("Station: %1").arg(i));
		}
		bool ok;
		QString text = QInputDialog::getItem(this, tr("select station"), tr("label"), list, 0, false, &ok);

		for (int i = 0; i < list.size(); i++)
		{
			if (text == list[i])
				stNum = i;
		}
	}
	else
		stNum = 0;

	history->OnShowDialog(stNum);
}
void MBsoftware::OnClickedGeneralSettings(void)
{

	QMessageBox::StandardButton reply;
	if ((login->currentRights == 1) || (login->currentRights == 2))
		settings->OnShowDialog();
	else
	{

		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			"You need Administrator or operaters rights to change settings!",
			QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();
	
}
void MBsoftware::OnClickedResetGoodBox(void)
{
	QMessageBox::StandardButton reply;
	reply = QMessageBox::question(this, tr("QMessageBox::question()"),
		"Reset GOOD BOX piece counters?",
		QMessageBox::Yes | QMessageBox::No);

	if (reply == QMessageBox::Yes)
	{
		pieceInBox = 0;
	}

}
void MBsoftware::OnClickedAboutUs()
{
	about->OnShowDialog();
}
void MBsoftware::onClientReadyRead()
{
	QString msg = client->readAll();

	/*if (msg == "Connection established")
		tcpConnection = true;
	client->write("OK!");
	client->flush();
	*/

}
void MBsoftware::OnClickedTCPconnction(int index)
{
	if (tcp.size() >= index)
		tcp[index]->OnShowDialog();
}
void MBsoftware::SaveVariables()
{
	QString fileName;
	QString line;
	QString dirPath;
	QDir dir;


	dirPath = QDir::currentPath() + QString("/%1/DynamicVariables/").arg(REF_FOLDER_NAME);

	if (!QDir(dirPath).exists())
		QDir().mkdir(dirPath);


	dir.setPath(dirPath);

	fileName = dirPath + "currentVariables.txt";
	QFile file(fileName);
	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QTextStream stream(&file);

		stream << currentType;
		stream << endl;
		stream << pieceInBox;
		stream << endl;
		stream << chartParam[0];
		stream << endl;
		stream << chartParam[1];
		stream << endl;

		file.close();
	}
}
void MBsoftware::LoadVariables()
{
	QString fileName;
	QString line;
	QString dirPath;
	QDir dir;


	dirPath = QDir::currentPath() + QString("/%1/DynamicVariables/").arg(REF_FOLDER_NAME);

	if (!QDir(dirPath).exists())
		QDir().mkdir(dirPath);


	dir.setPath(dirPath);

	fileName = dirPath + "currentVariables.txt";
		QFile file(fileName);
		if (file.open(QIODevice::ReadOnly))
		{
			QTextStream stream(&file);

			line = stream.readLine();
			currentType = line.toInt();

			line = stream.readLine();
			pieceInBox = line.toInt();
			line = stream.readLine();
			chartParam[0] = line.toInt();
			line = stream.readLine();
			chartParam[1] = line.toInt();
			file.close();
		}
		else
		{
			currentType = 0;
			chartParam[0] = 0;
			chartParam[1] = 0;
			pieceInBox = 0;
		}

		if (currentType >= types[0].size())
			currentType = 0;
		//prevType = currentType;
}
void MBsoftware::mouseDoubleClickEvent(QMouseEvent * e)
{
	QRect containtRect;
	int windowPos = -1;
	int nrStation = -1;
	int chartSelected = -1;
	bool ok;
	int curr;
	if (e->button() == Qt::LeftButton)
	{

		//qDebug() << e->pos();
		containtRect = ui.imageViewMainWindow_0->geometry();
		if (containtRect.contains(e->pos()) == true)
		{

				windowPos = 0;
				nrStation = 0;

		}
		containtRect = ui.imageViewMainWindow_1->geometry();
		if (containtRect.contains(e->pos()) == true)
		{
			containtRect = ui.imageViewMainWindow_1->geometry();
			if (containtRect.contains(e->pos()) == true)
			{
				windowPos = 0;
				nrStation = 1;
			}
		}
		

		if (windowPos >-1)
		{
			imageProcess->SetCurrentBuffer(nrStation, windowPos);
			imageProcess->ShowDialog(login->currentRights);
		}

		containtRect = ui.frameChart->geometry();
		if (containtRect.contains(e->pos()) == true)
		{
			chartSelected = 1;

		}
		containtRect = ui.frameChart_2->geometry();
		if (containtRect.contains(e->pos()) == true)
		{
			chartSelected = 1;
		}

		containtRect = ui.frameChart_3->geometry();
		if (containtRect.contains(e->pos()) == true)
		{
			chartSelected = 2;

		}
		containtRect = ui.frameChart_4->geometry();
		if (containtRect.contains(e->pos()) == true)
		{
			chartSelected = 2;
		}

		if (chartSelected > 0)
		{
			curr = QInputDialog::getInt(this, tr("Select Parameter"), tr("parameter:"), 0, 1,types[0][currentType]->parameterCounter, 1, &ok, NULL);
			if (ok)
				chartParam[chartSelected-1] = curr-1;
		}

	}

	

}

void MBsoftware::keyPressEvent(QKeyEvent *event)
{	
 	qDebug() << event->key();
	QString fileP;
	CPointFloat points[3];
	int i = 0;
	QString name;

	switch (event->key())
	{
	case  Qt::Key_1:
		testValveTop = 1;

		break;
	case  Qt::Key_2:

		testValveBottom = 1;
	
		
		break;

	case  Qt::Key_3:
	


		break;
	case  Qt::Key_4:
		dataArray[0][chartParam[0]][countTest] = value;
		countTest++;
		if (countTest >= CHART_DATA_SIZE)
		{
			countTest = 0;
			value = 50;
		}
		UpdateChartData(0,chartParam[0], countTest);
		value = value + 0.5;


		//int count = set0->count();
		//series0->attachAxis(axisY);
		//series0->attachAxis(axisX);


		chart[0]->axisX()->setRange(0, CHART_DATA_SIZE);
		chart[0]->axisY()->setRange(minValueChart[0], maxValueChart[0]);
		this->update();
		break;
	case  Qt::Key_5:
		MeasurePiece(0, 0);

	case  Qt::Key_6:
		MeasurePiece(1, 0);
		break;
	case  Qt::Key_7:
		for (int i = 0; i < 500; i++)
		{
			if(cam[0]->imageReady[i] > 0)
			cam[0]->image[i].SaveBuffer(QString("C:\\images\\%1.bmp").arg(i));
		}

		break;
	case  Qt::Key_8:
		
		break;

		break;
	case  Qt::Key_9:

		break;
	case  Qt::Key_F4:
		break;

	case  Qt::Key_F6:


		break;

	case  Qt::Key_A:
		cam[0]->DisableLive();
	
		break;
	case  Qt::Key_S:
		sprejetaCounter[0] = 0;
		sprejetaCounter[1] = 0;
		cam[0]->imageIndex = 0;
		///cam[0]->EnableLive();
		break;

	case  Qt::Key_R:
		cam[0]->imageIndex = 0;
		cam[1]->imageIndex = 0;
		sprejetaCounter[0] = 0;
		sprejetaCounter[1] = 0;


		break;

	case  Qt::Key_T:



		break;
	case  Qt::Key_E:
		emgMode = 0;

		break;
	case  Qt::Key_G:
		

		break;
	case  Qt::Key_I:
		

			QtConcurrent::run(imageProcess, &imageProcessing::ProcessingCamera2, 0, 0, 0);

		


		break;
	case  Qt::Key_J:


		break;
	case  Qt::Key_K:

		break;

	case Qt::Key_Space:
		if (startKey == 0)
			startKey = 1;
		else
			startKey = 0;
			break;
	case  Qt::Key_Escape:
	this->	repaint();
	for (int i = 0; i < cam.size(); i++)
	{
		if (cam[i]->isVisible())
			cam[i]->close();
	}

	for (int i = 0; i < smc.size(); i++)
	{
		if (smc[i]->isVisible())
			smc[i]->close();
	}
	for (int i = 0; i < controlCardMB.size(); i++)
	{
		if (controlCardMB[i]->isVisible())
			controlCardMB[i]->close();
	}
	for (int i = 0; i < statusBox.size(); i++)
	{
		if (statusBox[i]->isVisible())
			statusBox[i]->close();
	}
	for (int i = 0; i < lpt.size(); i++)
	{
		if (lpt[i]->isVisible())
			lpt[i]->close();
	}
	imageProcess->close();
		break;

	default:

		break;
	}
}

