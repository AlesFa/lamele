﻿#include "stdafx.h"
#include "imageProcessing.h"
#include "math.h"
#include "CLine.h"
#include "CPointFloat.h"
#include "CRectRotated.h"
#include "CCircle.h"
#include <conio.h>





std::vector<CCamera*>										imageProcessing::cam;
std::vector<CMemoryBuffer*>									imageProcessing::images;
CMemoryBuffer*												imageProcessing::currImage;
std::vector<Types*>											imageProcessing::types[NR_STATIONS];
Measurand*													imageProcessing::measureObject;
std::vector<Timer*>											imageProcessing::processingTimer;

Mat															imageProcessing::DrawImage;
vector<vector<vector<vector<vector<vector<float>>>>>>		imageProcessing::parameterTable;
QPoint														imageProcessing::origin;
float														imageProcessing::relativeFrequencies[256];


bool novaPalcka = true;
int processingcamera1Counter = 0;
int callProcessingCamera1Counter = 0;

imageProcessing::imageProcessing(QWidget *parent)
	: QWidget(parent)
{


	ui.setupUi(this);
	zoomFactor = 1;
	displayImage = 0;
	displayWindow = 0;
	prevDisplayImage = 0;
	prevDisplayWindow = 0;
	rowNum = 10;
	colNum = 4;
	currentStation = 0;
	isSceneAdded = false;


	ui.lineInsertImageIndex->setText("0");
	
	connect(ui.buttonImageUp, SIGNAL(pressed()), this, SLOT(OnPressedImageUp()));
	connect(ui.buttonImageDown, SIGNAL(pressed()), this, SLOT(OnPressedImageDown()));
	connect(ui.buttonCamDown, SIGNAL(pressed()), this, SLOT(OnPressedCamDown()));
	connect(ui.buttonCamUp, SIGNAL(pressed()), this, SLOT(OnPressedCamUp()));
	connect(ui.buttonZoomIn, SIGNAL(pressed()), this, SLOT(ZoomIn()));
	connect(ui.buttonReset, SIGNAL(pressed()), this, SLOT(ZoomReset()));
	connect(ui.buttonZoomOut, SIGNAL(pressed()), this, SLOT(ZoomOut()));
	connect(ui.buttonLoadImage, SIGNAL(pressed()), this, SLOT(OnLoadImage()));
	connect(ui.buttonLoadMultipleImages, SIGNAL(pressed()), this, SLOT(OnLoadMultipleImage()));
	connect(ui.buttonSaveImage, SIGNAL(pressed()), this, SLOT(OnSaveImage()));
	connect(ui.buttonProcessCam0, SIGNAL(pressed()), this, SLOT(OnProcessCam0()));
	connect(ui.buttonProcessCam1, SIGNAL(pressed()), this, SLOT(OnProcessCam1()));
	connect(ui.buttonProcessCam2, SIGNAL(pressed()), this, SLOT(OnProcessCam2()));
	connect(ui.buttonProcessCam3, SIGNAL(pressed()), this, SLOT(OnProcessCam3()));
	connect(ui.buttonProcessCam4, SIGNAL(pressed()), this, SLOT(OnProcessCam4()));


	connect(ui.addParameter, SIGNAL(pressed()), this, SLOT(onPressedAddParameter()));
	connect(ui.removeParameter, SIGNAL(pressed()), this, SLOT(onPressedRemoveParameters()));
	connect(ui.update, SIGNAL(pressed()), this, SLOT(onPressedUpdateParameters()));
	connect(ui.buttonSurfaceInspection, SIGNAL(pressed()), this, SLOT(onPressedSurfaceInspection()));
	connect(ui.imageView, SIGNAL(pressed()), this, SLOT(mousePressEvent()));
	connect(ui.imageView, SIGNAL(released()), this, SLOT(mouseReleaseEvent()));
	connect(ui.lineInsertImageIndex, SIGNAL(editingFinished()), this, SLOT(OnDoneEditingLineInsertImageIndex()));



	for (int i = 0; i < 7; i++)
	{
		processingTimer.push_back(new Timer());
	}
	rubberBand = new QRubberBand(QRubberBand::Rectangle, ui.imageView);//new rectangle band

	ui.imageView->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
	start = 0;


	variantManager = new QtVariantPropertyManager();
	

	topItem = variantManager->addProperty(QtVariantPropertyManager::groupTypeId(),
		QLatin1String("Function property"));

	variantFactory = new QtVariantEditorFactory();

	variantEditor = new QtTreePropertyBrowser();
	variantEditor->setFactoryForManager(variantManager, variantFactory);
	variantEditor->addProperty(topItem);
	variantEditor->setPropertiesWithoutValueMarked(true);
	variantEditor->setResizeMode(QtTreePropertyBrowser::ResizeMode::ResizeToContents);
	variantEditor->setRootIsDecorated(true);
	ui.ParameterLayout->addWidget(variantEditor);


	for (int i = 0; i < 50; i++) //za izpise kaj vracajo funkcije
	{
		ui.listFunctionsReturn->addItem("");
	}



	//variantEditor->show();


	for (int i = 0; i < 10; i++)
		threadsRunning[i] = false;
	//measureObject->currLamela = 0;
	int saveTmpIndex = 0;
	tmpPieceCounter[0] = 0;
	tmpPieceCounter[1] = 0;
	testMode = false;
	obdelanaCounter[0] = 0;
	obdelanaCounter[1] = 0;
}

void imageProcessing::closeEvent(QCloseEvent * event)
{
	ClearDraw();
}

imageProcessing::~imageProcessing()
{
	delete variantManager;
	delete variantFactory;
	delete variantEditor;
	qDeleteAll(scene->items());
	item.erase(item.begin(), item.end());
	cam.clear();
	delete scene;

	//delete item;
	//delete topItem;

}

void imageProcessing::ConnectImages(std::vector<CCamera*> inputCam, std::vector<CMemoryBuffer*> inputImages)
{

	cam = inputCam;
	images = inputImages;
	currImage = &cam[0]->image[0];
	displayWindow =0;
	displayImage = 0;

	ResizeDisplayRect();
	ShowImages();

}

void imageProcessing::ConnectMeasurand(Measurand * objects)
{

	measureObject = objects;
	
}

void imageProcessing::ConnectTypes(int station,std::vector<Types*> type)
{

		types[station] = type;

}


void imageProcessing::ConnectImages(std::vector<CCamera*> inputCam)
{
	cam = inputCam;

}


void imageProcessing::ShowDialog(int rights)
{	
	loginRights = rights;
	ReplaceCurrentBuffer();
	activateWindow();
	currentFunction = -1;
	for (int i = 0; i < item.size(); i++)
	{
		topItem->removeSubProperty(item[i]);
	}
	item.clear();
	//Če uporabnik nima administratorskih pravic se onemogoči nastavljanje parametrov slik
	ui.buttonProcessCam4->setEnabled(false);
	if ((rights == 1)|| (rights == 2))
	{
		if (rights == 1)
		{
			ui.addParameter->setEnabled(true);
			ui.removeParameter->setEnabled(true);
			ui.buttonProcessCam0->setEnabled(true);
			ui.buttonProcessCam1->setEnabled(true);
			ui.buttonProcessCam2->setEnabled(true);
			ui.buttonProcessCam3->setEnabled(true);

		}
		else
		{
			ui.addParameter->setEnabled(false);
			ui.removeParameter->setEnabled(false);
			ui.buttonProcessCam0->setEnabled(false);
			ui.buttonProcessCam1->setEnabled(false);
			ui.buttonProcessCam2->setEnabled(true);
			ui.buttonProcessCam3->setEnabled(false);

		}
		ui.update->setEnabled(true);
		ui.Cancel->setEnabled(true);

		
		ui.buttonSurfaceInspection->setEnabled(false);
	}

	else
	{
		ui.addParameter->setEnabled(false);
		ui.removeParameter->setEnabled(false);
		ui.update->setEnabled(false);
		ui.Cancel->setEnabled(false);
		ui.buttonProcessCam0->setEnabled(false);
		ui.buttonProcessCam1->setEnabled(false);
		ui.buttonProcessCam2->setEnabled(true);
		ui.buttonProcessCam3->setEnabled(false);
		ui.buttonSurfaceInspection->setEnabled(false);
	}
		setWindowState( Qt::WindowActive);
	show();
}

void imageProcessing::ClearDialog()
{
	int camNum = displayWindow;
	int imgNum = displayImage;;


	//Pobrišemo razporeditve parametrov
	

}

void imageProcessing::test()
{
	QFileDialog dialog;// uporabljen za load images from file
}

bool imageProcessing::eventFilter(QObject *obj, QEvent *event)
{
	uchar b = 0, g = 0, r = 0;
	QPoint point_mouse;
	QGraphicsLineItem * item;


	//QGraphicsScene::mousePressEvent(event);
	int x, y;

	if (event->type() == Qt::LeftButton)
	{
		int bla = 100;
	}

	if (event->type() == QEvent::MouseMove)
	{
		QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
		QPointF position = mouseEvent->pos();
		QPoint pos = QCursor::pos();


		QPointF scenePt = ui.imageView->mapToScene(mouseEvent->pos());

		//point_mouse = ui.imageView->mapFromScene(QCursor::pos());


		x = scenePt.x();
		y = scenePt.y();
		ui.labelX->setText(QString("X = %1").arg(x));
		ui.labelY->setText(QString("Y = %1").arg(y));

		if (currImage->buffer->empty())
			return false;

		if ((x < currImage->buffer[0].cols) && (y < currImage->buffer[0].rows) && (x > -1) && (y > -1))
		{
			if (currImage->buffer->channels() == 1)
			{
				b = g = r = currImage->buffer[0].data[currImage->buffer[0].channels()*(currImage->buffer[0].cols*y + x)];
			}
			else
			{
				b = currImage->buffer[0].data[currImage->buffer[0].channels()*(currImage->buffer[0].cols*y + x) + 0];
				g = currImage->buffer[0].data[currImage->buffer[0].channels()*(currImage->buffer[0].cols*y + x) + 1];
				r = currImage->buffer[0].data[currImage->buffer[0].channels()*(currImage->buffer[0].cols*y + x) + 2];
			}


			ui.labelG->setText(QString("G = %1").arg(g));
			ui.labelB->setText(QString("B = %1").arg(b));
			ui.labelR->setText(QString("R = %1 ").arg(r));


			return false;
		}


		return false;
	}


	if (event->type() == QEvent::MouseButtonRelease)
	{
		const QGraphicsSceneMouseEvent* const me = static_cast<const QGraphicsSceneMouseEvent*>(event);
		int bla = 10;


		


		return true;
	}

	return false;
}


void imageProcessing::OnLoadImage()
{
	Mat img;
	QString fileName = QFileDialog::getOpenFileName(this, tr("Load Image"), "C://", "bmp image (*.bmp)");
	String name;

	int channel;
	if (!fileName.isEmpty())
	{
		QMessageBox::information(this, tr("file loaded"), fileName);
		name = fileName.toStdString();

		img = imread(name, -1);
		*currImage->buffer = img;
		ReplaceCurrentBuffer();
	}


	

}

void imageProcessing::OnLoadMultipleImage()
{
	QFileDialog dialog(this);
	Mat img;
	String name;
	dialog.setDirectory(QDir::homePath());
	dialog.setFileMode(QFileDialog::ExistingFiles);
	dialog.setNameFilter(trUtf8("image file(*.bmp)"));
	dialog.setDirectory("C://");
	QStringList fileNames;
	if (dialog.exec())
		fileNames = dialog.selectedFiles();

	if (!fileNames.isEmpty())
	{
		for (int i = 0; i < fileNames.size(); i++)
		{
			name = fileNames[i].toStdString();
			img = imread(name, -1);
			if (displayWindow < cam.size())
			{
				if (i < cam[displayWindow]->image.size())
				{
					cam[displayWindow]->image[i].buffer[0] = img;
				}
			}
			else
			{
				if (i < images.size())
					*images[i]->buffer = img;

			}
		}
		QMessageBox::information(this, QString("file loaded"),QString("%1 files loaded").arg(fileNames.size()));
		ReplaceCurrentBuffer();
	}





}

void imageProcessing::OnSaveImage()
{
	
	//image.fill(Qt::red); // A red rectangle.
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save Image "),
		QString(),
		tr("Images (*.bmp)"));

	String name;
	name = fileName.toStdString();
	imwrite(name, *currImage->buffer);
	/*
	if (!fileName.isEmpty())
	{
	const QPixmap pixmap = ui.imageView->grab();
	pixmap.save(fileName);
	QMessageBox::information(this, tr("file saved"), fileName);

	}*/

}




void imageProcessing::ZoomOut()
{
	zoomFactor = zoomFactor - 0.1;
	if (zoomFactor > 0.09)
	{
		ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor * 100));
		//ui.imageView->fitInView(QRectF(0, 0, currImage->buffer[0].cols *zoomFactor, currImage->buffer[0].rows *zoomFactor), Qt::KeepAspectRatioByExpanding);
		ui.imageView->scale(0.9, 0.9);
	}
	else
	{
		zoomFactor = 0.1;
		ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor * 100));
	}

}

void imageProcessing::ZoomIn()
{

	zoomFactor = zoomFactor + 0.1;

	ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor*100));
	ui.imageView->scale(1.1, 1.1);

}

void imageProcessing::ZoomReset()
{

	zoomFactor = 1;
	ui.imageView->resetTransform();
	ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor*100));
	
}


void imageProcessing::ResizeDisplayRect()
{	
	ui.imageView->setGeometry(QRect(0, 0, currImage->buffer->cols, currImage->buffer->rows));
	ui.imageView->setAlignment(Qt::AlignTop | Qt::AlignLeft);
	ui.imageView->setSceneRect(QRect(0, 0, currImage->buffer->cols, currImage->buffer->rows));
}




void imageProcessing::ClearDraw()
{
	//dodamo na draw sliko 
	qDeleteAll(scene->items());
	//qDeleteAll(sceneForMainWindow->items());
	QImage qimgOriginal((uchar*)DrawImage.data, DrawImage.cols, DrawImage.rows, DrawImage.step, QImage::Format_RGB888);
	pixmapVideo = scene->addPixmap(QPixmap::fromImage(qimgOriginal));
	pixmapVideo->setPixmap(QPixmap::fromImage(qimgOriginal));
}
void imageProcessing::SetCurrentBuffer(int dispWindow, int dispImage)
{
	displayWindow = dispWindow;
	displayImage = dispImage;
	//ReplaceCurrentBuffer();
}

void imageProcessing::ReplaceCurrentBuffer()
{
	//if ((displayImage != prevDisplayImage) || (displayWindow != prevDisplayWindow))
	//{
		if (displayWindow == cam.size())
		{
			currImage = images[displayImage];
		}
		else
		{
			currImage = &cam[displayWindow]->image[displayImage];
		}

	if (displayWindow == cam.size())
	{
		ui.labelLocation->setText(QString("SAVED IMAGES: %1").arg(displayImage));
	}
	else
		ui.labelLocation->setText(QString("CAM: %1, IMAGE: %2").arg(displayWindow).arg(displayImage));

	ui.lineInsertImageIndex->setText(QString("%1").arg(displayImage));

	//if((displayImage != prevDisplayImage) || ( prevDisplayWindow != displayWindow)) // zaradi cudnega pojava na dialogu
	//ResizeDisplayRect();
	
	
	ClearDraw();
	ShowImages();


	prevDisplayImage = displayImage;
	prevDisplayWindow = displayWindow;

	if(displayWindow < cam.size())
	currentStation = displayWindow;



	ui.labelCurrStation->setText(QString("station: %1").arg(currentStation));
}



void imageProcessing::OnPressedImageUp()
{
	ClearDialog();
	displayImage++;
	if (displayWindow == cam.size())
	{
		if (displayImage > images.size()-1)
			displayImage = 0;
	}
	else
	{
		if (displayImage > cam[displayWindow]->num_images - 1)
			displayImage = 0;

	}
	ReplaceCurrentBuffer();
	//ReplaceParameters();

}

void imageProcessing::OnPressedImageDown()
{
	ClearDialog();
	displayImage--;
	if (displayWindow == cam.size())
	{
		if ((displayImage > images.size()) ||(displayImage < 0))
			displayImage = images.size()-1;
	}
	else
	{
		if((displayImage >= cam[displayWindow]->num_images - 1) || (displayImage < 0))
			displayImage = cam[displayWindow]->num_images - 1;

	}

	ReplaceCurrentBuffer();
	//ReplaceParameters();
}

void imageProcessing::OnPressedCamUp()
{	

	ClearDialog();
	displayImage = 0;
	displayWindow++;

	if ((displayWindow < 0) || (displayWindow > cam.size()))
		displayWindow = 0;

	ReplaceCurrentBuffer();
	//ReplaceParameters();
}

void imageProcessing::OnPressedCamDown()
{
	ClearDialog();
	displayImage = 0;
	displayWindow--;

	if ((displayWindow < 0) || (displayWindow > cam.size()))
		displayWindow = cam.size() ;

	ReplaceCurrentBuffer();
	
	//ReplaceParameters();
}

int imageProcessing::ConvertImageForDisplay(int imageNumber)
{
	Mat test;
	//Mat DrawImage;

	if (currImage->buffer->empty())
	{
		return 0;
	}

	if(currImage->buffer->channels() == 1)
		cvtColor(*currImage->buffer, test, COLOR_GRAY2RGB);
	else
		cvtColor(*currImage->buffer, test, COLOR_BGR2RGB);


	DrawImage = test(Rect(0, 0, test.cols, test.rows));
	
	//QImage qimgOriginal((uchar*)DrawImage.data, DrawImage.cols, DrawImage.rows, DrawImage.step, QImage::Format_RGB888);
	
	//showScene->setPixmap(QPixmap::fromImage(qimgOriginal));

		return 1;
}

void imageProcessing::ShowImages()
{
	if (ConvertImageForDisplay(0))
	{
		QImage qimgOriginal((uchar*)DrawImage.data, DrawImage.cols, DrawImage.rows, DrawImage.step, QImage::Format_RGB888);

		if (!isSceneAdded)
		{
			scene = new QGraphicsScene(this);
			sceneForMainWindow = new QGraphicsScene(this);
			ui.imageView->setScene(scene);
			setMouseTracking(true);
			ui.imageView->viewport()->installEventFilter(this);
			//scene->installEventFilter(this);
			
			pixmapVideo = scene->addPixmap(QPixmap::fromImage(qimgOriginal));
			pixmapVideo->setZValue(-100);
			isSceneAdded = true;
		}

		pixmapVideo->setPixmap(QPixmap::fromImage(qimgOriginal));
		zoomFactor = 1;
		ui.imageView->resetTransform();
		ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor*100));
		//ui.imageView->fitInView(QRectF(0, 0, currImage->buffer[0].cols *zoomFactor, currImage->buffer[0].rows *zoomFactor), Qt::KeepAspectRatioByExpanding);
	}
}
void imageProcessing::OnProcessCam0()
{
	currentFunction = 0;
	ClearDraw();
	PopulatePropertyBrowser(currentFunction);
	int imageNum = 0;
	bool ok;

	

	if (displayWindow < cam.size())
	{
		if (cam[displayWindow]->image.size() > 1)
		{
			imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, cam[displayWindow]->image.size() - 1, 1, &ok, NULL);
			if (ok)
				ProcessingCamera0(displayWindow, imageNum, 1);
		}
		else
		{
			imageNum = 0;
			ProcessingCamera0(displayWindow, imageNum, 1);
		}
	}


}
void imageProcessing::OnProcessCam1()//trenutno uporabljena za zdruzitev kosov
{
	int imageNum = 0;
	bool ok;
	ClearDraw();
	currentFunction = 1;
	PopulatePropertyBrowser(currentFunction);

	if (displayWindow < cam.size())
	{
		if (cam[displayWindow]->image.size() > 1)
		{
			imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, cam[displayWindow]->image.size() - 1, 1, &ok, NULL);
			if (ok)
				ProcessingCamera1(displayWindow, imageNum, 1);
		}
		else
		{
			imageNum = 0;
			ProcessingCamera1(displayWindow, imageNum, 1);
		}
	}
	else
	{
		ProcessingCamera1(displayWindow, imageNum, 1);

	}

}

void imageProcessing::OnProcessCam2()
{
	currentFunction = 2;
	ClearDraw();
	PopulatePropertyBrowser(currentFunction);

	int imageNum;
	bool ok;

	if (displayWindow < cam.size())
	{
		if (cam[displayWindow]->image.size() > 1)
		{
			imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, cam[displayWindow]->image.size() - 1, 1, &ok, NULL);
			if (ok)
				ProcessingCamera2(displayWindow, imageNum, 1);
		}
		else
		{
			/*imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, images.size() - 1, 1, &ok, NULL);

			if (ok)
				ProcessingCamera2(displayWindow, imageNum, 1);*/
		}
	}
	

}

void imageProcessing::OnProcessCam3()
{
	currentFunction = 3;
	PopulatePropertyBrowser(currentFunction);
		//ProcessingCamera3(0, 0, 1);


	int imageNum = 0;
	bool ok;

	if (displayWindow <= cam.size()-1)
	{
		if (cam[displayWindow]->image.size() > 1)
		{
			imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, cam[displayWindow]->image.size() - 1, 1, &ok, NULL);
			if (ok)
				ProcessingCamera3(displayWindow, imageNum, 1);
		}
		else
			ProcessingCamera3(displayWindow, imageNum, 1);
	}
	else
	{
		imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, images.size()- 1, 1, &ok, NULL);

		if (ok)
			ProcessingCamera3(displayWindow, imageNum, 1);
	}


}


void imageProcessing::OnProcessCam4()
{
	currentFunction = 4;
	PopulatePropertyBrowser(currentFunction);

	ProcessingCamera4(displayWindow, displayImage, 1);
}

void imageProcessing::OnDoneEditingLineInsertImageIndex()
{
	int bla = 100;
	QString index = ui.lineInsertImageIndex->text();
	int indexnum = index.toInt();
	
	int size = images.size();
	if (displayWindow == cam.size())
	{
		if (indexnum > size - 1)
			displayImage = size - 1;
		else if (indexnum < 0)
			displayImage = 0;
		else
			displayImage = indexnum;

	}

	else
	{
		if (indexnum > cam[displayWindow]->num_images - 1)
			displayImage = cam[displayWindow]->num_images - 1;
		else if (indexnum < 0)
			displayImage = 0;
		else
			displayImage = indexnum;

	}


	
	ReplaceCurrentBuffer();

	
}
/*void imageProcessing::Addparameter(int paramNum, QString paramName, float paramValue, int row, int col)
{
	QString value;

	//Preverimo trenutno kamero in sliko na kateri se nahajamo
	int camNum = displayWindow;
	int imgNum = displayImage;

	while (gridLayout.size() <= paramNum)
		gridLayout.push_back(new QGridLayout());
	gridLayout[paramNum]->setHorizontalSpacing(5);
	gridLayout[paramNum]->setVerticalSpacing(5);
	gridLayout[paramNum]->setObjectName(paramName);
	gridLayout[paramNum]->setSizeConstraint(QLayout::SetDefaultConstraint);
	gridLayout[paramNum]->setAlignment(Qt::AlignCenter);

	//Tvorimo oznake
	while (paramLabels.size() <= paramNum)
		paramLabels.push_back(new QLabel(ui.gridLayoutWidget));
	paramLabels[paramNum]->setObjectName(paramName);
	paramLabels[paramNum]->setMinimumSize(QSize(70, 20));
	paramLabels[paramNum]->setMaximumSize(QSize(70, 20));
	paramLabels[paramNum]->setAlignment(Qt::AlignLeft);
	paramLabels[paramNum]->setText(paramName);
	gridLayout[paramNum]->addWidget(paramLabels[paramNum]);

	//Tvorimo tekstovna polja za vrednost parametrov
	while (paramLineEdit.size() <= paramNum)
		paramLineEdit.push_back(new QLineEdit(ui.gridLayoutWidget));
	paramLineEdit[paramNum]->setObjectName(paramName);
	paramLineEdit[paramNum]->setMinimumSize(QSize(50, 20));
	paramLineEdit[paramNum]->setMaximumSize(QSize(50, 20));
	paramLineEdit[paramNum]->setAlignment(Qt::AlignLeft);
	paramLineEdit[paramNum]->setText(value.setNum(paramValue));
	paramLineEdit[paramNum]->show();
	gridLayout[paramNum]->addWidget(paramLineEdit[paramNum]);

	//Oznake in tekstovna polja razporedimo na grafičnem vmesniku
	ui.ParameterLayout->addLayout(gridLayout[paramNum], row, col, -1,3, Qt::AlignLeft|Qt::AlignTop);
	ui.scrollArea->widget()->setLayout(ui.ParameterLayout);

}*/



/*void imageProcessing::RemoveParameters()
{
	int camNum = displayWindow;
	int imgNum = displayImage;
	int row, col, rowSpan, colSpan, newRow, newCol, paramNum, newParamNum, colNumber;

	QInputDialog removeParameterName;
	QString paramName;
	QString insertedName;
	//Pot do konfiguracijskih datotek
	//Potrebno je dodati odvisnost od TRENUTNIH tipov in postaj!
	QString filePath = QDir::currentPath() + QString("/%1/DynamicVariables/types%2Station%3/camera%4/img%5_parameters.ini").arg(REF_FOLDER_NAME).arg(0).arg(NR_STATIONS).arg(camNum).arg(imgNum).arg(imgNum);
	QStringList valueList;
	QSize dlgSize(250, 250);
	QSettings settings(filePath, QSettings::IniFormat);

	//Nastavitve dialoga
	removeParameterName.setWindowTitle("Remove parameter");
	removeParameterName.resize(dlgSize);
	removeParameterName.setLabelText("Insert parameter name");
	removeParameterName.setInputMode(removeParameterName.TextInput);
	//Preberemo ime parametra
	if(removeParameterName.exec() == 1);
	{
		paramName = removeParameterName.textValue();
		paramNum = CountParameters();

		//Poiščemo indeks parametra in ga izbrišemo iz tabele
		for (int i = 0; i < paramNum; i++)
		{
			insertedName = paramLabels[i]->text();
			if (insertedName == paramName)
			{
				//Izbrišemo parameter iz tabele parametrov
				ui.ParameterLayout->getItemPosition(i, &row, &col, &rowSpan, &colSpan);
				parameterTable[0][0][camNum][imgNum][row].erase(parameterTable[0][0][camNum][imgNum][row].begin() + col);
				colNumber = parameterTable[0][0][camNum][imgNum][row].size();

				//Pobrišemo oznako in okence za najden indeks
				ui.ParameterLayout->removeItem(gridLayout[i]);
				paramLabels[i]->hide();
				paramLineEdit[i]->hide();
				paramLabels.erase(paramLabels.begin() + i);
				paramLineEdit.erase(paramLineEdit.begin() + i);
				gridLayout.erase(gridLayout.begin() + i);

				//Pobrišemo parameter iz datoteke
				settings.remove(insertedName);
				settings.sync();
				//Če smo izbrisali zadnji parameter datoteko izbrišemo
				valueList = settings.allKeys();
				if (valueList.size() == 0)
				{
					QFile configFile(filePath);
					configFile.remove();
				}

				//Prerazporedimo parametre
				for (int j = col; j < colNumber; j++)
				{
					newParamNum = CountParameters();
					for (int k = 0; k < newParamNum; k++)
					{
						ui.ParameterLayout->getItemPosition(k, &newRow, &newCol, &rowSpan, &colSpan);
						if (newRow == row && newCol == j + 1)
						{
							SaveParameters(camNum, imgNum, k, newRow, j);
						}
					}
				}

				//Novo razporejene parametre prikažemo
				ClearDialog();
				ReplaceCurrentBuffer();
				ReplaceParameters();

				return;
			}
		}
	}
}*/

/*void imageProcessing::UpdateParameters()
{
	int camNum = displayWindow;
	int imgNum = displayImage;
	int paramNum = paramLineEdit.size();
	int row, col, rowSpan, colSpan;

	//Pot do konfugiracijskih datotek
	QString dirPath = QDir::currentPath() + QString("/%1/DynamicVariables/types%2Station%3/camera%4").arg(REF_FOLDER_NAME).arg(0).arg(NR_STATIONS).arg(camNum).arg(imgNum);
	QDir dir;

	ImageParameter imgParameter;

	dir.setPath(dirPath);
	if (dir.exists())
	{	
		for (int i = 0; i < paramNum; i++)
		{
			//Posodobimo tabelo parametrov in shranimo posodobljene vrednosti
			ui.ParameterLayout->getItemPosition(i, &row, &col, &rowSpan, &colSpan);
			parameterTable[0][0][camNum][imgNum][row][col] = paramLineEdit[i]->text().toDouble();
			//SaveParameters(camNum, imgNum, i, row, col);
		}
	}
}*/



void imageProcessing::onPressedAddParameter()
{
	bool ok;
	int insertNum = 0;
	
	if (currentFunction > -1)
	{
		insertNum = QInputDialog::getInt(this, tr("QInputDialog::setNumParam()"),
			tr("insert num:"), item.size(), 0, item.size(), 1, &ok);
		if (ok)
		{

			QStringList selectableTypes;
			selectableTypes << tr("bool parameter") << tr("Int Parameter") << tr("Double Parameter") << tr("rectangle");
			int propType;

			QString parameterType = QInputDialog::getItem(this, tr("parameter Type select"),
				tr("Select:"), selectableTypes, 0, false, &ok);

			if (ok)
			{
				QString text = QInputDialog::getText(this, tr("QInputDialog::getText()"),
					tr("Parameter Name:"), QLineEdit::Normal, tr("name"), &ok);
				if (ok)
				{
					for (int i = 0; i < 4; i++)
					{
						if (parameterType.compare(selectableTypes[i]) == 0)
						{
							if (i == 0)//bool value
							{
								types[currentStation][currentType]->prop[currentFunction].insert(types[currentStation][currentType]->prop[currentFunction].begin() + insertNum, new CProperty());
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propName = text;
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propType = i;
								item.insert(item.begin() + insertNum, variantManager->addProperty(QVariant::Bool, QString("%1 ").arg(insertNum) + types[currentStation][currentType]->prop[currentFunction][insertNum]->propName));
								item[insertNum]->setValue(types[currentStation][currentType]->prop[currentFunction][insertNum]->boolValue);
								item[insertNum]->setEnabled(true);
							}
							else if (i == 1)//int value
							{
								types[currentStation][currentType]->prop[currentFunction].insert(types[currentStation][currentType]->prop[currentFunction].begin() + insertNum, new CProperty());
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propName = text;
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propType = i;
								item.insert(item.begin() + insertNum, variantManager->addProperty(QVariant::Int, QString("%1 ").arg(insertNum) + types[currentStation][currentType]->prop[currentFunction][insertNum]->propName));
								item[insertNum]->setValue(types[currentStation][currentType]->prop[currentFunction].back()->intValue);
								item[insertNum]->setAttribute(QLatin1String("minimum"), types[currentStation][currentType]->prop[currentFunction].back()->intMinValue);
								item[insertNum]->setAttribute(QLatin1String("maximum"), types[currentStation][currentType]->prop[currentFunction].back()->intMaxValue);
								item[insertNum]->setAttribute(QLatin1String("singleStep"), 10);
								item[insertNum]->setEnabled(true);



							}
							else if (i == 2)
							{
								types[currentStation][currentType]->prop[currentFunction].insert(types[currentStation][currentType]->prop[currentFunction].begin() + insertNum, new CProperty());
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propName = text;
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propType = i;
								item.insert(item.begin() + insertNum, variantManager->addProperty(QVariant::Double, QString("%1 ").arg(insertNum) + types[currentStation][currentType]->prop[currentFunction][insertNum]->propName));
								item[insertNum]->setValue(types[currentStation][currentType]->prop[currentFunction].back()->doubleValue);
								item[insertNum]->setAttribute(QLatin1String("minimum"), types[currentStation][currentType]->prop[currentFunction].back()->doubleMinValue);
								item[insertNum]->setAttribute(QLatin1String("maximum"), types[currentStation][currentType]->prop[currentFunction].back()->doubleMaxValue);
								item[insertNum]->setAttribute(QLatin1String("singleStep"), 10);
								item[insertNum]->setEnabled(true);

							}
							else if (i == 3)
							{
								types[currentStation][currentType]->prop[currentFunction].insert(types[currentStation][currentType]->prop[currentFunction].begin() + insertNum, new CProperty());
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propName = text;
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propType = i;
								item.insert(item.begin() + insertNum, variantManager->addProperty(QVariant::Double, QString("%1 ").arg(insertNum) + types[currentStation][currentType]->prop[currentFunction][insertNum]->propName));
								item[insertNum]->setValue(types[currentStation][currentType]->prop[currentFunction].back()->doubleValue);
								item[insertNum]->setAttribute(QLatin1String("minimum"), types[currentStation][currentType]->prop[currentFunction].back()->doubleMinValue);
								item[insertNum]->setAttribute(QLatin1String("maximum"), types[currentStation][currentType]->prop[currentFunction].back()->doubleMaxValue);
								item[insertNum]->setAttribute(QLatin1String("singleStep"), 10);
								item[insertNum]->setEnabled(true);

							}


						}
					}
					//item.insert(item.begin() + 2, item.back());
					//topItem->addSubProperty(item[insertNum]);
					PopulatePropertyBrowser(currentFunction);
				}
			}
		}
	}
	else
	{
		 QMessageBox::information(this, tr("information"), "No function is selected");
	}
		
	//! [2]

	/*QInputDialog addParameterName, addParameterValue, addRow, addColumn;
	QSize dlgSize(250, 250);
	QString paramName;
	ImageParameter parameter;

	int cam_num = displayWindow;
	int img_num = displayImage;
	int row, col, param_num;
	float paramValue;

	//Nastavimo parametre prvega dialoga
	addParameterName.setWindowTitle("Add new parameter");
	addParameterName.resize(dlgSize);
	addParameterName.setLabelText("Insert parameter name");
	addParameterName.setInputMode(addParameterName.TextInput);

	//Preverimo je uporabnik potrdil vnos imena novega parametra
	if (addParameterName.exec() == 1)
	{
		//Preverimo če ime že obstaja
		paramName = addParameterName.textValue();
		if (!CheckParameterName(paramName))
		{
			QMessageBox msg;

			msg.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
			msg.setIcon(QMessageBox::Warning);
			msg.setText("<center>Invalid paramater name<center>");
			msg.setText("<p align='center'>Invalid paramter name</p>");
			msg.exec();

			return;
		}
		// Nastavimo parametre drugega dialoga
		addParameterValue.setWindowTitle("Add new parameter");
		addParameterValue.resize(dlgSize);
		addParameterValue.setLabelText("Insert parameter value");
		addParameterValue.setInputMode(addParameterValue.DoubleInput);
		addParameterValue.setDoubleRange(0.0, 10000.0);

		//Preverimo ali je uporabnik potrdil vnos vrednosti novega parametra
		if(addParameterValue.exec() == 1);
		{
			//Vrstica v kateri se bo parameter prikazal
			addRow.setWindowTitle("Row number");
			addRow.resize(dlgSize);
			addRow.setLabelText("Insert row number");
			addRow.setInputMode(addParameterValue.IntInput);
			addRow.setIntRange(0, rowNum - 1);

			if (addRow.exec() == 1)
			{

				//Prebermo vrstico in stolpec novega parametra
				row = addRow.intValue();
				col = parameterTable[0][0][cam_num][img_num][row].size();
				paramValue = addParameterValue.doubleValue();
				//Preverimo če je vrsta že zasedena
				if (col == colNum)
				{
					do
					{
						row++;
						col = parameterTable[0][0][cam_num][img_num][row].size();

					} while (col == colNum);
				}
				//Dodamo nov parameter in ga shranimo
				parameterTable[0][0][cam_num][img_num][row].push_back(paramValue);
				param_num = CountParameters();
				Addparameter(param_num - 1, paramName, paramValue, row, col);
				col = parameterTable[0][0][cam_num][img_num][row].size() - 1;
				SaveParameters(cam_num, img_num, param_num - 1, row, col);
			}
		}
	}*/
}

void imageProcessing::onPressedRemoveParameters()
{
	int deleteNum = 0;
	bool ok;
	if (currentFunction > -1)
	{
		deleteNum = QInputDialog::getInt(this, tr("QInputDialog::setNumParam()"),
			tr("delete param:"), item.size()-1, 0, item.size(), 1, &ok);
		if (ok)
		{
			types[currentStation][currentType]->prop[currentFunction].erase(types[currentStation][currentType]->prop[currentFunction].begin() + deleteNum);
			//item.erase(item.begin() + deleteNum);

		}


		PopulatePropertyBrowser(currentFunction);

	}
	else
	{
		QMessageBox::information(this, tr("information"), "No function is selected");
	}





}

void imageProcessing::onPressedUpdateParameters()
{
	SaveFunctionParameters(currentType,currentFunction);
	//UpdateParameters();
}

void imageProcessing::onPressedSurfaceInspection()
{
	currentFunction = 8;
	ClearDraw();
	PopulatePropertyBrowser(currentFunction);
	

	if(displayWindow == cam.size())
	CheckDefects(displayImage, 1, 1);

}

void imageProcessing::mousePressEvent(QMouseEvent *event)
{
	origin = event->pos();

	if (!rubberBand)
		rubberBand = new QRubberBand(QRubberBand::Rectangle, this);

	rubberBand->setGeometry(QRect(origin, QSize()));
	rubberBand->show();
}

void imageProcessing::mouseReleaseEvent(QMouseEvent * event)
{
	rubberBand->setGeometry(QRect(origin, event->pos()).normalized());
	rubberBand->hide();
}

void imageProcessing::PopulatePropertyBrowser(int function)
{

	for (int i = 0; i < item.size(); i++)
	{
		topItem->removeSubProperty(item[i]);
	}

	item.clear();

	
	for (int i = 0; i < types[currentStation][currentType]->prop[function].size(); i++)
	{
		if (types[currentStation][currentType]->prop[function][i]->propType == 0)	//bool
		{
			item.push_back(variantManager->addProperty(QVariant::Bool, QString("%1 ").arg(i) + types[currentStation][currentType]->prop[function][i]->propName));
			item.back()->setValue(types[currentStation][currentType]->prop[function][i]->boolValue);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());
		}
		else if (types[currentStation][currentType]->prop[function][i]->propType == 1)	//int
		{
			item.push_back(variantManager->addProperty(QVariant::Int, QString("%1 ").arg(i) + types[currentStation][currentType]->prop[function][i]->propName));
			item.back()->setValue(types[currentStation][currentType]->prop[function][i]->intValue);
			item.back()->setAttribute(QLatin1String("minimum"), types[currentStation][currentType]->prop[function][i]->intMinValue);
			item.back()->setAttribute(QLatin1String("maximum"), types[currentStation][currentType]->prop[function][i]->intMaxValue);
			item.back()->setAttribute(QLatin1String("singleStep"), 10);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());
		}
		else if (types[currentStation][currentType]->prop[function][i]->propType == 2)	//float
		{
			item.push_back(variantManager->addProperty(QVariant::Double, QString("%1 ").arg(i) + types[currentStation][currentType]->prop[function][i]->propName));
			item.back()->setValue(types[currentStation][currentType]->prop[function][i]->doubleValue);
			item.back()->setAttribute(QLatin1String("minimum"), types[currentStation][currentType]->prop[function][i]->doubleMinValue);
			item.back()->setAttribute(QLatin1String("maximum"), types[currentStation][currentType]->prop[function][i]->doubleMaxValue);
			item.back()->setAttribute(QLatin1String("singleStep"), 10);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());
		}
		else if (types[currentStation][currentType]->prop[function][i]->propType == 3)	//float
		{
			item.push_back(variantManager->addProperty(QVariant::Rect, QString("%1 ").arg(i) + types[currentStation][currentType]->prop[function][i]->propName));
			item.back()->setValue(types[currentStation][currentType]->prop[function][i]->rectValue);
			//item.back()->setAttribute(QLatin1String("minimum"), types[currentStation][currentType]->prop[function][i]->doubleMinValue);
			//item.back()->setAttribute(QLatin1String("maximum"), types[currentStation][currentType]->prop[function][i]->doubleMaxValue);
			//item.back()->setAttribute(QLatin1String("singleStep"), 10);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());
		}
		else
		{

		}
		if ((loginRights == 1)||(loginRights == 2))
			item.back()->setEnabled(true);
		else
			item.back()->setEnabled(false);
	}

}

void imageProcessing::SaveFunctionParameters(int typeNr, int function)
{
	QStringList list;

	int type = -1;
	int intSize;
	if (function > -1)
	{
		for (int i = 0; i < item.size(); i++)
		{
			if (i < 10)
				intSize = 2;
			else if (i < 100)
				intSize = 3;
			type = item[i]->valueType();
			switch (type)
			{
			case QVariant::Bool: //bool
				types[currentStation][typeNr]->prop[function][i]->propType = 0;
				types[currentStation][typeNr]->prop[function][i]->propName = item[i]->propertyName().remove(0, intSize);
				types[currentStation][typeNr]->prop[function][i]->boolValue = item[i]->value().toBool();
				break;
			case QVariant::Int: //int
				types[currentStation][typeNr]->prop[function][i]->propType = 1;
				types[currentStation][typeNr]->prop[function][i]->propName = item[i]->propertyName().remove(0, intSize);
				types[currentStation][typeNr]->prop[function][i]->intValue = item[i]->value().toInt();

				break;
			case QVariant::Double: //double
				types[currentStation][typeNr]->prop[function][i]->propType = 2;
				types[currentStation][typeNr]->prop[function][i]->propName = item[i]->propertyName().remove(0, intSize);
				types[currentStation][typeNr]->prop[function][i]->doubleValue = item[i]->value().toDouble();
				break;
			case QVariant::Rect: //rectangle
				types[currentStation][typeNr]->prop[function][i]->propType = 3;
				types[currentStation][typeNr]->prop[function][i]->propName = item[i]->propertyName().remove(0, intSize);
				types[currentStation][typeNr]->prop[function][i]->rectValue = item[i]->value().toRect();
				//types[currentStation][typeNr]->prop[function][i]->rectValue = item[i]->value().toInt();
				break;
			}
		}

		//Pot do konfugiracijskih datotek

			QString dirPath = QDir::currentPath() + QString("/%1/DynamicVariables/types_station%2/").arg(REF_FOLDER_NAME).arg(currentStation);
			QDir dir;

			dir.setPath(dirPath);
			if (!dir.exists())
				dir.mkdir(dirPath);

			dirPath = dirPath + QString("parameters_%1/").arg(types[0][typeNr]->typeName);


			dir.setPath(dirPath);
			if (!dir.exists())
				dir.mkdir(dirPath);

			QString filePath = dirPath + QString("function_%1.ini").arg(function);

			QFile file(filePath);
			file.remove();

			QSettings settings(filePath, QSettings::IniFormat);

			for (int i = 0; i < types[currentStation][typeNr]->prop[function].size(); i++)
			{
				switch (types[currentStation][typeNr]->prop[function][i]->propType)
				{
				case 0://bool
					list << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->boolValue);
					break;
				case 1://integer
					list << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->intValue)
						<< QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->intMinValue) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->intMaxValue);
					break;
				case 2://double
					list << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->doubleValue)
						<< QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->doubleMinValue) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->doubleMaxValue);
					break;

				case 3://rectangle
					list << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->rectValue.x())
						<< QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->rectValue.y()) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->rectValue.width()) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->rectValue.height());
					break;


				}
				settings.setValue(QString("parameter%1").arg(i), list);
				list.clear();
			}

			QMessageBox::information(this, tr("information"), "Parameters saved");
		
	}
	else
		QMessageBox::information(this, tr("information"), "No function is selected");
	
		/*list << QString("%1").arg(toleranceHigh[i]) << QString("%1").arg(toleranceLow[i]) << QString("%1").arg(correctFactor[i]) << QString("%1").arg(offset[i]) << QString("%1").arg(nominal[i]) << QString("%1").arg(isActive[i]) << name[i];
		settings.setValue(QString("parameter%1").arg(i), list);
		list.clear();
	*/
	/*		for (int i = 0; i < paramNum; i++)
			{
				//Posodobimo tabelo parametrov in shranimo posodobljene vrednosti
				ui.ParameterLayout->getItemPosition(i, &row, &col, &rowSpan, &colSpan);
				parameterTable[0][0][camNum][imgNum][row][col] = paramLineEdit[i]->text().toDouble();
				SaveParameters(camNum, imgNum, i, row, col);
			}
		}
	}*/

}

void imageProcessing::SaveFunctionParametersOnNewType(int station, int typeNr, int function)
{

	QStringList list;
			//Pot do konfugiracijskih datotek

			QString dirPath = QDir::currentPath() + QString("/%1/DynamicVariables/types_station%2/").arg(REF_FOLDER_NAME).arg(station);
			QDir dir;

			dir.setPath(dirPath);
			if (!dir.exists())
				dir.mkdir(dirPath);

			dirPath = dirPath + QString("parameters_%1/").arg(types[station][typeNr]->typeName);


			dir.setPath(dirPath);
			if (!dir.exists())
				dir.mkdir(dirPath);

			QString filePath = dirPath + QString("function_%1.ini").arg(function);

			QSettings settings(filePath, QSettings::IniFormat);

			for (int i = 0; i < types[station][typeNr]->prop[function].size(); i++)
			{
				switch (types[station][typeNr]->prop[function][i]->propType)
				{
				case 0://bool
					list << QString("%1").arg(types[station][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[station][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[station][typeNr]->prop[function][i]->boolValue);
					break;
				case 1://integer
					list << QString("%1").arg(types[station][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[station][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[station][typeNr]->prop[function][i]->intValue)
						<< QString("%1").arg(types[station][typeNr]->prop[function][i]->intMinValue) << QString("%1").arg(types[station][typeNr]->prop[function][i]->intMaxValue);
					break;
				case 2://double
					list << QString("%1").arg(types[station][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[station][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[station][typeNr]->prop[function][i]->doubleValue)
						<< QString("%1").arg(types[station][typeNr]->prop[function][i]->doubleMinValue) << QString("%1").arg(types[station][typeNr]->prop[function][i]->doubleMaxValue);
					break;

				case 3://rectangle
					list << QString("%1").arg(types[station][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[station][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[station][typeNr]->prop[function][i]->rectValue.x())
						<< QString("%1").arg(types[station][typeNr]->prop[function][i]->rectValue.y()) << QString("%1").arg(types[station][typeNr]->prop[function][i]->rectValue.width()) << QString("%1").arg(types[station][typeNr]->prop[function][i]->rectValue.height());
					break;

				}
				settings.setValue(QString("parameter%1").arg(i), list);
				list.clear();
			}


}

void imageProcessing::DeleteTypeProperty(QString  typeName)
{




}

void imageProcessing::LoadTypesProperty()
{
	QStringList values;
	
	QString filePath;
	QString dirPath;
	QDir dir;
	QString functionName;
	int functionNr;
	int n = 0;


	for (int i = 0; i < NR_STATIONS; i++)
	{

		//filePath = "D:\\Git_MBvision\\MBsoftware32bit\\MBsoftware32Bit\\Win32\\Reference\\signali\\ControlCardMB0.ini";
		dirPath = QDir::currentPath() + QString("/%1/DynamicVariables/types_Station%2").arg(REF_FOLDER_NAME).arg(i);
		QSettings settings(dirPath, QSettings::IniFormat);

		if (!QDir(dirPath).exists())
			QDir().mkdir(dirPath);


		dir.setPath(dirPath);
		dir.setNameFilters(QStringList() << "parameters*");
		int count = dir.count();
		QStringList dirList = dir.entryList();
		for (int j = 0; j < count; j++)
		{

			filePath = dirPath + QString("/%1").arg(dirList.at(j));


			dir.setPath(filePath);
			dir.setNameFilters(QStringList() << "*.ini");
			int count2 = dir.count();
			QStringList list = dir.entryList();
			//inputs
			for (int k = 0; k < count2; k++)
			{
				n = 0;
				functionName = list[k];
				functionName.chop(4);
				functionName.remove(0, 9);
				functionNr = functionName.toInt();

				QString path = filePath + QString("/%1").arg(list.at(k));
				QSettings settings(path, QSettings::IniFormat);

				do {
					values.clear();
					values = settings.value(QString("parameter%1").arg(n)).toStringList();

					if (values.size() > 1)
					{
						types[i][j]->prop[functionNr].push_back(new CProperty());

						switch (values[0].toInt())
						{
						case 0://bool
							types[i][j]->prop[functionNr].back()->propType = values[0].toInt();
							types[i][j]->prop[functionNr].back()->propName = values[1];
							types[i][j]->prop[functionNr].back()->boolValue = values[2].toInt();
							break;
						case 1://integer
							types[i][j]->prop[functionNr].back()->propType = values[0].toInt();
							types[i][j]->prop[functionNr].back()->propName = values[1];
							types[i][j]->prop[functionNr].back()->intValue = values[2].toInt();
							types[i][j]->prop[functionNr].back()->intMinValue = values[3].toInt();
							types[i][j]->prop[functionNr].back()->intMaxValue = values[4].toInt();
							break;
						case 2://double
							types[i][j]->prop[functionNr].back()->propType = values[0].toInt();
							types[i][j]->prop[functionNr].back()->propName = values[1];
							types[i][j]->prop[functionNr].back()->doubleValue = values[2].toDouble();
							types[i][j]->prop[functionNr].back()->doubleMinValue = values[3].toDouble();
							types[i][j]->prop[functionNr].back()->doubleMaxValue = values[4].toDouble();
							break;
						case 3://rectangle
							types[i][j]->prop[functionNr].back()->propType = values[0].toInt();
							types[i][j]->prop[functionNr].back()->propName = values[1];
							types[i][j]->prop[functionNr].back()->rectValue.setX(values[2].toInt());
							types[i][j]->prop[functionNr].back()->rectValue.setY(values[3].toInt());
							types[i][j]->prop[functionNr].back()->rectValue.setWidth(values[4].toInt());
							types[i][j]->prop[functionNr].back()->rectValue.setHeight(values[5].toInt());

							break;
						}
					}
					n++;
				} while (values.size() > 0);
			}
		}
	}
}

int imageProcessing::ProcessingCamera0(int nrCam, int imageIndex, int draw) 
{//poisci objekt

	processingTimer[0]->SetStart();
	QRect areaRect;
	areaRect.setRect(1, 1, 700, 400);
	if(draw)
	scene->addRect(areaRect, QPen(Qt::red), QBrush(Qt::NoBrush));
	int inte;
	CPointFloat tocka;
	int step = 64;
	int zaporednaY = 2;
	int zaporednaCounterY = 0;
	int prevZaporednaCounterY = 0;
	int zaporednaCounterx = 0;
	int zaporednaX = 2;
	int foundY = 0;
	int prevFoundY = 0;


	CPointFloat grobiCenter;
	int xSum = 0;
	int ySum = 0;
	CPointFloat tockeOblike[10000];


	vector<CPointFloat> oblika;



	for (int x = areaRect.left(); x < areaRect.right(); x+= 8)
	{
		foundY = 0;
		for (int y = areaRect.top(); y < areaRect.bottom(); y+= 8)
		{
			inte = cam[nrCam]->image[imageIndex].GetAverageIntensity(x,y);
			if (inte < 40)
			{
				zaporednaCounterY++;
				tocka.SetPointFloat(x, y);
				tockeOblike[zaporednaCounterY] = tocka;
				if (draw)
				scene->addItem(tocka.DrawDot(QPen(Qt::green), QBrush(Qt::green), 3));
				foundY = 1;

			}

		}
		if (foundY == 0)
			zaporednaCounterY = 0;



		if ((zaporednaCounterY == 0) && (prevZaporednaCounterY > 1))
		{
			if (prevZaporednaCounterY > 50)//dovolj velika oblika najdena.
			{
				for (int i = 0; i < 10000; i++)
				{
					if (tockeOblike[i].x > 0)
					{
						oblika.push_back(tockeOblike[i]);
					}
				}
			}

		}
		prevZaporednaCounterY = zaporednaCounterY;
	}


	for (int i = 0; i < oblika.size(); i++)
	{
		xSum += oblika[i].x;
		ySum += oblika[i].y;
	}

	grobiCenter.SetPointFloat(xSum / oblika.size(), ySum / oblika.size());
	CLine groboLine[360];
	int lineCounter = 0;
	CPointFloat tockeOkrog;
	vector <CPointFloat> tocniCenterTocke;
	CPointFloat tocniCenter;
	for (int i = 0; i < 360; i += 32)
	{
		groboLine[lineCounter].SetLine(grobiCenter, i);
		if ((i < 90) ||(i > 270))
		{
		//scene->addItem(groboLine[lineCounter].DrawLine(areaRect, QPen(Qt::yellow)));
			groboLine[lineCounter].p2 = groboLine[lineCounter].GetPointAtDistanceP2(200);
		}
		else
		{
			//scene->addItem(groboLine[lineCounter].DrawLine(areaRect, QPen(Qt::green)));
			groboLine[lineCounter].p2 = groboLine[lineCounter].GetPointAtDistanceP2(-200);
		}
		tocniCenterTocke.push_back(cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(groboLine[lineCounter], 1, 5, true, 150));
		tockeOkrog = tocniCenterTocke.back();
		if (draw)
		scene->addItem(tockeOkrog.DrawDot(QPen(Qt::magenta), QBrush(Qt::magenta), 2));
		lineCounter++;
	}
	xSum = 0;
	ySum = 0;
	for (int i = 0; i < tocniCenterTocke.size(); i++)
	{
		xSum += tocniCenterTocke[i].x;
		ySum += tocniCenterTocke[i].y;
	}
	tocniCenter.SetPointFloat(xSum / tocniCenterTocke.size(), ySum / tocniCenterTocke.size());
	if (draw)
	scene->addItem(tocniCenter.DrawDot(QPen(Qt::magenta), QBrush(Qt::magenta), 10));

	CPointFloat tocenObris[360];
	CLine tocnoLine[360];
	float distance[360];
	lineCounter = 0;
	for (int i = 0; i < 360; i += 1)
	{
		tocnoLine[lineCounter].SetLine(tocniCenter, i);
		if ((i < 90) || (i > 270))
		{

			tocnoLine[lineCounter].p2 = tocnoLine[lineCounter].GetPointAtDistanceP2(200);
			if (draw)
		scene->addItem(tocnoLine[lineCounter].DrawSegment(QPen(Qt::yellow), 1));
		}
		else
		{

			tocnoLine[lineCounter].p2 = tocnoLine[lineCounter].GetPointAtDistanceP2(-200);
			if (draw)
			scene->addItem(tocnoLine[lineCounter].DrawSegment(QPen(Qt::green), 1));
		}
		tocenObris[lineCounter] = (cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(tocnoLine[lineCounter], 1, 2, true, 150));
		distance[lineCounter] = tocniCenter.GetDistance(tocenObris[lineCounter]);
		if (draw)
		scene->addItem(tocenObris[lineCounter].DrawDot(QPen(Qt::red), QBrush(Qt::red), 2));
		lineCounter++;
	}
	int globMax = 0;
	int xStart = 1000;
	int xEnd = 0;
	int yStart = 1000;
	int yEnd = 0;


	for (int i = 0; i < 360; i++)
	{
		if (distance[i] > globMax)
			globMax = distance[i];

		if (tocenObris[i].x > xEnd)
			xEnd = tocenObris[i].x;

		if (tocenObris[i].x < xStart)
			xStart = tocenObris[i].x;

		if (tocenObris[i].y > yEnd)
			yEnd = tocenObris[i].y;

		if (tocenObris[i].y < yStart)
			yStart = tocenObris[i].y;

	}

	QRect oblikaRect;
	oblikaRect.setCoords(xStart, yStart, xEnd, yEnd);
	if (draw)
		scene->addRect(oblikaRect, QPen(Qt::red), QBrush(Qt::NoBrush));
	
	int localMax = globMax * 0.80;
	int firstMax = 0;
	int firstX = 0;
	int firstY = 0;
	int firstCounter = 0;
	int secCounter = 0;
	int secX = 0; 
	int secY = 0;
	int firstMaxCounter = 0;
	int secMaxCounter = 0;
	int secMax = 0;
	CPointFloat desniRob;
	CPointFloat leviRob;

	for (int i = 0; i < 360; i++)
	{
		if ((i < 90) || (i > 270))
		{
			if (distance[i] > localMax)
			{
				firstX += tocenObris[i].x;
				firstY += tocenObris[i].y;
				firstCounter++;
			}
		}
		else
		{
			if (distance[i] > localMax)
			{
				secX += tocenObris[i].x;
				secY += tocenObris[i].y;
				secCounter++;
			}
		}
	}
	desniRob.SetPointFloat(firstX / firstCounter, firstY / firstCounter);
	leviRob.SetPointFloat(secX / secCounter, secY / secCounter);
	if (draw)
	{
		scene->addItem(leviRob.DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 10));
		scene->addItem(desniRob.DrawDot(QPen(Qt::yellow), QBrush(Qt::yellow), 10));
	}

	//cam[nrCam]->image[imageIndex].DrawEdges(QColor(255, 100, 100));*/
	processingTimer[0]->SetStop();
	processingTimer[0]->ElapsedTime();


	return 0;
}
int imageProcessing::ProcessingCamera1(int nrCam, int imageIndex, int draw)
{
	cam[nrCam]->image[imageIndex].SaveBuffer(QString("C:\\images\\%1\\").arg(nrCam), QString("%1.bmp").arg(imageIndex));

	return 0;
}

int imageProcessing::ProcessingCamera2(int NrCam, int imageIndex, int draw) // sledenje lamel
{
	processingTimer[NrCam]->SetStart();
	processingTimer[NrCam + 2]->SetStart();


	int width = cam[NrCam]->image[imageIndex].buffer->cols;
	int width2 = images[NrCam]->buffer->rows;
	int offsetRectX;
	int offsetRectY;
	QRect areaRect = types[NrCam][currentType]->prop[2][1]->rectValue;
	int findThreshold = types[NrCam][currentType]->prop[2][0]->intValue;
	QRect heightRect = types[NrCam][currentType]->prop[2][2]->rectValue;
	Rect regRect;
	offsetRectX = areaRect.x();
	offsetRectY = areaRect.y();
	regRect.x = areaRect.x();
	regRect.y = areaRect.y();
	regRect.width = areaRect.width();
	regRect.height = areaRect.height();

	Mat region = cam[NrCam]->image[imageIndex].buffer[0];
	Mat region2 = cam[NrCam]->image[imageIndex].buffer[0].clone();


	vector<vector<Point> > contours;
	//vector<vector<Point> > contours2;

	if (draw)
	{
		scene->addRect(areaRect, QPen(Qt::yellow), QBrush(Qt::NoBrush));
		scene->addRect(heightRect, QPen(Qt::green), QBrush(Qt::NoBrush));
	}
	threshold(region2, region2, findThreshold, 255, 0);
	try
	{
		findContours(region2(regRect), contours, RETR_LIST, CHAIN_APPROX_SIMPLE);
	}
	catch (cv::Exception& e)
	{
		const char* err_msg = e.what();
		//qDebug << "exception caught: " << err_msg << std::endl;
	}



	Moments m;
	vector<Rect> PredVrstnimRedomBoundRect(contours.size());
	vector<Rect> boundRectVmes;
	vector<Rect> boundRect;
	vector<CPointFloat> PredVrstnimRedomcenter(contours.size());
	vector<CPointFloat> centerVmes;
	vector<CPointFloat> center;
	vector<int> PredVrstnimRedomArea(contours.size());
	vector<int> areaVmes;
	vector<int> area;
	vector<vector<Point> > contoursPredVrstnimRedom(contours.size());
	vector<vector<Point> > contoursVmes;
	vector<vector<Point> > contoursFinal;

	float maxX = 0;
	float prevMax = 0;
	int z = 0;
	int ind = 0;
	int krog = 0;
	QRect rect;
int x, y, w, h;
int allLamel =0;
int sorted = 0;


if (contours.size() > 0)
{
	for (int i = 0; i < contours.size() - 1; i++)
	{
		m = moments(contours[i]);

		if ((int)m.m00 > 2000)
		{
			PredVrstnimRedomcenter[i].SetPointFloat(m.m10 / m.m00, m.m01 / m.m00);
			PredVrstnimRedomBoundRect[i] = boundingRect(contours[i]);
			if (PredVrstnimRedomcenter[i].x > 150 && PredVrstnimRedomcenter[i].x < 600)
			{
				centerVmes.push_back(PredVrstnimRedomcenter[i]);
				boundRectVmes.push_back(PredVrstnimRedomBoundRect[i]);
				areaVmes.push_back(m.m00);
				contoursVmes.push_back(contours[i]);
			}
		}
	}

}
do
{
	maxX = 0;
	ind = 0;
	z = 0;
	if (centerVmes.size() > 0)
	{
		for (z; z < centerVmes.size(); z++)
		{
			if (prevMax == 0)
			{
				if (centerVmes[z].x > maxX)
				{
					maxX = centerVmes[z].x;
					ind = z;
				}
			}
			else
			{
				if ((centerVmes[z].x > maxX) && (centerVmes[z].x < prevMax))
				{
					maxX = centerVmes[z].x;
					ind = z;
				}

			}
		}
		center.push_back(centerVmes[ind]);
		boundRect.push_back(boundRectVmes[ind]);
		area.push_back(areaVmes[ind]);
		contoursFinal.push_back(contoursVmes[ind]);
		prevMax = maxX;
		krog++;
		if (krog == centerVmes.size())
			sorted = 1;
	}
	else sorted = 1;

} while (sorted == 0);



int index = -1;
Rect drawRect;
Rect drawSideRect;
int xOffset, yOffset;
int off = 0;
vector<RotatedRect> minEllipse(center.size());
for (int i = 0; i < center.size(); i++)
{

	index = -1;
	if (boundRect[i].width > 60 && boundRect[i].height > 30)
	{

		if (NrCam == 1)
		{
			images[tmpPieceCounter[NrCam]]->buffer[0] = cam[NrCam]->image[imageIndex].buffer[0].clone();
			tmpPieceCounter[NrCam]++;
			//cam[NrCam]->image[imageIndex].SaveBuffer(QString("C:\\image\\tmp\\Station%1\\%2\\").arg(NrCam).arg(tmpPieceCounter[NrCam]), QString("%1.bmp").arg(imageIndex));
			if (tmpPieceCounter[NrCam] >= 1000)
				tmpPieceCounter[NrCam] = 999;
		}
		//cam[NrCam]->image[imageIndex].SaveBuffer(QString("C:\\image\\tmp\\Station%1\\%2\\").arg(NrCam).arg(tmpPieceCounter[NrCam]), QString("%1.bmp").arg(imageIndex));

		if (center[i].x > 150 && center[i].x < 600)
		{
			//threshold(region(boundRect[i]), region(boundRect[i]), 40, 255, CV_THRESH_TOZERO);
			//findContours(region(boundRect[i]), contours2, RETR_LIST, CHAIN_APPROX_SIMPLE);
			index = GetLamelaIndex(NrCam,center[i],imageIndex);//tukaj izberemo kateri kos je in mu in mu pripisemo pravi index
			rect.setRect(boundRect[i].x, boundRect[i].y, boundRect[i].width, boundRect[i].height);
			if (index >= 0)
			{
				if (center[i].x > 250)
				{
					if (measureObject->isMeasured[NrCam][index] == 0)
					{
						drawRect.x = boundRect[i].x - 15;
						xOffset = -drawRect.x;
						drawRect.y = 0;
						yOffset = -drawRect.y;
						drawRect.width = boundRect[i].width + 30;
						off = 540 - drawRect.y;
						drawRect.height = 539;

						if ((drawRect.x >= 0) && (drawRect.y >= 0) && (drawRect.width < 720))
						{
							displayLamela[NrCam][index] = cam[NrCam]->image[imageIndex].buffer[0](drawRect).clone();
							//MeasureLamel(NrCam, imageIndex, rect, center[i], index, draw,xOffset,yOffset);
							MeasureLamelRadij(NrCam, imageIndex, contoursFinal[i], rect, center[i], index, draw, offsetRectX + xOffset, offsetRectY + yOffset);
							measureObject->area[NrCam][index] = area[i];
							measureObject->xOffsetDraw[NrCam][index] = offsetRectX + xOffset;
							measureObject->yOffsetDraw[NrCam][index] = offsetRectY + yOffset;
							measureObject->imageIndex[NrCam][index] = imageIndex;
							measureObject->isMeasured[NrCam][index] = 1;
							processingTimer[NrCam + 2]->SetStop();
							measureObject->processTime[NrCam][index] = processingTimer[NrCam + 2]->ElapsedTime();
						}
						else
							break;
					}
				}
			}






			if (draw)
			{
				QFont font("Times", 12, QFont::Bold);
				QGraphicsTextItem * io = new QGraphicsTextItem;
				io->setPos(center[i].x, center[i].y);
				io->setPlainText(QString("%1").arg(index));
				io->setDefaultTextColor((Qt::red));

				scene->addItem(io);
				scene->addRect(rect, QPen(Qt::red), QBrush(Qt::NoBrush));
				scene->addItem(center[i].DrawDot(QPen(Qt::red), QBrush(Qt::red), 5));
			}

		}
	}
	
}

	processingTimer[NrCam]->SetStop();
	processingTimer[NrCam]->ElapsedTime();


	cam[NrCam]->imageReady[imageIndex] = 3;
	if(testMode == true)
	{
		measureObject->isMeasuredSpeed[NrCam][index] = 1;
		measureObject->isProcessed[NrCam][index] = 0;

	}
	if(!draw)
	threadsRunning[cam[NrCam]->image[imageIndex].stThread] = false;


	obdelanaCounter[NrCam]++;
	obdelanaCounter[NrCam] = obdelanaCounter[NrCam] & 0xffff;
	return 0;
}

void imageProcessing::MeasureLamel(int nrCam, int imageIndex, QRect rect, CPointFloat center, int nrLamela, int draw, int offsetX, int offsetY)//x in y offset sta samo za izrise na glavni zaslon
{

	processingTimer[nrCam+2]->SetStart();
	int bla = 100;
	int filling = 6000;// = cv::floodFill(region, centerPoint, 255, &boundRect[i], cv::Scalar(), 200);
	int poz = 0;

	CLine finoLine[360];
	int lineCounter = 0;
	CPointFloat obrisTocke[360];
	CPointFloat tocka;
	float distanceFromCenter[360];
	float globMaxLevo = 0;
	float globMaxDesno = 0;
	int step = 1;
	CCircle radijZg, RadijSp;
	CPointFloat radijZgTocke[3];
	CPointFloat radijSpTocke[3];
	vector<CPointFloat> tockeZG;
	vector<CPointFloat> tockeSP;
	
	for (int k = 0; k < 360; k += step)
	{
		finoLine[lineCounter].SetLine(center, k);
		if ((k < 90) || (k > 270))
		{
			finoLine[lineCounter].p2 = finoLine[lineCounter].GetPointAtDistanceP2(200);
			//if (draw)
				//scene->addItem(finoLine[lineCounter].DrawSegment(QPen(Qt::yellow), 1));
		}
		else
		{
			finoLine[lineCounter].p2 = finoLine[lineCounter].GetPointAtDistanceP2(-200);
			//if (draw)
			//	scene->addItem(finoLine[lineCounter].DrawSegment(QPen(Qt::green), 1));
		}

		tocka = (cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(finoLine[lineCounter], 1, 2, true, 80));

		if ((k > 30) && (k < 150))
		{
			tockeSP.push_back(CPointFloat(tocka));
		}
		else if ((k > 210) && (k < 330))
			tockeZG.push_back(CPointFloat(tocka));

		obrisTocke[lineCounter] = tocka;
		if (draw)
			scene->addItem(tocka.DrawDot(QPen(Qt::green), QBrush(Qt::green), 3));

		distanceFromCenter[lineCounter] = center.GetDistance(tocka);
		measureObject->tmpTockeLamel[nrLamela][lineCounter] = tocka;
		obrobaLameleDraw[nrCam][nrLamela][lineCounter].x = tocka.x + offsetX;
		obrobaLameleDraw[nrCam][nrLamela][lineCounter].y = tocka.y + offsetY;
		measureObject->distanceFromCenter[nrCam][nrLamela][lineCounter] = distanceFromCenter[lineCounter];
		measureObject->tockeLamele[nrCam][nrLamela][lineCounter] = tocka;
		
		if ((k < 90) || (k > 270))
		{

			if (distanceFromCenter[lineCounter] > globMaxDesno)
				globMaxDesno = (int)distanceFromCenter[lineCounter];
		}
		else
		{

			if (distanceFromCenter[lineCounter] > globMaxLevo)
				globMaxLevo = (int)distanceFromCenter[lineCounter];
		}

		lineCounter++;
	}
	float minD = globMaxDesno * 0.8;
	float maxD = globMaxDesno * 0.9;
	float minL = globMaxLevo * 0.8;
	float maxL = globMaxLevo * 0.9;
	globMaxDesno *= 0.8;
	globMaxLevo *= 0.8;
	CPointFloat tezisceLevo;
	CPointFloat tezisceDesno;
	int levoX = 0;
	int levoY = 0;
	int desnoX = 0;
	int desnoY = 0;
	int levoCounter = 0;
	int desnoCounter = 0;
	lineCounter = 0;
	//iskanje orientacije
	for (int z = 0; z < 360; z += step)
	{
		if ((z < 90) || (z > 270))
		{
			if ((distanceFromCenter[lineCounter] > minD) && (distanceFromCenter[lineCounter] < maxD))
			{
				desnoX += obrisTocke[lineCounter].x;
				desnoY += obrisTocke[lineCounter].y;
				desnoCounter++;
			}
		}
		else
		{
			if ((distanceFromCenter[lineCounter] > minL) && (distanceFromCenter[lineCounter] < maxL))
			{
				levoX += obrisTocke[lineCounter].x;
				levoY += obrisTocke[lineCounter].y;
				levoCounter++;
			}
		}
		lineCounter++;
	}

	/*radijZg.SetCircleNpoints(tockeZG, 200, 300);
	RadijSp.SetCircleNpoints(tockeSP, 200, 300);

	if (draw)
	{
		scene->addItem(radijZg.DrawCircle(QPen(Qt::magenta), QBrush(Qt::NoBrush), 20));
		scene->addItem(RadijSp.DrawCircle(QPen(Qt::yellow), QBrush(Qt::NoBrush), 20));
	}*/
	tezisceLevo.SetPointFloat(levoX / levoCounter, levoY / levoCounter);
	tezisceDesno.SetPointFloat(desnoX / desnoCounter, desnoY / desnoCounter);
	CLine simetrala;
	float angle;
	simetrala.SetLine(tezisceLevo, tezisceDesno);
	angle = simetrala.GetLineAngleInDegrees();
	measureObject->tmpAngleLamel[nrLamela];
	measureObject->angle[nrCam][nrLamela] = angle;

	CLine pravokotnicaNaSimetralo;
	CLine sirinaLine[3];
	CLine visinaLine[3];
	CPointFloat  sirinaLevo[3];
	CPointFloat sirinaDesno[3];
	CPointFloat  visinaTop[3];
	CPointFloat visinaBottom[3];

	pravokotnicaNaSimetralo = simetrala.GetPerpendicular(center);


	widthLineDraw[nrCam][nrLamela].p1 = CPointFloat(tezisceLevo.x + offsetX, tezisceLevo.y + offsetY);
	widthLineDraw[nrCam][nrLamela].p2 = CPointFloat(tezisceDesno.x + offsetX, tezisceDesno.y + offsetY);
	//heightLineDraw[nrCam][nrLamela] = pravokotnicaNaSimetralo;
	if (draw)
	{
		for (int i = 0; i < 3; i++)
		{
			scene->addItem(simetrala.DrawLine(rect, QPen(Qt::green)));
			scene->addItem(pravokotnicaNaSimetralo.DrawLine(rect, QPen(Qt::blue)));
		}
	}

	sirinaLine[0] = simetrala.GetParallel(4);
	sirinaLine[1] = simetrala.GetParallel(0);
	sirinaLine[2] = simetrala.GetParallel(-4);
	visinaLine[0] = pravokotnicaNaSimetralo.GetParallel(4);
	visinaLine[1] = pravokotnicaNaSimetralo.GetParallel(0);
	visinaLine[2] = pravokotnicaNaSimetralo.GetParallel(-4);

	for (int i = 0; i < 3; i++)
	{
		sirinaLine[i].p1 = pravokotnicaNaSimetralo.GetIntersectionPoint(sirinaLine[i]);
		sirinaLine[i].p2 = sirinaLine[i].GetPointAtDistanceP1(-(rect.width() / 2 + 20));
		sirinaLevo[i] = (cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(sirinaLine[i], 1, 2, true, 120));
		sirinaLine[i].p2 = sirinaLine[i].GetPointAtDistanceP1((rect.width() / 2 + 20));
		sirinaDesno[i] = (cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(sirinaLine[i], 1, 2, true, 120));
		if (draw)
		{
			scene->addItem(sirinaLevo[i].DrawDot(QPen(Qt::red), QBrush(Qt::red), 5));
			scene->addItem(sirinaDesno[i].DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 5));
		}

		visinaLine[i].p1 = simetrala.GetIntersectionPoint(visinaLine[i]);
		visinaLine[i].p2 = visinaLine[i].GetPointAtDistanceP1(-(rect.height() / 2 + 20));
		visinaTop[i] = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(visinaLine[i], 1, 2, true, 120);
		visinaLine[i].p2 = visinaLine[i].GetPointAtDistanceP1((rect.height() / 2 + 20));
		visinaBottom[i] = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(visinaLine[i], 1, 2, true, 120);

		if (draw)
		{
			scene->addItem(visinaTop[i].DrawDot(QPen(Qt::red), QBrush(Qt::red), 5));
			scene->addItem(visinaBottom[i].DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 5));
		}
	}

	if (draw)
	{
		for (int i = 0; i < 3; i++)
		{
			scene->addItem(sirinaLine[i].DrawLine(rect, QPen(Qt::green)));
			scene->addItem(visinaLine[i].DrawLine(rect, QPen(Qt::blue)));
		}
	}

	if (draw)
	{
		scene->addItem(tezisceLevo.DrawDot(QPen(Qt::yellow), QBrush(Qt::yellow), 3));
		scene->addItem(tezisceDesno.DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 3));
	}

	float dolzina = 0;
	float sirina = 0;
	int dolzinaCounter = 0;
	int sirinaCounter = 0;
	float tmpDolzina, tmpSirina;
	for (int i = 0; i < 3; i++)
	{
		tmpDolzina = abs(sirinaLevo[i].GetDistance(sirinaDesno[i]));
		if (tmpDolzina > 0)
		{
			dolzina += tmpDolzina;
			dolzinaCounter++;
		}


		tmpSirina = abs(visinaTop[i].GetDistance(visinaBottom[i]));
		if (tmpSirina > 0)
		{
			sirina += tmpSirina;
			sirinaCounter++;
		}
	}
		if (dolzinaCounter > 0)
			measureObject->dolzina[nrCam][nrLamela] = dolzina / dolzinaCounter;
		else
			measureObject->dolzina[nrCam][nrLamela] = 0;

		if (sirinaCounter > 0)
			measureObject->sirina[nrCam][nrLamela] = sirina / sirinaCounter;
		else
			measureObject->sirina[nrCam][nrLamela] = 0;


		//merjenje visine

		QRect heightRect = types[nrCam][currentType]->prop[2][2]->rectValue;
		CPointFloat centerHeight = center;
		CPointFloat leftHeight = center;
		CPointFloat rightHeight = center;
		CLine widthLine;

		QRect centerHeightRect;
		QRect leftHeightRect;
		QRect rightHeightRect;
		centerHeightRect.setRect(center.x - 5, heightRect.y(), 10, heightRect.height());
		leftHeightRect.setRect(tezisceLevo.x + 15, heightRect.y(), 10, heightRect.height());
		rightHeightRect.setRect(tezisceDesno.x -15 , heightRect.y(), 10, heightRect.height());
		/*centerHeightRect.setX(center.x - 5);
		centerHeightRect.setWidth(10);
		centerHeightRect.setHeight(heightRect.height());
		centerHeightRect.setY(heightRect.y());*/

		cam[nrCam]->image[imageIndex].rect = centerHeightRect;
		cam[nrCam]->image[imageIndex].CreateIntensity();
		cam[nrCam]->image[imageIndex].HorizontalIntensity(2);
		cam[nrCam]->image[imageIndex].DetectTransitionsBackward(50);
		if (cam[nrCam]->image[imageIndex].detectedPoints.size() > 0)
		{
			centerHeight = cam[nrCam]->image[imageIndex].detectedPoints[0];
		}

		cam[nrCam]->image[imageIndex].rect = leftHeightRect;
		cam[nrCam]->image[imageIndex].CreateIntensity();
		cam[nrCam]->image[imageIndex].HorizontalIntensity(2);
		cam[nrCam]->image[imageIndex].DetectTransitionsBackward(50);
		if (cam[nrCam]->image[imageIndex].detectedPoints.size() > 0)
		{
			leftHeight = cam[nrCam]->image[imageIndex].detectedPoints[0];
		}

		cam[nrCam]->image[imageIndex].rect = rightHeightRect;
		cam[nrCam]->image[imageIndex].CreateIntensity();
		cam[nrCam]->image[imageIndex].HorizontalIntensity(2);
		cam[nrCam]->image[imageIndex].DetectTransitionsBackward(50);
		if (cam[nrCam]->image[imageIndex].detectedPoints.size() > 0)
		{
			rightHeight = cam[nrCam]->image[imageIndex].detectedPoints[0];
		}

		/*centerHeight.y = heightRect.y() + 10;
		widthLine.SetLine(rect.x()-15, centerHeight.y, rect.right()+15, centerHeight.y);
		widthLine.p1 = centerHeight;
		tocka = (cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(widthLine, 1, 2, true, 100));
		widthLine.p1 = centerHeight;
		tocka = (cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(widthLine, 1, 2, true, 100));
		*/
		if (draw)
		{
			scene->addRect(heightRect, QPen(Qt::red), QBrush(Qt::NoBrush));
			scene->addRect(centerHeightRect, QPen(Qt::yellow), QBrush(Qt::NoBrush));
			scene->addRect(leftHeightRect, QPen(Qt::yellow), QBrush(Qt::NoBrush));
			scene->addRect(rightHeightRect, QPen(Qt::yellow), QBrush(Qt::NoBrush));
			scene->addItem(centerHeight.DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 3));
			scene->addItem(leftHeight.DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 3));
			scene->addItem(rightHeight.DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 3));
			//scene->addItem(widthLine.DrawSegment(QPen(Qt::red), 1));
		}






	measureObject->isMeasured[nrCam][nrLamela] = 1;
	processingTimer[nrCam+2]->SetStop();
	processingTimer[nrCam+2]->ElapsedTime();
}

void imageProcessing::MeasureLamelRadij(int nrCam, int imageIndex,vector<Point>contour, QRect boundingBox, CPointFloat center, int nrLamela, int draw, int offseX, int offsetY)
{

RotatedRect box = fitEllipse(contour);
Point2f vtx[4];



box.points(vtx);
CPointFloat robneTocke[4];
for (int i = 0; i < 4; i++)
{
	robneTocke[i].x = vtx[i].x;
	robneTocke[i].y = vtx[i].y;
	if(draw)
	scene->addItem(robneTocke[i].DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 3));
}
CLine levoLine;
levoLine.SetLine(robneTocke[0], robneTocke[3]);
CLine desnoLine;
desnoLine.SetLine(robneTocke[1], robneTocke[2]);
float distanceL, distanceR;

distanceL = robneTocke[0].GetDistance(robneTocke[3])/2;
distanceR = robneTocke[1].GetDistance(robneTocke[2])/2;

CPointFloat levoOrient, desnoOrient;
if (robneTocke[0].x > robneTocke[3].x)
{
	levoOrient = levoLine.GetPointAtDistanceP1(-distanceL);
	desnoOrient = desnoLine.GetPointAtDistanceP1(-distanceR);
}
else
{
	levoOrient = levoLine.GetPointAtDistanceP1(distanceL);
	desnoOrient = desnoLine.GetPointAtDistanceP1(distanceR);
}

CLine simetrala; 
simetrala.SetLine(levoOrient, desnoOrient);
if (draw)
{
	scene->addItem(levoOrient.DrawDot(QPen(Qt::magenta), QBrush(Qt::magenta), 5));
	scene->addItem(desnoOrient.DrawDot(QPen(Qt::magenta), QBrush(Qt::magenta), 5));
}

CLine pravokotnicaNaSimetralo;
CLine sirinaLine[3];
CLine visinaLine[3];
CPointFloat  sirinaLevo[3];
CPointFloat sirinaDesno[3];
CPointFloat  visinaTop[3];
CPointFloat visinaBottom[3];


pravokotnicaNaSimetralo = simetrala.GetPerpendicular(center);


//widthLineDraw[nrCam][nrLamela].p1 = CPointFloat(le.x + offsetX, tezisceLevo.y + offsetY);
//widthLineDraw[nrCam][nrLamela].p2 = CPointFloat(tezisceDesno.x + offsetX, tezisceDesno.y + offsetY);


sirinaLine[0] = simetrala.GetParallel(4);
sirinaLine[1] = simetrala.GetParallel(0);
sirinaLine[2] = simetrala.GetParallel(-4);
visinaLine[0] = pravokotnicaNaSimetralo.GetParallel(4);
visinaLine[1] = pravokotnicaNaSimetralo.GetParallel(0);
visinaLine[2] = pravokotnicaNaSimetralo.GetParallel(-4);

for (int i = 0; i < 3; i++)
{
	sirinaLine[i].p1 = pravokotnicaNaSimetralo.GetIntersectionPoint(sirinaLine[i]);
	sirinaLine[i].p2 = sirinaLine[i].GetPointAtDistanceP1(-(boundingBox.width() / 2 + 50));
	sirinaLevo[i] = (cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(sirinaLine[i], 1, 2, true, 120));
	sirinaLine[i].p2 = sirinaLine[i].GetPointAtDistanceP1((boundingBox.width() / 2 + 50));
	sirinaDesno[i] = (cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(sirinaLine[i], 1, 2, true, 120));
	if (draw)
	{
		scene->addItem(sirinaLevo[i].DrawDot(QPen(Qt::red), QBrush(Qt::red), 5));
		scene->addItem(sirinaDesno[i].DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 5));
	}

	visinaLine[i].p1 = simetrala.GetIntersectionPoint(visinaLine[i]);
	visinaLine[i].p2 = visinaLine[i].GetPointAtDistanceP1(-(boundingBox.height() / 2 + 30));
	visinaTop[i] = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(visinaLine[i], 1, 2, true, 120);
	visinaLine[i].p2 = visinaLine[i].GetPointAtDistanceP1((boundingBox.height() / 2 + 30));
	visinaBottom[i] = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(visinaLine[i], 1, 2, true, 120);

	if (draw)
	{
		scene->addItem(visinaTop[i].DrawDot(QPen(Qt::red), QBrush(Qt::red), 5));
		scene->addItem(visinaBottom[i].DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 5));
	}
}

if (draw)
{
	for (int i = 0; i < 3; i++)
	{
		scene->addItem(sirinaLine[i].DrawLine(boundingBox, QPen(Qt::green)));
		scene->addItem(visinaLine[i].DrawLine(boundingBox, QPen(Qt::blue)));
	}
}


float dolzina = 0;
float sirina = 0;
int dolzinaCounter = 0;
int sirinaCounter = 0;
float tmpDolzina, tmpSirina;
for (int i = 0; i < 3; i++)
{
	tmpDolzina = abs(sirinaLevo[i].GetDistance(sirinaDesno[i]));
	if (tmpDolzina > 0)
	{
		dolzina += tmpDolzina;
		dolzinaCounter++;
	}


	tmpSirina = abs(visinaTop[i].GetDistance(visinaBottom[i]));
	if (tmpSirina > 0)
	{
		sirina += tmpSirina;
		sirinaCounter++;
	}
}
if (dolzinaCounter > 0)
measureObject->dolzina[nrCam][nrLamela] = dolzina / dolzinaCounter;
else
measureObject->dolzina[nrCam][nrLamela] = 0;

if (sirinaCounter > 0)
measureObject->sirina[nrCam][nrLamela] = sirina / sirinaCounter;
else
measureObject->sirina[nrCam][nrLamela] = 0;




CRectRotated zgorajRect;
CRectRotated spodajRect;
float distanceZg;
float distanceSp;
distanceZg = robneTocke[0].GetDistance(robneTocke[1]);
distanceSp = robneTocke[3].GetDistance(robneTocke[2]);

CLine zgorajLine;
CLine spodajLine;

zgorajLine.SetLine(robneTocke[0], robneTocke[1]);
spodajLine.SetLine(robneTocke[3], robneTocke[2]);

int fromEdge = types[nrCam][currentType]->prop[2][4]->intValue;
CPointFloat zgorajLevo = zgorajLine.GetPointAtDistanceP1(fromEdge);
CPointFloat zgorajDesno = zgorajLine.GetPointAtDistanceP2(-fromEdge);
CPointFloat sredinaLevo = simetrala.GetPointAtDistanceP1(fromEdge);
CPointFloat sredinaDesno = simetrala.GetPointAtDistanceP2(-fromEdge);
CPointFloat spodajLevo = spodajLine.GetPointAtDistanceP1(fromEdge);
CPointFloat spodajDesno = spodajLine.GetPointAtDistanceP2(-fromEdge);

float angle = simetrala.GetLineAngleInRadians();
float testAngle  = simetrala.GetLineAngleInDegrees();
measureObject->angle[nrCam][nrLamela] = testAngle;
zgorajLevo= zgorajLevo.GetPointAtDistanceAndAlpha(-20, angle + M_PI / 2);
zgorajDesno= zgorajDesno.GetPointAtDistanceAndAlpha(-20, angle + M_PI / 2);
spodajLevo= spodajLevo.GetPointAtDistanceAndAlpha(20, angle + M_PI / 2);
spodajDesno= spodajDesno.GetPointAtDistanceAndAlpha(20, angle + M_PI / 2);


if (draw)
{
	scene->addItem(zgorajLevo.DrawDot(QPen(Qt::red), QBrush(Qt::red), 5));
	scene->addItem(zgorajDesno.DrawDot(QPen(Qt::red), QBrush(Qt::red), 5));
	scene->addItem(sredinaLevo.DrawDot(QPen(Qt::red), QBrush(Qt::red), 5));
	scene->addItem(sredinaDesno.DrawDot(QPen(Qt::red), QBrush(Qt::red), 5));
	scene->addItem(spodajLevo.DrawDot(QPen(Qt::red), QBrush(Qt::red), 5));
	scene->addItem(spodajDesno.DrawDot(QPen(Qt::red), QBrush(Qt::red), 5));

}


zgorajRect.AddPoint(zgorajLevo);
zgorajRect.AddPoint(sredinaLevo);
zgorajRect.AddPoint(sredinaDesno);
zgorajRect.AddPoint(zgorajDesno);


spodajRect.AddPoint(sredinaLevo);
spodajRect.AddPoint(spodajLevo);
spodajRect.AddPoint(spodajDesno);
spodajRect.AddPoint(sredinaDesno);
vector <CPointFloat> tockeZgoraj;
vector <CPointFloat> tockeSpodaj;
CCircle zgorajCircle;
CCircle spodajCircle;
cam[nrCam]->image[imageIndex].GetFirstEdgePointsVectorRotated(zgorajRect, 1, 100, 2, true, 80, tockeZgoraj, 1, true);
cam[nrCam]->image[imageIndex].GetFirstEdgePointsVectorRotated(spodajRect, 0, 100, 2, true, 80, tockeSpodaj, 1, true);

zgorajCircle.SetCircleNpoints(tockeZgoraj, 100, 400);
spodajCircle.SetCircleNpoints(tockeSpodaj, 100, 400);
if (draw)
{
	for (int i = 0; i < tockeZgoraj.size(); i++)
	{
		scene->addItem(tockeZgoraj[i].DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 2));
	}
	for (int i = 0; i < tockeSpodaj.size(); i++)
	{
		scene->addItem(tockeSpodaj[i].DrawDot(QPen(Qt::green), QBrush(Qt::green), 2));
	}
	scene->addItem(zgorajCircle.DrawCircle(QPen(Qt::green),QBrush(Qt::NoBrush), 20));
	scene->addItem(spodajCircle.DrawCircle(QPen(Qt::yellow),QBrush(Qt::NoBrush), 20));

	scene->addItem(zgorajRect.DrawPolygonLines(QPen(Qt::blue), QBrush(Qt::NoBrush)));
	scene->addItem(spodajRect.DrawPolygonLines(QPen(Qt::green), QBrush(Qt::NoBrush)));

	scene->addItem(levoOrient.DrawDot(QPen(Qt::red), QBrush(Qt::red), 3));
	scene->addItem(desnoOrient.DrawDot(QPen(Qt::red), QBrush(Qt::red), 3));
	scene->addItem(simetrala.DrawSegment(QPen(Qt::red), 3));
	scene->addItem(zgorajRect.DrawPolygonLines(QPen(Qt::red), QBrush(Qt::NoBrush)));
}

CPointFloat krogCenter = zgorajCircle.GetCenter();

for (int i = 0; i < tockeZgoraj.size(); i++)
{
	measureObject->distanceToCircleZgoraj[nrCam][nrLamela].push_back(tockeZgoraj[i].GetDistance(krogCenter));
	measureObject->tockeKrogaZgoraj[nrCam][nrLamela].push_back(tockeZgoraj[i]);

}
krogCenter = spodajCircle.GetCenter();
for (int i = 0; i < tockeSpodaj.size(); i++)
{
	measureObject->distanceToCircleSpodaj[nrCam][nrLamela].push_back(tockeSpodaj[i].GetDistance(krogCenter));
	measureObject->tockeKrogaSpodaj[nrCam][nrLamela].push_back(tockeSpodaj[i]);
}
measureObject->krogZgoraj[nrCam][nrLamela] = zgorajCircle;
measureObject->krogSpodaj[nrCam][nrLamela] = spodajCircle;




//merjenje visine
float heightThreshold = types[nrCam][currentType]->prop[2][3]->intValue;

QRect heightRect = types[nrCam][currentType]->prop[2][2]->rectValue;
CPointFloat centerHeight = center;
CPointFloat leftHeight = center;
CPointFloat rightHeight = center;
CPointFloat right2Height = center;
CPointFloat left2Height = center;
CLine widthLine;

QRect centerHeightRect;
QRect leftHeightRect;
QRect rightHeightRect;
QRect right2HeightRect;
QRect left2HeightRect;
centerHeightRect.setRect(center.x - 5, heightRect.y(), 10, heightRect.height());
leftHeightRect.setRect(levoOrient.x + 20, heightRect.y(), 10, heightRect.height());
rightHeightRect.setRect(desnoOrient.x - 20, heightRect.y(), 10, heightRect.height());
int newrectPos;

newrectPos = ((centerHeightRect.x() - leftHeightRect.x()) / 2);
left2HeightRect.setRect(leftHeightRect.x()+ newrectPos , heightRect.y(), 10, heightRect.height());
right2HeightRect.setRect(centerHeightRect.x() + newrectPos, heightRect.y(), 10, heightRect.height());

float height = 0;
int heightCounter = 0;

cam[nrCam]->image[imageIndex].rect = centerHeightRect;
cam[nrCam]->image[imageIndex].CreateIntensity();
cam[nrCam]->image[imageIndex].HorizontalIntensity(2);
cam[nrCam]->image[imageIndex].DetectTransitionsBackward(heightThreshold);
if (cam[nrCam]->image[imageIndex].detectedPoints.size() > 0)
{
	centerHeight = cam[nrCam]->image[imageIndex].detectedPoints[0];
	height += centerHeight.y - heightRect.y();
	heightCounter++;

}
cam[nrCam]->image[imageIndex].rect = left2HeightRect;
cam[nrCam]->image[imageIndex].CreateIntensity();
cam[nrCam]->image[imageIndex].HorizontalIntensity(2);
cam[nrCam]->image[imageIndex].DetectTransitionsBackward(heightThreshold);
if (cam[nrCam]->image[imageIndex].detectedPoints.size() > 0)
{
	left2Height = cam[nrCam]->image[imageIndex].detectedPoints[0];
	height += left2Height.y - heightRect.y();
	heightCounter++;

}

cam[nrCam]->image[imageIndex].rect = right2HeightRect;
cam[nrCam]->image[imageIndex].CreateIntensity();
cam[nrCam]->image[imageIndex].HorizontalIntensity(2);
cam[nrCam]->image[imageIndex].DetectTransitionsBackward(heightThreshold);
if (cam[nrCam]->image[imageIndex].detectedPoints.size() > 0)
{
	right2Height = cam[nrCam]->image[imageIndex].detectedPoints[0];
	height += right2Height.y - heightRect.y();
	heightCounter++;

}

cam[nrCam]->image[imageIndex].rect = leftHeightRect;
cam[nrCam]->image[imageIndex].CreateIntensity();
cam[nrCam]->image[imageIndex].HorizontalIntensity(2);
cam[nrCam]->image[imageIndex].DetectTransitionsBackward(heightThreshold);
if (cam[nrCam]->image[imageIndex].detectedPoints.size() > 0)
{
	leftHeight = cam[nrCam]->image[imageIndex].detectedPoints[0];
	height += leftHeight.y - heightRect.y();
	heightCounter++;
}

cam[nrCam]->image[imageIndex].rect = rightHeightRect;
cam[nrCam]->image[imageIndex].CreateIntensity();
cam[nrCam]->image[imageIndex].HorizontalIntensity(2);
cam[nrCam]->image[imageIndex].DetectTransitionsBackward(heightThreshold);
if (cam[nrCam]->image[imageIndex].detectedPoints.size() > 0)
{
	rightHeight = cam[nrCam]->image[imageIndex].detectedPoints[0];
	height += rightHeight.y - heightRect.y();
	heightCounter++;
}
if (heightCounter > 1)
{
	height /= heightCounter;
}
else
height = 0;


CLine heightLine;

heightLine.SetLine(leftHeight, rightHeight);
float hDev[3];
 hDev[0] = heightLine.GetDistance(centerHeight);
 hDev[1] = heightLine.GetDistance(left2Height);
 hDev[2] = heightLine.GetDistance(right2Height);

 float maxDev = 0;
 for (int i = 0; i < 3; i++)
 {
	 if (hDev[i] > maxDev)
		 maxDev = hDev[i];
 }

float heightDiff = heightLine.GetDistance(centerHeight);

if (draw)
{
	scene->addItem(heightLine.DrawSegment(QPen(Qt::green), 3));

}
measureObject->visina[nrCam][nrLamela] = height;
measureObject->visinaOdstopanja[nrCam][nrLamela] = maxDev;


/*centerHeight.y = heightRect.y() + 10;
widthLine.SetLine(rect.x()-15, centerHeight.y, rect.right()+15, centerHeight.y);
widthLine.p1 = centerHeight;
tocka = (cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(widthLine, 1, 2, true, 100));
widthLine.p1 = centerHeight;
tocka = (cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(widthLine, 1, 2, true, 100));
*/
if (draw)
{
	//scene->addRect(heightRect, QPen(Qt::red), QBrush(Qt::NoBrush));
	scene->addRect(centerHeightRect, QPen(Qt::yellow), QBrush(Qt::NoBrush));
	scene->addRect(leftHeightRect, QPen(Qt::yellow), QBrush(Qt::NoBrush));
	scene->addRect(left2HeightRect, QPen(Qt::yellow), QBrush(Qt::NoBrush));
	scene->addRect(rightHeightRect, QPen(Qt::yellow), QBrush(Qt::NoBrush));
	scene->addRect(right2HeightRect, QPen(Qt::yellow), QBrush(Qt::NoBrush));
	scene->addItem(centerHeight.DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 3));
	scene->addItem(leftHeight.DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 3));
	scene->addItem(rightHeight.DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 3));
	scene->addItem(right2Height.DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 3));
	scene->addItem(left2Height.DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 3));
	//scene->addItem(widthLine.DrawSegment(QPen(Qt::red), 1));
}








}

int imageProcessing::GetLamelaIndex(int station,CPointFloat center, int imageIndex)
{
	int active = 0;
	int prevActive = -1;
	int activeArray[POMNILNIK_LAMEL];
	int activeArrayIndex = 0;
	int newIndex = 0;
	
	for(int i = 0; i < POMNILNIK_LAMEL; i++)
	{
		activeArray[i] = 0;
	}

	//ce ni nobena lamela aktivna zacnemo z 0
	for (int i = 0; i < POMNILNIK_LAMEL; i++)
	{
		if (measureObject->isActive[station][i] == 1)
		{
			activeArray[active] = i;
			active++;
		}
	}


	int meja;
	if (testMode == 0)
		meja = 300;
	else
		meja = 500;

	int tmp = -1;//preverimo h kateri lameli pripada nov center
	for (int i = 0; i < POMNILNIK_LAMEL; i++)
	{
		if (measureObject->isActive[station][i]== 1)
		{
			tmp = i;
			newIndex = measureObject->imageIndexObdelava[station][i][measureObject->measureSpeedCounter[station][i] - 1] + 1;
			if (newIndex > cam[0]->num_images - 1)
				newIndex = 0;
			if ((measureObject->center[station][i][measureObject->measureSpeedCounter[station][i] - 1].x < center.x) && (measureObject->center[station][i][measureObject->measureSpeedCounter[station][i] - 1].x + 150 > center.x) && (newIndex == imageIndex ))//ce ze ne pripda obstojeci lameli ustvari novo
			{
				measureObject->center[station][i][measureObject->measureSpeedCounter[station][i]] = center;
				measureObject->imageIndexObdelava[station][i][measureObject->measureSpeedCounter[station][i]] = imageIndex;
				measureObject->measureSpeedCounter[station][i]++;
				if (measureObject->measureSpeedCounter[station][i] > NR_CENTER)
					measureObject->measureSpeedCounter[station][i] = NR_CENTER - 1;
				if (measureObject->isProcessed[station][i] == 0)
				{
					if (measureObject->measureSpeedCounter[station][i] >= 1)
					{
						if (measureObject->isMeasured[station][i] == 1)
						{
							processingTimer[station + 2]->SetStop();
							measureObject->processTime[station][i] = processingTimer[station + 2]->ElapsedTime();
						}
						measureObject->isMeasuredSpeed[station][i] = 1;
						tmp = i;

					}
					tmp = i;
					/*measureObject->measureSpeedCounter[station][i]++;
					if (measureObject->measureSpeedCounter[station][i] > NR_CENTER)
						measureObject->measureSpeedCounter[station][i] = NR_CENTER - 1;*/
					break;
				}
				else//je ze sprocesirana postavimo nazaj na 0
				{

					if(center.x > 350)
					measureObject->isActive[station][i] = 0;
					//measureObject->ResetMeasurand(i);
					tmp = i;
					break;

				}

			}
			else
				tmp = -1;
		}
		else
		tmp = -1;

	}

	/*if (active > 2)//ce so vec kot 2 aktivni pobrisi zadnjo ker je ze obdelana
	{
		measureObject->ResetMeasurand(activeArray[0]);

	}*/

	int ret;
	 if (tmp == -1)//ce se ne pripada nobeni lameli ustvari novo
	{
		 if (center.x < meja)//tukaj daj na < 350
		 {
			 ret = measureObject->currLamela[station];
			 measureObject->ResetMeasurand(station,measureObject->currLamela[station]);
			 measureObject->isActive[station][measureObject->currLamela[station]] = 1;
			 measureObject->center[station][measureObject->currLamela[station]][0] = center;
			 measureObject->imageIndexObdelava[station][measureObject->currLamela[station]][0] = imageIndex;
			 measureObject->isProcessed[station][measureObject->currLamela[station]] = 0;
			 measureObject->measureSpeedCounter[station][measureObject->currLamela[station]]++;
			 measureObject->currLamela[station]++;
			 if (measureObject->currLamela[station] >= POMNILNIK_LAMEL)
				 measureObject->currLamela[station] = 0;

			 /*tmpPieceCounter[station]++;
			 if (tmpPieceCounter[station] > 100)
				 tmpPieceCounter[station] = 0;*/

			 return ret;

		 }
		return -1;
	}
	
return tmp;
}

int imageProcessing::ProcessingCamera3(int id, int imageIndex, int draw)//zdruzitev vseh 4ih zdruzenih slik po sirini.
{
	processingTimer[3]->SetStart();

	CLine findVertical;
	QRect detectRect;
	detectRect.setCoords(1420, 10, 1250, 2000);

	findVertical.SetLine(CPointFloat(1450, 10), CPointFloat(1450, 2000));

	scene->addItem(findVertical.DrawSegment(QPen(Qt::red), 3));
	scene->addRect(detectRect,QPen(Qt::red),QBrush(Qt::NoBrush));
	CPointFloat topPoint, bottomPoint,levoZgoraj,desnoZgoraj, levoSpodaj, desnoSpodaj;

	topPoint = cam[id]->image[imageIndex].GetEdgePointOnSegment2D(findVertical,true, 2, true, 100);
	bottomPoint = cam[id]->image[imageIndex].GetEdgePointOnSegment2D(findVertical, false, 2, true, 100);

	CLine zgoraj;
	CLine spodaj;
	QRect zgorajRect, zgorajRectDesno,spodajRect,SpodajRectDesno;

	zgorajRect.setCoords(topPoint.x - 500, topPoint.y + 50, topPoint.x, topPoint.y + 40);
	zgorajRectDesno.setCoords(topPoint.x , topPoint.y + 50, topPoint.x+500, topPoint.y + 40);
	spodajRect.setCoords(bottomPoint.x-500 , bottomPoint.y - 50, bottomPoint.x, bottomPoint.y - 40);
	SpodajRectDesno.setCoords(bottomPoint.x , bottomPoint.y - 50, bottomPoint.x+500, bottomPoint.y - 40);

	cam[id]->image[imageIndex].rect = zgorajRect;
	cam[id]->image[imageIndex].CreateIntensity();
	cam[id]->image[imageIndex].VerticalIntensity(2);
	cam[id]->image[imageIndex].DetectTransitionsBackward(100);
	//scene->addItem(cam[id]->image[imageIndex].DrawIntensity(QPen(Qt::yellow)));
	if (cam[id]->image[imageIndex].detectedPoints.size() > 0)
	{
		levoZgoraj = cam[id]->image[imageIndex].detectedPoints[0];
	}


	cam[id]->image[imageIndex].rect = zgorajRectDesno;
	cam[id]->image[imageIndex].CreateIntensity();
	cam[id]->image[imageIndex].VerticalIntensity(2);
	cam[id]->image[imageIndex].DetectTransitions(100);
	//scene->addItem(cam[id]->image[imageIndex].DrawIntensity(QPen(Qt::yellow)));
	if (cam[id]->image[imageIndex].detectedPoints.size() > 0)
	{
		desnoZgoraj = cam[id]->image[imageIndex].detectedPoints[0];
	}


	cam[id]->image[imageIndex].rect = spodajRect;
	cam[id]->image[imageIndex].CreateIntensity();
	cam[id]->image[imageIndex].VerticalIntensity(2);
	cam[id]->image[imageIndex].DetectTransitionsBackward(100);
	//scene->addItem(cam[id]->image[imageIndex].DrawIntensity(QPen(Qt::yellow)));
	if (cam[id]->image[imageIndex].detectedPoints.size() > 0)
	{
		levoSpodaj = cam[id]->image[imageIndex].detectedPoints[0];
	}


	cam[id]->image[imageIndex].rect = SpodajRectDesno;
	cam[id]->image[imageIndex].CreateIntensity();
	cam[id]->image[imageIndex].VerticalIntensity(2);
	cam[id]->image[imageIndex].DetectTransitions(100);
	//scene->addItem(cam[id]->image[imageIndex].DrawIntensity(QPen(Qt::yellow)));
	if (cam[id]->image[imageIndex].detectedPoints.size() > 0)
	{
		desnoSpodaj = cam[id]->image[imageIndex].detectedPoints[0];
	}

	QRect zgorajDetect, spodajDetect;

	zgorajDetect.setCoords(levoZgoraj.x - 25, topPoint.y - 25, desnoZgoraj.x + 25, topPoint.y + 150);
	spodajDetect.setCoords(levoSpodaj.x - 25, bottomPoint.y + 25, desnoSpodaj.x + 25, bottomPoint.y - 150);

	scene->addRect(zgorajDetect, QPen(Qt::red), QBrush(Qt::NoBrush));
	scene->addRect(spodajDetect, QPen(Qt::red), QBrush(Qt::NoBrush));


	/*cam[id]->image[imageIndex].CreateCommonPointsWhite(150, 50000);
	cam[id]->image[imageIndex].FindCommonPointsWhiteFastBoundingBox(200, zgorajDetect);
	//cam[id]->image[imageIndex].FilterWhiteShapesBySize(5, zgorajDetect.width() - 100, 5, zgorajDetect.height() - 10);
	scene->addItem(cam[id]->image[imageIndex].DrawCommonPointsBoundingBoxTest(QRect(0, 0, 2448, 2048)));
	*/

	cam[id]->image[imageIndex].CreateCommonPointsBlack(150, 50000);
	cam[id]->image[imageIndex].FindCommonPointsBlackFastBoundingBox(130, zgorajDetect);
	//cam[id]->image[imageIndex].FilterBlackShapesBySize(5, zgorajDetect.width() - 100, 5, zgorajDetect.height() - 10);
	scene->addItem(cam[id]->image[imageIndex].DrawCommonPointsBoundingBoxTest(QRect(0, 0, 2448, 2048)));

	cam[id]->image[imageIndex].FindCommonPointsBlackFastBoundingBox(120, spodajDetect);
	//cam[id]->image[imageIndex].FilterBlackShapesBySize(2, spodajDetect.width() - 50, 2, abs(spodajDetect.height() - 10));
	scene->addItem(cam[id]->image[imageIndex].DrawCommonPointsBoundingBoxTest(QRect(0, 0, 2448, 2048)));

	/*zgoraj.SetLine(CPointFloat(topPoint.x, topPoint.y + 50), 0);
	zgoraj.p2 = zgoraj.GetPointAtDistanceP2(-500);
	levoZgoraj = cam[id]->image[imageIndex].GetEdgePointOnSegment2D(zgoraj, true, 2, true, 30);
	zgoraj.p2 = zgoraj.GetPointAtDistanceP2(1000);
	desnoZgoraj = cam[id]->image[imageIndex].GetEdgePointOnSegment2D(zgoraj, true, 2, true, 30);
	spodaj.SetLine(CPointFloat(bottomPoint.x, bottomPoint.y - 50), 90);

	scene->addItem(zgoraj.DrawSegment(QPen(Qt::yellow), 3));
	scene->addItem(spodaj.DrawSegment(QPen(Qt::yellow), 3));*/

/*	cam[id]->image[imageIndex].rect = detectRect;
	cam[id]->image[imageIndex].CreateIntensity();
	cam[id]->image[imageIndex].HorizontalIntensity(2);
	cam[id]->image[imageIndex].DetectTransitions(60);
	scene->addItem(cam[id]->image[imageIndex].DrawIntensity(QPen(Qt::yellow)));
	if (cam[id]->image[imageIndex].detectedPoints.size() > 0)
	{
		topPoint =cam[id]->image[imageIndex].detectedPoints[0];
	}
	cam[id]->image[imageIndex].DetectTransitionsBackward(60);
	if (cam[id]->image[imageIndex].detectedPoints.size() > 0)
	{
		bottomPoint = cam[id]->image[imageIndex].detectedPoints[0];
	}
	*/
	scene->addItem(topPoint.DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 5));
	scene->addItem(bottomPoint.DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 5));
	scene->addItem(levoZgoraj.DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 5));
	scene->addItem(desnoZgoraj.DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 5));
	scene->addItem(levoSpodaj.DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 5));
	scene->addItem(desnoSpodaj.DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 5));


	processingTimer[3]->SetStop();
	processingTimer[3]->ElapsedTime();


	return 0;
}

int imageProcessing::ProcessingCamera4(int id, int imageIndex, int draw) //nova funkcija za poteg karakteristike  modula X os 
{
	ClearDraw();



	return 0;
}

int imageProcessing::CheckDefects(int nrCam, int piece,int draw)
{
	processingTimer[nrCam + 2]->SetStart();


		if (draw)
		{
			for (int i = 0; i < 50; i++)
			{
				ui.listFunctionsReturn->item(i)->setText("");
			}

			int index = 0;
			//ui.listFunctionsReturn->item(index)->setText(QString("Standard Deviation: %1").arg(measureObject->stDev[nrCam][nrPiece]));
			//index++;
		
		}


	
	processingTimer[nrCam + 2]->SetStop();
	processingTimer[nrCam + 2]->ElapsedTime();
	return 0;
}






int imageProcessing::InspectSurface(CMemoryBuffer *image, Rect region,int  imageIndex, int nrPiece, int draw)
{
	
//	Mat img = images[imageIndex]->buffer[0](region);
	

	//int blurFilterSize = types[currentStation][currentType]->prop[8][6]->intValue;
	




	return 0;
}




