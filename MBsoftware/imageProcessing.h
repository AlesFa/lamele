﻿#pragma once

#include <QWidget>
#include "ui_imageProcessing.h"
#include "Camera.h"
#include "CCustomGraphicsItem.h"
#include "Timer.h"
#include "Measurand.h"
#include "qtpropertymanager.h"
#include "qtvariantproperty.h"
#include "qttreepropertybrowser.h"
#include "Types.h"

using namespace std;
using namespace cv;

struct ImageParameter
{
	QString name;
	float value;
	int row;
	int col;

};

class imageProcessing : public QWidget
{
	Q_OBJECT

public:
	imageProcessing(QWidget *parent = Q_NULLPTR);
	void closeEvent(QCloseEvent * event);
	//imageProcessing();
	~imageProcessing();


	static std::vector<CCamera*>									cam;
	static std::vector<CMemoryBuffer*>								images;
	static std::vector<Timer*>										processingTimer;
	static vector<vector<vector<vector<vector<vector<float>>>>>>	parameterTable;
	static Measurand*												measureObject;
	static std::vector<Types*>										types[NR_STATIONS];
	static CMemoryBuffer*											currImage;
	static Mat														DrawImage;
	static QPoint													origin;
	static float													relativeFrequencies[256];

	float zoomFactor;
	float zoomMultiplier;
	int displayWindow;
	int displayImage;
	int prevDisplayWindow;
	int prevDisplayImage;
	int rowNum;
	int colNum;
	int currentStation;
	bool isSceneAdded;
	int loginRights;

	QRubberBand*  rubberBand;
	QPoint pos;
	QPoint pos2;
	int start;

	QGraphicsPixmapItem*	pixmapVideo;

	void ConnectImages(std::vector<CCamera*> inputCam, std::vector <CMemoryBuffer*> inputImages);
	void ConnectMeasurand(Measurand* objects);
	void ConnectTypes(int station,std::vector<Types*> type);
	void ConnectImages(std::vector<CCamera*> inputCam);
	void ShowDialog(int rights);
	void ClearDialog();
	void ReplaceCurrentBuffer();
	//void Addparameter(int param_num, QString paramName, float pramValue, int row, int col);
	//void ReplaceParameters();
	//void RemoveParameters();
	//void UpdateParameters();
	//void SaveStatistic(QString filename, float mean, float sd);
	void test();
	//vector<ImageParameter> GetCurrentParameters(int typeNum, int stationNum, int camNum, int imgNum);
	QGraphicsScene*	scene;
	QGraphicsScene*	sceneForMainWindow;

	//qpropertyBrowser za lastnosti tipov in funkcij
	QtVariantPropertyManager *variantManager; //dodajamo QtProperty in QtVariantProperty
	QtVariantEditorFactory *variantFactory; //dodat v editor
	QtTreePropertyBrowser *variantEditor; //widget za prikaz GPropertyBrowser
	QtProperty *topItem;				//top item
	vector<QtVariantProperty*> item;
	int currentFunction;
	int currentType;
	void PopulatePropertyBrowser(int function);			//prepise tabele v qpropertyBrowser
	void SaveFunctionParameters(int type, int currentFunction);//shrani v datoteke parametre funkcije kadar se spreminja oz dodaja parametri
	void SaveFunctionParametersOnNewType(int station, int type, int currentFunction);//shrani v datoteke parametre funkcije kadar se spreminja oz dodaja parametri
	void DeleteTypeProperty(QString type);
	void LoadTypesProperty();



	//std::vector<QtVariantProperty> *item;	//subITEM


protected:
	bool eventFilter(QObject *obj, QEvent *event);




private:
	Ui::imageProcessing ui;
	
	QGraphicsEllipseItem* ellipses;
	QGraphicsRectItem* rectangles;
	QGraphicsLineItem* lines;
	QGraphicsPolygonItem* polygons;
	QGraphicsPixmapItem* pixels;
	QGraphicsPathItem* paths;
	CCustomGraphicsItem* myItem;

	vector<QGridLayout*> gridLayout;
	vector<QLabel*> paramLabels;
	vector<QLineEdit*> paramLineEdit;
	vector<QToolButton*> removeButton;
	QSignalMapper* signalMapper;


public slots:
	void OnPressedImageUp();
	void OnPressedImageDown();
	void OnPressedCamUp();
	void OnPressedCamDown();
	int ConvertImageForDisplay(int imageNumber);
	void ShowImages();
	void ResizeDisplayRect();
	void OnProcessCam0(); //za klicanje obdelave iz gumba
	void OnProcessCam1();
	void OnProcessCam2();
	void OnProcessCam3();
	void OnProcessCam4();
	void OnDoneEditingLineInsertImageIndex();
	void onPressedAddParameter();
	void onPressedRemoveParameters();
	void onPressedUpdateParameters();
	void onPressedSurfaceInspection();
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);

	int ProcessingCamera0(int id, int imageIndex,int draw); //uporabi za iskanje tezisc na laserski crti
	int ProcessingCamera1(int id, int imageIndex, int draw);// obdelava kamera 1
	int ProcessingCamera2(int id, int imageIndex, int draw);// obdelava kamera 2
	int ProcessingCamera3(int id, int imageIndex, int draw);// obdelava kamera karakteristika
	int ProcessingCamera4(int id, int imageIndex, int draw);// obdelava kamera karakteristika
	int CheckDefects(int indexImage, int nrPiece, int draw);

	//Obdelava površine
	//Funkcija za izračun orientacije objekta/oblike na binarni sliki. Vrne kot v radianih!
		int InspectSurface(CMemoryBuffer *image, Rect region, int imageIndex, int nrPiece, int draw);


	void ClearDraw();
	void SetCurrentBuffer(int dispWindow, int dispImage);
	void ZoomOut();
	void ZoomIn();
	void ZoomReset();
	void OnLoadImage();
	void OnLoadMultipleImage();
	void OnSaveImage();

signals:
	void imagesReady();




//za porjekt lamele
public:
	void MeasureLamel(int nrCam, int imageIndex, QRect boundingBox, CPointFloat center, int nrLamela, int draw, int offseX, int offsetY);
	void MeasureLamelRadij(int nrCam, int imageIndex, vector<Point>,QRect boundingBox, CPointFloat center, int nrLamela, int draw, int offseX, int offsetY);
	//void MeasureLamelRadij(int nrCam, int imageIndex, QRect boundingBox, CPointFloat center, int nrLamela, int draw, int offseX, int offsetY);
	int  GetLamelaIndex(int nrCam, CPointFloat center,int imageIndex);
	bool threadsRunning[10];
	int tmpPieceCounter[NR_STATIONS];

	Mat displayLamela[NR_STATIONS][POMNILNIK_LAMEL];
	Mat displayLamelaSide[NR_STATIONS][POMNILNIK_LAMEL];
	CPointFloat	obrobaLameleDraw[NR_STATIONS][POMNILNIK_LAMEL][180];
	CLine	widthLineDraw[NR_STATIONS][POMNILNIK_LAMEL];
	CLine heightLineDraw[NR_STATIONS][POMNILNIK_LAMEL];
	bool testMode;
	int obdelanaCounter[NR_STATIONS];


};
