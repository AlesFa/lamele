#pragma once

#include <QWidget>
#include "ui_GeneralSettings.h"

class GeneralSettings : public QWidget
{
	Q_OBJECT

public:
	//GeneralSettings(QWidget *parent = Q_NULLPTR);
	GeneralSettings();
	~GeneralSettings();
	void closeEvent(QCloseEvent * event);


	bool testIOMode;
	float disToValveS0;
	float disToValveS1;
	int valveDurationS0;
	int valveDurationS1;
	int valve2DelayS0;
	int valve2DelayS1;
	int valve2DurationS0;
	int valve2DurationS1;


	bool cleanEnable;
	int cleanPeriod;
	int cleanDurationBottom;
	int cleanDelayBottom;
	int cleanDurationTop;
	int cleanDelayTop;
	
	bool elevatorVibratorEnable;

	int pieceInBox;
	bool stopSystemEn;
	int autostopTime;
	int workingMode;

	bool outSignalEnable;
	int outSignalDuration;
	bool inputSignalEnable;

	void OnShowDialog();
	void SaveSettings();
	int LoadSettings();
	void SetState();

private:
	Ui::GeneralSettings ui;


public slots:
	void	OnConfirm();
	void	OnRejected();
	void	OnClickedNonStop();
	void	OnClickedStopMode();
	void	OnClickedStopDelayMode();



};
