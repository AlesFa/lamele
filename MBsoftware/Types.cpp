#include "stdafx.h"
#include "Types.h"





Types::Types()
{
	ui.setupUi(this);
	

}


Types::~Types()
{
}


void Types::Init(QString name, QString filePath)
{
	signalMapper = new QSignalMapper(this);
	id = 0;
	conditionalMeasurementsCounter = 0;
	goodMeasurementsCounter = 0;
	badMeasurementsCounter = 0;
	totalMeasurementsCounter = 0;
	goodPercentage = 0;
	badPercentage = 0;
	parametersChanged = true;
	typeName = name;
	typePath = filePath;
	ReadParameters(filePath);
	ReadCounters();

	for (int i = 0; i < parameterCounter; i++)
	{
		measuredValue.push_back(0);
		isGood.push_back(0);
		conditional.push_back(0);
	}

	prop.resize(20); //definirano za lastnosti 20 funkcij
	//prop[0].size();


	connect(ui.buttonOK, SIGNAL(clicked()), this, SLOT(OnConfirm()));
	connect(ui.buttonAddParameter, SIGNAL(clicked()), this, SLOT(OnAddParameter()));
	connect(ui.butttonCancel, SIGNAL(clicked()), this, SLOT(OnCancel()));
	connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(OnRemoveParameter(int)));
}

void Types::ReadParameters(QString filePath)
{
	QStringList values;
	int count = 0;

	QString path = filePath + QString("/%1.ini").arg(typeName);
	QSettings settings(path, QSettings::IniFormat);


	//inputs
	do {
		values.clear();
		values = settings.value(QString("parameter%1").arg(count)).toStringList();

		if (values.size() > 1)
		{
			//input[j].SetSignal(values[0], address, values[1].toInt());

			toleranceHigh.push_back(values[0].toFloat());
			toleranceLow.push_back(values[1].toFloat());
			correctFactor.push_back(values[2].toFloat());
			offset.push_back(values[3].toFloat());
			nominal.push_back(values[4].toFloat());
			toleranceHighCond.push_back(values[5].toFloat());
			toleranceLowCond.push_back(values[6].toFloat());
			isActive.push_back(values[7].toInt());
			isConditional.push_back(values[8].toInt());
			name.push_back(values[9]);
			AddParameter(count);
			count++;

			
		}
	} while (values.size() > 0);

	parameterCounter = count;


}

void Types::WriteParameters()
{
	QStringList list;
	int count = 0;

	QString path = typePath + QString("/%1.ini").arg(typeName);
	QSettings settings(path, QSettings::IniFormat);
	QFile::remove(path);
	QFile file(path);
	for (int i = 0; i < parameterCounter; i++)
	{
		list << QString("%1").arg(toleranceHigh[i]) << QString("%1").arg(toleranceLow[i]) << QString("%1").arg(correctFactor[i])
		<< QString("%1").arg(offset[i]) << QString("%1").arg(nominal[i]) << QString("%1").arg(toleranceHighCond[i]) << QString("%1").arg(toleranceLowCond[i]) << QString("%1").arg(isActive[i]) << QString("%1").arg(isConditional[i]) << name[i];
		settings.setValue(QString("parameter%1").arg(i), list);
		list.clear();
	}
	if (parameterCounter == 0)//pri ustvarjanju novih tipov. ce file ne obstaja ga naredi
	{
		settings.setValue("test", "value");
		settings.sync();
	}


}

void Types::WriteParametersBackUp()
{
	QStringList list;
	int count = 0;

	QString path = typePath + QString("/BackUpTypes/");

	if (!QDir(path).exists())
		QDir().mkdir(path);



	path = typePath + QString("/BackUpTypes/%1_%2_%3_%4_%5_%6.ini").arg(typeName).arg(QDate::currentDate().day()).arg(QDate::currentDate().month()).arg(QDate::currentDate().year()).arg(QTime::currentTime().hour()).arg(QTime::currentTime().minute());

	QSettings settings(path, QSettings::IniFormat);

	for (int i = 0; i < parameterCounter; i++)
	{
		list << QString("%1").arg(toleranceHigh[i]) << QString("%1").arg(toleranceLow[i]) << QString("%1").arg(correctFactor[i])
		<< QString("%1").arg(offset[i]) << QString("%1").arg(nominal[i]) << QString("%1").arg(toleranceHighCond[i]) << QString("%1").arg(toleranceLowCond[i]) << QString("%1").arg(isActive[i]) << QString("%1").arg(isConditional[i]) << name[i];
		settings.setValue(QString("parameter%1").arg(i), list);
		list.clear();
	}


}

void Types::ReadCounters()
{
	QString filename = typePath + QString("/counters_%1.txt").arg(typeName);
	QString line;

	QFile file(filename);
	if (file.open(QIODevice::ReadOnly))
	{
		QTextStream stream(&file);


		line = stream.readLine();
		goodMeasurementsCounter = line.toInt();

		line = stream.readLine();
		badMeasurementsCounter = line.toInt();

		line = stream.readLine();
		conditionalMeasurementsCounter = line.toInt();

		file.close();
	}
}

void Types::OnShowDialog(int rights)
{
	ui.labelTypeName->setText(typeName);
	if ((rights == 1) || (rights == 2))
	{
		
		for (int i = 0; i < parameterCounter; i++)
		{
			checkActive[i]->setEnabled(true);
			checkConditional[i]->setEnabled(true);
			editCorrection[i]->setEnabled(true);
			editMaxTol[i]->setEnabled(true);
			editMinTol[i]->setEnabled(true);
			editMinTolCond[i]->setEnabled(true);
			editMaxTolCond[i]->setEnabled(true);
			editNominal[i]->setEnabled(true);
			editOffset[i]->setEnabled(true);
			paramEditLine[i]->setEnabled(true);
			if(rights == 1)
				removeButton[i]->setEnabled(true);
			else
				removeButton[i]->setEnabled(false);
		}
		if (rights == 1)
		{
			//ui.buttonAddParameter->setEnabled(true); tuki za primer naprave v spanji zaradi napake pri dodajanju parametrovmen
			ui.buttonAddParameter->setEnabled(false);
		}
		else
			ui.buttonAddParameter->setEnabled(false);


	}
	else
	{
		for (int i = 0; i < parameterCounter; i++)
		{
			checkActive[i]->setEnabled(false);
			checkConditional[i]->setEnabled(false);
			editCorrection[i]->setEnabled(false);
			editMaxTol[i]->setEnabled(false);
			editMinTol[i]->setEnabled(false);
			editMinTolCond[i]->setEnabled(false);
			editMaxTolCond[i]->setEnabled(false);
			editNominal[i]->setEnabled(false);
			editOffset[i]->setEnabled(false);
			removeButton[i]->setEnabled(false);
			paramEditLine[i]->setEnabled(false);
		}

	ui.buttonAddParameter->setEnabled(false);

	}
	setWindowModality(Qt::ApplicationModal);
	show();
}

void Types::WriteCounters()
{
	QString filename = typePath + QString("/counters_%1.txt").arg(typeName);
	QString line;

	QFile file(filename);
	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QTextStream stream(&file);

		stream << goodMeasurementsCounter;
		stream << endl;
		stream << badMeasurementsCounter;
		stream << endl;
		stream << conditionalMeasurementsCounter;
		stream << endl;

		file.close();
	}
}
void Types::ResetCounters()
{

	goodMeasurementsCounter = 0;
	badMeasurementsCounter = 0;
	conditionalMeasurementsCounter = 0;
}



void Types::SetMeasuredValue(float value, int nParam)
{
	measuredValue[nParam] = value * correctFactor[nParam] + offset[nParam];
	IsGood(nParam); //preveri ce je v toleranci
}

void Types::SetMeasuredValue(int value, int nParam)
{
	measuredValue[nParam] = value * correctFactor[nParam] + offset[nParam];
	IsGood(nParam);//preveri ce je v toleranci
}

void Types ::SetMeasuredValue(bool value, int nParam)
{
	measuredValue[nParam] = value;
	IsGood(nParam);//preveri ce je v toleranci
}

int Types::IsGood(int nParam)	//check if nominal is set
{
	if (nominal[nParam] == 0.0)
	{
		if ((measuredValue[nParam] >= (toleranceLow[nParam])) && (measuredValue[nParam] <= (toleranceHigh[nParam])))
		{
			isGood[nParam] = 1;
			return 1;
		}
		else
		{
			if (isConditional[nParam] == 1)
			{
				if (abs(toleranceHighCond[nParam] + toleranceLowCond[nParam]) > 0) //pogojna parametra
				{
					if ((measuredValue[nParam] >= (toleranceLowCond[nParam])) && (measuredValue[nParam] <= (toleranceHighCond[nParam])))
					{
						isGood[nParam] = 2;
						return 2;
					}
				}
			}
			isGood[nParam] = 0;
			return 0;
		}

	}
	else //nominal is set check tolerance + nominal 
	{
		if ((measuredValue[nParam] >= (toleranceLow[nParam] + nominal[nParam])) && (measuredValue[nParam] <= (toleranceHigh[nParam] + nominal[nParam])))
		{
			isGood[nParam] = 1;
			return 1;
		}
		else
		{
			if (isConditional[nParam] == 1)
			{
				if (abs(toleranceHighCond[nParam] + toleranceLowCond[nParam]) > 0) //pogojna parametra
				{
					if ((measuredValue[nParam] >= (nominal[nParam] - toleranceLowCond[nParam])) && (measuredValue[nParam] <= (nominal[nParam] + toleranceHighCond[nParam])))
					{
						isGood[nParam] = 2;
						return 2;
					}
				}

			}
			isGood[nParam] = 0;
			return 0;

		}
	}
	isGood[nParam] = 0;
	return 0;
}

int Types::AllGood(void)
{
	int i;
	allGood = 1;
	for (i = 0; i < parameterCounter; i++)
	{
		if (isActive[i])
		{
			if (IsGood(i) != 1)
			{
				allGood = 0;
				return allGood;
			}

		}
	}

	return allGood;
}
int Types::IsConditional(void)
{
	int allCounter = 0;
	int condCounter = 0;
	int allCond = 0;
	int goodCounter = 0;
	int badCounter = 0;
	int curr;
	int cond = 1;
	for (int i = 0; i < parameterCounter; i++)
	{
		if (isActive[i])
		{
			allCounter++;
			if (isConditional[i])
				allCond++;

			curr = IsGood(i);
			if (curr == 1)
				goodCounter++;
			else if (curr == 2)
				condCounter++;
			else
				badCounter++;

	
		}

	}
	if (badCounter > 0)
		cond = 0;
	else if (allCounter == goodCounter)
		cond = 1;
	else 
		cond = 2;


	return cond;

}
int Types::SaveMeasurements(QString path)
{

		QString currentFilepath, output;
		int good, size, i, j, spaceSize;

		good = AllGood();


		currentFilepath = path;

		if(!QDir(path).exists())
		QDir().mkdir(path);


		currentFilepath.append(QString("/%1").arg(typeName));
		//currentFilepath.append(("%s\\"), (CStringA)typeName);

		if (!QDir(currentFilepath).exists())
			QDir().mkdir(currentFilepath);


		currentFilepath.append(QString("/measurments_%1_%2_%3.txt").arg(QDate::currentDate().day()).arg(QDate::currentDate().month()).arg(QDate::currentDate().year()));

		
		QString a;
		int rowSize = 0;

		QFile file(currentFilepath);

		
		if (file.open(QIODevice::ReadWrite | QIODevice::Text))
		{
			QTextStream in(&file);
			size = file.size();
	
	
				in.readAll();
		
			if (size < 50) // ce je prazna datoteka, se nimamo oznak kot
			{
				//izpisuje samo aktivne kote
				in << "COUNTER\tDATE______\tTIME_\tTYPE_\tGOOD\t";

				for (i = 0; i < parameterCounter; i++)
				{
					if (isActive[i])
					{
						in << QString("%1").arg(name[i]);
						//fprintf_s(File, "%s", (CStringA)name[i]);
						/*spaceSize = 20 - name[i].length();
						for (j = 0; j < spaceSize; j++)
							in <<"_";
							*/
						in <<"\tMIN TOL\tMAX TOL\t";
					}

				}
				in << endl;
			}
			a = QString("%1\t").arg(id);
			in << a;  //tukaj dodamo se ID

			 a =  QString("%1.%2.%3\t%4:%5:%6\t%7\t%8\t").arg(QDate::currentDate().day()).arg(QDate::currentDate().month()).arg(QDate::currentDate().year())
				.arg(QTime::currentTime().hour()).arg(QTime::currentTime().minute()).arg(QTime::currentTime().second()).arg(typeName).arg(good);
				//dateTime.tm_mday, dateTime.tm_mon, dateTime.tm_year,
				//dateTime.tm_hour, dateTime.tm_min, dateTime.tm_sec,
				//(CStringA)typeName, good); // datum, cas opreater
			 in << a;
			for (i = 0; i < parameterCounter; i++)
			{
				if (isActive[i])
				{
					a = QString("%1 \t%2 \t%3 \t").arg(measuredValue[i]).arg(toleranceLow[i]).arg(toleranceHigh[i]);
					in << a;
				}
			}
			in << endl;

			file.close();
			return 1;



		}

	return 0;
}
void Types::AddParameter(int index)
{
	QString editString;
	int height;
	ui.verticalLayout->setSpacing(6);
	QSpacerItem		*spacer;
	spacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Expanding);;
	
	while (horizontalLayoutParam.size() <= index)
		horizontalLayoutParam.push_back(new QHBoxLayout());

	horizontalLayoutParam[index]->setSpacing(6);
	editString = QString("horizontalLayoutParam%1").arg(index);
	horizontalLayoutParam[index]->setObjectName(editString);
	horizontalLayoutParam[index]->setSizeConstraint(QLayout::SetDefaultConstraint);

	editString = QString("labelStev%1").arg(index);
	while (labelStevilka.size() <= index)
		labelStevilka.push_back(new QLabel(ui.verticalLayoutWidget));

	labelStevilka[index]->setObjectName(editString);
	labelStevilka[index]->setMinimumSize(QSize(40, 25));
	labelStevilka[index]->setMaximumSize(QSize(40, 30));
	labelStevilka[index]->setAlignment(Qt::AlignCenter);
	labelStevilka[index]->setText(QString("%1").arg(index));
	horizontalLayoutParam[index]->addWidget(labelStevilka[index]);

	editString = QString("editName%1").arg(index);
	while (paramEditLine.size() <= index)
		paramEditLine.push_back(new QLineEdit(ui.verticalLayoutWidget));

	paramEditLine[index]->setObjectName(editString);
	paramEditLine[index]->setMinimumSize(QSize(200, 25));
	paramEditLine[index]->setMaximumSize(QSize(200, 30));
	paramEditLine[index]->setText(name[index]);
	horizontalLayoutParam[index]->addWidget(paramEditLine[index]);

	editString = QString("editMinTol%1").arg(index);
	while (editMinTol.size() <= index)
		editMinTol.push_back(new QLineEdit(ui.verticalLayoutWidget));

	editMinTol[index]->setObjectName(editString);
	editMinTol[index]->setMinimumSize(QSize(120, 25));
	editMinTol[index]->setMaximumSize(QSize(120, 30));
	editMinTol[index]->setText(QString("%1").arg(toleranceLow[index]));
	horizontalLayoutParam[index]->addWidget(editMinTol[index]);

	editString = QString("editMaxTol%1").arg(index);
	while (editMaxTol.size() <= index)
		editMaxTol.push_back(new QLineEdit(ui.verticalLayoutWidget));

	editMaxTol[index]->setObjectName(editString);
	editMaxTol[index]->setMinimumSize(QSize(120, 25));
	editMaxTol[index]->setMaximumSize(QSize(120, 30));
	editMaxTol[index]->setText(QString("%1").arg(toleranceHigh[index]));
	horizontalLayoutParam[index]->addWidget(editMaxTol[index]);

	editString = QString("editNominal%1").arg(index);
	while (editNominal.size() <= index)
		editNominal.push_back(new QLineEdit(ui.verticalLayoutWidget));

	editNominal[index]->setObjectName(editString);
	editNominal[index]->setMinimumSize(QSize(120, 25));
	editNominal[index]->setMaximumSize(QSize(120, 30));
	editNominal[index]->setText(QString("%1").arg(nominal[index]));
	horizontalLayoutParam[index]->addWidget(editNominal[index]);

	editString = QString("editOffset%1").arg(index);
	while (editOffset.size() <= index)
		editOffset.push_back(new QLineEdit(ui.verticalLayoutWidget));

	editOffset[index]->setObjectName(editString);
	editOffset[index]->setMinimumSize(QSize(120, 25));
	editOffset[index]->setMaximumSize(QSize(120, 30));
	editOffset[index]->setText(QString("%1").arg(offset[index]));

	horizontalLayoutParam[index]->addWidget(editOffset[index]);

	editString = QString("editCorrection%1").arg(index);
	while (editCorrection.size() <= index)
		editCorrection.push_back(new QLineEdit(ui.verticalLayoutWidget));

	editCorrection[index]->setObjectName(editString);
	editCorrection[index]->setMinimumSize(QSize(120, 25));
	editCorrection[index]->setMaximumSize(QSize(120, 30));
	editCorrection[index]->setText(QString("%1").arg(correctFactor[index]));
	horizontalLayoutParam[index]->addWidget(editCorrection[index]);

	editString = QString("editMinTolCond%1").arg(index);
	while (editMinTolCond.size() <= index)
		editMinTolCond.push_back(new QLineEdit(ui.verticalLayoutWidget));

	editMinTolCond[index]->setObjectName(editString);
	editMinTolCond[index]->setMinimumSize(QSize(120, 25));
	editMinTolCond[index]->setMaximumSize(QSize(120, 30));
	editMinTolCond[index]->setText(QString("%1").arg(toleranceLowCond[index]));
	horizontalLayoutParam[index]->addWidget(editMinTolCond[index]);

	editString = QString("editMaxTolCond%1").arg(index);
	while (editMaxTolCond.size() <= index)
		editMaxTolCond.push_back(new QLineEdit(ui.verticalLayoutWidget));

	editMaxTolCond[index]->setObjectName(editString);
	editMaxTolCond[index]->setMinimumSize(QSize(120, 25));
	editMaxTolCond[index]->setMaximumSize(QSize(120, 30));
	editMaxTolCond[index]->setText(QString("%1").arg(toleranceHighCond[index]));
	horizontalLayoutParam[index]->addWidget(editMaxTolCond[index]);


	editString = QString("checkActive%1").arg(index);
	while (checkActive.size() <= index)
		checkActive.push_back(new QCheckBox(ui.verticalLayoutWidget));

	checkActive[index]->setObjectName(editString);
	checkActive[index]->setChecked(isActive[index]);
	checkActive[index]->setMinimumSize(QSize(120, 25));
	checkActive[index]->setMaximumSize(QSize(120, 30));
	horizontalLayoutParam[index]->addWidget(checkActive[index]);

	editString = QString("checkConditional%1").arg(index);
	while (checkConditional.size() <= index)
		checkConditional.push_back(new QCheckBox(ui.verticalLayoutWidget));

	checkConditional[index]->setObjectName(editString);
	checkConditional[index]->setChecked(isConditional[index]);
	checkConditional[index]->setMinimumSize(QSize(120, 25));
	checkConditional[index]->setMaximumSize(QSize(120, 30));
	horizontalLayoutParam[index]->addWidget(checkConditional[index]);


	while (removeButton.size() <= index)
		removeButton.push_back(new QToolButton(ui.verticalLayoutWidget));


	QIcon XIcon;
	QString fileP  = QDir::currentPath() + QString("\\res\\X.bmp").arg(index);
	//QString fileP =   QString("C:\\projekti\\PalckeNew\\MBsoftware32Bit\\res\\X.bmp");
	QImage image(fileP);
	if (!image.isNull())
		XIcon.addPixmap(QPixmap::fromImage(image));
		removeButton[index]->setMinimumSize(QSize(25, 25));
		removeButton[index]->setMaximumSize(QSize(25, 25));
		removeButton[index]->setIconSize(QSize(25, 25));


		removeButton[index]->setIcon(XIcon);

		horizontalLayoutParam[index]->addWidget(removeButton[index]);



		connect(removeButton[index], SIGNAL(clicked()), signalMapper, SLOT(map()));
		signalMapper->setMapping(removeButton[index], index);

		//connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(OnClickedShowCamButton(int)));



	//horizontalLayoutParam[index]->addWidget(removeButton[index]);



	horizontalLayoutParam[index]->addSpacerItem(spacer);



	ui.verticalLayout->addLayout(horizontalLayoutParam[index]);

	ui.scrollArea->setWidgetResizable(true);
	if (((index + 1) * 50) < 910)
	{
		ui.scrollArea->setGeometry(QRect(10, 66, 1000, (index + 1) * 50 + 20));
	}
	else
	{
		ui.scrollArea->setGeometry(QRect(10, 66, 1000, 910));
	}
	ui.verticalLayout->setGeometry(QRect(0, 0, 1000, (index + 1) * 50));

	ui.scrollArea->widget()->setLayout(ui.verticalLayout);
}

void Types::UpdateCounter(bool good)
{
	//Preverimo ali je kos dober in primerno posodobimo �tevce
	if (good)
	{
		goodMeasurementsCounter++;
	}

	else
	{
		badMeasurementsCounter++;
	}

	totalMeasurementsCounter++;
	//Posodobimo procente
	goodPercentage = (float(goodMeasurementsCounter) / float(totalMeasurementsCounter))*100;
	badPercentage = (float(badMeasurementsCounter) / float(totalMeasurementsCounter))*100;

}

void Types::OnUpdate()
{
	for (int i = 0; i < parameterCounter; i++)
	{


	editMaxTol[i]->setText(QString("%1").arg(toleranceHigh[i]));
	editMinTol[i]->setText(QString("%1").arg(toleranceLow[i]));
	editOffset[i]->setText(QString("%1").arg(offset[i]));
	editNominal[i]->setText(QString("%1").arg(nominal[i]));
	checkActive[i]->setText(QString("%1").arg(isActive[i]));
	paramEditLine[i]->setText(QString("%1").arg(name[i]));
	editMinTolCond[i]->setText(QString("%1").arg(toleranceLowCond[i]));
	editMaxTolCond[i]->setText(QString("%1").arg(toleranceHighCond[i]));
	editMinTol[i]->setText(QString("%1").arg(toleranceLow[i]));
	editCorrection[i]->setText(QString("%1").arg(correctFactor[i]));
	checkConditional[i]->setText(QString("%1").arg(isConditional[i]));



	}
}

void Types::OnConfirm()
{
	for (int i = 0; i < parameterCounter; i++)
	{
		if (toleranceHigh[i] != editMaxTol[i]->text().toFloat())
			parametersChanged = true;
		if (toleranceLow[i] != editMinTol[i]->text().toFloat())
			parametersChanged = true;
		if (toleranceHighCond[i] != editMaxTolCond[i]->text().toFloat())
			parametersChanged = true;
		if (toleranceLowCond[i] != editMinTolCond[i]->text().toFloat())
			parametersChanged = true;
		if (offset[i] != editOffset[i]->text().toFloat())
			parametersChanged = true;
		if(nominal[i] != editNominal[i]->text().toFloat())
			parametersChanged = true;
		if (isConditional[i] != (int)checkConditional[i]->isChecked())
			parametersChanged = true;
		if(isActive[i] != (int)checkActive[i]->isChecked())
			parametersChanged = true;
		if(correctFactor[i] != editCorrection[i]->text().toFloat())
			parametersChanged = true;
		if(name[i] != paramEditLine[i]->text())
			parametersChanged = true;
	}
	if (parametersChanged == true)
	{
		WriteParametersBackUp();

		for (int i = 0; i < parameterCounter; i++)
		{
			toleranceHigh[i] = editMaxTol[i]->text().toFloat();
			toleranceLow[i] = editMinTol[i]->text().toFloat();
			toleranceHighCond[i] = editMaxTolCond[i]->text().toFloat();
			toleranceLowCond[i] = editMinTolCond[i]->text().toFloat();
			offset[i] = editOffset[i]->text().toFloat();
			nominal[i] = editNominal[i]->text().toFloat();
			isActive[i] = checkActive[i]->isChecked();
			isConditional[i] = checkConditional[i]->isChecked();
			correctFactor[i] = editCorrection[i]->text().toFloat();
			name[i] = paramEditLine[i]->text();
		}
		WriteParameters();
	}
	hide();

}
void Types::OnCancel()
{
	hide();
}
void Types::OnRemoveParameter(int param)
{
	QMessageBox msgBox;
	msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);



	msgBox.setIcon(QMessageBox::Warning);
	msgBox.setText(QString("Remove parameter:%1?").arg(param));
	msgBox.setWindowTitle("warning");
	if (msgBox.exec() == QMessageBox::Ok)
	{

		WriteParametersBackUp();
		parameterCounter--;
		parametersChanged = true;
		toleranceHigh.erase(toleranceHigh.begin() + param);
		toleranceLow.erase(toleranceLow.begin() + param);
		toleranceHighCond.erase(toleranceHighCond.begin() + param);
		toleranceLowCond.erase(toleranceLowCond.begin() + param);
		offset.erase(offset.begin() + param);
		nominal.erase(nominal.begin() + param);
		isActive.erase(isActive.begin() + param);
		isConditional.erase(isConditional.begin() + param);
		correctFactor.erase(correctFactor.begin() + param);
		name.erase(name.begin() + param);


		measuredValue.erase(measuredValue.begin() + param);
		isGood.erase(isGood.begin() + param);
		conditional.erase(conditional.begin() + param);


		for (int i = 0; i < removeButton.size(); i++)
		{
			signalMapper->removeMappings(removeButton[i]);
		}
		removeButton[param]->hide();
		removeButton.erase(removeButton.begin() + param);
		labelStevilka[param]->hide();
		labelStevilka.erase(labelStevilka.begin() + param);

		paramEditLine[param]->hide();
		paramEditLine.erase(paramEditLine.begin() + param);
		editMinTol[param]->hide();
		editMinTol.erase(editMinTol.begin() + param);
		editMaxTol[param]->hide();
		editMaxTol.erase(editMaxTol.begin() + param);	
		editMinTolCond[param]->hide();
		editMinTolCond.erase(editMinTolCond.begin() + param);
		editMaxTolCond[param]->hide();
		editMaxTolCond.erase(editMaxTolCond.begin() + param);
		editNominal[param]->hide();
		editNominal.erase(editNominal.begin() + param);
		editOffset[param]->hide();
		editOffset.erase(editOffset.begin() + param);
		editCorrection[param]->hide();
		editCorrection.erase(editCorrection.begin() + param);
		checkActive[param]->hide();
		checkActive.erase(checkActive.begin() + param);
		checkConditional[param]->hide();
		checkConditional.erase(checkConditional.begin() + param);





		for (int i = 0; i < removeButton.size(); i++)
		{
			signalMapper->setMapping(removeButton[i], i);
			labelStevilka[i]->setText(QString("%1").arg(i));
		}

		WriteParameters();
	}
}
void Types::OnAddParameter()
{
	QStringList values;
	toleranceHigh.push_back(0.0);
	toleranceLow.push_back(0.0);
	toleranceHighCond.push_back(0.0);
	toleranceLowCond.push_back(0.0);
	offset.push_back(0.0);
	nominal.push_back(0.0);
	isActive.push_back(0);
	isConditional.push_back(0);
	correctFactor.push_back(1.0);
	name.push_back(QString("Parameter%1").arg(parameterCounter));

	
	measuredValue.push_back(0);
	isGood.push_back(0);
	conditional.push_back(0);

	values.append(QString("%1").arg(toleranceHigh.back()));
	values.append(QString("%1").arg(toleranceLow.back()));
	values.append(QString("%1").arg(offset.back()));
	values.append(QString("%1").arg(nominal.back()));
	values.append(QString("%1").arg(isActive.back()));
	values.append(QString("%1").arg(isConditional.back()));
	values.append(QString("%1").arg(correctFactor.back()));
	values.append(QString("%1").arg(name.back()));

	AddParameter(parameterCounter);
	//prevParameterCounter = parameterCounter;
	parameterCounter++;
	parametersChanged = true;
	WriteParameters();
}


