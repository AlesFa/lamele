#pragma once
#include "tisudshl.h"
#include "Camera.h"

// ImagingSource

using namespace DShowLib;

class ImagingSource :public CCamera,public DShowLib::GrabberListener
{

public:
	ImagingSource();
	ImagingSource(const ImagingSource& camera);// Copy constructor
	virtual ~ImagingSource();


	virtual void overlayCallback(Grabber& caller, smart_ptr<OverlayBitmap> pBitmap, const tsMediaSampleDesc& MediaSampleDesc);
	virtual void frameReady( DShowLib::Grabber& caller, smart_ptr<DShowLib::MemBuffer> pBuffer, DWORD FrameNumber ); 
	//virtual void frameReady(Grabber& caller, smart_ptr<MemBuffer> pBuffer, DWORD FrameNumber);
	virtual void deviceLost(Grabber& caller);
	virtual void deviceListChanged(Grabber& caller, const DeviceListChangeData& reserved);
	// Attributes
public:
	// Save one image and mark it as saved
	void saveImage(smart_ptr<MemBuffer> pBuffer, DWORD currFrame);
	// Setup the buffersize. 
	void setBufferSize(unsigned long NumBuffers);

public:
	Grabber * pGrabber;
	Grabber::tMemBufferPtr		m_pBuffer;
	FrameHandlerSink::tFHSPtr	pSink;
	tMemBufferCollectionPtr		pCollection;
	smart_com<IFrameFilter>		pFilter;
	//GrabberListener			*pcListener;
	UINT_PTR					m_nWindowTimer;


protected:


public:
	static bool InitLibrary();

	virtual int Init(int selectGrabber, QString cameraName, int VideoFormat, QString VideoFormatS);
	virtual int Init(QString serialNumber, QString VideoFormatS);
	virtual int Init();
	virtual int Start(double FPS, bool trigger, bool listener);
	virtual int Start(double FPS, bool trigger, bool listener, bool flipV, bool flipH);
	virtual int Start();
	virtual int GrabImage();
	virtual int GrabImage(int imageNr);
	virtual int MemoryUnlock(int imageNr);
	virtual void MemoryUnlockAll();
	virtual int MemoryLock();
	virtual bool SetWhiteBalanceAbsolute(long red, long green, long blue);
	virtual bool SetExposureAbsolute(double dExposure);
	virtual double GetExposureAbsolute();

	virtual bool SetGainAbsolute(double dGain);
	virtual int	GetGainAbsolute();
	virtual void CloseGrabber(void);
	virtual bool OnSettingsImage(void);
	virtual bool SaveReferenceSettings(void);
	virtual bool LoadReferenceSettings(void);
	virtual bool EnableLive();
	virtual bool DisableLive();
	virtual bool SetPartialOffsetX(int xOff);
	virtual int GetPartialOffsetX();
	virtual bool SetPartialOffsetY(int yOff);
	virtual int GetPartialOffsetY();
	virtual int EnableStrobe(int polarity);
	virtual int DisableStrobe();
	//	virtual void DisplayImageRing(CDC *dc, int imageNr, CRect displayRect);
	//	virtual void DisplayLiveImage(CDC *dc, CRect displayRect);	
	//	virtual void DisplayLiveImage(CDC *dc, CRect displayRect, CRect sourceRect); //live picture
	//	virtual void DisplayLiveImage(CDC *dc);
	/*virtual bool IsDeviceValid();
	//	virtual void DisplayData(CDC *pDC, CRect displayRec);
	virtual bool EnableLive();
	virtual bool DisableLive();
	virtual void ShowImages();
	virtual void SetPacketSize();*/
};
