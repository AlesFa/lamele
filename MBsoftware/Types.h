#pragma once


#include "stdafx.h"
#include "ui_types.h"
#include "CProperty.h"

using namespace::std;
class Types : public QWidget
{
	Q_OBJECT

public:
	Types();
	~Types();

	QString					typeName;
	QString					typePath;
	int						parameterCounter;

	std::vector<float>		measuredValue;
	std::vector<int>		isGood;
	std::vector<int>		conditional;

	std::vector<float>		toleranceHigh;
	std::vector<float>		toleranceLow;
	std::vector<float>		toleranceHighCond;
	std::vector<float>		toleranceLowCond;
	std::vector<float>		correctFactor;
	std::vector<float>		offset;
	std::vector<float>		nominal;
	std::vector<int>		isActive;
	std::vector<int>		isConditional;
	std::vector<QString>	name;
	int						stationNumber;
	int						id;
	int						conditionalMeasurementsCounter;
	int						goodMeasurementsCounter;
	int						badMeasurementsCounter;
	int						totalMeasurementsCounter;
	int						allGood; //vse dobre meritve
	float					goodPercentage;
	float					badPercentage;
	bool					parametersChanged; //zastavica za zaznavanje spremembe parametrov in toleranc
	std::vector<vector<CProperty*>>	prop;
	//za urejenje dialoga

private:
	vector<QHBoxLayout*> horizontalLayoutParam;
	vector<QLabel*> labelStevilka;
	vector<QLineEdit*> paramEditLine;
	vector<QLineEdit*> editMinTol;
	vector<QLineEdit*> editMaxTol;
	vector<QLineEdit*> editNominal;
	vector<QLineEdit*> editOffset;
	vector<QLineEdit*> editMinTolCond;
	vector<QLineEdit*> editMaxTolCond;
	vector<QLineEdit*> editCorrection;
	vector<QCheckBox*> checkConditional;

	vector<QCheckBox*> checkActive;
	vector<QToolButton*> removeButton;
	QSignalMapper* signalMapper;
	//QWidget *horizontalLayoutWidget;


private:
	Ui::typesUi ui;
public:
	void Init(QString name, QString filePath);
	void ReadParameters(QString filePath);
	void WriteParameters();
	void WriteParametersBackUp();
	void ReadCounters();
	void OnShowDialog(int rights);
	void WriteCounters();
	void ResetCounters();
	void SetMeasuredValue(float value, int nParam);
	void SetMeasuredValue(int value, int nParam);
	void SetMeasuredValue(bool value, int nParam);
	int IsGood(int nParam);
	int AllGood(void);
	int IsConditional(void);
	int SaveMeasurements(QString path);
	void AddParameter(int index);
	void UpdateCounter(bool good);
	void OnUpdate();

public slots:
	void OnConfirm();
	void OnCancel();
	void OnRemoveParameter(int param);
	void OnAddParameter();

};
